### What is this repository for? ###

TenFor is a C++ package enabling manipulation of tensor-algebraic expressions directly within C++ source code. Using TenFor it is possibly to represent arbitrary tensor-algebraic operations - e.g. T^{ik} = g^{ij} T^i_j - in C++ source using expressions such as

T(i_, k_) = Sum(j_, g(i_, j_) * T(i_, j_)). [1]

****WARNING: TenFor was originally developed as a module of the numerical  relativity code SpEC and is currently partway-through the process of being refactored from the latter. Not all features currently work and compilation has not been tested outside of Fedora. Documentation is at present near-nonexistent.

Depending on the compilation mode chosen [1] will be implemented in any of three ways. In NONACCEL a recursive tree of expression templates allow [1] to be evaluated on the CPU with a single compilation. This mode performs comparably to hand-written for loops using gcc but seems to impair performance by about a factor of 2 using Intel compilers.

The other two modes, ACCEL_CPU and ACCEL_CUDA, require two separate compilations. During the first compilation, which will use the flag CODE_WRITER, a static library file containing all TenFor expressions desired by the user code will be linked to the TCodeWriter executable. This executable will loop through a special set of headers in the TenFor object files which enumerate all such expressions. Once run, TCodeWriter will generate C++ source code implementing these expressions in low-level C or CUDA code. The second compilation, which will not use CODE_WRITER, links TenFor and the auto-generated expressions to the user code.

***WARNING: Only NONACCEL mode is currently implemented in the public repo.


### How do I get set up? ###

Compiling with Nonaccel:

- Get dependencies - you'll need a C++11-capable compiler. We've tested with gcc/4.8.3 and Intel 15.
- Edit environment.env according to your machine. If you aren't using modules you can just delete all the "module" commands. Set TLOOPS_HOME= to the TenFor base directory.
- source environment.env
- Copy the appropriate Makefiles/ into the top-level directory as "MakefileRules".
- edit MakefileRules. You don't need to change COMPILE_MODE or CODE_WRITER, but may need to edit CPP (the compiler command, default icc) and AR. If you don't want debug flags, delete the -g from DEBUG = -g.
- make
- Link ${TLOOPS_HOME}/lib/libNonaccel.o to your project.

### Contribution guidelines ###

- If you see something you can fix, send an email!

- All files in this repo are protected under the MIT license (described in LICENSE.txt). 

### Who do I talk to? ###
Adam Lewis: adam.lws@gmail.com