/// \file
/// This file defines helper-template classes associating the symbol
/// (e.g. '*') and a textual representation of it (e.g. 'mult') with
/// each operator used in expression templates.  These strings are 
/// used in the generation code that generates explicit C-functions
/// representing an expression template. 

#ifndef OperatorSymbols_hpp
#define OperatorSymbols_hpp

#include <string>
#include "ExpressionTemplates.hpp"



template<class X>
struct OperatorSymbols { };


//================================================================
// OperatorSymbols for binary operators +, -, *, /, atan2, pow, min, max
//
// Each one must have three member-functions:
//  - Name() returns its name, w/o 'Op', e.g. 'Add' for AddOp
//  - CombineNameWithOperator constructs a combined name for 
//    the C-function.  This must be a valid C identifier.
//  - CombineBodyWithOperator constructs the combined body for 
//    the expression inside the C-function.  This must be a valid C
//================================================================
template<>
struct OperatorSymbols<AddOp> {
  static std::string Symbol() { return "+"; }
  static std::string Name() { return "Add"; }
  static std::string CombineNameWithOperator(const std::string& Lname,
					     const std::string& Rname) {
    return "O"+Lname+"a"+Rname+"C";//+"_add_"+
  }
  static std::string CombineBodyWithOperator(const std::string& Lbody,
					     const std::string& Rbody) {
    return "("+Lbody+"+"+Rbody+")";
  }
};
//================
template<>
struct OperatorSymbols<MultOp> {
  static std::string Symbol() { return "*"; }
  static std::string Name() { return "Mult"; }
  static std::string CombineNameWithOperator(const std::string& Lname,
					     const std::string& Rname) {
    return "O"+Lname+"t"+Rname+"C";//"_mult_"
  }
  static std::string CombineBodyWithOperator(const std::string& Lbody,
					     const std::string& Rbody) {
    return "("+Lbody+"*"+Rbody+")";
  }
};
//================-
template<>
struct OperatorSymbols<SubOp> {
  static std::string Symbol() { return "-"; }
  static std::string Name() { return "Sub"; }
  static std::string CombineNameWithOperator(const std::string& Lname,
					     const std::string& Rname) {
    return "O"+Lname+"m"+Rname+"C";//"_minus_"
  }
  static std::string CombineBodyWithOperator(const std::string& Lbody,
					     const std::string& Rbody) {
    return "("+Lbody+"-"+Rbody+")";
  }
};
template<>
struct OperatorSymbols<DivOp> {
  static std::string Symbol() { return "/"; }
  static std::string Name() { return "Div"; }
  static std::string CombineNameWithOperator(const std::string& Lname,
					     const std::string& Rname) {
    return "O"+Lname+"d"+Rname+"C";//"_div_"
  }
  static std::string CombineBodyWithOperator(const std::string& Lbody,
					     const std::string& Rbody) {
    return "("+Lbody+"/"+Rbody+")";
  }
};

template<>
struct OperatorSymbols<Atan2Op> {
  static std::string Symbol() { return "atan2"; }
  static std::string Name() { return "Atan2"; }
  static std::string CombineNameWithOperator(const std::string& Lname,
					     const std::string& Rname) {
    return "atan2_O"+Lname+"__"+Rname+"C";
  }
  static std::string CombineBodyWithOperator(const std::string& Lbody,
					     const std::string& Rbody) {
    return "atan2("+Lbody+","+Rbody+")";
  }
};
template<>
struct OperatorSymbols<PowOp> {
  static std::string Symbol() { return "pow"; }
  static std::string Name() { return "Pow"; }
  static std::string CombineNameWithOperator(const std::string& Lname,
					     const std::string& Rname) {
    return "pow_O"+Lname+"__"+Rname+"C";
  }
  static std::string CombineBodyWithOperator(const std::string& Lbody,
					     const std::string& Rbody) {
    return "pow("+Lbody+","+Rbody+")";
  }
};
template<>
struct OperatorSymbols<MinOp> {
  static std::string Symbol() { return "min"; }
  static std::string Name() { return "Min"; }
  static std::string CombineNameWithOperator(const std::string& Lname,
					     const std::string& Rname) {
    return "min_O"+Lname+"__"+Rname+"C";
  }
  static std::string CombineBodyWithOperator(const std::string& Lbody,
					     const std::string& Rbody) {
    return "min("+Lbody+","+Rbody+")";
  }
};
template<>
struct OperatorSymbols<MaxOp> {
  static std::string Symbol() { return "max"; }
  static std::string Name() { return "Max"; }
  static std::string CombineNameWithOperator(const std::string& Lname,
					     const std::string& Rname) {
    return "max_O"+Lname+"__"+Rname+"C";
  }
  static std::string CombineBodyWithOperator(const std::string& Lbody,
					     const std::string& Rbody) {
    return "max("+Lbody+","+Rbody+")";
  }
};


//================================================================
// OperatorSymbols for unary operators
//
// Each one must have two member-functions:
//  - Symbol returns the symbols representing this operator (e.g. '-', 'sqrt')
//  - Name returns the name of the operator, w/o 'Op', e.g. 'negate' 
//    for negateOp.   This serves as a textual representation of the 
//    operator to be used when constructing the C-function name.
//================================================================


template<>
struct OperatorSymbols<doNothingOp> {
  static std::string Symbol() { return " "; }
  static std::string Name()   { return "doNothing"; }
};

template<>
struct OperatorSymbols<negateOp> {
  static std::string Symbol() { return "-"; }
  static std::string Name()   { return "negate"; }
};

template<>
struct OperatorSymbols<sqrtOp> {
  static std::string Symbol() { return "sqrt"; }
  static std::string Name()   { return "sqrt"; }
}; template<>
struct OperatorSymbols<sqrOp> {
  static std::string Symbol() { return "sqr"; }
  static std::string Name()   { return "sqr"; }
};
template<>
struct OperatorSymbols<cubeOp> {
  static std::string Symbol() { return "cube"; }
  static std::string Name()   { return "cube"; }
};
template<>
struct OperatorSymbols<fabsOp> {
  static std::string Symbol() { return "fabs"; }
  static std::string Name()   { return "fabs"; }
};
template<>
struct OperatorSymbols<expOp> {
  static std::string Symbol() { return "exp"; }
  static std::string Name()   { return "exp"; }
};
 template<>
struct OperatorSymbols<logOp> {
  static std::string Symbol() { return "log"; }
  static std::string Name()   { return "log"; }
};
template<>
struct OperatorSymbols<log10Op> {
  static std::string Symbol() { return "log10"; }
  static std::string Name()   { return "log10"; }
};
template<>
struct OperatorSymbols<cosOp> {
  static std::string Symbol() { return "cos"; }
  static std::string Name()   { return "cos"; }
};
template<>
struct OperatorSymbols<acosOp> {
  static std::string Symbol() { return "acos"; }
  static std::string Name()   { return "acos"; }
};
template<>
struct OperatorSymbols<sinOp> {
  static std::string Symbol() { return "sin"; }
  static std::string Name()   { return "sin"; }
};
template<>
struct OperatorSymbols<asinOp> {
  static std::string Symbol() { return "asin"; }
  static std::string Name()   { return "asin"; }
};
template<>
struct OperatorSymbols<coshOp> {
  static std::string Symbol() { return "cosh"; }
  static std::string Name()   { return "cosh"; }
};
template<>
struct OperatorSymbols<sinhOp> {
  static std::string Symbol() { return "sinh"; }
  static std::string Name()   { return "sinh"; }
};
template<>
struct OperatorSymbols<tanhOp> {
  static std::string Symbol() { return "tanh"; }
  static std::string Name()   { return "tanh"; }
};
template<>
struct OperatorSymbols<tanOp> {
  static std::string Symbol() { return "tan"; }
  static std::string Name()   { return "tan"; }
};
template<>
struct OperatorSymbols<atanOp> {
  static std::string Symbol() { return "atan"; }
  static std::string Name()   { return "atan"; }
};

template<>
struct OperatorSymbols<RoundOp> {
  static std::string Symbol() { return "round"; }
  static std::string Name()   { return "Round"; }
};

template<>
struct OperatorSymbols<StepFunctionOp> {
  static std::string Symbol() { return "StepFunction"; }
  static std::string Name()   { return "StepFunction"; }
};

template<>
struct OperatorSymbols<floorOp> {
  static std::string Symbol() { return "floor"; }
  static std::string Name()   { return "floor"; }
};

template<>
struct OperatorSymbols<ceilOp> {
  static std::string Symbol() { return "ceil"; }
  static std::string Name()   { return "ceil"; }
};


//================================================================
// OperatorSymbols for assignment operators
//
// Each one must have two member-functions:
//  - OperatorDecl() returns the C++ member function name (e.g. 'operator+=')
//  - Symbol returns the C-string representing this operation  (e.g. '+=')
//  - Name returns a textual representation of the operator to be used 
//    when constructing the C-function name (e.g. 'PlusEqual').
//
// These classes are ONLY used in CUdaSourceCodeWriter
//================================================================

template<>
struct OperatorSymbols<SetEqualOp> {
  static std::string OperatorDecl() { return "SetEqual"; }
  static std::string Symbol() { return "="; } //=
  static std::string Name()   { return "SetEqual"; }
};
template<>
struct OperatorSymbols<PlusEqualOp> {
  static std::string OperatorDecl() { return "operator+="; }
  static std::string Symbol() { return "+="; }
  static std::string Name()   { return "PlusEqual"; }
};
template<>
struct OperatorSymbols<MinusEqualOp> {
  static std::string OperatorDecl() { return "operator-="; }
  static std::string Symbol() { return "-="; }
  static std::string Name()   { return "MinusEqual"; }
};
template<>
struct OperatorSymbols<MultEqualOp> {
  static std::string OperatorDecl() { return "operator*="; }
  static std::string Symbol() { return "*="; }
  static std::string Name()   { return "MultEqual"; }
};
template<>
struct OperatorSymbols<DivEqualOp> {
  static std::string OperatorDecl() { return "operator/="; }
  static std::string Symbol() { return "/="; }
  static std::string Name()   { return "DivEqual"; }
};






#endif
