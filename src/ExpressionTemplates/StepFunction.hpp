#ifndef StepFunction_hpp
#define StepFunction_hpp

inline double StepFunction(const double& x) { return (x<0.0 ? 0.0 : 1.0); }

#endif
