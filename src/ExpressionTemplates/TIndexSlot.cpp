#include "TIndexSlot.hpp"
#include <vector>

//3 and 4 dimensional TIndexes are given separate namespaces
//to make them easier to distinguish within code using them
//(i.e. if the TIndex i_ is to be used usercode must contain
//using TIndex3::i_; thus specifying the TIndex dimension).
namespace TIndex3 {
  TIndexSlot<3,0,0> i_;
  TIndexSlot<3,1,0> j_;
  TIndexSlot<3,2,0> k_;
  TIndexSlot<3,3,0> l_;
  TIndexSlot<3,4,0> m_;
  TIndexSlot<3,5,0> n_;
  TIndexSlot<3,6,0> o_;
  TIndexSlot<3,7,0> p_;
}

namespace TIndex4 {
  TIndexSlot<4,0,0> a_;
  TIndexSlot<4,1,0> b_;
  TIndexSlot<4,2,0> c_;
  TIndexSlot<4,3,0> d_;
  TIndexSlot<4,4,0> e_;
  TIndexSlot<4,5,0> f_;
  TIndexSlot<4,6,0> g_;
  TIndexSlot<4,7,0> h_;
}
