#ifndef TAssignment_hpp
#define TAssignment_hpp
namespace ImplicitTensorLoops {
   
  
  
  // helper classes to encode '=', '+=', '-=', '*=', and '/='
  //
  // in each modify() function, the first argument is always
  // L=DataMesh.  It is templated on L nevertheless, because 
  // otherwise DataMesh.hpp would have to be #included.  
  //
  // This code, however, will be included whenever Tensor.hpp is used.
  // To avoid needless extra #include files, we avoid direct
  // dependencies on DataMesh.hpp
  struct TSetEqualOp {
    
    //this is the code previously in DataMeshOperators.hpp
    //The previous idea had been to call it from lhs=rhs, but
    //since we now route such calls through TLoopApply,
    //this creates an infinite loop.
    template<class L, class R>
    static inline void modify(L& lhs, const R& rhs) {
      double* d = lhs.Data();
      const int sz=lhs.Size();
      for(int i=0; i<sz; ++i)
        d[i]=rhs[i];
    }
  
    //specialization for RHS=double
    template<class L>
    static inline void modify(L& lhs, const double& rhs) {
      double* d = lhs.Data();
      const int sz=lhs.Size();
      for(int i=0; i<sz; ++i)
        d[i]=rhs;
    }
  };

  struct TPlusEqualOp {
    template<class L, class R>
    static inline void modify(L& lhs, const R& rhs) {
      double* d = lhs.Data();
      const int sz=lhs.Size();
      for(int i=0; i<sz; ++i)
        d[i]+=rhs[i];
    }
    //specialization for RHS=double
    template<class L>
    static inline void modify(L& lhs, const double& rhs) {
      double* d = lhs.Data();
      const int sz=lhs.Size();
      for(int i=0; i<sz; ++i)
        d[i]+=rhs; 
    }
  };

  struct TMinusEqualOp {
    template<class L, class R>
    static inline void modify(L& lhs, const R& rhs) {
      double* d = lhs.Data();
      const int sz=lhs.Size();
      for(int i=0; i<sz; ++i)
        d[i]-=rhs[i];
    }
    //specialization for RHS=double
    template<class L>
    static inline void modify(L& lhs, const double& rhs) {
      double* d = lhs.Data();
      const int sz=lhs.Size();
      for(int i=0; i<sz; ++i)
        d[i]-=rhs;
    }
  };

  struct TMultEqualOp {
    template<class L, class R>
    static inline void modify(L& lhs, const R& rhs) {
      double* d = lhs.Data();
      const int sz=lhs.Size();
      for(int i=0; i<sz; ++i)
        d[i]*=rhs[i];
    }
    //specialization for RHS=double
    template<class L>
    static inline void modify(L& lhs, const double& rhs) {
      double* d = lhs.Data();
      const int sz=lhs.Size();
      for(int i=0; i<sz; ++i)
        d[i]*=rhs;
    }
  };

  struct TDivEqualOp {
    template<class L, class R>
    static inline void modify(L& lhs, const R& rhs) {
      double* d = lhs.Data();
      const int sz=lhs.Size();
      for(int i=0; i<sz; ++i)
        d[i]/=rhs[i];
    }
    //specialization for RHS=double
    template<class L>
    static inline void modify(L& lhs, const double& rhs) {
      double* d = lhs.Data();
      const int sz=lhs.Size();
      for(int i=0; i<sz; ++i)
        d[i]/=rhs;
    }
  };

}

#endif
