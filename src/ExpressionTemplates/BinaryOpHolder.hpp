#ifndef BinaryOpHolder_hpp
#define BinaryOpHolder_hpp

namespace ImplicitTensorLoops {

  //================================================================
  // iBinaryOpHolder
  //================================================================
  //
  // Assembles **by value** the hierarchy of indexed BinaryOp's.  This
  // extends the lifetime of the instances of sub-BinaryOp's (which
  // are referenced from the higher-level BinaryOp's) until the entire
  // expression is evaluated.
  //
  template<class iBinaryOp_t>
  struct BinaryOpHolder;


  //--------------------------------
  // BinaryOpOperand
  //--------------------------------
  //
  // Policy-class that keeps the operands L, R of
  // an iBinary<L,Op,R> in the correct way:
  //   if L/R = double or DataMesh:  by reference.
  //   if L/R = BinaryOpHolder<..>:  by value

  // Leaf 1:  double, DataMesh  (by reference)
  template<class T>
  struct BinaryOpOperand {
    typedef const T& type;
    static type get(const T& t) { return t; };
  };


  // Recursion:  hold BinaryOpHolder<..>'s by value
  template<class L, class Op, class R>
  struct BinaryOpOperand<iBinaryOp<L,Op,R> > {
    typedef const BinaryOpHolder<iBinaryOp<L,Op,R> > type;
    static const typename iBinaryOp<L,Op,R>::ExpandIndices_t& 
    get(const type& t) { return t.op; }
  };

  
  //--------------------------------
  // recursive definition of BinaryOpHolder
  //--------------------------------

  // Recursion (1): binary iBinaryOp's
  template<class L, class Op, class R> 
  struct BinaryOpHolder<iBinaryOp<L,Op,R>> {
    typedef iBinaryOp<L, Op, R> iBinaryOp_t;
    BinaryOpHolder(const iBinaryOp_t& op_):
      lhs(op_.lhs),
      rhs(op_.rhs),
      op(BinaryOpOperand<L>::get(lhs),
	 BinaryOpOperand<R>::get(rhs) )
	{ };
    typename BinaryOpOperand<L>::type lhs;
    typename BinaryOpOperand<R>::type rhs;
    typename iBinaryOp_t::ExpandIndices_t op;
  };

  // Recursion (2): unary iBinaryOp's (i.e. unary-)
  template<class Op, class R> 
  struct BinaryOpHolder<iBinaryOp<EmptyType,Op,R>> {
    typedef iBinaryOp<EmptyType, Op, R> iBinaryOp_t;
    BinaryOpHolder(const iBinaryOp_t& op_):
      rhs(op_.rhs),
      op(BinaryOpOperand<R>::get(rhs)) 
	{  };
    typename BinaryOpOperand<R>::type rhs;
    typename iBinaryOp_t::ExpandIndices_t op;
  };
  
  //--------------------------------
  // leaf specializations for iBinaryOp
  //--------------------------------
  
  // end recursion (1): indexed Tensor<DataMesh>
  template<class ...Symm, class ...Indices>
  struct BinaryOpHolder<iBinaryOp<TIndexStructure<TSymmetry<Symm...>,
						  Indices...>, 
				  EmptyType,DataMesh>
			> {
    typedef iBinaryOp<TIndexStructure<TSymmetry<Symm...>,
				      Indices...>, 
		      EmptyType,DataMesh> iBinaryOp_t; 
  BinaryOpHolder(const iBinaryOp_t& op_):
      op(op_.ExpandIndices()) // extract DataMesh& by indexing Tensor
    { };
    const DataMesh& op;
  };
  
  // end recursion (2): indexed Tensor<Tensor<DataMesh>>
  template<class TIndexStructure1, class TIndexStructure2>
  struct BinaryOpHolder<iBinaryOp<std::pair<TIndexStructure1, 
					    TIndexStructure2>,
				  EmptyType,DataMesh>
			> {
    typedef iBinaryOp<std::pair<TIndexStructure1, 
				TIndexStructure2>,
		      EmptyType,DataMesh> iBinaryOp_t; 
    BinaryOpHolder(const iBinaryOp_t& op_):
      op(op_.ExpandIndices()) // extract DataMesh& by indexing Tensor
    { };
    const DataMesh& op;
  };


  //--------------------------------
  // specializations for interoperability with TLoopApply
  //--------------------------------
  //
  // These specializations are used for assignments to double and
  // DataMesh.  They allow the code inside TLoopApply to work identically
  // for double, DataMesh, and for iBinaryOp<L,Op,R>
  
  // A(i_,j_) = double
  template<>
  struct BinaryOpHolder<double> {
    BinaryOpHolder(const double& d): op(d) { }
    const double& op;
  };

  // A(i_,j_) = DataMesh
  template<>
  struct BinaryOpHolder<DataMesh> {
    BinaryOpHolder(const DataMesh& dm): op(dm) { }
    const DataMesh& op;
  };

  template<class L, class O, class R>
  struct BinaryOpHolder<BinaryOp<L,O,R> > {
      BinaryOpHolder(const BinaryOp<L,O,R>& bOp): op(bOp) { }
    const BinaryOp<L,O,R>& op;
  };
}

#endif
