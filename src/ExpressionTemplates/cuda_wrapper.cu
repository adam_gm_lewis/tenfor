//#include "Utils/ErrorHandling/MyErr.hpp"
#include "Require.hpp"
#include "Assert.hpp"
#include "cuda_profiler_api.h"
#include "nvToolsExt.h"
#include "nvToolsExtCuda.h"
#include "nvToolsExtCudaRt.h"
#include <iostream>

//#include "Utils/SizeStatistics///GpuEvent.hpp"
//CUDA
#include "cuda_wrapper.hpp"
namespace GPU{
    class error_bundle{
      public:
        error_bundle():mErr(false),
                       mErrstring(""){}
        bool& GetError(){  return mErr; }   
        std::string& GetErrstring(){ return mErrstring; }
      private:
        bool mErr;
        std::string mErrstring;
    };

    bool& err() {
        static error_bundle theBundle;
        return theBundle.GetError();
    }

    std::string& errstring() {
        static error_bundle theBundle;
        return theBundle.GetErrstring();
    }
  
    //CUDA
    //These error routines should not be called by
    //external code.
    bool dError(cudaError_t& err,std::string& errstring){
      if (err!=cudaSuccess){
          errstring = cudaGetErrorString(err);   
          return false;
      }
      return true;
    }
   
    //CUresult
    bool dResult(CUresult& res, std::string& errstring){
      if (res!=CUDA_SUCCESS){
        //I have no idea why this function wants a const char**,
        //but it does.
        const char* c = errstring.c_str();
        res = cuGetErrorString(res, &c);
        return false;
      }
      return true;
    }

    //Calling this starts the clock on a cuda event based timer.
    //cudaEvent_t vars 'start' and 'stop' should be declared beforehand.
    void cuda_tick(cudaEvent_t& start)
    {
        //cudaEventCreate(&start);
        //cudaEventCreate(&stop);
        cudaEventRecord(start);
    }

    //This stops the clock on a cuda event based timer. If 'timing' is 1,
    //results are output to stdout. Otherwise they are sent to a separate
    //file formatted for easy graphing.
    void cuda_tock(cudaEvent_t& start, cudaEvent_t& stop, float &time)
    {
        cudaEventRecord(stop);
        cudaEventSynchronize(stop);
        time=0; //in ms +- 0.5 microseconds
        cudaEventElapsedTime(&time, start, stop);
        //cudaEventDestroy(start);
        //cudaEventDestroy(stop);
    }
    
    //This wraps common CUDA calls so they can be used without resorting to the 
    //.cu extension. This is useful when #ACCEL_CUDA and #NONACCEL versions of 
    //some class are to be held in the same file, for example.

    bool dGetDeviceName(std::string& name, int devNum, std::string& errstring){
      cudaDeviceProp prop;
      cudaError_t err = cudaGetDeviceProperties(&prop, devNum);
      name = prop.name;
      return dError(err,errstring);
    }

    void dShowDeviceProperties(int devNum=0){
      
      cudaDeviceProp prop;
      cudaError_t err=cudaGetDeviceProperties(&prop,devNum);
      std::string errstring;
      REQUIRE(GPU::dError(err,errstring),errstring);
      
      std::cout << prop.name << std::endl;
      std::cout << "MAX GRID SIZE: " << std::endl;
      for(int i=0; i<3; ++i) std::cout << prop.maxGridSize[i] <<",";
      std::cout << std::endl;
      
      std::cout << "MAX THREADS DIM: " << std::endl;
      for(int i=0; i<3; ++i) std::cout << prop.maxThreadsDim[i] <<",";
      std::cout << std::endl;
      std::cout << "MAX THREADS/BLOCK: " << prop.maxThreadsPerBlock << std::endl;
      std::cout << "MAX THREADS/SM: " << prop.maxThreadsPerMultiProcessor << std::endl;
      std::cout << "SM COUNT: " << prop.multiProcessorCount << std::endl;
      std::cout << "REGS/BLOCK: " << prop.regsPerBlock << std::endl;
      std::cout << "TOTAL GLOBAL MEM: " << prop.totalGlobalMem << std::endl;
      std::cout << "TOTAL CONST MEM: " << prop.totalConstMem << std::endl;
      std::cout << "L2 CACHE SIZE: " << prop.l2CacheSize << std::endl;
      std::cout << "SHARED MEM/BLOCK: " << prop.sharedMemPerBlock << std::endl;
      std::cout << "WARP SIZE: " << prop.warpSize << std::endl;
      std::cout << "CLOCK RATE: " << prop.clockRate << std::endl;
      std::cout << "MEM BUS WIDTH: " << prop.memoryBusWidth << std::endl;
      std::cout << "MEM CLOCK RATE: " << prop.memoryClockRate << std::endl;
    
      
    }
    
    bool RuntimeGetVersion(int* runtimeVersion, std::string& errstring){
      cudaError_t err = cudaRuntimeGetVersion(runtimeVersion);
      return dError(err,errstring); 
    }


    bool dCudaMemGetInfo(size_t* free, size_t* total, std::string& errstring){
      cudaError_t err = cudaMemGetInfo(free, total);
      return dError(err,errstring);
    }

    bool dMemcpyHostToDevice(void* dst, const void* src, const size_t count,
            std::string& errstring){
      //GpuEvent("HostToDevice", count);
      cudaError_t err=cudaMemcpy(dst,src,count,cudaMemcpyHostToDevice);
      return dError(err,errstring);
    }

    bool dMemcpyDeviceToHost(void* dst, const void* src, const size_t count,
        std::string& errstring){
      
      //GpuEvent("DeviceToHost", count);
      cudaError_t err=cudaMemcpy(dst,src,count,cudaMemcpyDeviceToHost);
      return dError(err,errstring);
   } 
    bool dMemcpyDeviceToDevice(void* dst, const void* src, const size_t count,
        std::string& errstring){
      //GpuEvent("DeviceToDevice", count);
      cudaError_t err=cudaMemcpy(dst,src,count,cudaMemcpyDeviceToDevice);
      return dError(err,errstring);
   } 
    
    bool dFree(void* pointer, std::string& errstring){
      cudaError_t err = cudaFree(pointer);
      return dError(err,errstring);
    }
    
    bool dMalloc(void** pointer, const size_t size, std::string& errstring){
      cudaError_t err = cudaMalloc(pointer,size);
      return dError(err,errstring);
    }

    bool dSynchronize(std::string& errstring){
      cudaError_t err;  
      err=cudaDeviceSynchronize();
      return GPU::dError(err,errstring);
    }
    
    bool dLastError(std::string& errstring){
      cudaError_t err = cudaGetLastError();
      return GPU::dError(err,errstring);
    }
    
    void dSynchronize(){
      cudaDeviceSynchronize();
    }
    void dRequire(cudaError_t& err){
        std::string errstring;
        REQUIRE(dError(err,errstring),errstring);
    }
    
    void dAssert(cudaError_t& err){
        std::string errstring;
        ASSERT(dError(err,errstring),errstring);
    }
    
    void profilerStart()
    {
        //std::cout << "PROFILER STARTING" << std::endl;
        cudaProfilerStart();
    }

    void profilerStop()
    {
        //std::cout << "PROFILER STOPPING" << std::endl;
        cudaProfilerStop();
    }


    bool dGetDeviceCount(int& devCount, std::string& errstring){
      cudaError_t err = cudaGetDeviceCount(&devCount);
      return dError(err,errstring);
    }
    
    void SetDevice(const int device) {
      //First actually set the device 
      cudaError_t err=cudaSetDevice(device);
      std::string errstring;  
      REQUIRE(dError(err,errstring),errstring); 
      std::cerr << "Using device rank: " << device << std::endl;
      
      //Now set the context...
      //delete the existing context, just in case
      /* 
      std::string errstring; 
      CUresult res; 
     
      //CUcontext* old_ctx=0;
      CUresult res=cuCtxGetCurrent(old_ctx);
      REQUIRE(dResult(res,errstring),errstring); 
      
      res=cuCtxDestroy(*old_ctx);
      REQUIRE(dResult(res,errstring),errstring); 
      
      //Now make a new context
      CUdevice this_device;
      CUcontext new_ctx;
      uint flag =  
          CU_CTX_SCHED_SPIN;  //Makes CUDA actively spin when waiting for the 
                                //GPU. Reduces GPU latency at cost of 
                                //performance of parallel CPU threads.
      // uint flag= 
      //      CU_CTX_LMEM_RESIZE_TO_MAX; //Stops CUDA from reallocating local GPU
                                       //memory between kernels. Saves on 
                                       //allocation time, but may increase
                                       //local memory usage.
      //uint flag = 0;

      res=cuDeviceGet(&this_device,0);
      REQUIRE(dResult(res,errstring),errstring); 
      
      res=cuCtxCreate(&new_ctx,flag,this_device);
      REQUIRE(dResult(res,errstring),errstring); 

    
    */
    }
    
    void dInitializeDevice() {
      cudaFree(0);
    }
}
