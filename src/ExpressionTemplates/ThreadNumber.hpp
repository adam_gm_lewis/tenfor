/// \file
/// Defines functions related to the number of threads.

#ifndef ThreadNumber_hpp
#define ThreadNumber_hpp

/// Returns the thread number of the calling function.
int GetThisThreadNum();

/// Returns the number of threads active in this section.
///  If in a sequential section of the code, this will return 1.
int GetNumThreads();

/// Set the number of threads desired for parallel sections.
///  NOTE: This should probably be called in a sequential section of the code.
void SetNumThreads(const int kNumThreads);

/// Get the maximum number of threads used (considering only number of threads
///  sent to SetNumThreads.
int GetMaxNumThreadsUsed();

#endif
