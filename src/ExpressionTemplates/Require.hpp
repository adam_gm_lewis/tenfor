/// Defines REQUIRE macro.

#ifndef Require_hpp
#define Require_hpp

#include <sstream>
#include <string>

// Function to handle REQUIRE failure. Should never return.
void RequireFail(const char* expression, const char* file, const int line,
                 std::string msg) __attribute__((noreturn));

/// REQUIRE macro
/// REQUIRE enforces the truth of an expression. If the expression is false,
/// an error is printed and the program exits.
#define REQUIRE(a,m) \
do { if (!(a)) { \
  std::ostringstream rmsg; rmsg << m; \
  RequireFail(#a, __FILE__, __LINE__, rmsg.str()); \
} } while (0)

#endif // Require_hpp
