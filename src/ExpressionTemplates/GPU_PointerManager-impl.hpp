#ifndef GPUPOINTERMANAGER_IMPL_HPP
#define GPUPOINTERMANAGER_IMPL_HPP


//This code depends on code within Tensor, so we keep it separate from the
//.hpp.
#ifdef ACCEL_CUDA
//#include "Utils/DataMesh/DataMesh.hpp"
#include <map>
#include <memory>
#include <vector>

namespace ManGlobals{
  inline int MakeKey(const int d, const std::vector<int>& idx_key){
    //We need a unique integer label to serve as the key. We use
    //n1+Dim(n2+Dim*(n3+...)) where the ni's are elements of idx_key.
    int key = 0;
    {
      long unsigned int i = idx_key.size()-1;
      while(i>0){
        key+=idx_key[i];
        key*=d;
        --i;
      }
      key+=idx_key[0];
    }
    return key;
  }

  inline int MagicDim(){ return 4; }
}

inline Tensor_GPUPointers<Tensor<DataMesh > >& 
  GPU_PointerManager<Tensor<DataMesh> >::GPUPointers(
      Tensor<DataMesh>& me, const int nfree, const std::vector<int>& idx_key)
{
  
  typedef Tensor_GPUPointers<Tensor<DataMesh> > GPU_t;
  typedef std::unique_ptr<GPU_t> ptr_t;
  typedef std::pair<int,ptr_t> pair_t;
  typedef std::pair<ptr_t,bool> map_t;
 
  ASSERT(idx_key.size()==(long unsigned int)me.Rank(), "Invalid idx_key size");
  
  int key = ManGlobals::MakeKey(ManGlobals::MagicDim(),idx_key);
  //std::cout << "idx_key: " << idx_key << " key: " << key << std::endl;
  
  if(mGPUPointers.count(key) == 1){ //key found
    //std::cout << "FOUND" << std::endl;
    return *(mGPUPointers.at(key));
  }else{
    auto it = 
      mGPUPointers.insert
        (std::make_pair(key,ptr_t(new GPU_t(me,nfree,idx_key))));
    //std::cout << "NOT FOUND" << std::endl;
    return *(it.first->second);
  }
}

inline Tensor_GPUPointers<Tensor<DataMesh > >& 
  GPU_PointerManager<Tensor<DataMesh> >::GPUPointers(
      const Tensor<DataMesh>& me, const int nfree, const std::vector<int>& idx_key)
{
  Tensor<DataMesh> & MeNonConst 
    = const_cast<Tensor<DataMesh>&>(me);
  return (*this).GPUPointers(MeNonConst, nfree, idx_key);
}


inline Tensor_GPUPointers<Tensor<Tensor<DataMesh > > >& 
  GPU_PointerManager<Tensor<Tensor<DataMesh> > >::GPUPointers(
      Tensor<Tensor<DataMesh> >& me, 
      const int nfreeL,
      const std::vector<int>& idx_key, const int nfreeR)
{
  typedef Tensor_GPUPointers<Tensor<Tensor<DataMesh> > > GPU_t;
  typedef std::unique_ptr<GPU_t> ptr_t;
  typedef std::pair<int,ptr_t> pair_t;
  typedef std::pair<ptr_t,bool> map_t;
 
  //ASSERT(idx_key.size()==(long unsigned int)me.Rank(), "Invalid idx_key size");
  
  int key = ManGlobals::MakeKey(ManGlobals::MagicDim(),idx_key);
  
  if(mGPUPointers.count(key) == 1){ //key found
    //std::cout << "FOUND" << std::endl;
    return *(mGPUPointers.at(key));
  }else{
    auto it = 
      mGPUPointers.insert
        (std::make_pair(key,ptr_t(new GPU_t(me,nfreeL,idx_key,nfreeR))));
    //std::cout << "NOT FOUND" << std::endl;
    return *(it.first->second);
  }
}

//Cast away const
inline Tensor_GPUPointers<Tensor<Tensor<DataMesh > > >& 
  GPU_PointerManager<Tensor<Tensor<DataMesh> > >::GPUPointers(
      const Tensor<Tensor<DataMesh> >& me, 
      const int nfreeL,
      const std::vector<int>& idx_key, const int nfreeR)
{
  Tensor<Tensor<DataMesh> > & MeNonConst 
    = const_cast<Tensor<Tensor<DataMesh> >& >(me);
  return (*this).GPUPointers(MeNonConst, nfreeL, idx_key, nfreeR);
}


#include "Tensor_GPUPointers-impl.hpp"
#endif
#endif
