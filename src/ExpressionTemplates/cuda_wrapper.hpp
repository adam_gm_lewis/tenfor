#ifndef CUDA_WRAPPER_HPP
#define CUDA_WRAPPER_HPP

#include <string>
#include <cuda_runtime.h>
//these functions don't require any cuda-only structs as arguments, so we 
//implement 'do nothing' versions of them for NONACCEL. This makes less
//'#ifdef ACCEL_CUDA's necessary throughout the code.
//#include "cublas_v2.h"
namespace GPU{
    void dShowDeviceProperties(int devNum);
    void dSynchronize();
    void profilerStart();
    void profilerStop();
    void SetDevice(const int rank);
    void cuda_tick(cudaEvent_t& start);
    void cuda_tock(cudaEvent_t& start, cudaEvent_t& stop, float &time);
    //enum cudaError_t;
    //typedef int cudaEvent_t;
    bool& err();
    std::string& errstring();
  
    bool dGetDeviceName(std::string& name, int devNum, std::string& errstring);
    bool dSynchronize(std::string& errstring);
    bool dLastError(std::string& errstring);
    bool dError(cudaError_t& err, std::string& errstring);
    void dRequire(cudaError_t& err);
    void dAssert(cudaError_t& err);
    bool dCudaMemGetInfo(size_t* free, size_t* total, std::string& errstring);
    bool dMemcpyDeviceToHost(void* dst, const void* src, const size_t count,
          std::string& errstring);
    bool dMemcpyHostToDevice(void* dst, const void* src, const size_t count,
        std::string& errstring);
    bool dMemcpyDeviceToDevice(void* dst, const void* src, const size_t count,
        std::string& errstring);
    
    bool dMalloc(void** pointer, size_t size, std::string& errstring);

    bool dFree(void* pointer, std::string& errstring);
    
    bool dGetDeviceCount(int& devCount, std::string& errstring);
    bool RuntimeGetVersion(int* runtimeVersion,std::string& errstring);

    void dInitializeDevice();
}

#endif
