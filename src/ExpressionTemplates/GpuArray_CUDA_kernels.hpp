#ifndef GpuArray_CUDA_kernels_hpp
#define GpuArray_CUDA_kernels_hpp

void AssignConstantWrapper(double* devPtr, const double s, const int nwords,
  const int nblocks, const int threadsperblock);

void AddConstantWrapper(double* devPtr, const double s, const int nwords,
  const int nblocks, const int threadsperblock);

void SubtractConstantWrapper(double* devPtr, const double s, const int nwords,
  const int nblocks, const int threadsperblock);

void MultiplyByConstantWrapper(double* devPtr, const double s, const int nwords,
  const int nblocks, const int threadsperblock);

void DivideByConstantWrapper(double* devPtr, const double s, const int nwords,
  const int nblocks, const int threadsperblock);

#endif
