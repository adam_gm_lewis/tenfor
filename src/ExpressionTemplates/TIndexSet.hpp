#ifndef TIndexSet_hpp
#define TIndexSet_hpp
/// \file Defines TIndexSet

#include <string>
#include "TIndex.hpp"


namespace ImplicitTensorLoops {


  //================================================================
  // struct TIndexSet<TIndices...>
  //================================================================
  //
  // A set of TIndex<>'s.  Each distinct <dim,label>-pair occurs at
  // most once.  The TIndex'es are placed in a canonical ordering to
  // allow compile-time comparisons between two TIndexSet's.
  //
  // Because TIndexSet's operate on TIndex<dim,label>'s, they do not
  // distinguish between different 'offsets' in
  // TIndexSlot<dim,label,offset>.  Therefore, the expression
  // A(i_)*B(i_+1), which has two different TIndexSlot<>'s, has
  // nevertheless a TIndexSet of size 1.
  //
  // MEMBER FUNCTIONS
  //   - static const int Size; // number of elements
  //
  // INVARIANTS:
  //   TIndex<dim,label> are primarily ordered by increasing value of
  //   'dim', and secondarily by increasing value of 'label'.
  //
  template<class... Tindices> struct TIndexSet { };

  template<int dim, int label, class ...Tail>
  struct TIndexSet<TIndex<dim,label>, Tail...> {
    typedef TIndexSet<Tail...> BASE;
    static const int Size=1+BASE::Size;
  };

  template<>
  struct TIndexSet<> {
    static const int Size=0;
  };



  //--------------------------------
  // Helper class
  //--------------------------------
  //
  // prepend a new TIndex at the front of a TIndexSet.
  // Do not perform checks for ordering.
  template<class Tidx, class TIndexSet>
  struct PrependInFrontOfTIndexSet {};

  template<int dim1, int label1, typename... Tail>
  struct PrependInFrontOfTIndexSet<TIndex<dim1, label1>,
				   TIndexSet<Tail...> > {
    typedef TIndexSet<TIndex<dim1, label1>, Tail...> type;
  };



  //================================================================
  // Insert TIndex_t into TIndexSet
  //================================================================
  //
  // Inserts a new TIndex, preserving the order of TIndexSet.
  //
  template<class newTIndex, class TIndexSet_t>
  struct InsertIntoTIndexSet;

  // {0} + TIndex
  template<int dim, int label>
  struct InsertIntoTIndexSet<TIndex<dim, label>, TIndexSet<> > {
    typedef TIndexSet<TIndex<dim, label> > type;
  };

  // recursion 1:  TIndex + (same TIndex, ...)
  template<int dim1, int label1, typename... Tail>
  struct InsertIntoTIndexSet<TIndex<dim1, label1>,
			     TIndexSet<TIndex<dim1, label1>, Tail...  > > {
    typedef TIndexSet<TIndex<dim1, label1>, Tail...> type;
  };

  // recursion 2:  TIndex + (different Tindex, ...)
  template<int dim1, int label1, int dim2, int label2, typename... Tail>
  struct InsertIntoTIndexSet<TIndex<dim1, label1>,
			     TIndexSet<TIndex<dim2, label2>, Tail...  > > {
    typedef typename
      std::conditional
      < dim1<dim2 || (dim1==dim2 && label1<label2), // is new index first?
        // yes: place it first
           TIndexSet<TIndex<dim1, label1>,
		     TIndex<dim2, label2>,
		     Tail... >,
        // no: recurse into Tail
           typename PrependInFrontOfTIndexSet
               <TIndex<dim2, label2>,
		typename InsertIntoTIndexSet<TIndex<dim1, label1>,
					     TIndexSet<Tail...> >::type
		>::type
      >::type type;
  };



  //================================================================
  // Union of two TIndexSets
  //================================================================
  //
  template<class TIndexSet1, class TIndexSet2>
  struct UnionOfTIndexSets;

  // recursion: Move one element from TIndexSet2 into TIndexSet1
  template<class TIndexSet1, int dim2, int label2, class... Tail>
  struct UnionOfTIndexSets<TIndexSet1,
			   TIndexSet<TIndex<dim2, label2>, Tail...> > {
    typedef typename
      UnionOfTIndexSets
      < typename InsertIntoTIndexSet<TIndex<dim2,label2>, TIndexSet1>::type,
	TIndexSet<Tail...>
	>::type type;
  };

  // break recursion
  template<class TIndexSet1>
  struct UnionOfTIndexSets<TIndexSet1, TIndexSet< > > {
    typedef TIndexSet1 type;
  };


  //================================================================
  // Is TIndex_t contained in TIndexSet_t?
  //================================================================
  //
  template<class TIndex_t, class TIndexSet_t>
  struct IsTIndexInTIndexSet;

  // break recursion:  1st index matches
  template<int dim1, int label1, typename... Tail>
  struct IsTIndexInTIndexSet<TIndex<dim1, label1>,
			     TIndexSet<TIndex<dim1, label1>,
   				       Tail...  > > {
    const static bool value=true;
  };

  // recursion into tail
  template<int dim1, int label1, int dim2, int label2, typename... Tail>
  struct IsTIndexInTIndexSet<TIndex<dim1, label1>,
			    TIndexSet<TIndex<dim2, label2>, Tail...  > > {
    const static bool value=IsTIndexInTIndexSet<TIndex<dim1,label1>,
						TIndexSet<Tail...> >::value;
  };

  // break recursion: not found
  template<int dim, int label>
  struct IsTIndexInTIndexSet<TIndex<dim, label>, TIndexSet<>> {
    const static bool value=false;
  };


  //================================================================
  // Remove TIndex_t from TIndexSet_t
  //================================================================
  //
  // static_assert()'s if TIndex_to_remove is not in TIndexSet_t
  //
  template<class TIndex_t, class TIndexSet_t>
  struct RemoveFromTIndexSet;


  // break recursion:  1st index matches
  template<int dim1, int label1, typename... Tail>
  struct RemoveFromTIndexSet<TIndex<dim1, label1>,
			     TIndexSet<TIndex<dim1, label1>,
   				       Tail...  > > {
    typedef TIndexSet<Tail...> type;
  };


  // recursion into tail
  template<int dim1, int label1, int dim2, int label2, typename... Tail>
  struct RemoveFromTIndexSet<TIndex<dim1, label1>,
			     TIndexSet<TIndex<dim2, label2>, Tail...  > > {
    // sanity check, to guarantee the specialization above is called
    static_assert(dim1!=dim2 || label1!=label2,
		  "Error.  This template should not be called when the "
		  "first TIndex matches");

    // Error message placed here to avoid an extra specialization
    static_assert(TIndexSet<Tail...>::Size>0,
		  "TIndex not found in TIndexSet");

    typedef typename PrependInFrontOfTIndexSet
      <TIndex<dim2, label2>,
       typename RemoveFromTIndexSet<TIndex<dim1, label1>,
				    TIndexSet<Tail...> >::type
       >::type type;
  };

}

#endif
