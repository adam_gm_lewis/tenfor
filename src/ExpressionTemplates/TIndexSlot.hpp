#ifndef TIndexSlot_hpp
#define TIndexSlot_hpp

#include "TIndex.hpp"
#include "Require.hpp"
//#include "Utils/LowLevelUtils/ConvertNumberToString.hpp"
#include <string>

namespace ImplicitTensorLoops {

  // indicates a TIndex which ranges over dim different values.
  // (spatial TensorIndices would have dim==3, SpaceTime-TensorIndeces
  // would have Dim=4).  label is used to distinguish different
  // TensorIndices This class is used only as a tag, to convey
  // information about tensor-indices **at compile-time**.
  // Therefore, for now, the implenentation is an empty class.
  template<int dim, int label, int offset>
  class TIndexSlot: public TIndex<dim, label> {
  public:
    // default constructors/destructor etc ok.

    static const int Offset=offset;

    static std::string VarName() {
      std::string tmp=TIndex<dim,label>::VarName();
      for(int i=0; i<offset; ++i)
	tmp+="+1";
      return tmp;
    }

    static std::string CPlusPlusName() {
      std::string tmp="TIndexSlot<"+std::to_string(dim)+","
        +std::to_string(label) + "," + std::to_string(offset) + ">";
      return tmp;
    }


    static int Value() { return TIndex<dim,label>::Value()+offset; }

  private:
    // FIXME:  Decide whether this friend is needed.
    template<int curr_dim, class TIndex_t, class iBinaryOp_t>
    friend struct PartialSum;
  };

  //  template<int dim, int label>
  //  int TIndexSlot<dim,label>::mCounter;

  template<int d, int l, int o>
  TIndexSlot<d,l,o+1> operator+(const TIndexSlot<d,l,o>& ,
					  const int offset) {
    REQUIRE(offset==1,
	    "operator+() for TIndexSlot can only be used with '+1' as second\n"
	    "argument.  If you should need a '+2', write '+1+1'.  If you\n"
	    "find yourself wanting something even different, please get in\n"
	    "touch with Adam Lewis or Harald Pfeiffer and discuss options.\n");
    return TIndexSlot<d, l, o+1>();
  };
}

namespace TIndex3 {
  using namespace ImplicitTensorLoops;
  // concrete tensor-indices for calculations
  extern TIndexSlot<3,0,0> i_;
  extern TIndexSlot<3,1,0> j_;
  extern TIndexSlot<3,2,0> k_;
  extern TIndexSlot<3,3,0> l_;
  extern TIndexSlot<3,4,0> m_;
  extern TIndexSlot<3,5,0> n_;
  extern TIndexSlot<3,6,0> o_;
  extern TIndexSlot<3,7,0> p_;
}

namespace TIndex4 {
  using namespace ImplicitTensorLoops;
  extern TIndexSlot<4,0,0> a_;
  extern TIndexSlot<4,1,0> b_;
  extern TIndexSlot<4,2,0> c_;
  extern TIndexSlot<4,3,0> d_;
  extern TIndexSlot<4,4,0> e_;
}

#endif
