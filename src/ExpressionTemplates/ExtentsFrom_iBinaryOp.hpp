#ifndef EXTENTS_FROM_i_BINARY_OP 
#define EXTENTS_FROM_i_BINARY_OP
namespace ImplicitTensorLoops {
  
  //Extra namespace to hide the helper classes from external code
  namespace ExtentsHelpers{
    
    //Generic definition 
    template<class iBOp_t> 
    struct ExtentsFromiBOpHelper;

    //Non-leaf case - recurse on the rhs class
    //This will repeatedly call itself on class R until it reaches a leaf
    //class, in which case we call one of the specializations below.
    template<class L, class O, class R>
    struct ExtentsFromiBOpHelper<iBinaryOp<L, O, R> >{
      typedef iBinaryOp<L,O,R> iBOp_t;
      ExtentsFromiBOpHelper<iBOp_t>() {};
    
      template<class DM_t>
      static inline const IPoint& Extents(const iBOp_t& op){
        static const ExtentsHelpers::ExtentsFromiBOpHelper<R> Helper;
        return Helper.template Extents<DM_t>(op.rhs);
      }
    
    };
    
    //template<class L, class O, class R>
    //ExtentsFromiBOpHelper<iBinaryOp<L,O,R>>::
    //static inline const IPoint& Extents<DataMesh>(const 
    //  iBinaryOp<L,O,R>& op);

    //Eventually we will reach one of the following leaf-type specializations,
    //which actually return a MyVector. We assume without checking that 
    //all the leafs in the iBOp have the same extents, but if DEBUG is on
    //we at least check that all the DataMeshes in the leaf we end up in
    //do.
     
    //Tensor<Tensor<DataMesh> >
    template<class TS1, class TS2>
    struct ExtentsFromiBOpHelper<
      iBinaryOp<std::pair<TS1, TS2>,EmptyType,  DataMesh>> 
      
    {
      typedef iBinaryOp<std::pair<TS1, TS2>,EmptyType,  DataMesh> iBOp_t; 
      ExtentsFromiBOpHelper<iBOp_t>() {}; 
      
      //The extra template avoids 'invalid use of incomplete type' errors
      //surrounding the simulataneous use of forward-declared type DataMesh
      //for its member function 'Extents' and as a template parameter.
      template<class DM_t>
      static inline const IPoint& Extents(const iBOp_t& op)
      {
        const Tensor<Tensor<DM_t> >& theTTensor = op.TheTTensor();
        const IPoint& TheExtents 
                              = theTTensor.begin()->begin()->Extents();
        #ifdef DEBUG
        //check all extents agree
        for(auto& TDM : theTTensor){
          for(auto& DM : TDM) {
            ASSERT(DM.Extents() == TheExtents, "rhs had non-matching extents!");
          }
        }
        #endif
        return TheExtents;
        
      }
    };
    
    //Tensor<DataMesh>
    template<class ...Symm, class ...Indices>
    struct ExtentsFromiBOpHelper<
          iBinaryOp<TIndexStructure<TSymmetry<Symm...>, 
            Indices...>, EmptyType,  DataMesh>>
      {
        typedef iBinaryOp<TIndexStructure<TSymmetry<Symm...>, 
                  Indices...>, EmptyType,  DataMesh> iBOp_t;
        ExtentsFromiBOpHelper<iBOp_t>(){};
        //The extra template avoids 'invalid use of incomplete type' errors
        //surrounding the simulataneous use of forward-declared type DataMesh
        //for its member function 'Extents' and as a template parameter.
        template<class DM_t>
        static inline const IPoint& Extents(const iBOp_t& op)
        {
          const Tensor<DM_t>& theTensor = op.TheTensor();
          const IPoint& TheExtents = theTensor.begin()->Extents();
          //const IPoint& TheExtents = theTensor.begin()->Extents();
          #ifdef DEBUG
          //check all extents agree
          for(auto& DM : theTensor){
            ASSERT(DM.Extents() == TheExtents, "rhs had non-matching extents!");
          }
          #endif
     
        return TheExtents;
      }
    };
    

    //DataMesh
    template<>
    struct ExtentsFromiBOpHelper<DataMesh>
    {
      ExtentsFromiBOpHelper<DataMesh>(){};
      
      //The extra template avoids 'invalid use of incomplete type' errors
      //surrounding the simulataneous use of forward-declared type DataMesh
      //for its member function 'Extents' and as a template parameter.
      template<class DM_t>
      static inline const IPoint& Extents(const DM_t& op){
        return op.Extents();
      }
    };
  
  } //close namespace ExtentsHelpers

  //The actual function - this is the only thing to be called from outside
  template<class iBOp_t>
  static inline const IPoint& ExtentsFrom_iBinaryOp(const iBOp_t& op){
    static const ExtentsHelpers::ExtentsFromiBOpHelper<iBOp_t> Helper;
    
    //Here we actually tell the code DM_t is supposed to be DataMesh.
    //We need the 'template' keyword to clarify for the compiler
    //that Extents<DataMesh> is a function called 'Extents' templated
    //on DataMesh, and not a function called 'Extents<DataMesh>'.

    return Helper.template Extents<DataMesh>(op);
  }

} //close namespace ImplicitTensorLoops

#endif
