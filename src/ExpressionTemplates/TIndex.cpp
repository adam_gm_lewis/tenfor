#include "TIndex.hpp"
//#include "Utils/LowLevelUtils/ConvertNumberToString.hpp"
#include <vector>



namespace ImplicitTensorLoops {
  template<int dim, int label>
  std::string TIndex<dim,label>::VarName() {
    static_assert((dim==3 || dim==4) && (0<=label && label<=7),
		  "VarName not defined for these values of dim and label");
    if(dim==3) {
      std::vector<std::string> temp({"i","j","k","l", "m","n","o","p"});
      return temp[label];
    }
    std::vector<std::string> temp({"a","b","c","d","e","f","g","h"});
    return temp[label];
  }

  template class TIndex<3,0>;
  template class TIndex<3,1>;
  template class TIndex<3,2>;
  template class TIndex<3,3>;
  template class TIndex<3,4>;
  template class TIndex<3,5>;
  template class TIndex<3,6>;
  template class TIndex<3,7>;
  
  template class TIndex<4,0>;
  template class TIndex<4,1>;
  template class TIndex<4,2>;
  template class TIndex<4,3>;
  template class TIndex<4,4>;
  template class TIndex<4,5>;
  template class TIndex<4,6>;
  template class TIndex<4,7>;

}
