#ifndef OUTPUT_VECTOR_HPP
#define OUTPUT_VECTOR_HPP
#include <string>
#include <sstream>
template <class X>
std::vector<X> ConcatVectors(const std::vector<X>& v1, 
                             const std::vector<X>& v2) {
    std::vector<X> out(v1);
    out.insert(out.end(), v2.begin(), v2.end());
    return out;
}


template <class X>
std::string VecToString(const std::vector<X>& vec) {
  std::ostringstream os;
  const std::size_t sz = vec.size();
  os << "(";
  if (sz>=1) {
    os << vec[0];
    for(std::size_t i=1; i<sz; ++i) os << "," << vec[i];
  }
  os << ")";
  return os.str();
}

template <class X>
std::ostream& OutputVector(std::ostream& os, const std::vector<X>& vec) {
  const std::size_t sz = vec.size();
  os << "(";
  if (sz>=1) {
    os << vec[0];
    for(std::size_t i=1; i<sz; ++i) os << "," << vec[i];
  }
  os << ")";
  return os;
}

#endif
