#include "GpuArray_CUDA_kernels.hpp"
//=
__global__ void AssignConstantKernel(double* devPtr, const double s, 
    const int nwords)
{
    int idx = threadIdx.x + blockDim.x * blockIdx.x;
    if (idx < nwords) devPtr[idx]=s;
}

//+=
__global__ void AddConstantKernel(double* devPtr, const double s, 
    const int nwords)
{
    int idx = threadIdx.x + blockDim.x * blockIdx.x;
    if (idx < nwords) devPtr[idx]+=s;
}

//-=
__global__ void SubtractConstantKernel(double* devPtr, const double s, 
    const int nwords)
{
    int idx = threadIdx.x + blockDim.x * blockIdx.x;
    if (idx < nwords) devPtr[idx]-=s;
}

// *=
__global__ void MultiplyByConstantKernel(double* devPtr, const double s, 
    const int nwords)
{
    int idx = threadIdx.x + blockDim.x * blockIdx.x;
    if (idx < nwords) devPtr[idx]*=s;
}

// /=
__global__ void DivideByConstantKernel(double* devPtr, const double s, 
    const int nwords)
{
    int idx = threadIdx.x + blockDim.x * blockIdx.x;
    if (idx < nwords) devPtr[idx]/=s;
}

void AssignConstantWrapper(double* devPtr, const double s, const int nwords,
  const int nblocks, const int threadsperblock){
  AssignConstantKernel<<<nblocks,threadsperblock>>>(devPtr,s,nwords);
}

void AddConstantWrapper(double* devPtr, const double s, const int nwords,
  const int nblocks, const int threadsperblock){
  AddConstantKernel<<<nblocks,threadsperblock>>>(devPtr,s,nwords);
}

void SubtractConstantWrapper(double* devPtr, const double s, const int nwords,
  const int nblocks, const int threadsperblock){
  SubtractConstantKernel<<<nblocks,threadsperblock>>>(devPtr,s,nwords);
}

void MultiplyByConstantWrapper(double* devPtr, const double s, const int nwords,
  const int nblocks, const int threadsperblock){
  MultiplyByConstantKernel<<<nblocks,threadsperblock>>>(devPtr,s,nwords);
}

void DivideByConstantWrapper(double* devPtr, const double s, const int nwords,
  const int nblocks, const int threadsperblock){
  DivideByConstantKernel<<<nblocks,threadsperblock>>>(devPtr,s,nwords);
}
