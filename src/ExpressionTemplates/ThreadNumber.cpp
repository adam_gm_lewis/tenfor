#include "ThreadNumber.hpp"

#include "WorkerPool.hpp"

#ifdef _OPENMP
#  include <omp.h>

#  define OMP_THREAD_NUM() omp_get_thread_num()
#  define OMP_NUM_THREADS() omp_get_num_threads()
#else
#  define OMP_THREAD_NUM() 0
#  define OMP_NUM_THREADS() 1
#endif

int maxNumThreadsUsed = 1;

int GetThisThreadNum() {
  const int WPthread = WorkerPoolGetThisThreadNum();
  if(WPthread != 0) {
    ASSERT(OMP_THREAD_NUM() == 0,
           "Both WorkerPool and OpenMP report nonzero thread numbers!");
    return WPthread;
  }
  return OMP_THREAD_NUM();
}

int GetNumThreads() {
  const int WPthreads = WorkerPoolGetNumThreads();
  if(WPthreads != 1) {
    ASSERT(OMP_NUM_THREADS() == 1,
           "Both WorkerPool and OpenMP are running at once!");
    return WPthreads;
  }
  return OMP_NUM_THREADS();
}

void SetNumThreads(const int kNumThreads)
{
#ifdef _OPENMP
  omp_set_num_threads(kNumThreads);
#endif
  maxNumThreadsUsed =
    maxNumThreadsUsed >= kNumThreads ? maxNumThreadsUsed : kNumThreads;
}

int GetMaxNumThreadsUsed() { return maxNumThreadsUsed; }
