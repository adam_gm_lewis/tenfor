#ifndef TENSOR_GPUPOINTERS_HPP
#define TENSOR_GPUPOINTERS_HPP
//This is to be included at the *beginning* of Tensor.hpp. It contains code
//that Tensor needs from Tensor_GPUPointers, but no code that the latter 
//needs from the former.

template<class X> class Tensor;
class DataMesh;
//#include "Utils/DataMesh/DataMesh.hpp"
//The generic template is just a placeholder. It keeps us from having to
//specialize Tensor.hpp itself.
template<class X>
class Tensor_GPUPointers: private NonCopyable{
  public:  
    Tensor_GPUPointers(X&) {}; //Do nothing
};

#ifdef ACCEL_CUDA
#include "cuda_wrapper.hpp"
#include "CudaMemoryHandler.hpp"
#include "NonCopyable.hpp"
#include "Mesh.hpp"
#include <string>
#include "MeshIndex.hpp"
template<>
class Tensor_GPUPointers<Tensor<DataMesh> >: private NonCopyable{
  public:
    Tensor_GPUPointers(Tensor<DataMesh>& me, const int nfree,
        const std::vector<int>& idx_key);
    ~Tensor_GPUPointers();
    const double** GetPointers();  //Syncs then returns mp_devPointers.
    double** GetPointersNonConst();//Syncs then returns mp_devPointersNC.
    //const int* IndexVector(){return mp_devIndex;} 
  private: 
    void SynchronizePointers();    
                                  //Loop through the pointers in the *Tensor*
                                   //and make sure they agree with those in
                                   //mp_hstPointers. If they don't, copy them
                                   //into mp_hstPointers, then copy 
                                   //mp_hstPointers into mp_devPointers. If
                                   //the pointers disagreed, do the same for
                                   //the NonConst pointers also.
    void SynchronizePointersNonConst(); //Above for non-const.
    void BuildIndexList(const int nfree, const std::vector<int>& IdxKey);
    bool isOutOfSync;              //True if hst and dev pointers disagree.
    Tensor<DataMesh>& mTensor;     //Reference to the relevant Tensor.
    const int mDim;
    const int mTDim;
    int mSize;               //Total number of Tensor elements.
    
    const double** mp_hstPointers; //Tracks the pointers on the host (but they
                                   //point to the device).
    double** mp_devPointers;       //Tracks the pointers on the device.
    
    std::vector<IPoint> mIdxList;     //List of Tensor indices in the order
                                   //they will be looped over.
    bool mErr;                     //True if an error is triggered.
    std::string mErrstring;        //Stores the error message.
};

template<>
class Tensor_GPUPointers<Tensor<Tensor<DataMesh> > >: private NonCopyable{
  public:
    Tensor_GPUPointers(Tensor<Tensor<DataMesh> >& me, const int nfreeL,
        const std::vector<int>& idx_key, const int nfreeR);
    ~Tensor_GPUPointers();
    const double** GetPointers();  //Syncs then returns mp_devPointers.
    double** GetPointersNonConst();//Syncs then returns mp_devPointersNC.
    //const int* IndexVector(){return mp_devIndex;} 
  private: 
    void SynchronizePointers();    
                                  //Loop through the pointers in the *Tensor*
                                   //and make sure they agree with those in
                                   //mp_hstPointers. If they don't, copy them
                                   //into mp_hstPointers, then copy 
                                   //mp_hstPointers into mp_devPointers. If
                                   //the pointers disagreed, do the same for
                                   //the NonConst pointers also.
    void SynchronizePointersNonConst(); //Above for non-const.
    void BuildIndexList(const int nfreeL, const int nfreeR,
        const std::vector<int>& IdxKey);
    bool isOutOfSync;              //True if hst and dev pointers disagree.
    Tensor<Tensor<DataMesh> >& mTensor;     //Reference to the relevant Tensor.
    const int mDimL;
    const int mDimR;
    const int mTDimL;
    const int mTDimR;
    const int mSize;               //Total number of Tensor elements.
    const double** mp_hstPointers; //Tracks the pointers on the host (but they
                                   //point to the device).
    double** mp_devPointers; //Tracks the pointers on the device.
    //FIXME: use a separate index instead of a nested std::vector to keep track
    //of inner and outer components
    std::vector<IPoint> mIdxListL;
    std::vector<IPoint> mIdxListR;
    bool mErr;                     //True if an error is triggered.
    std::string mErrstring;        //Stores the error message.

};
#endif


#endif
