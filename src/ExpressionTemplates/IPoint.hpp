#ifndef IPoint_hpp
#define IPoint_hpp
#include <vector>
typedef std::vector<int> IPoint;
 
//============================================================================
/// Returns offset into array of given Extents at a given Index.
/// If both Index and Extents are empty, returns zero.
int ComputeIndex(const IPoint& Index, const IPoint& Extents);
//============================================================================

//============================================================================
/// returns the IPoint corresponding to the indices along the various
/// sub-dimensions corresponding to the given offset.
//============================================================================
const IPoint ComputeIndex(const int offset, const IPoint& Extents);

//============================================================================
/// Returns true if ComputeIndex would return a valid index for these 
/// arguments.
bool IsComputeIndexValid(const IPoint& Index, const IPoint& Extents);
//============================================================================
#endif
