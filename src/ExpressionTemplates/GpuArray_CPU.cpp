#include "GpuArray.hpp"
//#include "GpuStatistics.hpp"
#include "MemsetToZero.hpp"
#include <cstring>
//=================================================================
GpuArray::GpuArray()
  : mSz(0), 
    mpData(0)
{
  //GpuStatisticsNew(sz, 0); 
}

//=================================================================

//=================================================================

GpuArray::GpuArray(const int sz)
  : mSz(sz), 
    mpData(new double[sz])
{
  //GpuStatisticsNew(sz, 0); 
}

//=================================================================

GpuArray::GpuArray(const GpuArray & rhs)
  : mSz(rhs.mSz), 
    mpData(new double[rhs.mSz])
{
  //GpuStatisticsNew(mSz, 0); 
  memcpy(static_cast<void*>(mpData),
	 static_cast<const void*>(rhs.HostData()), rhs.mSz*sizeof(double));
}

//=================================================================

GpuArray& GpuArray::operator=(GpuArray rhs) {
  swap(*this, rhs); 
  return *this;
}

//================================================================

GpuArray::GpuArray(GpuArray&& rhs):GpuArray() {
  swap(*this, rhs); 
  //return *this;
}
//================================================================

//================================================================
void swap(GpuArray& first, GpuArray& second){
  using std::swap;
  swap(first.mSz,second.mSz);
  swap(first.mpData,second.mpData);
}
//================================================================

GpuArray::~GpuArray() {
  delete[] mpData;
  //GpuStatisticsDestroy(mSz, 0); 
}

//================================================================

void GpuArray::Resize(const int new_size) {
  if(new_size!=Size()) {
    //GpuStatisticsResizeHost(Size(), new_size);
    delete[] mpData;
    mSz=new_size;
    mpData=new double[mSz];
  }
}
