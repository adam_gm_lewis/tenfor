//=============================================================================
/// \file
/// Functions for squaring, cubing, etc. 

#ifndef Exponents_hpp
#define Exponents_hpp

/// Square a number.
/// Note: There's an overload for DataMesh'es in ExpressionTemplates.hpp
template<class T> 
inline T sqr(const T &x)  {return x*x;}

/// Raise to third power.
/// Note: There's an overload for DataMesh'es in ExpressionTemplates.hpp
template<class T> 
inline T cube(const T &x) {return x*x*x;}

/// Raise to fourth power.
/// Note: There's an overload for DataMesh'es in ExpressionTemplates.hpp
/// pow4 was quad, but quad gets used for quad-precission math on some
/// machines like the SGI, so it isn't a good idea to use this name.
template<class T> 
inline T pow4(const T &x) {return sqr(sqr(x));}


#endif // Exponents_hpp
