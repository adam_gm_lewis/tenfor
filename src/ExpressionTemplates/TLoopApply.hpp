#ifndef TLoopApply_hpp
#define TLoopApply_hpp

//These functions are called during operations on BinaryOp or iBinaryOps.
//With NONACCEL defined, they perform whatever operation is specified by the
//ApplyOp. Otherwise they call the relevant automatically generated function.

//The functions are actually just a single call to either NonaccelTLoopApply
//or to TLoopApply-impl (the automatically generated functions) - unless
//ACCEL_DEBUG is defined. In that case temporary data are generated and run
//through NonaccelTLoopApply, then compared against the TLoopApply-impl
//results. This is the only reason that TLoopApply template specializations are
//required; the specializations allow the type which the BinaryOp was generated
//from to be deduced (i.e. whether the parent code was an operation on a
//Tensor<DM>, Tensor<Tensor<DM> >, or DM).

//Any compilation unit instantiating the TLoopApply functions needs access
//to their *implementations*. Therefore the function definitions cannot be
//placed in a separate header file (unless the instantiations were also
//#included). Furthermore, the functions *must* be
//inlined to avoid multiple definition errors at link time.

//DataMesh, Tensor, iBinaryOp, BinaryOp, and NonConstiBinaryOp are all used
//below, but in such a way that their types may be kept implicit. Code
//requiring explicit typing of these will fail.

//These have to be declared for many of the #includes to work.
namespace ImplicitTensorLoops {
  template<class L, class O, class R> struct iBinaryOp;
  template<class L, class O, class R> struct NonConstiBinaryOp;
  struct SumOp;
}
template<class X> class Tensor;
class DataMesh;
//#include "Utils/MiscUtils/SimpleProfiler.hpp"

#ifndef NONACCEL 
  //The automatic functions
  #include "AutoExpressions.hpp"
#endif

//This order is important! BinaryOpHolder needs the iBinaryOp definitions.
#include "BinaryOpHolder.hpp"
#ifdef ACCEL_DEBUG
  #include "TNameHelper.hpp"
  #include <list>
  #include "TAssignmentHelpers.hpp"
  #include "TComparisonHelpers.hpp"
#endif


#ifdef CODE_WRITER
  //Does the function tagging required for automatic code generation
  #include "ConcreteTExpressionTreeHouse.hpp"
#endif

namespace ImplicitTensorLoops{
  #ifdef NONACCEL
  //iBinaryOp, BinaryOp 
  template<class L, class O, class ApplyOp,  class RHS>
  void TLoopApply_impl(NonConstiBinaryOp<L,O,DataMesh>& lhs, const ApplyOp& op,
          const RHS& rhs) {
    NonaccelTLoopApply(lhs, op, rhs);
  }

  //DataMesh
  template <class ApplyOp, class RHS>
  void TLoopApply_impl(DataMesh& lhs, const ApplyOp& op, const RHS& rhs) {
    NonaccelTLoopApply(lhs, op, rhs);
  }
  #endif

  #ifdef ACCEL_DEBUG
  //iBinaryOp, BinaryOp - resolve the iBinaryOp with nonaccel expression 
  //templates and return the results in a list.
  template<class L, class O, class ApplyOp,  class RHS>
  std::list<DataMesh> TLoopApply_debug(NonConstiBinaryOp<L,O,DataMesh>& lhs, 
                                       const ApplyOp&, const RHS& rhs) {
    std::list<DataMesh> TempHold;
    lhs.CheckUniqueIndices();
    lhs.CheckSymmetries();
    for(lhs.Reset(); lhs; ++lhs) {
      BinaryOpHolder<RHS> holder(rhs);
      auto DM = lhs.ExpandIndices();
      ApplyOp::modify(DM, holder.op);
      TempHold.emplace_back(DM);
    }
    lhs.Reset();
    return TempHold;
  }
 
  //Output the operation in question and ensure lhs and the data in to_compare
  //are equal.
  template<class L, class O, class ApplyOp,  class RHS>
  void TLoopApply_compare(NonConstiBinaryOp<L,O,DataMesh>& lhs, 
                          const ApplyOp&, const RHS& rhs, 
                          const std::list<DataMesh>& to_compare) {
    std::ostringstream compstr;
    compstr << "\nLHS : " << TNameHelper<LHS>::ShortName()
                        << "\nOP : " << TNameHelper<ApplyOp>::ShortName()
                        << "\nRHS : " << TNameHelper<RHS>::ShortName();
    std::list<DataMesh> accelTempHold;
    for(lhs.Reset(); lhs; ++lhs) {
      auto DM = lhs.ExpandIndices();
      accelTempHold.emplace_back(DM);
    }
    comparestd::listDM(to_compare, accelTempHold, compstr.str());
  }

  #endif
  //***************************************************************************
  //*NonaccelTLoopApply
  //***************************************************************************
  //*Executes the expression encoded within the RHS. Performance should
  //*be equivalent to C++ for loops around the DataMesh operations.
  //*This has been confirmed by experiment for gcc. icc seems to generate rather 
  //*slow code from these.
  //***************************************************************************

  //This is the template for iBinaryOp (i.e. Tensor) operations. Special
  //treatment of the indices is required in this case.
  template<class L, class O, class ApplyOp,  class RHS>
  void NonaccelTLoopApply(NonConstiBinaryOp<L,O,DataMesh>& lhs, const ApplyOp&,
          const RHS& rhs) {

    //Uncomment this for information about which expression is being
    //processed.
    //std::cout << TNameHelper<NonConstiBinaryOp<L,O,DataMesh> >::ShortName()
    //<< " "  << TNameHelper<ApplyOp>::ShortName() << " "
    //<< TNameHelper<RHS>::ShortName() << std::endl;

    //SimpleProfiler prof("TLoopApply::Nonaccel");
    lhs.CheckUniqueIndices();
    lhs.CheckSymmetries();
    for(lhs.Reset(); lhs; ++lhs) {
      BinaryOpHolder<RHS> holder(rhs);
      ApplyOp::modify(lhs.ExpandIndices(), holder.op);
    }
  }


  //LHS = BinaryOp - DataMesh operations. The BinaryOpHolder allows data to
  //survive their scope.
  template <class L, class O, class ApplyOp, class RHS>
  void NonaccelTLoopApply(BinaryOp<L,O,DataMesh>& lhs, const ApplyOp&, 
      const RHS& rhs) {
    BinaryOpHolder<RHS> holder(rhs);
    ApplyOp::modify(lhs, holder.op);
  }

  //LHS = DataMesh
  template <class ApplyOp, class RHS>
  void NonaccelTLoopApply(DataMesh& lhs, const ApplyOp&, const RHS& rhs) {
    BinaryOpHolder<RHS> holder(rhs);
    ApplyOp::modify(lhs, holder.op);
  }

  //***************************************************************************
  //*TLoopApply
  //***************************************************************************
  //*1. Routes code to NonaccelTLoopApply or to the relevant automatically
  //*   generated TLoopApply-impl as appropriate.
  //*2. With CODE_WRITER defined, calls ConcreteTExpressionTreeHouse, which
  //*   generates the tagging and strings necessary for automatic code
  //*   generation.
  //*3. With ACCEL_DEBUG defined, deduces the type of the LHS in the parent
  //*   code, creates a temporary, and uses it to test the Nonaccel against the
  //*   automatically generated code.
  //***************************************************************************

  //This will be called by Tensor<Tensor<DataMesh> > = blah
  template<class ...Symm1, class ...Symm2,
    class ...Indices1, class ...Indices2,
    class ApplyOp, class RHS>
  inline void TLoopApply(NonConstiBinaryOp<std::pair<
        TIndexStructure<TSymmetry<Symm1...>, Indices1...>,
        TIndexStructure<TSymmetry<Symm2...>, Indices2...> >,
        EmptyType,DataMesh>& lhs,
        const ApplyOp& op, const RHS& rhs){

    typedef NonConstiBinaryOp<std::pair<
        TIndexStructure<TSymmetry<Symm1...>, Indices1...>,
        TIndexStructure<TSymmetry<Symm2...>, Indices2...> >,
        EmptyType,DataMesh> LHS;

    //SimpleProfiler prof("TLoopApply::TTDM");
    //Tag if we're compiling for CodeWriter
    #ifdef CODE_WRITER
    TCodeWriter::ConcreteTExpressionTreeHouse<LHS,ApplyOp,RHS> tag;
    #endif

    //Evaluate the nonaccel expression on a temporary if we're debugging.
    //We use a list for the output here in order to avoid modifying the lhs
    //(it's not so simple to just copy an iBinaryOp). Otherwise, this code is
    //equivalent to NonaccelTLoopApply.
    #ifdef ACCEL_DEBUG
    std::list<DataMesh> TempHold = TLoopApply_debug(lhs, op, rhs);
    #endif

    //Do the main evaluation
    TLoopApply_impl(lhs,op,rhs);

    //Compare the temporary with the result
    #ifdef ACCEL_DEBUG
    TLoopApply_compare(lhs, op, rhs, TempHold); 
    #endif
  }

  //Tensor<DataMesh> = blah. Note this essentially differs from the above
  //only in the typedef.
  template<class ...Symm, class ...Indices, class ApplyOp, class RHS>
  inline void TLoopApply(NonConstiBinaryOp<TIndexStructure
                              <TSymmetry<Symm...>, Indices...>,
                                     EmptyType, DataMesh>& lhs,
                  const ApplyOp& op, const RHS& rhs) {

    typedef NonConstiBinaryOp<TIndexStructure<TSymmetry<Symm...>, Indices...>,
                                     EmptyType, DataMesh>LHS;

    //SimpleProfiler prof("TLoopApply::TDM");
    //Tag if we're compiling for CodeWriter
    #ifdef CODE_WRITER
    TCodeWriter::ConcreteTExpressionTreeHouse<LHS,ApplyOp,RHS> tag;
    #endif

    //Evaluate the nonaccel expression on a temporary if we're debugging
    #ifdef ACCEL_DEBUG
    std::list<DataMesh> TempHold = TLoopApply_debug(lhs, op, rhs);
    #endif

    //Do the main evaluation
    TLoopApply_impl(lhs,op,rhs);

    //Compare the temporary with the result
    #ifdef ACCEL_DEBUG
    TLoopApply_compare(lhs, op, rhs, TempHold); 
    #endif
  }

  //DataMesh = blah
  template<class ApplyOp, class RHS> 
  inline void TLoopApply(DataMesh& lhs, const ApplyOp& op, const RHS& rhs)
  {
    //Tag if we're compiling for CodeWriter
    #ifdef CODE_WRITER
    TCodeWriter::ConcreteTExpressionTreeHouse<DataMesh,ApplyOp,RHS> tag;
    #endif

    //Evaluate the nonaccel expression on a temporary if we're debugging
    #ifdef ACCEL_DEBUG
    DataMesh temp = lhs;
    BinaryOpHolder<RHS> holder(rhs);
    ApplyOp::modify(temp,holder.op);
    #endif

    //Do the main evaluation
    TLoopApply_impl(lhs,op,rhs);

    //Compare the temporary with the result
    #ifdef ACCEL_DEBUG
    std::ostringstream compstr;
        compstr << "\n LHS : DataMesh"
                        << "\nOP : " << TNameHelper<ApplyOp>::ShortName()
                        << "\nRHS : " << TNameHelper<RHS>::ShortName();

    compareDM(temp,lhs,compstr.str());
    #endif
  }
}

#endif
