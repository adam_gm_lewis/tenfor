#ifndef iBinaryOp_impl_hpp
#define iBinaryOp_impl_hpp
#include "OutputVector.hpp"
// iBInaryOp-impl.hpp
//
template <class T>
class Tensor;

namespace ImplicitTensorLoops {


  //================================================================
  // iBinaryOp<..., DataMesh>
  //================================================================

  template<class ...Symm, class ...Indices>
  const DataMesh& iBinaryOp<TIndexStructure<TSymmetry<Symm...>, Indices...>,
			    EmptyType,
			    DataMesh>::ExpandIndices() const {
    mTIndexStructure.GetAllIndices(idx);
    return mTensor(idx);
  }


  template<class ...Symm, class ...Indices>
  const DataMesh& iBinaryOp<TIndexStructure<TSymmetry<Symm...>, Indices...>,
			    EmptyType,
			    DataMesh>
  ::GetComponent(const std::initializer_list<int>& l) const {
    REQUIRE(l.size()==static_cast<size_t>(TIndexStructure_t::NFree),
	      "Inconsistency when calling GetComponent");
      mTIndexStructure.ExpandFreeIndices(l.begin(), idx.begin() );
      return mTensor(idx);
    }
  
  
  template<class ...Symm, class ...Indices>
  Tensor_GPUPointers<Tensor<DataMesh> >&
    iBinaryOp<TIndexStructure<TSymmetry<Symm...>, Indices...>,
			    EmptyType,
			    DataMesh>::GPUPointers() const{
    const int nfree = mTIndexStructure.GetNFree();
    IPoint idx_key(0);
    mTIndexStructure.GetFreeIndices(idx_key);
    return mTensor.GPUPointers(nfree,idx_key);
  }
  

  /*
  template<class ...Symm, class ...Indices>
  const double* 
    iBinaryOp<TIndexStructure<TSymmetry<Symm...>, Indices...>,
			    EmptyType,
			    DataMesh>::GPUData() const {
    const int nfree = mTIndexStructure.GetNFree();
    IPoint idx_key(MV::Size(0));
    mTIndexStructure.GetAllIndices(idx_key);
    return mTensor.GPUPointers(nfree,idx_key).GetPointers();
  }
  */
  // return an index that unambiguously identifies the precise set
  // of fixed indices.  This index can be used to store the data
  // returned by GetFreeComponentPointers in a data-structure that
  // is longer lived than the iBinaryOp.
  template<class ...Symm, class ...Indices>
  int iBinaryOp<TIndexStructure<TSymmetry<Symm...>, Indices...>,
		EmptyType,
		DataMesh>::GetFreeComponentPointersIdx() const {
    // Algorithm:  Construct a tuple of length equal to the rank of the
    // tensor.  the value is 0 for each implicit index, and fixed+1 for
    // each index that is fixed to a value 'fixed'.
    // Interpret this tuple as a tensor.Dim()+1-base number, where the
    // last index represents the one's.

    IPoint freeIndex;
    mTIndexStructure.GetFreeIndices(freeIndex);
    const int dim=mTensor.Dim()+1;
    int result=0;
    for(size_t i=0; i<freeIndex.size(); ++i)
      result=dim*result+freeIndex[i]+1;
    return result;
  }

  //These layers of templating are necessary to avoid 'invalid use of
  //incomplete type' errors caused by the simulataneous use of forward-
  //declared type DataMesh for its member function 'Extents' and as a 
  //template parameter.
  struct DataMeshHelper
  {
    template<class DM_t>
    static inline double* GetDataFromForwardDeclaredDM(const DM_t& DM){
      return DM.Data();
    }
  };
  // returns an array of pointers into the data stored inside the
  // Tensor<DataMesh>.  The array has size Tensor.Dim()^NFree
  // array[a+Dim*(b+Dim*c+...)] corresponds to the tenor component
  // where the first implicit index takes value a, the second
  // implicit index takes value b, etc.  During this process, all
  // **fixed** indices are kept at whatever their fixed values are.
  template<class ...Symm, class ...Indices>
  std::vector<double*>
  iBinaryOp<TIndexStructure<TSymmetry<Symm...>, Indices...>,
	    EmptyType,
	    DataMesh>::GetFreeComponentPointers() const {
    IPoint freeIndex;
    mTIndexStructure.GetFreeIndices(freeIndex);

    // count number of output pointers, and allocate output storage
    size_t sz=1;
    for(int i=0; i<mTIndexStructure.Nfree; ++i)
      sz*=mTensor.Dim();
    std::vector<double*> result(sz);

    // loop over all components

    // (1) initialize loop-counter with values of free indices
    IPoint curr(mTensor.Rank(),0);
    for(size_t digit=0; digit<mTensor.Rank(); ++digit) {
      if(freeIndex[digit]>=0) curr[digit]=freeIndex[digit];
    }
    size_t curr_idx=0;
    bool done=false;
    while(!done) {
      // get current pointer
      const DataMeshHelper Helper{};
      result[curr_idx]=
        Helper.template GetDataFromForwardDeclaredDM<DataMesh>(mTensor(curr));
      // increment
      ++curr_idx;
      for(size_t digit=0; digit<mTensor.Rank(); ++digit) {
	if(freeIndex[digit]<0) continue; // fixed index doesn't change
	++curr[digit];
	if(curr[digit]<mTensor.Dim()) {
	  // we've reached a digit which hasn't yet exceeded its range.
	  // stop incrementing and move on.
	  break;
	}
	// digit at end of range, reset to zero and continue in for-loop
	// with next digit.
	curr[digit]=0;
	if(digit==mTensor.Rank()-1)
	  done=true;
      }
    }
    REQUIRE(curr_idx==sz, "internal error");
    return result;
  }




  //================================================================
  // iBinaryOp<std::pair<...>, ..., DataMesh>::ExpandIndices()
  //================================================================
  template<class TIndexStructure1, class ...dSymm, class ...dIndices>
  const DataMesh& iBinaryOp<std::pair<TIndexStructure1,
				      TIndexStructure<TSymmetry<dSymm...>,
						      dIndices...>
				      >,
			    EmptyType,
			    DataMesh>::ExpandIndices() const {
    mTIndexStructure.GetAllIndices(idx);
    mdTIndexStructure.GetAllIndices(didx);
    return mTTensor(idx)(didx);
  }

  template<class TIndexStructure1, class ...dSymm, class ...dIndices>
  const DataMesh& iBinaryOp<std::pair<TIndexStructure1,
				      TIndexStructure<TSymmetry<dSymm...>,
						      dIndices...>
				      >,
			    EmptyType,
			    DataMesh>
  ::GetComponent(const std::initializer_list<int>& l,
                 const std::initializer_list<int>& m) const {
    REQUIRE(((l.size()==static_cast<size_t>(TIndexStructure1::NFree))
           &&(m.size()==static_cast<size_t>(TIndexStructure2::NFree))),
	      "Inconsistency when calling GetComponent\n"
              <<"l.size(): " << l.size()
              <<", static_cast<size_t>(TIndexStructure1::NFree): " <<
              static_cast<size_t>(TIndexStructure1::NFree)
              <<"\nm.size(): " << m.size()
              <<", static_cast<size_t>(TIndexStructure2::NFree): " <<
              static_cast<size_t>(TIndexStructure2::NFree));

      mTIndexStructure.ExpandFreeIndices(l.begin(), idx.begin() );
      mdTIndexStructure.ExpandFreeIndices(m.begin(), didx.begin() );
      return mTTensor(idx)(didx);
    }

  template<class TIndexStructure1, class ...dSymm, class ...dIndices>
  Tensor_GPUPointers<Tensor<Tensor<DataMesh> > > &
        iBinaryOp<std::pair<TIndexStructure1,
				      TIndexStructure<TSymmetry<dSymm...>,
						      dIndices...>
				      >,
			    EmptyType,
			    DataMesh>::GPUPointers() const{
    
    const int nfreeL = mTIndexStructure.GetNFree();
    const int nfreeR = mdTIndexStructure.GetNFree();
    
    IPoint idx_keyL(0);
    IPoint idx_keyR(0);
    mTIndexStructure.GetFreeIndices(idx_keyL);
    mdTIndexStructure.GetFreeIndices(idx_keyR);

    IPoint idx_key(ConcatVectors(idx_keyL,idx_keyR));
    return mTTensor.GPUPointers(nfreeL,idx_key,nfreeR);
  }

  //================================================================
  // NonConstiBinaryOp<std::pair<...>, ..., DataMesh>::ExpandIndices()
  //================================================================
  
  template<class TIndexStructure1, class ...dSymm, class ...dIndices>
  Tensor_GPUPointers<Tensor<Tensor<DataMesh> > >&
    NonConstiBinaryOp<std::pair<TIndexStructure1,
            TIndexStructure<TSymmetry<dSymm...>, dIndices...> >,
			    EmptyType,
			    DataMesh>::GPUPointers() {
    const int nfreeL = constBASE::mTIndexStructure.GetNFree();
    const int nfreeR = constBASE::mTIndexStructure.GetNFree();
    
    IPoint idx_keyL(0);
    IPoint idx_keyR(0);
    constBASE::mTIndexStructure.GetFreeIndices(idx_keyL);
    constBASE::mdTIndexStructure.GetFreeIndices(idx_keyR);

    IPoint idx_key(ConcatVectors(idx_keyL,idx_keyR));
    return NonConstTTensor().GPUPointersNonConst(nfreeL,idx_key,nfreeR);
  }
  
  template<class TIndexStructure1, class ...dSymm, class ...dIndices>
  DataMesh& NonConstiBinaryOp<std::pair<TIndexStructure1,
				      TIndexStructure<TSymmetry<dSymm...>,
						      dIndices...>
				      >,
			    EmptyType,
			    DataMesh>::ExpandIndices() {
    constBASE::mTIndexStructure.GetAllIndices(constBASE::idx);
    constBASE::mdTIndexStructure.GetAllIndices(constBASE::didx);
    return NonConstTTensor(constBASE::idx)(constBASE::didx);
  }

  template<class TIndexStructure1, class ...dSymm, class ...dIndices>
  DataMesh& NonConstiBinaryOp<std::pair<TIndexStructure1,
				      TIndexStructure<TSymmetry<dSymm...>,
						      dIndices...>
				      >,
			      EmptyType,DataMesh>
                          ::GetComponent(const std::initializer_list<int>& l,
                                         const std::initializer_list<int>& m)
  {
    REQUIRE(l.size()==static_cast<size_t>(TIndexStructure1::NFree),
	      "Inconsistency when calling GetComponent");
    REQUIRE(m.size()==static_cast<size_t>(TIndexStructure2::NFree),
	      "Inconsistency when calling GetComponent");

    constBASE::mTIndexStructure.ExpandFreeIndices(l.begin(),
						  constBASE::idx.begin() );
    constBASE::mdTIndexStructure.ExpandFreeIndices(m.begin(),
						   constBASE::didx.begin() );
    return NonConstTTensor(constBASE::idx)(constBASE::didx);
    }

  //================================================================
  // NonConstiBinaryOp::ExpandIndices(), ::CheckSymmetries()
  //================================================================

  template<class ...Symm, class ...Indices>
  DataMesh& NonConstiBinaryOp<TIndexStructure<TSymmetry<Symm...>, Indices...>,
			      EmptyType,
			      DataMesh>::ExpandIndices() {
    constBASE::mTIndexStructure.GetAllIndices(constBASE::idx);
    return NonConstTensor()(constBASE::idx);
  };

  template<class ...Symm, class ...Indices>
  Tensor_GPUPointers<Tensor<DataMesh> >&
    NonConstiBinaryOp<TIndexStructure<TSymmetry<Symm...>, Indices...>,
			    EmptyType,
			    DataMesh>::GPUPointers() {
    const int nfree = constBASE::mTIndexStructure.GetNFree();
    IPoint idx_key(0);
    constBASE::mTIndexStructure.GetFreeIndices(idx_key);
    return NonConstTensor().GPUPointersNonConst(nfree,idx_key);
  }


  template<class ...Symm, class ...Indices>
  DataMesh& NonConstiBinaryOp<TIndexStructure<TSymmetry<Symm...>, Indices...>,
			      EmptyType,
			      DataMesh>
  ::GetComponent(const std::initializer_list<int>& l) {
    REQUIRE(l.size()==static_cast<size_t>(TIndexStructure_t::NFree),
	    "Inconsistency when calling GetComponent");
    constBASE::mTIndexStructure.ExpandFreeIndices(l.begin(),
						  constBASE::idx.begin() );
    return NonConstTensor()(constBASE::idx);
  }


  template<class ...Symm, class ...Indices>
  void NonConstiBinaryOp<TIndexStructure<TSymmetry<Symm...>, Indices...>,
			 EmptyType,
			 DataMesh>::CheckSymmetries() const {
    const std::string tsym
      =SymmetryString<TSymmetry<Symm...>>(constBASE::mTensor.Rank());
    REQUIRE(constBASE::mTensor.Structure()==
	    TensorStructure(constBASE::mTensor.Dim(), tsym),
	    "ImplicitTensorLoops: Wrong symmetry specifier"
	    "on left-hand-side of assignment\n"
	    "  TensorStructure=" << constBASE::mTensor.Structure() << "\n"
	    "  TSymmetry<...> =   \"" << tsym << "\"\n"
	    "(it is required to "
	    "specify the symmetries in each assignment, because this allows "
	    "to fix the loop-ranges at **compile-time**, which is beneficial "
	    "for GPU coding)"
	    );
  }
  
  //Resize the DMs in the iBOp to match the size of rhs
  template<class ...Symm, class ...Indices>
  template<class DM_t>
  void NonConstiBinaryOp<TIndexStructure<TSymmetry<Symm...>, Indices...>,
                         EmptyType,
                         DataMesh>::
   CheckExtentsAndResizeToMatch(const DM_t& rhs){
    //DataMesh* DM = static_cast<DataMesh*>(rhs);
    IPoint Extents = rhs->Extents();
    DoResizeWork<DataMesh>(Extents);
  }
  
  //Resize the DMs in the iBOp to match the size of rhs
  template<class ...Symm, class ...Indices>
  template<class L, class O, class R>
  void NonConstiBinaryOp<TIndexStructure<TSymmetry<Symm...>, Indices...>,
                         EmptyType,
                         DataMesh>::
    CheckExtentsAndResizeToMatch(const iBinaryOp<L,O,R>& rhs){
      IPoint Extents = ExtentsFrom_iBinaryOp(rhs);
      DoResizeWork<DataMesh>(Extents);
  }
   
  //Resize the DMs in the iBOp to match the size of rhs
  template<class ...Symm, class ...Indices>
  template<class DM_t>
  void NonConstiBinaryOp<TIndexStructure<TSymmetry<Symm...>, Indices...>,
                         EmptyType,
                         DataMesh>::
    DoResizeWork(const IPoint& Extents){
    //assume all elements have same extents in production code 
    Tensor<DM_t>& theTensor = NonConstTensor();
    if(theTensor.begin()->Extents()!= Extents){
      theTensor = Tensor<DM_t>(theTensor.Structure(),DM_t(Mesh(Extents)));
      
      //for ( auto & thisDM : NonConstTensor() ){
      //  thisDM = DM_t(Mesh(Extents)); 
      //}
    }
    #ifdef DEBUG
    //if we're debugging, make sure
    else{
      for (auto & DM: NonConstTensor() ){
        ASSERT(DM.Extents() == Extents, 
            "lhs did not have uniform extents across indices!");
      }
    }
    #endif
  }

}
#include "ExtentsFrom_iBinaryOp.hpp"
#endif
