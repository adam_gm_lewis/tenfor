

// convenience header to #include the needed files for implicit tensor
// loops.
//
// here are the headers that must be included at the *end* of Tensor.hpp, 
// because they require a definition of Tensor<T>.

//#include "TLoopApply-impl.hpp"
#include "iBinaryOp-impl.hpp"

