//============================================================================
/// \file 
/// Defines classes and macros useful for writing tests.
#ifndef UtilsForTesting_hpp
#define UtilsForTesting_hpp
//============================================================================
#include "Finite.hpp"
//#include "std::cerr.hpp"  // for ProcID
//============================================================================
#include <string>
#include <cstdlib>
#include <iostream>
#include <cstring> // for 'strcpy'
#include <type_traits>
//============================================================================
/// \name Testing-macros.  
/// These macros test what their name says.  If the test fails, they
/// output a message.  Furthermore, a running count is kept about the
/// number of tests passed and failed.  In order to test a certain C++
/// type, the function \c MaximalDifference must be overloaded for
/// that type.
//@{
#define IS_ZERO(x, msg) \
  { double d__qt=UtilsForTesting::Difference(x,0.); \
    if(d__qt>0.0) { \
      UtilsForTesting::LogFailedTest(); \
      std::cerr << __FILE__ << "(" << __LINE__ << ") " \
                << "IS_ZERO failed [" << d__qt << "] " << msg << std::endl; } }

#define IS_TRUE(x, msg) \
  { UtilsForTesting::IncrementNumberOfTests(); \
    if(!(x)) { \
      UtilsForTesting::LogFailedTest(); \
      std::cerr << __FILE__ << "(" << __LINE__ << ") " \
            << "IS_TRUE failed " << msg << std::endl; } }

#define IS_ZERO_EPS(x, msg) \
  { double d__qt=UtilsForTesting::Difference(x, 0.0); \
    if(d__qt>UtilsForTesting::Eps()) { \
      UtilsForTesting::LogFailedTest(); \
      std::cerr << __FILE__ << "(" << __LINE__ << ") "\
                << "IS_ZERO_EPS failed [" << d__qt << "] " << msg \
                << std::endl;} }

#define IS_EQUAL(x, y,msg) \
 { double d__qt=UtilsForTesting::Difference(x,y); \
    if(d__qt>0.0) { \
      UtilsForTesting::LogFailedTest(); \
      std::cerr << __FILE__ << "(" << __LINE__ << ") "\
                << "IS_EQUAL failed  [" << d__qt << "] " << msg << std::endl;}}

#define IS_EQUAL_EPS(x,y, msg) \
 { double d__qt=UtilsForTesting::Difference(x,y); \
    if(d__qt>UtilsForTesting::Eps()) { \
      UtilsForTesting::LogFailedTest(); \
      std::cerr << __FILE__ << "(" << __LINE__ << ") " \
                << "IS_EQUAL_EPS failed [" << d__qt << "] " << msg \
                << std::endl; }}
//@}

//============================================================================

/// Collects results from tests within a test-executable.
class UtilsForTesting {
public:
  static int NumberOfTests();
  static int NumberOfTestsPassed();
  static int NumberOfTestsFailed();
  static double MaximumError();
  static double Eps();
  static void SetEps(const double NewEps);
  static void ResetMaximumErrorToZero();
  template<class LHS, class RHS> 
  static double Difference(const LHS& lhs, const RHS& rhs);
  static void LogFailedTest() { ++mNumberOfTestsFailed; }
  static void IncrementNumberOfTests()  { ++mNumberOfTests; }
private:
  static int mNumberOfTests;
  static int mNumberOfTestsFailed;
  static double mMaximumError;
  static double mEps;
};

//================================================================
// DEFINITIONS OF MaximalDifference
//
// Each type to be compared must have MaximalDifference-functions.
// Here, some general purpose ones are predefined. These MUST
// return a non-negative number, and 0 if arguments are equal.
//================================================================

// define this in the cpp file as it uses fabs from <cmath>
double MaximalDifference(const long double lhs, const long double rhs);

template <class LHS, class RHS,
          class =
          typename std::enable_if<std::is_arithmetic<LHS>::value &&
                                  std::is_arithmetic<RHS>::value>::type>
double MaximalDifference(const LHS lhs, const RHS rhs) {
  return MaximalDifference(static_cast<long double>(lhs),
                           static_cast<long double>(rhs));
}

// so we can compare strings with IS_EQUAL
// strcmp() returns 0 if the arguments are equal
inline double MaximalDifference(const char* lhs, const char* rhs) {
  return strcmp(lhs, rhs)==0 ? 0 : 1;
}
inline double MaximalDifference(const std::string& lhs, const char* rhs) {
  return strcmp(lhs.c_str(), rhs)==0 ? 0 : 1;
}
inline double MaximalDifference(const char* lhs, std::string& rhs) {
  return strcmp(lhs, rhs.c_str())==0 ? 0 : 1;
}
inline double MaximalDifference(const std::string& lhs, 
				const std::string& rhs) {
  return lhs==rhs ? 0 : 1;
}

// Containers
// note: template types are not restricted to have a certain number of
// template arguments, but they must have a const_iterator.  The third
// argument is unused and should not be specified.  It only exists to
// prevent the compiler from choosing this overload of
// MaximalDifference for types that don't have iterators.
template<class T>
double MaximalDifference(const T& lhs, const double rhs,
                         typename T::const_iterator =
                         typename T::const_iterator())
{
  double MaximumError = 0.;
  typename T::const_iterator it=lhs.begin();
  for(; it!=lhs.end(); ++it) {
    const double Error = MaximalDifference(*it, rhs);
    if(Finite(MaximumError) && (!Finite(Error) || Error>MaximumError)) {
      MaximumError = Error;
    }
  }
  return MaximumError;
}

template<class T>
double MaximalDifference(const T& lhs, const T& rhs,
                         typename T::const_iterator =
                         typename T::const_iterator())
{
  double MaximumError = 0.;
  typename T::const_iterator it1=lhs.begin();
  typename T::const_iterator it2=rhs.begin();
  for(; it1!=lhs.end() && it2!=rhs.end(); ++it1, ++it2) {
    const double Error = MaximalDifference(*it1, *it2);
    if(Finite(MaximumError) && (!Finite(Error) || Error>MaximumError)) {
      MaximumError = Error;
    }
  }
  if(it1 != lhs.end() || it2 != rhs.end()) 
    return MyLimits::NaN(); // Container lengths differ!
  return MaximumError;
}

// Last attempt: try operator==.  Need to make sure this is considered
// a worse match than any of the above versions when they apply.
// Easiets way to do that is to make sure it doesn't match the
// arithmetic cases at all.
template <class LHS, class RHS,
          class =
          typename std::enable_if<!(std::is_arithmetic<LHS>::value &&
                                    std::is_arithmetic<RHS>::value)>::type>
double MaximalDifference(const LHS& lhs, const RHS& rhs) {
  return lhs == rhs ? 0 : 1;
}

// note: could be non-template, if the macros called MaximalDifference
// themselves.
template<class LHS, class RHS> 
double UtilsForTesting::Difference(const LHS& lhs, const RHS& rhs)
{
  double Error = MaximalDifference(lhs, rhs);
  if(Finite(mMaximumError) && (!Finite(Error) || Error>mMaximumError)) {
    mMaximumError = Error;
  }
  if(!Finite(Error)) {
    std::cerr << "FOLLOWING TEST NOT FINITE -> ";
    Error=1e300; 
  }
  ++mNumberOfTests;
  return Error;
}
//============================================================================

#endif  // UtilsForTesting_hpp
