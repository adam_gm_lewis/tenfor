/// \file
/// This file holds a threadsafe contianer class of variable size, but you
///  should use PerThread if at all possible

#ifndef LazyPerThread_hpp
#define LazyPerThread_hpp

#include "ThreadNumber.hpp"
//#include "Utils/MyContainers/std::list.hpp"

#include <mutex>
#include <list>

/// This will hold as many copies of an object as there are calling threads.
///  If the calling thread is index m, and we have N objects, if m+1 > N, this
///  will create as many objects as needed until we have m+1 = N objects.
///  Due to this check every time we call the object, you should try to use
///  PerThread, as it will be faster!
template <class Object>
class LazyPerThread {
public:
  /// This will hold as many copies as necessary of the object, using the
  ///  default constructor
  ///  Due to this check every time we call the object, you should try to use
  ///  PerThread, as it will be faster!
  LazyPerThread<Object>();
  /// This will hold as many copies as necessary of the object, using the
  ///  copy constructor
  ///  Due to this check every time we call the object, you should try to use
  ///  PerThread, as it will be faster!
  LazyPerThread<Object>(const Object& copy);
  ~LazyPerThread<Object>();

  /// Get a reference to the object associated with the calling thread.
  ///  If the calling thread requires new objects to be created, the objects
  ///  will either have the default constructor value, or the copy constructor
  ///  value, depending on which constructor you used.
  Object& GetReference() const;

  /// Get the number of objects this container currently holds
  int Size() const { return mNumKnownThreads; }

  /// Get the object associated with the thread with number 'index'
  Object& operator[] (const int index) const;
  Object& operator() () const { return this->GetReference(); }
private:
  void AddCopiesPerThread(const int kCurrentThreadNum) const;

  mutable int mNumKnownThreads;
  const bool mkUseCopyConstructor;
  const Object mkCopy;
  mutable std::mutex mLock;
  mutable std::list<Object> mList;
};

template <class Object>
LazyPerThread<Object>::LazyPerThread() :
  mNumKnownThreads(0),
  mkUseCopyConstructor(false),
  mkCopy()
{
  AddCopiesPerThread(0);
}

template <class Object>
LazyPerThread<Object>::LazyPerThread(const Object& kCopy) :
  mNumKnownThreads(0),
  mkUseCopyConstructor(true),
  mkCopy(kCopy)
{
  AddCopiesPerThread(0);
}

template <class Object>
LazyPerThread<Object>::~LazyPerThread<Object>() { }

template <class Object>
Object& LazyPerThread<Object>::GetReference() const
{
  const int kCurrentThreadNum = GetThisThreadNum();
  if (kCurrentThreadNum + 1 > mNumKnownThreads)
  {
    //Need to update the list to have one copy per thread
    AddCopiesPerThread(kCurrentThreadNum);
  }

  return (*this)[kCurrentThreadNum];
}

template <class Object>
Object& LazyPerThread<Object>::operator[] (const int index) const
{
  ASSERT(index < mNumKnownThreads,
         "Requesting index larger than the number of known threads");
  typename std::list<Object>::iterator it(mList.begin());
  for (int i = 0; i < index; i++) ++it;

  return *it;
}

template <class Object>
void LazyPerThread<Object>::
       AddCopiesPerThread(const int kCurrentThreadNum) const
{
  std::lock_guard<std::mutex> scopedLocker(mLock);
  for ( ;
       mNumKnownThreads <= kCurrentThreadNum;
       mNumKnownThreads++)
  {
    if (mkUseCopyConstructor)
    {
      mList.emplace_back(mkCopy);
    }
    else
    {
      mList.emplace_back();
    }
  }
}

#endif
