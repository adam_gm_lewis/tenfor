#ifndef iBinaryOp_hpp
#define iBinaryOp_hpp

#include "TIndexStructure.hpp"
#include "TensorStructure.hpp"
#include "TAssignmentHelpers.hpp"
#include "TLoopApply.hpp"

#include <vector>

//#ifndef CODE_WRITER
//  #include "Utils/Tensor/AutoExpressions/AutoExpressions.hpp"
//#endif

template<class T>
class Tensor;

template<class X>
class Tensor_GPUPointers;
namespace ImplicitTensorLoops {

  template<class LHS, class OP, class RHS>
  struct iBinaryOp;
  //================================================================
  // iBinaryOp specializations for recursive expression trees
  //================================================================

  //---------------------------------------
  // d*iBinaryOp,  iBinaryOp*d, iBinaryOp/d
  //---------------------------------------
  template<class L, class Op, class R>
  struct iBinaryOp<double,MultOp,iBinaryOp<L,Op,R> > {
    typedef BinaryOp<double, MultOp,
		     typename iBinaryOp<L,Op,R>::ExpandIndices_t>
      ExpandIndices_t;
    typedef typename iBinaryOp<L,Op,R>::TIndexSet_t TIndexSet_t;
    iBinaryOp(const double& d, const iBinaryOp<L,Op,R>& op):
      lhs(d), rhs(op) { };

    const double& lhs;
    const iBinaryOp<L,Op,R>& rhs;
  };
  template<class L, class Op, class R>
  struct iBinaryOp<iBinaryOp<L,Op,R>,MultOp,double> {
    typedef BinaryOp<typename iBinaryOp<L,Op,R>::ExpandIndices_t,
		     MultOp,
		     double> ExpandIndices_t;
    typedef typename iBinaryOp<L,Op,R>::TIndexSet_t TIndexSet_t;
    iBinaryOp(const iBinaryOp<L,Op,R>& op, const double& d):
      lhs(op), rhs(d) { };
    const iBinaryOp<L,Op,R>& lhs;
    const double& rhs;
  };
  template<class L, class Op, class R>
  struct iBinaryOp<iBinaryOp<L,Op,R>,DivOp,double> {
    typedef BinaryOp<typename iBinaryOp<L,Op,R>::ExpandIndices_t,
		     DivOp,
		     double> ExpandIndices_t;
    typedef typename iBinaryOp<L,Op,R>::TIndexSet_t TIndexSet_t;
    iBinaryOp(const iBinaryOp<L,Op,R>& op, const double& d):
      lhs(op), rhs(d) { };
    const iBinaryOp<L,Op,R>& lhs;
    const double& rhs;
  };
  // d/iBinaryOp meaningless for tensor-calculus


  //-----------------------------------------------------------
  // DataMesh*iBinaryOp,  iBinaryOp*DataMesh iBinaryOp/DataMesh
  //-----------------------------------------------------------
  template<class L, class Op, class R>
  struct iBinaryOp<DataMesh,MultOp,iBinaryOp<L,Op,R> > {
    typedef BinaryOp<DataMesh, MultOp,
		     typename iBinaryOp<L,Op,R>::ExpandIndices_t>
      ExpandIndices_t;
    typedef typename iBinaryOp<L,Op,R>::TIndexSet_t TIndexSet_t;
    iBinaryOp(const DataMesh& dm, const iBinaryOp<L,Op,R>& op):
      lhs(dm), rhs(op) { };
    const DataMesh& lhs;
    const iBinaryOp<L,Op,R>& rhs;
  };
  template<class L, class Op, class R>
  struct iBinaryOp<iBinaryOp<L,Op,R>,MultOp,DataMesh > {
    typedef BinaryOp<typename iBinaryOp<L,Op,R>::ExpandIndices_t,
		     MultOp,
		     DataMesh> ExpandIndices_t;
    typedef typename iBinaryOp<L,Op,R>::TIndexSet_t TIndexSet_t;
    iBinaryOp(const iBinaryOp<L,Op,R>& op, const DataMesh& dm):
      lhs(op), rhs(dm) { };
    const iBinaryOp<L,Op,R>& lhs;
    const DataMesh& rhs;
  };
  template<class L, class Op, class R>
  struct iBinaryOp<iBinaryOp<L,Op,R>,DivOp,DataMesh > {
    typedef BinaryOp<typename iBinaryOp<L,Op,R>::ExpandIndices_t,
		     DivOp,
		     DataMesh> ExpandIndices_t;
  typedef typename iBinaryOp<L,Op,R>::TIndexSet_t TIndexSet_t;
    iBinaryOp(const iBinaryOp<L,Op,R>& op, const DataMesh& dm):
      lhs(op), rhs(dm) { };
    const iBinaryOp<L,Op,R>& lhs;
    const DataMesh& rhs;
  };
  // DM/iOp meaningless for tensor-calculus


  // -----------------------------------------------------------
  // BinaryOp*iBinaryOp,  iBinaryOp*BinaryOp, iBinaryOp/BinaryOp
  // -----------------------------------------------------------
  template<class L1, class Op1, class R1,
	   class L2, class Op2, class R2>
  struct iBinaryOp<BinaryOp<L1,Op1,R1>,MultOp,iBinaryOp<L2,Op2,R2> > {
    typedef BinaryOp<BinaryOp<L1,Op1,R1>, MultOp,
		     typename iBinaryOp<L2,Op2,R2>::ExpandIndices_t>
      ExpandIndices_t;
    typedef typename iBinaryOp<L2,Op2,R2>::TIndexSet_t TIndexSet_t;
    iBinaryOp(const BinaryOp<L1,Op1,R1>& l, const iBinaryOp<L2,Op2,R2>& r):
      lhs(l), rhs(r) { };
    const  BinaryOp<L1,Op1,R1>& lhs;
    const iBinaryOp<L2,Op2,R2>& rhs;
  };
  template<class L1, class Op1, class R1,
	   class L2, class Op2, class R2>
  struct iBinaryOp<iBinaryOp<L1,Op1,R1>,MultOp,BinaryOp<L2,Op2,R2> > {
    typedef BinaryOp<typename
		     iBinaryOp<L1,Op1,R1>::ExpandIndices_t,
		     MultOp,
		     BinaryOp<L2,Op2,R2> > ExpandIndices_t;
    typedef typename iBinaryOp<L1,Op1,R1>::TIndexSet_t TIndexSet_t;
    iBinaryOp(const iBinaryOp<L1,Op1,R1>& l, const BinaryOp<L2,Op2,R2>& r):
      lhs(l), rhs(r) { };
    const iBinaryOp<L1,Op1,R1>& lhs;
    const  BinaryOp<L2,Op2,R2>& rhs;
  };
  template<class L1, class Op1, class R1,
	   class L2, class Op2, class R2>
  struct iBinaryOp<iBinaryOp<L1,Op1,R1>,DivOp,BinaryOp<L2,Op2,R2> > {
    typedef BinaryOp<typename
		     iBinaryOp<L1,Op1,R1>::ExpandIndices_t,
		     DivOp,
		     BinaryOp<L2,Op2,R2> > ExpandIndices_t;
    typedef typename iBinaryOp<L1,Op1,R1>::TIndexSet_t TIndexSet_t;
    iBinaryOp(const iBinaryOp<L1,Op1,R1>& l, const BinaryOp<L2,Op2,R2>& r):
      lhs(l), rhs(r) { };
    const iBinaryOp<L1,Op1,R1>& lhs;
    const  BinaryOp<L2,Op2,R2>& rhs;
  };
  // Op/iOp meaningless for tensor-calculus


  // -------------------
  // iBinaryOp*iBinaryOp
  // -------------------
  template<class L1, class Op1, class R1,
	   class L2, class Op2, class R2>
  struct iBinaryOp<iBinaryOp<L1,Op1,R1>, MultOp, iBinaryOp<L2,Op2,R2> > {
    typedef BinaryOp<typename iBinaryOp<L1,Op1,R1>::ExpandIndices_t,
		     MultOp,
		     typename iBinaryOp<L2,Op2,R2>::ExpandIndices_t>
        ExpandIndices_t;
      typedef typename UnionOfTIndexSets
          < typename iBinaryOp<L1,Op1,R1>::TIndexSet_t,
	    typename iBinaryOp<L2,Op2,R2>::TIndexSet_t
	    >::type TIndexSet_t;
    iBinaryOp(const iBinaryOp<L1,Op1,R1>& l, const iBinaryOp<L2,Op2,R2>& r):
      lhs(l), rhs(r) { };
    const iBinaryOp<L1,Op1,R1>& lhs;
    const iBinaryOp<L2,Op2,R2>& rhs;
  };
  // iOp/iOp meaningless for tensor-calculus


  // ----------------------------------------
  // iBinaryOp+iBinaryOp, iBinaryOp-iBinaryOp
  // ----------------------------------------
  // Tensor-Calculus allows only same free indices on both sides
  // of a + or a -.  Therefore, {d, DM, BinaryOp}+- iBinaryOp is
  // not defined.
  template<class L1, class Op1, class R1,
	   class L2, class Op2, class R2>
  struct iBinaryOp<iBinaryOp<L1,Op1,R1>,AddOp,iBinaryOp<L2,Op2,R2> > {
    typedef
     BinaryOp<typename iBinaryOp<L1,Op1,R1>::ExpandIndices_t,
	      AddOp,
	      typename iBinaryOp<L2,Op2,R2>::ExpandIndices_t> ExpandIndices_t;
    typedef typename iBinaryOp<L1,Op1,R1>::TIndexSet_t TIndexSet_t;

    iBinaryOp(const iBinaryOp<L1,Op1,R1>& l, const iBinaryOp<L2,Op2,R2>& r):
      lhs(l), rhs(r) { };
    const iBinaryOp<L1,Op1,R1>& lhs;
    const iBinaryOp<L2,Op2,R2>& rhs;
  };
  template<class L1, class Op1, class R1,
	   class L2, class Op2, class R2>
  struct iBinaryOp<iBinaryOp<L1,Op1,R1>,SubOp,iBinaryOp<L2,Op2,R2> > {
    typedef
     BinaryOp<typename iBinaryOp<L1,Op1,R1>::ExpandIndices_t,
	      SubOp,
	      typename iBinaryOp<L2,Op2,R2>::ExpandIndices_t> ExpandIndices_t;
    typedef typename iBinaryOp<L1,Op1,R1>::TIndexSet_t TIndexSet_t;

    iBinaryOp(const iBinaryOp<L1,Op1,R1>& l, const iBinaryOp<L2,Op2,R2>& r):
      lhs(l), rhs(r) { };
    const iBinaryOp<L1,Op1,R1>& lhs;
    const iBinaryOp<L2,Op2,R2>& rhs;
  };

  // ----------
  // -iBinaryOp
  // ----------
  template<class L2, class Op2, class R2>
  struct iBinaryOp<EmptyType,negateOp,iBinaryOp<L2,Op2,R2> > {
    typedef
     BinaryOp<EmptyType, negateOp,
	      typename iBinaryOp<L2,Op2,R2>::ExpandIndices_t> ExpandIndices_t;
    typedef typename iBinaryOp<L2,Op2,R2>::TIndexSet_t TIndexSet_t;
    iBinaryOp(const iBinaryOp<L2,Op2,R2>& r):
      rhs(r) { };
    const iBinaryOp<L2,Op2,R2>& rhs;
  };
  // +iBinaryOp is replaced by BinaryOp in operator+(const iBinaryOp&)



  //================================================================
  // Leaf Specializations
  // ================================================================


  // Specialization 1: T<DataMesh>
  //
  // The first template argument is 'overloaded' to represent
  // the TIndexStructure of the Tensor
  //
  template<class ...Symm, class ...Indices>
  struct iBinaryOp<TIndexStructure<TSymmetry<Symm...>, Indices...>,
		   EmptyType,  DataMesh> {
    typedef TIndexStructure<TSymmetry<Symm...>, Indices...> TIndexStructure_t;

    typedef typename TIndexStructure_t::TIndexSet_t TIndexSet_t;
    typedef DataMesh ExpandIndices_t;

    iBinaryOp(const Tensor<DataMesh>& tensor, Indices... indices):
      mTensor(tensor),
      mTIndexStructure(indices...),
      idx(TIndexStructure_t::Rank)
    { };

    const DataMesh& ExpandIndices() const;
    const Tensor<DataMesh>& TheTensor() const { return mTensor; }
    void operator=(const iBinaryOp<TIndexStructure_t,
		   EmptyType, DataMesh>& op)=delete;

    

    // TCodeWriter Interface

    // 'freeIndices' contains a concrete set of free indices of this
    // TIndexStructure, expand to full set of indices for the Tensor
    // (i.e. add offsets and insert bound indices) and return
    // corresponding component.
    //
    // This function is called from TCodeWriter-written code to initialize
    // arrays of pointers.
    const DataMesh& GetComponent(const std::initializer_list<int>& l) const;
    Tensor_GPUPointers<Tensor<DataMesh> >& GPUPointers() const;

    // return an index that unambiguously identifies the precise set
    // of fixed indices.  This index can be used to store the data
    // returned by GetFreeComponentPointers in a data-structure that
    // is longer lived than the iBinaryOp.
    int GetFreeComponentPointersIdx() const;

    // returns an array of pointers into the data stored inside the
    // Tensor<DataMesh>.  The array has size Tensor.Dim()^NFree
    // array[a+Dim*(b+Dim*c+...)] corresponds to the tenor component
    // where the first implicit index takes value a, the second
    // implicit index takes value b, etc.  During this process, all
    // **fixed** indices are kept at whatever their fixed values are.
    std::vector<double*> GetFreeComponentPointers() const;


  // Returns the value of the N-th index (0-based counting).
  // If that index is a fixed index, that value is returned.
  // If it is a  TIndex, the current value of the TIndex is returned.
  template<int N>
    int GetNthIndex() const {
      return mTIndexStructure.template GetNthIndex<N>();
    }
    const TIndexStructure_t& TStructure() const { return mTIndexStructure; }
  protected: // the NonConstiBinaryOp needs access
    const Tensor<DataMesh>& mTensor;
    TIndexStructure_t mTIndexStructure;
    mutable IPoint idx; // used for indexing
  };



  // Specialization 2: partially indexed T<T<DataMesh>>
  //
  // This is an indermediate class that appears as return type of
  // Tensor<Tensor<DataMesh>>::operator()(const Indices...) const
  //
  // The only purpose of this class is further indexing to an object
  // on the "DataMesh-level".
  //
  template<class ...Symm, class ...Indices>
  struct iBinaryOp<TIndexStructure<TSymmetry<Symm...>, Indices...>,
		   EmptyType,  Tensor<DataMesh>> {
    typedef TIndexStructure<TSymmetry<Symm...>, Indices...> TIndexStructure1;

    iBinaryOp(const Tensor<Tensor<DataMesh>>& tt, Indices... indices):
      mTIndexStructure(indices...),
      mTTensor(tt)
      { };

    // operator()
    template<class ...dIndices>
      iBinaryOp<std::pair<TIndexStructure1,
    			  TIndexStructure<TSymmetry<>, dIndices...> >,
    		EmptyType,  DataMesh>
      operator()(dIndices... dindices) const {
      return iBinaryOp<std::pair<TIndexStructure1,
				 TIndexStructure<TSymmetry<>,
						 dIndices...> >,
		       EmptyType,  DataMesh
		       >
	(mTTensor, mTIndexStructure, dindices...);
    };

  private:
    const TIndexStructure1 mTIndexStructure;
    const Tensor<Tensor<DataMesh>>& mTTensor;
  };

  // Specialization 3: T<T<DataMesh>>
  //
  // The first template argument is a std::pair, representing the
  // TIndexStructure of the outer Tensor *and* the inner Tensor
  //
  template<class TIndexStructure1, class ...dSymm, class ...dIndices>
  struct iBinaryOp<std::pair<TIndexStructure1,
			     TIndexStructure<TSymmetry<dSymm...>, dIndices...>
			     >,
		   EmptyType,  DataMesh> {
    typedef TIndexStructure<TSymmetry<dSymm...>, dIndices...> TIndexStructure2;

    // union of "outer" and "inner" indices
    typedef typename
      UnionOfTIndexSets<typename TIndexStructure1::TIndexSet_t,
			typename TIndexStructure2::TIndexSet_t
			>::type TIndexSet_t;

    typedef DataMesh ExpandIndices_t;

    iBinaryOp(const Tensor<Tensor<DataMesh>>& ttensor,
	      const TIndexStructure1& indices,
	      dIndices... dindices):
      mTIndexStructure(indices),
      mdTIndexStructure(dindices...),
      mTTensor(ttensor),
      idx(TIndexStructure1::Rank),
      didx(TIndexStructure2::Rank)
    { };

    Tensor_GPUPointers<Tensor<Tensor<DataMesh> > > & GPUPointers() const;
    const Tensor<Tensor<DataMesh> >& TheTTensor() const {return mTTensor;}
    const DataMesh& ExpandIndices() const;
    const DataMesh& GetComponent(const std::initializer_list<int>& l,
                 const std::initializer_list<int>& m) const;
    //include typedef expanding to full Nfree/Range?
  protected:
    const TIndexStructure1 mTIndexStructure;
    const TIndexStructure2 mdTIndexStructure;
    const Tensor<Tensor<DataMesh>>& mTTensor;
    mutable IPoint idx, didx;
  };


  //================================================================
  // LHS use: NonConstiBinaryOp
  //================================================================
  //
  // NonConstiBinaryOp -- for use on left-hand-side.  Can be downcast
  // to iBinaryOp, if indexing a non-const Tensor on the right-hand-side
  // returns a NonConstiBinaryOp.
  //

  template<class LHS, class OP, class RHS>
  struct NonConstiBinaryOp;

  template<class ...Symm, class ...Indices>
  struct NonConstiBinaryOp<TIndexStructure<TSymmetry<Symm...>, Indices...>,
			   EmptyType,  DataMesh>:
    public iBinaryOp<TIndexStructure<TSymmetry<Symm...>, Indices...>,
		     EmptyType,  DataMesh> {

    // Forward members from iBinaryOp
    typedef iBinaryOp<TIndexStructure<TSymmetry<Symm...>, Indices...>,
			      EmptyType,  DataMesh> constBASE;
    typedef typename constBASE::TIndexStructure_t TIndexStructure_t;
    typedef typename constBASE::TIndexSet_t TIndexSet_t;
    NonConstiBinaryOp(Tensor<DataMesh>& t, Indices... indices):
      constBASE(t, indices...) { };

    static std::string CPlusPlusName() {
      return "NonConstiBinaryOp<"+
          TIndexStructure_t::CPlusPlusName()+",EmptyType,DataMesh>";
    }

    //--------------------------------
    // Sanity checks
    //--------------------------------

    // check that indices on left-hand-side are distinct
    static void CheckUniqueIndices(){
      static_assert(TIndexStructure_t::IndicesAreUnique,
		    "repeated indices on left-hand-side of implict "
		    "Tensor-assignment");
    }

    // check that indices on LHS and RHS match
    template<class L, class Op, class R>
    static void CheckIndexEquality(const iBinaryOp<L,Op,R>& ) {
      typedef typename iBinaryOp<L,Op,R>::TIndexSet_t rhs_TIndexSet;
      static_assert(std::is_same<TIndexSet_t, rhs_TIndexSet>::value,
		    "Indices on left and right side of assignment must match");
    }

    // TSymmetry consistent with Tensor?
    void CheckSymmetries() const;

    Tensor_GPUPointers<Tensor<DataMesh> >& GPUPointers();

    //--------------------------------
    // iteration over indices
    //--------------------------------
    void Reset() {    constBASE::mTIndexStructure.Reset();  }
    void operator++() {  ++constBASE::mTIndexStructure; }
    explicit operator bool() {
      return constBASE::mTIndexStructure.operator bool(); }
    DataMesh& ExpandIndices();

    // the only way to create a NonConstiBinaryOp, is to pass in a
    // non-const Tensor<DataMesh>&.  This reference is stored as a
    // const Tensor<DataMesh>&, but since it originated from a non-const
    // &, it is safe to cast away the const.
    Tensor<DataMesh>& NonConstTensor() {
      return const_cast<Tensor<DataMesh>&>(constBASE::mTensor);
    }

    //Resize the DMs in the iBOp to match the size of rhs
    template<class DM_t>
    void CheckExtentsAndResizeToMatch(const DM_t& rhs);
    
    template<class L, class O, class R>
    void CheckExtentsAndResizeToMatch(const iBinaryOp<L,O,R>& rhs);
  
    template<class DM_t>
    void DoResizeWork(const IPoint& Extents); 
    
    DataMesh& GetComponent(const std::initializer_list<int>& l);

    //--------------------------------
    // assignment operators
    //--------------------------------

    // assignment-operator is never a template.  Define it explicitly,
    // to avoid automatically defined assignment operator for A(i_)=B(i_)
    void operator=(const double d) {
      TLoopApply(*this, TSetEqualOp(), d);
    }
    void operator=(const DataMesh& dm) {
      CheckExtentsAndResizeToMatch(dm);
      TLoopApply(*this, TSetEqualOp(), dm);
    }
    
    template<class L, class O, class R>
    void operator=(const iBinaryOp<L,O,R>& op) {
      CheckIndexEquality(op);
      CheckExtentsAndResizeToMatch(op);
      TLoopApply(*this, TSetEqualOp(), op);
    }
    
    void operator=(const NonConstiBinaryOp<TIndexStructure_t,
		   EmptyType, DataMesh>& op) {
      (*this) = static_cast<const constBASE&>(op);
      //TLoopApply(*this, TSetEqualOp(),
	//	 static_cast<const constBASE&>(op));
    }
    
    

    
    
    
    void operator+=(const double d) {
      TLoopApply(*this, TPlusEqualOp(), d);
    }
    void operator+=(const DataMesh& dm) {
      TLoopApply(*this, TPlusEqualOp(), dm);
    }

    template<class L, class O, class R>
    void operator+=(const iBinaryOp<L,O,R>& op) {
      CheckIndexEquality(op);
      TLoopApply(*this, TPlusEqualOp(), op);
    }

    void operator-=(const double d) {
      TLoopApply(*this, TMinusEqualOp(), d);
    }
    void operator-=(const DataMesh& dm) {
      TLoopApply(*this, TMinusEqualOp(), dm);
    }

    template<class L, class O, class R>
    void operator-=(const iBinaryOp<L,O,R>& op) {
      CheckIndexEquality(op);
      TLoopApply(*this, TMinusEqualOp(), op);
    }


    void operator*=(const double d) {
      TLoopApply(*this, TMultEqualOp(), d);
    }
    void operator*=(const DataMesh& dm) {
      TLoopApply(*this, TMultEqualOp(), dm);
    }

    void operator/=(const double d) {
      TLoopApply(*this, TDivEqualOp(), d);
    }
    void operator/=(const DataMesh& dm) {
      TLoopApply(*this, TDivEqualOp(), dm);
    }

  };




  // Specialization for T<T<DM> >
  template<class TIndexStructure1, class ...dSymm, class ...dIndices>
  struct NonConstiBinaryOp<std::pair<TIndexStructure1,
				     TIndexStructure<TSymmetry<dSymm...>,
						     dIndices...>
				     >,
                              EmptyType,
                              DataMesh>:
      public iBinaryOp<std::pair<TIndexStructure1,
				 TIndexStructure<TSymmetry<dSymm...>,
						 dIndices...>
                                 >,
		       EmptyType,
		       DataMesh>{

      // Forward members from iBinaryOp
      typedef iBinaryOp<std::pair<TIndexStructure1,
                                   TIndexStructure<TSymmetry<dSymm...>,
                                                        dIndices...>
                                 >, EmptyType, DataMesh> constBASE;
      //typedef typename constBASE::TIndexStructure1 TIndexStructure1;
      typedef typename constBASE::TIndexStructure2 TIndexStructure2;
      typedef typename constBASE::TIndexSet_t TIndexSet_t;
      NonConstiBinaryOp(Tensor<Tensor<DataMesh> >& Tt,
                        const TIndexStructure1& indices,
                        dIndices... dindices):
        constBASE(Tt, indices, dindices...) { };

      static std::string CPlusPlusName() {
        return "NonConstiBinaryOp<"+TIndexStructure1::CPlusPlusName()+
                                    TIndexStructure2::CPlusPlusName()+
          ",EmptyType,DataMesh>";
      }

    //--------------------------------
    // Sanity checks
    //--------------------------------

    // check that indices on left-hand-side are distinct
    static void CheckUniqueIndices(){
      static_assert(TIndexSet_t::Size
		    == TIndexStructure1::NFree+TIndexStructure2::NFree,
		    "repeated indices on left-hand-side of implict "
		    "Tensor-assignment");
    }

    // check that indices on LHS and RHS match
    template<class L, class Op, class R>
    static void CheckIndexEquality(const iBinaryOp<L,Op,R>& ) {
      typedef typename iBinaryOp<L,Op,R>::TIndexSet_t rhs_TIndexSet;
      static_assert(std::is_same<TIndexSet_t, rhs_TIndexSet>::value,
		    "Indices on left and right side of assignment must match");
    }

    // TSymmetry consistent with Tensor?
    void CheckSymmetries() const;


    Tensor_GPUPointers<Tensor<Tensor<DataMesh> > >& GPUPointers();
    //--------------------------------
    // iteration over indices
    //--------------------------------
    DataMesh& ExpandIndices();

    // the only way to create a NonConstiBinaryOp, is to pass in a
    // non-const Tensor<DataMesh>&.  This reference is stored as a
    // const Tensor<DataMesh>&, but since it originated from a non-const
    // &, it is safe to cast away the const.
    Tensor<Tensor<DataMesh> >& NonConstTTensor() {
      return const_cast<Tensor<Tensor<DataMesh> >& >(constBASE::mTTensor);
    }



    DataMesh& GetComponent(const std::initializer_list<int>& l,
                 const std::initializer_list<int>& m);


    //--------------------------------
    // assignment operators
    //--------------------------------

    // assignment-operator is never a template.  Define it explicitly,
    // to avoid automatically defined assignment operator for A(i_)=B(i_)
    void operator=(const NonConstiBinaryOp<std::pair<TIndexStructure1,
                                        TIndexStructure<TSymmetry<dSymm...>,
                                                        dIndices...>
                                        >,
                              EmptyType,
                              DataMesh>& op) {
      TLoopApply(*this, TSetEqualOp(),
		 static_cast<const constBASE&>(op));
    }
    void operator=(const double d) {
      TLoopApply(*this, TSetEqualOp(), d);
    }
    void operator=(const DataMesh& dm) {
      TLoopApply(*this, TSetEqualOp(), dm);
    }
    template<class L, class O, class R>
    void operator=(const iBinaryOp<L,O,R>& op) {
      CheckIndexEquality(op);
      TLoopApply(*this, TSetEqualOp(), op);
    }

    void operator+=(const double d) {
      TLoopApply(*this, TPlusEqualOp(), d);
    }
    void operator+=(const DataMesh& dm) {
      TLoopApply(*this, TPlusEqualOp(), dm);
    }

    template<class L, class O, class R>
    void operator+=(const iBinaryOp<L,O,R>& op) {
      CheckIndexEquality(op);
      TLoopApply(*this, TPlusEqualOp(), op);
    }

    void operator-=(const double d) {
      TLoopApply(*this, TMinusEqualOp(), d);
    }
    void operator-=(const DataMesh& dm) {
      TLoopApply(*this, TMinusEqualOp(), dm);
    }

    template<class L, class O, class R>
    void operator-=(const iBinaryOp<L,O,R>& op) {
      CheckIndexEquality(op);
      TLoopApply(*this, TMinusEqualOp(), op);
    }


    void operator*=(const double d) {
      TLoopApply(*this, TMultEqualOp(), d);
    }
    void operator*=(const DataMesh& dm) {
      TLoopApply(*this, TMultEqualOp(), dm);
    }

    void operator/=(const double d) {
      TLoopApply(*this, TDivEqualOp(), d);
    }
    void operator/=(const DataMesh& dm) {
      TLoopApply(*this, TDivEqualOp(), dm);
    }
  };










  //================================================================
  // operator+, operator- operator*, operator/  to create iBinaryOps
  //================================================================
  //
  // These operators are called from user-code to trigger construction
  // of iBinaryOps, and the enable ImplicitTensorLoops.


  // ==== d*iOp,  iOp*d, iOp/d ====
  template<class L, class Op, class R>
  iBinaryOp<double, MultOp, iBinaryOp<L,Op,R>>
    operator*(const double& d, const iBinaryOp<L,Op,R>& op) {
    return iBinaryOp<double, MultOp, iBinaryOp<L,Op,R>>(d, op);
  }
  template<class L, class Op, class R>
  iBinaryOp<iBinaryOp<L,Op,R>,MultOp,double>
  operator*(const iBinaryOp<L,Op,R>& op, const double& d) {
    return iBinaryOp<iBinaryOp<L,Op,R>,MultOp,double>(op, d);
  }
  template<class L, class Op, class R>
  iBinaryOp<iBinaryOp<L,Op,R>,DivOp,double>
  operator/(const iBinaryOp<L,Op,R>& op, const double& d) {
    return iBinaryOp<iBinaryOp<L,Op,R>,DivOp,double>(op, d);
  }
  // d/iOp meaningless for tensor-calculus


  // ==== DM*iOp,  iOp*DM, iOp/DM ====
  template<class L, class Op, class R>
  iBinaryOp<DataMesh, MultOp, iBinaryOp<L,Op,R>>
    operator*(const DataMesh& d, const iBinaryOp<L,Op,R>& op) {
    return iBinaryOp<DataMesh, MultOp, iBinaryOp<L,Op,R>>(d, op);
  }
  template<class L, class Op, class R>
  iBinaryOp<iBinaryOp<L,Op,R>,MultOp,DataMesh>
  operator*(const iBinaryOp<L,Op,R>& op, const DataMesh& d) {
    return iBinaryOp<iBinaryOp<L,Op,R>,MultOp,DataMesh>(op, d);
  }
  template<class L, class Op, class R>
  iBinaryOp<iBinaryOp<L,Op,R>,MultOp,DataMesh>
  operator/(const iBinaryOp<L,Op,R>& op, const DataMesh& d) {
    return iBinaryOp<iBinaryOp<L,Op,R>,DivOp,DataMesh>(op, d);
  }
  // DM/iOp meaningless for tensor-calculus


  // ==== BinaryOp*iOp,  iOp*BinaryOp, iBinaryOp/BinaryOp ====
  template<class L1, class Op1, class R1,
	   class L2, class Op2, class R2>
  iBinaryOp<BinaryOp<L1,Op1,R1>,MultOp,iBinaryOp<L2,Op2,R2>>
    operator*(const  BinaryOp<L1,Op1,R1>& l,
	      const iBinaryOp<L2,Op2,R2>& r) {
    return iBinaryOp<BinaryOp<L1,Op1,R1>,MultOp,iBinaryOp<L2,Op2,R2>>(l,r);
  }
  template<class L1, class Op1, class R1,
	   class L2, class Op2, class R2>
  iBinaryOp<iBinaryOp<L1,Op1,R1>,MultOp,BinaryOp<L2,Op2,R2>>
    operator*(const iBinaryOp<L1,Op1,R1>& l,
	      const  BinaryOp<L2,Op2,R2>& r) {
    return iBinaryOp<iBinaryOp<L1,Op1,R1>,MultOp,BinaryOp<L2,Op2,R2>>(l,r);
  }
  template<class L1, class Op1, class R1,
	   class L2, class Op2, class R2>
  iBinaryOp<iBinaryOp<L1,Op1,R1>,MultOp, BinaryOp<L2,Op2,R2>>
    operator/(const iBinaryOp<L1,Op1,R1>& l,
	      const  BinaryOp<L2,Op2,R2>& r) {
    return iBinaryOp<BinaryOp<L1,Op1,R1>,DivOp,BinaryOp<L2,Op2,R2>>(l,r);
  }
  // Op/iOp meaningless for tensor-calculus


  // ==== iOp*iOp
  template<class L1, class Op1, class R1,
	   class L2, class Op2, class R2>
  iBinaryOp<iBinaryOp<L1,Op1,R1>,MultOp,iBinaryOp<L2,Op2,R2>>
    operator*(const iBinaryOp<L1,Op1,R1>& l,
	      const iBinaryOp<L2,Op2,R2>& r) {
    return iBinaryOp<iBinaryOp<L1,Op1,R1>,MultOp,iBinaryOp<L2,Op2,R2>>(l,r);
  }
  // iOp/iOp meaningless for tensor-calculus


  // ==== iOp+iOp, iOp-iOp ====
  template<class L1, class Op1, class R1,
	   class L2, class Op2, class R2>
  iBinaryOp<iBinaryOp<L1,Op1,R1>,AddOp,iBinaryOp<L2,Op2,R2>>
    operator+(const iBinaryOp<L1,Op1,R1>& l,
	      const iBinaryOp<L2,Op2,R2>& r) {
    typedef typename iBinaryOp<L1,Op1,R1>::TIndexSet_t LHS_Indices;
    typedef typename iBinaryOp<L2,Op2,R2>::TIndexSet_t  RHS_Indices;
    static_assert(std::is_same<LHS_Indices,RHS_Indices>::value,
		  "Indices on left and right side of operator+ must match");
    return iBinaryOp<iBinaryOp<L1,Op1,R1>,AddOp,iBinaryOp<L2,Op2,R2>>(l,r);
  }
  template<class L1, class Op1, class R1,
	   class L2, class Op2, class R2>
  iBinaryOp<iBinaryOp<L1,Op1,R1>,SubOp,iBinaryOp<L2,Op2,R2>>
    operator-(const iBinaryOp<L1,Op1,R1>& l,
	      const iBinaryOp<L2,Op2,R2>& r) {
    static_assert(!std::is_same<R2, Tensor<DataMesh>>::value,
		  "unexpected type. Check indices of Tensor<Tensor<DataMesh>>");
    typedef typename iBinaryOp<L1,Op1,R1>::TIndexSet_t LHS_Indices;
    typedef typename iBinaryOp<L2,Op2,R2>::TIndexSet_t RHS_Indices;
    static_assert(std::is_same<LHS_Indices,RHS_Indices>::value,
		  "Indices on left and right side of operator- must match");
    return iBinaryOp<iBinaryOp<L1,Op1,R1>,SubOp,iBinaryOp<L2,Op2,R2>>(l,r);
  }

  // ==== unary+, unary- ====
  template<class L2, class Op2, class R2>
  iBinaryOp<L2,Op2,R2>
  operator+(const iBinaryOp<L2,Op2,R2>& r) {
    return r;  // just eat the plus sign
  }
  template<class L2, class Op2, class R2>
  iBinaryOp<EmptyType,negateOp,iBinaryOp<L2,Op2,R2>>
    operator-(const iBinaryOp<L2,Op2,R2>& r) {
    return iBinaryOp<EmptyType,negateOp,iBinaryOp<L2,Op2,R2>>(r);
  }



}

#endif
