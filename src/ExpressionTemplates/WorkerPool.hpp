/// \file
/// Defines thread worker pool for general multithreading via pthreads

#ifndef WorkerPool_hpp
#define WorkerPool_hpp

#include <iostream>
#include <stdexcept>
#include <utility>
#include <vector>

// Use the old <atomic> header name with libstdc++ 4.4
#if defined(__GLIBCXX__) && (__GNUC__ == 4) && (__GNUC_MINOR__ == 4)
#include <cstdatomic>
#else
#include <atomic>
#endif

#include <unistd.h>
#include <pthread.h>

#include "Assert.hpp"

/// Get a thread identifier that is different for each job in a WorkerPool.
int WorkerPoolGetThisThreadNum();
/// Get the number of threads currently being used by WorkerPool.
int WorkerPoolGetNumThreads();
/// Set the thread number.  Only for use by WorkerPool itself.
void WorkerPoolSetThisThreadNum(int num);
/// Set the number of threads currently in use.  Only for use by
/// WorkerPool itself.
void WorkerPoolSetNumThreads(int num);

/// Thread allocation and management class for multithreading
/// The first template argument should be the input data type for the
/// computation. The second should be the result type for the computation.
template <class T, class U>
class WorkerPool {
  typedef T params_type;
  typedef U result_type;
public:

  /// Default constructor initializes mutex
  WorkerPool(unsigned int threads = 0) : mThreads(threads) {
    if (mThreads == 0) {
      mThreads = sysconf(_SC_NPROCESSORS_ONLN);
    }
  }

  /// Launches work.
  void PerformWork(const std::vector<params_type>& all_params);

  // Pools are not copyable.
  WorkerPool(const WorkerPool&) = delete;
  WorkerPool& operator=(const WorkerPool&) = delete;

protected:
  /// Update a progress bar on the screen.
  /// This is a helper method for implementing classes to allow them to show
  /// a useful progress indicator. It must be called unconditionally in the
  /// HandleResult method in order to display properly.
  void UpdateProgress();

private:
  /// Perform a calculation to find WorkResult given GlobalState and
  /// WorkParams. Each subclass must provide this. It should be expected
  /// to be called on multiple threads at once.
  virtual result_type Compute(const params_type& params) const = 0;

  /// Do something with the result of the calculation. This gets called
  /// by each worker after it has computed a result. Implementers should
  /// aim for designs which avoid the need for locks, for efficiency.
  virtual void HandleResult(const params_type& params,
                            const result_type& result) = 0;

  /// Thread worker for each thread in pool. This will handle retrieving work
  /// for the thread and passing it to the Compute method. It will also
  /// call the routine to handle available output.
  static void* ThreadWorker(void* arg);


  long mThreads;
  // Bookkeeping for input
  std::vector<params_type> mInput;
  std::atomic<size_t> mInput_off;
  // Progress tracker
  std::atomic<unsigned long> mProgress;

  typedef std::pair<WorkerPool*, int> ThreadArg;
};

template<class T,class U> void WorkerPool<T,U>::PerformWork
(const std::vector<params_type>& all_params) {
  mInput = all_params;
  mInput_off = 0;
  mProgress = 0;

  // This will not work if multiple WorkerPools are active at once.
  // It will need smarter logic if someone wants to do that.
  //
  // The first worker thread shares its number with the main thread so
  // that the numbers range over [0, mThreads).  These two threads
  // never do nontrivial things concurrently, so this should be safe.
  std::vector<pthread_t> threads(mThreads);
  std::vector<ThreadArg> threadArgs(mThreads);
  ASSERT(WorkerPoolGetNumThreads() == 1, "Can't call WorkerPool recursively");
  WorkerPoolSetNumThreads(static_cast<int>(mThreads));
  for (int t=0; t < mThreads; ++t) {
    threadArgs[t] = ThreadArg(this, t);
    pthread_create(&threads[t], nullptr, WorkerPool::ThreadWorker,
                   &threadArgs[t]);
  }

  for (int t=0; t < mThreads; ++t) {
    pthread_join(threads[t], nullptr);
  }
  WorkerPoolSetNumThreads(1);
}

template<class T,class U> void* WorkerPool<T,U>::ThreadWorker(void* arg) {
  ThreadArg* p_arg = static_cast<ThreadArg*>(arg);
  if (p_arg == nullptr) {
    throw std::invalid_argument("Bad argument passed to worker.");
  }
  WorkerPool& pool = *p_arg->first;
  WorkerPoolSetThisThreadNum(p_arg->second);
  const std::vector<params_type>& params = pool.mInput;

  while (true) {
    const size_t this_off = pool.mInput_off++;

    if (this_off >= params.size()) {
      break;
    }

    result_type result = pool.Compute(params[this_off]);

    // Handle result
    pool.HandleResult(params[this_off], result);
  }

  return nullptr;
}

template<class T,class U> void WorkerPool<T,U>::UpdateProgress() {
  unsigned long oldProgress = mProgress++;
  unsigned long percentage = oldProgress * 100 / mInput.size();
  const unsigned long newPercentage = (oldProgress + 1) * 100 / mInput.size();

  while (newPercentage > percentage) {
    percentage++;
    if (percentage % 10 == 0) {
      std::cout << percentage << std::flush;
    } else {
      std::cout << "." << std::flush;
    }
  }
}

#endif
