//=============================================================================
/// \file
/// Defines class TensorStructure.
//=============================================================================
#ifndef TensorStructure_hpp
#define TensorStructure_hpp
//=============================================================================
#include "IPoint.hpp"
#include "Assert.hpp"
//=============================================================================
#include <string>
//=============================================================================


// ***************************************************************
// TensorIndexTable
//
// This class holds all functionality that is necessary to deal with
// _one_ specific set of symmetries.  It knows dimension, rank, and
// how to translate indices (n1, n2, ..., n_rank) into a 1-dimensional
// index that respects symmetries (i.e. (1,2) and (2,1) yield the
// same number for a "11" tensor.
//
// There is _one_ instantiation of this class for each set of
// symmetries, no matter how many Tensors with this symmetries are
// allocated.  This is enforced by TensorIndexTableHolder, which is
// defined in the .cpp file. 
// ****************************************************************/

class TensorIndexTableHolder; // defined in TensorStructure.cpp

// Internal class to be used by TensorStructure.
class TensorIndexTable {
private: // additional uniqueness enforcements
  friend class TensorIndexTableHolder;
  TensorIndexTable(int d,const std::string&);
  //default destructor and copy constructor are fine
  TensorIndexTable& operator=(const TensorIndexTable &); // never defined
public:
  const int& Dim()  const {return mDim;}
  const int& Size() const {return mSize;}
  const int& Rank() const {return mRank;}
  const std::string& Spec() const {return mSpec;}
  // access functions
  int Index() const {
    ASSERT(mRank==0, "mRank=" << mRank);
    return 0;
  };
  int Index(size_t n1) const {
    ASSERT(mRank==1, "mRank=" << mRank);
    ASSERT(0<=n1 && n1<mDim, "n1=" << n1<<", mDim="<<mDim<<", mRank="<<mRank);
    return n1;
  };
  int Index(int n1,int n2) const {
    ASSERT(mRank==2, "mRank=" << mRank);
    ASSERT(0<=n1 && n1<mDim, "n1=" << n1<<", mDim="<<mDim<<", mRank="<<mRank);
    ASSERT(0<=n2 && n2<mDim, "n2=" << n2<<", mDim="<<mDim<<", mRank="<<mRank);
    return mIndex[n1+mDim*n2];
  };
  int Index(int n1,int n2,int n3) const {
    ASSERT(mRank==3, "mRank=" << mRank);
    ASSERT(0<=n1 && n1<mDim, "n1=" << n1<<", mDim="<<mDim<<", mRank="<<mRank);
    ASSERT(0<=n2 && n2<mDim, "n2=" << n2<<", mDim="<<mDim<<", mRank="<<mRank);
    ASSERT(0<=n3 && n3<mDim, "n3=" << n3<<", mDim="<<mDim<<", mRank="<<mRank);
    return mIndex[n1+mDim*(n2+mDim*n3)];
  };
  int Index(int n1,int n2,int n3,int n4) const {
    ASSERT(mRank==4, "mRank=" << mRank); ASSERT(0<=n1 && n1<mDim, "n1=" << n1<<", mDim="<<mDim<<", mRank="<<mRank);
    ASSERT(0<=n2 && n2<mDim, "n2=" << n2<<", mDim="<<mDim<<", mRank="<<mRank);
    ASSERT(0<=n3 && n3<mDim, "n3=" << n3<<", mDim="<<mDim<<", mRank="<<mRank);
    ASSERT(0<=n4 && n4<mDim, "n4=" << n4<<", mDim="<<mDim<<", mRank="<<mRank);
    return mIndex[n1+mDim*(n2+mDim*(n3+mDim*n4))];
  };
  int Index(int n1,int n2,int n3,int n4,int n5) const {
    ASSERT(mRank==5, "mRank=" << mRank);
    ASSERT(0<=n1 && n1<mDim, "n1=" << n1<<", mDim="<<mDim<<", mRank="<<mRank);
    ASSERT(0<=n2 && n2<mDim, "n2=" << n2<<", mDim="<<mDim<<", mRank="<<mRank);
    ASSERT(0<=n3 && n3<mDim, "n3=" << n3<<", mDim="<<mDim<<", mRank="<<mRank);
    ASSERT(0<=n4 && n4<mDim, "n4=" << n4<<", mDim="<<mDim<<", mRank="<<mRank);
    ASSERT(0<=n5 && n5<mDim, "n5=" << n5<<", mDim="<<mDim<<", mRank="<<mRank);
    return mIndex[n1+mDim*(n2+mDim*(n3+mDim*(n4+mDim*n5)))];
  };
  int Index(int n1,int n2,int n3,int n4,int n5,int n6) const {
    ASSERT(mRank==6, "mRank=" << mRank);
    ASSERT(0<=n1 && n1<mDim, "n1=" << n1<<", mDim="<<mDim<<", mRank="<<mRank);
    ASSERT(0<=n2 && n2<mDim, "n2=" << n2<<", mDim="<<mDim<<", mRank="<<mRank);
    ASSERT(0<=n3 && n3<mDim, "n3=" << n3<<", mDim="<<mDim<<", mRank="<<mRank);
    ASSERT(0<=n4 && n4<mDim, "n4=" << n4<<", mDim="<<mDim<<", mRank="<<mRank);
    ASSERT(0<=n5 && n5<mDim, "n5=" << n5<<", mDim="<<mDim<<", mRank="<<mRank);
    ASSERT(0<=n6 && n6<mDim, "n6=" << n6<<", mDim="<<mDim<<", mRank="<<mRank);
    return mIndex[n1+mDim*(n2+mDim*(n3+mDim*(n4+mDim*(n5+mDim*n6))))];
  };
  int Index(int n1,int n2,int n3,int n4,int n5,int n6,int n7) const {
    ASSERT(mRank==7, "mRank=" << mRank);
    ASSERT(0<=n1 && n1<mDim, "n1=" << n1<<", mDim="<<mDim<<", mRank="<<mRank);
    ASSERT(0<=n2 && n2<mDim, "n2=" << n2<<", mDim="<<mDim<<", mRank="<<mRank);
    ASSERT(0<=n3 && n3<mDim, "n3=" << n3<<", mDim="<<mDim<<", mRank="<<mRank);
    ASSERT(0<=n4 && n4<mDim, "n4=" << n4<<", mDim="<<mDim<<", mRank="<<mRank);
    ASSERT(0<=n5 && n5<mDim, "n5=" << n5<<", mDim="<<mDim<<", mRank="<<mRank);
    ASSERT(0<=n6 && n6<mDim, "n6=" << n6<<", mDim="<<mDim<<", mRank="<<mRank);
    ASSERT(0<=n7 && n7<mDim, "n7=" << n7<<", mDim="<<mDim<<", mRank="<<mRank);
    return mIndex[n1+mDim*(n2+mDim*(n3+mDim*(n4+mDim*(n5+mDim*(n6+mDim*n7)))))];
  };
  // Index by an IPoint that holds n1,n2,n3,...
  int Index(const std::vector<int>& index) const; 

  // Will the Index(const MV<int>&) function succeed?
  bool IsIndexValid(const std::vector<int>& index) const; 

  // reverse lookup: Given the place in the data-array, what are the indices?
  const IPoint& Indices(const int index1D) const { return mIndices[index1D]; }

  // Given the place in the data-array, how many combinations (n0,n1,...)
  // map to this place because of symmetries.
  int Multiplicity(const int index1D) const 
    {return mMultiplicity[static_cast<size_t>(index1D)];}

  std::ostream& Output(std::ostream& os) const;
  bool operator==(const TensorIndexTable& rhs) const;
private:
  // Sets 'out' equal to a canonical set of indices that is equivalent
  // to 'in' by symmetries.  The canonical list has earlier indices
  // greater than or equal to later indices.
  void ApplySymmToIndexList(const std::string& s,const std::vector<int> &in,
			    std::vector<int> &out);
  // *** PRIVATE DATA ***
  int mDim;
  int mRank;
  int mSize;       // Number of independent components of the tensor
  std::string mSpec;     // the canonicalized spec string.
    //Symm strings get mapped to the lowest available character.
    //"09", "AB", "ab", "ba", "zy" all get mapped to mSpec == "ab"
  IPoint mExtents; // holds (Dim, Dim, ... Dim) 

  // When indexing a Tensor, i.e. Tensor(n0,n1,n2,...), a unique
  // number is constructed from the indices: Index=n0+dim*(n1+dim*(n2+...))
  //
  // mIndex[Index] gives the position in the data array
  // corresponding to the element indexed by (n0,n1,n2,...), taking
  // into account symmetries.
  std::vector<int> mIndex;

  // inverse of mIndex:  given the location in the 1-D data array, what are
  // the first set of indices that correspond to it? 
  std::vector<IPoint> mIndices;

  // mMultiplicity[mIndex[Index]] is the number of combinations of n0,n1,n2,...
  // corresponding to the element indexed by (n0,n1,n2,...). There is more
  // than one combination if the Tensor has symmetries.
  std::vector<int> mMultiplicity;
};

// ****************************************************************
// FindTensorIndexTable
// 
// Public access to TensorIndexTables.  Creation and storage hidden 
// inside TensorIndexTableHolder
// ****************************************************************

const TensorIndexTable* FindTensorIndexTable(const int dim,
                                             const std::string& spec);

////////////////////////////////////////////////////////////////
/// Handles index-structure of Tensor's.  
/// A Tensor<T> keeps data (elements of type T) and a TensorStructure.
///
/// The index-structure consists of the dimension (i.e. the range of possible
/// values for each index), the rank (number of indices) and the symmetry
/// specification. 
/// 
/// TensorStructure is in fact a wrapper around TensorIndexTable,
/// providing value-syntax, and hiding the fact that TensorIndexTable's
/// are unique and cached.
///
/// Two TensorStructures are considered equal if they represent the same
/// sequence of distinct indices (in other words, the iterations generated by
/// their corresponding TensorIters would be identical).  It is therefore
/// possible for two TensorStructures to be equal even if their public fields
/// differ (in particular, rank-0 TensorStructures are equal regardless of their
/// dimension).
////////////////////////////////////////////////////////////////
class TensorStructure {
public: // Constructor
  //TensorStructure::Empty constructor
  TensorStructure():
    mpIndexTable(0),
    mIsEmpty(true) { }
  //Actual TensorStructure
  TensorStructure(const int dim_in,const std::string& spec): 
    mpIndexTable(FindTensorIndexTable(dim_in, spec)),
    mIsEmpty(false) { };
  // default copy constructor, assignment operator & destructor ok
public:
  // no other class can automatically be converted to TensorStructure,
  // so it is inmaterial whether operators== and != are member functions
  // or not.  Keep them member functions for convenience.
  /// Returns 'true' if 'this' and 'rhs' represent the same sequence of distinct
  /// indices.
  bool operator==(const TensorStructure& rhs) const { 
    //Exactly one is empty, so we can't check that they match
    if (IsEmpty() != rhs.IsEmpty()) {
      return false;
    }
    //Return true if both are empty.
    // If kBothEmpty = false, then we can safely check the rest for a match
    const bool kBothEmpty = IsEmpty() && rhs.IsEmpty();
    return kBothEmpty ||
      ((Rank()==0 && rhs.Rank()==0) || *mpIndexTable==*rhs.mpIndexTable);
  }
  bool operator!=(const TensorStructure& rhs) const { return !(rhs==*this);}
public: 
  // *** Access -- forward to TensorIndexTable ***
  const int& Dim() const {
    AssertNotEmpty();
    return mpIndexTable->Dim();}
  const int& Rank() const {
    AssertNotEmpty();
    return mpIndexTable->Rank();}
  int Size() const {
    return IsEmpty() ? 0 : mpIndexTable->Size();}
  int Index() const {
    AssertNotEmpty();
    return mpIndexTable->Index();}
  std::string Spec() const {
    AssertNotEmpty();
    return mpIndexTable->Spec();}
  int Index(int n1) const { 
    AssertNotEmpty();
    return mpIndexTable->Index(n1); }
  int Index(int n1,int n2) const { 
    AssertNotEmpty();
    return mpIndexTable->Index(n1, n2); };
  int Index(int n1,int n2,int n3) const { 
    AssertNotEmpty();
    return mpIndexTable->Index(n1, n2, n3); };
  int Index(int n1,int n2,int n3,int n4) const { 
    AssertNotEmpty();
    return mpIndexTable->Index(n1, n2, n3, n4); };
  int Index(int n1,int n2,int n3,int n4,int n5) const {
    AssertNotEmpty();
    return mpIndexTable->Index(n1, n2, n3, n4, n5); };
  int Index(int n1,int n2,int n3,int n4,int n5,int n6) const {
    AssertNotEmpty();
    return mpIndexTable->Index(n1, n2, n3, n4, n5, n6); };
  int Index(int n1,int n2,int n3,int n4,int n5,int n6,int n7) const {
    AssertNotEmpty();
    return mpIndexTable->Index(n1, n2, n3, n4, n5, n6, n7); };
  // Index by an IPoint that holds n1,n2,n3,...
  int Index(const IPoint& index) const {
    AssertNotEmpty();
    return mpIndexTable->Index(index); };

  // Will the Index(const MV<int>&) function succeed?
  bool IsIndexValid(const std::vector<int>& index) const {
    AssertNotEmpty();
    return mpIndexTable->IsIndexValid(index); }

  // reverse lookup: Given the place in the data-array, what are the indices?
  const IPoint& Indices(const int index1D) const { 
    AssertNotEmpty();
    return mpIndexTable->Indices(index1D); }

  // Given the location in the data-array, how many combinations (n0,n1,...)
  // map to this place because of symmetries.
  int Multiplicity(const int index1D) const {
    AssertNotEmpty();
    return mpIndexTable->Multiplicity(index1D); }

  std::ostream& Output(std::ostream& os) const {
    AssertNotEmpty();
    return mpIndexTable->Output(os);}

  bool IsEmpty() const {
    return mIsEmpty; }

  // for Tensor(TensorStructure::Empty) constructor
  enum eEmptyConstructorTag { Empty };

private:    // *** Implementation Details ***
  void AssertNotEmpty() const {
    ASSERT(!mIsEmpty,
           "Cannot call functions on an empty tensor structure");}

  const TensorIndexTable* mpIndexTable; // 'our' IndexTable
  bool mIsEmpty;
};


inline std::ostream& operator<<(std::ostream& os, const TensorStructure& s) {
  return s.Output(os); }

#endif 
