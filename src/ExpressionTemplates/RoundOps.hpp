//=============================================================================
/// \file
/// Implements rounding operations: Round(), IRound(), Trunc(), ITrunc()
//=============================================================================
#ifndef INCLUDED_RoundOps_hpp
#define INCLUDED_RoundOps_hpp
//=============================================================================
/// Rounds to the nearest integer.
/// Builtin round() is not available on all compilers.
double Round(const double& x);

/// Rounds to the nearest integer and returns an int
int IRound(const double& x);

/// Returns the integer part of a double as a double.
/// Builtin trunc() is not available on all compilers.
double Trunc(const double& x);

/// Returns the integer part of a double as an int.
int ITrunc(const double& x);

/// Returns the floor of a double as an int.
int IFloor(const double& x);

/// Returns the ceiling of a double as an int.
int ICeil(const double& x);

/// Returns the fractional part of a double (x - floor(x))
double Frac(const double& x);
//=============================================================================

#endif
