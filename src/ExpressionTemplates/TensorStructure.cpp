//=============================================================================
#include "TensorStructure.hpp" // should be first include
#include "Require.hpp"
#include "IPoint.hpp"
//#include "Utils/MyContainers/MyMap.hpp"
#include "LazyPerThread.hpp"
//=============================================================================
#include <map>
#include <cstring>
#include <list>
#include <mutex>
//=============================================================================


//--------------------------------------------------------------
// Methods for class TensorIndexTable
//--------------------------------------------------------------
TensorIndexTable::TensorIndexTable(int d,const std::string& spec) :
  mDim(d), mRank(static_cast<int>(spec.length())),
  mSize(0), mSpec(spec), mExtents(mRank, mDim),
  mIndex(), mIndices(),mMultiplicity()
{
  //------------------------------------------|
  // Zero rank tensors need special treatment.|
  //------------------------------------------|
  if(mRank==0) {
    mIndex = std::vector<int>(1, 0);
    mIndices = std::vector<IPoint>();
    mMultiplicity = std::vector<int>(1, 0);
    mSize          = 1;
    return;
  }

  // Get size of index table and allocate it.
  // Size should be dim^rank, but we cannot exponentiate efficiently in C
  int indexTableSize = 1;
  for(int r=0;r<mRank;r++) indexTableSize*=mDim;
  mIndex = std::vector<int>(indexTableSize);

  //-----------------------------------------------|
  // Get size of data array and create table entry |
  //-----------------------------------------------|
  std::vector<int> indx(mRank, 0), indx2(mRank, 0);

  // Set up index table and compute size
  for(int tableindex=0;tableindex<indexTableSize; ++tableindex) {
    ApplySymmToIndexList(spec,indx,indx2);
    if(indx==indx2) {
      mIndex[tableindex] = mSize; ++mSize;
    } else {
      mIndex[tableindex]=mIndex[ComputeIndex(indx2, mExtents)];
    }
    // Increment indices
    for(int r=0;r<mRank;r++) {
      if(++indx[r]<mDim) break; // No rollover
      indx[r] = 0;             // This index rolls over, go to next one.
    }
  }

  // set up the reverse lookup:
  mIndices = std::vector<IPoint>(mSize, IPoint());  
  indx = std::vector<int>(mRank, 0);
  for(int k=0; k<indexTableSize; ++k) {
    // indx goes through all possible indices (doublecounting symmetries)
    // k is the corresponding "flattened" index
    // mIndex[k] is the uniquified element for storing data
    // mIndices at that data-storage location is set to indx.
    // for symmetric tensors, double counted elements in mIndices are
    // overwritten.  That gives row major order, which we prefer
    // [e.g. for gij -> (0,0), (0,1), (0,2), (1,1), (1,2), (2,2) ]
    mIndices[mIndex[k]]=indx;

    // Increment indices
    for(int r=0;r<mRank;r++) {
      if(++indx[r]<mDim) break; // No rollover
      indx[r] = 0;             // This index rolls over, go to next one.
    }
  }

  // Set up multiplicities
  mMultiplicity =  std::vector<int>(mSize, 0);
  for(int k=0;k<indexTableSize;++k) ++mMultiplicity[mIndex[k]];

  // Store the unique symmetry string in mSpec
  std::map<char, char> symmCharMap;
  char nextChar = 'a';
  int counter = 0;
  for(const auto & elem : spec) {
    auto key = symmCharMap.find(elem);
    if (key != symmCharMap.end()) { //key found
        mSpec[counter] = key->second;
    }else{
        symmCharMap.emplace(elem, nextChar);
        mSpec[counter] = nextChar;
        ++nextChar;
    }
    ++counter;
  }
  
  //MyMap<char, char> symmCharMap;
  //char nextChar = 'a';
  //int counter = 0;
  //for(const auto & elem : spec) {
    //if (symmCharMap.KeyExists(elem)) {
      //mSpec[counter] = symmCharMap[elem];
    //}
    //else {
      ////We didn't add this char yet.  Map it to the next char available
      //symmCharMap.Insert(elem, nextChar);
      //mSpec[counter] = nextChar;
      //++nextChar;
    //}
    //++counter;
  //}
}

// Index by an IPoint that holds n1,n2,n3,...
int TensorIndexTable::Index(const IPoint& index) const {
  return mIndex[ComputeIndex(index, mExtents)];
}

bool TensorIndexTable::IsIndexValid(const IPoint& index) const {
  return IsComputeIndexValid(index,mExtents);
}

std::ostream& TensorIndexTable::Output(std::ostream& os) const {
  os << "{" << mDim << " \"" << mSpec << "\"}";
  return os;
}

bool TensorIndexTable::operator==(const TensorIndexTable& rhs) const {
  return mDim==rhs.mDim && mRank==rhs.mRank && mSpec==rhs.mSpec;
}


// Computes vector "out" which is the same as vector "in" by symmetry,
// except the first index of a symmetric pair is always greater than the
// second.  Example: s="11", in=(1,3) yields  out=(3,1)
//
// NOTE: This ordering is essential to the algorithm in the
// TensorIndexTable constructor.
void TensorIndexTable::ApplySymmToIndexList(const std::string& s,
					    const std::vector<int> &in,
					    std::vector<int> &out) {
  const int r = static_cast<int>(s.length());
  // Sanity check: Sizes should be the same
  ASSERT(in.size()==r, "size mismatch");
  out = in;
  for(int i=0;i<r;i++) {
    for(int j=i+1;j<r;j++) {
      if(s[j]==s[i]) {      // Symmetry found for this value of i.
	if(out[j]>out[i]) { // Switch the two indices
          using std::swap;
	  swap(out[i], out[j]);
	}
      }
    }
  }
}

// ****************************************************************
// class TensorIndexTableHolder
//
// This class keeps a list of all TensorIndexTable's that are used.
// Its public interface consists solely out of
//    TensorIndexTable* Find(const int dim, const char* spec)
// which returns the appropriate TensorIndexTable (if not present,
// it is added).  "122" and "baa" will result in the _same_
// TensorIndexTable*.
// ****************************************************************
class TensorIndexTableHolder {
public:
  TensorIndexTableHolder() { };
  ~TensorIndexTableHolder();
private:
  TensorIndexTableHolder& operator=(const TensorIndexTableHolder& );
public:
  TensorIndexTableHolder(const TensorIndexTableHolder& );
  // return the (unique) TensorIndexTable for that dim and symmetries
  // if not there, allocate it
  const TensorIndexTable* Find(const int dim, const std::string& spec);
private:
  // this list holds all TensorIndexTables.  There is exactly one
  // TensorIndexTable per symmetry (i.e. "122" and "abb" have the
  // __same__ TensorIndexTable)
  std::list<TensorIndexTable*> mIndexTables;

  // spec_cache provides a map from specification strings ("122") to
  // TensorIndexTables (as stored in mIndexTables).  spec_cache
  // contains separate entries for "122" and "abb" however, these
  // entries point to the same TensorIndexTable*.
  typedef std::map<const std::string, TensorIndexTable*> CacheType;
  CacheType spec_cache;

  std::mutex spec_cacheLock;
};


//--------------------------------------------------------------
// Methods for class IndexTableListHolder
//--------------------------------------------------------------
TensorIndexTableHolder::~TensorIndexTableHolder() {
  for(auto & elem : mIndexTables) {
    delete elem;
  }
  // clear the map:
  while(!spec_cache.empty()) {
    auto it=spec_cache.begin();
    spec_cache.erase(it);
  }
}

TensorIndexTableHolder::TensorIndexTableHolder(
  const TensorIndexTableHolder& kCopy) {
  //First deep copy the mIndexTables list
  for (const auto & elem : kCopy.mIndexTables)
  {
    mIndexTables.push_back(new TensorIndexTable(*elem));
  }

  for (auto it = kCopy.spec_cache.begin(); it != kCopy.spec_cache.end(); ++it)
  {
    const std::string& kKey = it->first;
    const TensorIndexTable* kpCopy = it->second;
    bool notFound = true;
    for (std::list<TensorIndexTable*>::const_iterator
           kItOld = kCopy.mIndexTables.begin(),
           itNew = mIndexTables.begin();
           kItOld != kCopy.mIndexTables.end() && notFound;
           ++kItOld, ++itNew)
    {
      if (it->second == kpCopy)
      {
        notFound = false;
        spec_cache[kKey] =  *itNew;
      }
    }
    ASSERT(!notFound, "We didn't find this key... problem");
  }
}

const TensorIndexTable* TensorIndexTableHolder::Find(const int dim,
						     const std::string& spec) {
  //Lock cache until end of function.
  std::lock_guard<std::mutex> scopedCacheLocker(spec_cacheLock);
  ASSERT(dim<26, "Maybe the trick to encode dimension in the first "
         " character fails for this high dimension. Better check!");
  std::string s = static_cast<char>(dim + 65) + spec;

  {  // in there already?
    CacheType::const_iterator it=spec_cache.find(s);
    if(it!=spec_cache.end()) {
      return it->second;
    }
  }
  // well, sortcut based on symmetry-string didn't work.
  // Need to work harder and compare TensorIndexTable's explicitly
  auto  temp=new TensorIndexTable(dim, spec);
  typedef std::list<TensorIndexTable*>::const_iterator iter;
  for(iter it=mIndexTables.begin(); it!=mIndexTables.end(); ++it)
    if(**it == *temp) // deep compare
      { // bingo
	spec_cache[s]=*it;  // insert pointer into spec_cache;
	delete temp; // don't need temp - have this TensorIndexTable already
	return *it;
      };
  // don't have these symmetries yet - insert them
  mIndexTables.push_back(temp);
  spec_cache[s]=temp;
  // don't delete temp - will be done in ~TensorIndexTableHolder()
  return temp;
}


// public point of access
const TensorIndexTable* FindTensorIndexTable(const int dim,
                                             const std::string& spec) {
  static LazyPerThread<TensorIndexTableHolder> mTables;
  return mTables.GetReference().Find(dim, spec);
  //static TensorIndexTableHolder table;
  //return table.Find(dim, spec);
}


