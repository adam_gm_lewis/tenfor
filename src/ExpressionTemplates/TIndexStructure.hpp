#ifndef TIndexStructure_hpp
#define TIndexStructure_hpp

#include "TIndexSet.hpp"
#include "TIndexSlot.hpp"
#include "TSymmetry.hpp"
#include "IPoint.hpp"
#include "OutputVector.hpp"
#include <vector>
#include <stdexcept> //std::out_of_range
#include <map>
namespace ImplicitTensorLoops {



  // helper for helper:  Get N-th baseclase
  template<int N, class TIndexStructure_t>
  struct GetNthBaseClass {
    typedef typename GetNthBaseClass<N-1,
				     typename TIndexStructure_t::BASE
				     >::type type;
  };
  template<class TIndexStructure_t>
  struct GetNthBaseClass<0,TIndexStructure_t> {
    typedef TIndexStructure_t type;
  };


  // represents the indices into a Tensor<DataMesh>.
  // Each index can be either bound to a specified int,
  // or can be a free index represented by a TIndex<> type.
  //
  // Each template argument represents one index as follows:
  //   - int: A value index, bound to this value
  //   - TIndex<int,int> a free index to be looped over
  //
  // The generic template is not defined.  The specialized templates
  // define:
  //   - typename BASE: the type of the TIndexStructure with first index
  //                    removed
  //   - typename TIndexSet_t: The TIndexSet of all free indices
  //   - const int Rank: The rank of this IndexStructure
  //   - const int Nfree: number of free indices
  //   - const int NUniqueFree:  number of unique free indices
  //   - static const bool FirstIndexIsFree: true if the first index
  //                                         is a TIndex
  //   - SetIndex(IPoint& idx): set idx to the indices of this
  //                                   TIndexStructure, retrieving the
  //                                   TIndex values.
  //   - void Reset() reset all free indices to their starting value
  //   - void operator++() increment to next index
  //   - operator bool() const  -- have we finished iterating?
  //
  //   - GetFreeIndices(IPoint& freeIndices)
  //          resize freeIndixes to size equal to rank of TIndexStructure
  //          for each implicit index, set the corresonding entry in
  //          freeIndices to -1.  For each fixed index, set it to the
  //          value the index is fixed to.
  //
  // If and only if the first slot of the TIndexStructure is a TIndex<...>:
  //   - typename FirstSlotTIndex_t: The TIndex<...> of the first slot

  template<class TSymmetry_t, class ...indices> struct TIndexStructure;


  // end the iteration through the indices of the tensor
  // When we get there, we must have exhausted all TSymmetry-inequalities,
  // therefore the first argument is the empty TSymmetry<>.
  template<>
  struct TIndexStructure<TSymmetry<> > {
    static const int Rank=0;
    static const int NFree=0;
    static const int NUniqueFree=0;
    static const bool IndicesAreUnique=(NFree==NUniqueFree);
    typedef TIndexSet<> TIndexSet_t;

    void GetAllIndices(IPoint& idx) const {
      if(idx.size()!=0) idx = IPoint(0);
    }

    void ConstructIndexKey(IPoint& idx_key) const{
      if(idx_key.size()!=0) idx_key = IPoint(0);
    }

    void GetFreeIndices(IPoint& freeIndices) const {
      if(freeIndices.size()!=0) freeIndices = IPoint(0);
    }

    int GetNFree() const{
      return NFree;
    }

    void Fill(int*) const { return; }
    void FillForKey(int*) const { return; }
    void FillFreeIndices(int*) const { return; }

    void Reset() { mDone=false; };
    void operator++() {
      // this class terminates the TIndexStructure.  If all previous
      // indices have been iterated through, this operator++ is called.
      // This happens only when all indices are exhausted, so remember
      // that we are done.
      mDone=true;
    }

    explicit operator bool() const {
      return !mDone;
    }

    //--------------------------------
    // TCodeWriter interface
    //--------------------------------
    static std::string VarDeclaration() {
      return "";
    }

    static std::string CUDAVarDeclaration() {
      return "";
    }


    static IPoint Range() {
      return IPoint();
    }

    void ExpandFreeIndices(std::initializer_list<int>::const_iterator /* in */,
			   IPoint::iterator /* out */) const{
    }
    // in tandem with TExpressionLeaf<iBinaryOp<...,DataMesh>>
    static std::string LoopIndexedExpression
      (const std::map<std::string,int>&) {
      return "";
    }

    //Returns the list of indices without square brackets
    static std::string CUDALoopIndexedExpression
      (size_t,
       const std::map<std::string,int>&,bool) {
      return "";
    }
    
      // in tandem with TExpressionLeaf<iBinaryOp<...,DataMesh>>
    static std::string ForLoopsOverTensorIndices() {
        return "";
    }

  private:
    bool mDone; // is set to true when iterating is done
  };

  //----------------------------------------------------------------
  // specialization for bound first index:
  //----------------------------------------------------------------
  //
  // The bound index is represented by the type 'int' in
  // the template argument list.  The value of the 'int'
  // is stored in a member-variable, and is therefore not
  // known at compile-time.
  template<class ...Symm, class ...Tail>
  struct TIndexStructure<TSymmetry<Symm...>, int, Tail...>:
    public TIndexStructure<typename TSymmetry<Symm...>::Shift_t,
			   Tail...> {
    typedef TIndexStructure<typename TSymmetry<Symm...>::Shift_t,
			    Tail...> BASE;
    typedef typename BASE::TIndexSet_t TIndexSet_t;
    static const int Rank;
    static const int NFree=BASE::NFree;
    static const int NUniqueFree=TIndexSet_t::Size;
    static const bool IndicesAreUnique=(NFree==NUniqueFree);
    static const bool FirstIndexIsFree=false;

    TIndexStructure(const int fIdx, Tail... tail):
      BASE(tail...), mIdx(fIdx) { }
    void Reset() {
      BASE::Reset();
    }
    void operator++() {
      BASE::operator++();
    }
    explicit operator bool() const {
      // query directly terminating base-class TIndexStructure<>
      // see comment in TIndexStructure<> below
      return TIndexStructure<TSymmetry<> >::operator bool();
    }

    // Access
    int GetFirstIndex() const {
      return mIdx;
    }
    template<int N>
    int GetNthIndex() const {
      return GetNthBaseClass<N,
			     TIndexStructure<TSymmetry<Symm...>,
					     int, Tail...>
			     >::type::GetFirstIndex();
    }

    void GetAllIndices(IPoint& idx) const {
      if(idx.size()!=Rank) idx = std::vector<int>(Rank);
      Fill(idx.data());
    }

    void ConstructIndexKey(IPoint& idx_key) const{
      if(idx_key.size()!=Rank) idx_key = std::vector<int>(Rank);
      FillForKey(idx_key.data());
    }

    void GetFreeIndices(IPoint& freeIndices) const {
      if(freeIndices.size()!=Rank) freeIndices = std::vector<int>(Rank);
      FillFreeIndices(freeIndices.data());
    }


    int GetNFree() const{
      return NFree;
    }
    //--------------------------------
    // TCodeWriter interface
    //--------------------------------
    static std::string VarDeclaration() {
      return BASE::VarDeclaration();
    }

    static std::string CUDAVarDeclaration() {
      return BASE::CUDAVarDeclaration();
    }

    static IPoint Range() {
      return BASE::Range();
    }

    void ExpandFreeIndices(std::initializer_list<int>::const_iterator in,
			   IPoint::iterator out) const{
      *out=mIdx;
      BASE::ExpandFreeIndices(in, ++out);
   }


    // in tandem with TExpressionLeaf<iBinaryOp<...,DataMesh>>
  static std::string LoopIndexedExpression
  (const std::map<std::string,int>& FixedIndices=std::map<std::string,int>()) {
    return BASE::LoopIndexedExpression(FixedIndices);
  }

  static std::string CUDALoopIndexedExpression
  (size_t bracecount=0,
   const std::map<std::string,int>& FixedIndices=std::map<std::string,int>(),
   bool isTTDm = false) {
    return BASE::CUDALoopIndexedExpression(bracecount, FixedIndices,isTTDm);
  }
    // in tandem with TExpressionLeaf<iBinaryOp<...,DataMesh>>
    static std::string ForLoopsOverTensorIndices() {
      return BASE::ForLoopsOverTensorIndices();
    }
  private:
    int mIdx;
  protected:
    void Fill(int* ptr) const {
      *ptr=mIdx;
      BASE::Fill(ptr+1);
    }

    void FillForKey(int* ptr) const {
      *ptr=mIdx;
      BASE::FillForKey(ptr+1);
    }

  void FillFreeIndices(int* ptr) const {
      *ptr=mIdx;
      BASE::FillFreeIndices(ptr+1);
    }

  };


  // if this variable is defined inside the class specialization,
  // clang issues an "Undefined symbols" linker error.
  template<class ...Symm, class ...Tail>
  const int TIndexStructure<TSymmetry<Symm...>,
  			    int, Tail...>::Rank
  = 1+TIndexStructure<TSymmetry<Symm...>, int, Tail...>::BASE::Rank;


  //----------------------------------------------------------------
  // specialization for free first index:
  // ----------------------------------------------------------------
  // the implict index is represented by a TIndexSlot<dim, label,
  // offset>, which encodes at compile-time which index this is
  // (e.g. i_, k_), and its offset (e.g.  i_ vs.  i_+1).
  template<class ...Symm, int d, int l, int o, class ...Tail>
  struct TIndexStructure<TSymmetry<Symm...>, TIndexSlot<d,l,o>, Tail...>:
    public TIndexStructure<typename TSymmetry<Symm...>::Shift_t, Tail...> {

    typedef TIndexStructure<typename TSymmetry<Symm...>::Shift_t,
				 Tail...> BASE;
    typedef typename InsertIntoTIndexSet
      < TIndex<d, l>, typename BASE::TIndexSet_t>::type TIndexSet_t;

    typedef TIndexSlot<d,l,o> FirstSlotTIndex_t;
    typedef TIndex<d,l> FirstSlotTIndexBare_t;

    static const int Rank; // =1+BASE::Rank;
    static const int NFree=1+BASE::NFree;
    static const int NUniqueFree=TIndexSet_t::Size;
    static const bool IndicesAreUnique=(NFree==NUniqueFree);
    static const bool FirstIndexIsFree=true;

    TIndexStructure(const TIndexSlot<d,l,o> /* idx */, Tail... tail):
      BASE(tail...) { }

    // FIXME:  Should this be static?
    void Reset() {
      // iterating this slot is forwarded into its TIndex<d,l>
      TIndex<d,l>::Reset();
      BASE::Reset();
    }
    void operator++() {
      TIndex<d,l>::Increment();
      if(TIndex<d,l>::Done() ) {
	BASE::operator++();
	TIndex<d,l>::Reset(LowerBoundFirstIndex(*this));
      }
    }
    explicit operator bool() const {
      // query directly terminating base-class IndexStructure<>
      // see comment in TIndexStructure<> below
      return TIndexStructure<TSymmetry<> >::operator bool();
    }

    // Access
    int GetFirstIndex() const {
      return TIndexSlot<d,l,o>::Value();
    }
    template<int N>
    int GetNthIndex() const {
      return GetNthBaseClass<N,
			     TIndexStructure<TSymmetry<Symm...>,
					     TIndex<d,l>, Tail...>
			     >::type::GetFirstIndex();
    }
    void GetAllIndices(IPoint& idx) const {
      if(idx.size()!=Rank) idx = IPoint(Rank);
      Fill(idx.data());
    }

    void ConstructIndexKey(IPoint& idx_key) const{
      if(idx_key.size()!=Rank) idx_key = IPoint(Rank);
      FillForKey(idx_key.data());
    }
    void GetFreeIndices(IPoint& freeIndices) const{
      if(freeIndices.size()!=Rank) freeIndices = IPoint(Rank);
      FillFreeIndices(freeIndices.data());
    }

    // FIXME:  Is this needed?  (info available through Class::NFree
    int GetNFree() const{
      return NFree;
    }

    // FIXME:  There is no longer a contained TIndex
    //const TIndex<dim,label>& GetFirstTIndex() const { return mIdx; }


    //--------------------------------
    // TCodeWriter interface
    //--------------------------------
    static std::string VarDeclaration() {
      //return BASE::VarDeclaration()+"["+std::to_string(d)+"]";
      return "["+std::to_string(d)+"]"+BASE::VarDeclaration(); //reversed?
    }

    static std::string CUDAVarDeclaration() {
      if (BASE::CUDAVarDeclaration()=="")
        return std::to_string(d); //to avoid the comma
      else
        return std::to_string(d)+","+BASE::CUDAVarDeclaration();
    }

    static std::vector<int> Range() {
      std::vector<int> rng(BASE::Range());
      rng.insert(rng.begin(), d);
      return rng;
    }

    // FIXME:  add function descripton
    void ExpandFreeIndices(std::initializer_list<int>::const_iterator in,
			   IPoint::iterator out) const{
      *out= o + *in;
      BASE::ExpandFreeIndices(++in, ++out);
    }

    static std::string LoopIndexedExpressionCommon
      (const std::map<std::string,int>& 
        FixedIndices=std::map<std::string,int>()) {
        //FixedIndices: maps index letters to the bound values they are
        //to be replaced with (for sums)

        std::string idx = FirstSlotTIndexBare_t::VarName();
        if(FixedIndices.count(idx) == 1){ //key found
          idx=std::to_string(o + FixedIndices.at(idx));  
        }else{ 
          //if no replacement happened, this isn't a sum index so we
          //want the offset
          idx = FirstSlotTIndex_t::VarName();
        }
        return idx;
    }
    
        
        // in tandem with TExpressionLeaf<iBinaryOp<...,DataMesh>>
    static std::string LoopIndexedExpression
      (const std::map<std::string,int>& 
        FixedIndices=std::map<std::string,int>()) {
        std::string idx = LoopIndexedExpressionCommon(FixedIndices);
        return
            "["+idx+"]"+BASE::LoopIndexedExpression(FixedIndices);
    }

    static std::string CUDALoopIndexedExpression
      (size_t bracecount=0,
        const std::map<std::string,int>& FixedIndices=
                                  std::map<std::string,int>(),
        bool isTTDm = false) {
        
        std::string idx = LoopIndexedExpressionCommon(FixedIndices);
        
        //check if we are at the bottom
        if(!isTTDm && (BASE::CUDALoopIndexedExpression
          (bracecount, FixedIndices, isTTDm)).empty()){
            std::string closing_braces(bracecount,')');
            return idx+closing_braces;
        }else{
          ++bracecount;
          return
            idx+"+4*("+
              BASE::CUDALoopIndexedExpression(bracecount, FixedIndices, isTTDm);
        }
    }

  protected:
    void Fill(int* ptr) const {
      *ptr=TIndexSlot<d,l,o>::Value();
      BASE::Fill(ptr+1);
    }

    void FillForKey(int* ptr) const{
      *ptr = d;
      BASE::FillForKey(ptr+1);
    }


    void FillFreeIndices(int* ptr) const{
      *ptr = -1;
      BASE::FillFreeIndices(ptr+1);
    }
  };


  // if this variable is defined inside the class specialization,
  // clang issues an "Undefined symbols" linker error.
  template<class ...Symm, int d, int l, int o, class ...Tail>
  const int TIndexStructure<TSymmetry<Symm...>,
			    TIndexSlot<d,l,o>, Tail...>::Rank
  =1+TIndexStructure<TSymmetry<Symm...>,
		     TIndexSlot<d,l,o>, Tail...>::BASE::Rank;


  //================================================================
  // LowerBoundFirstIndex
  //================================================================

  template<class TIndexStructure_t>
  struct LowerBoundFirstIndexHelper {
    // The general template does not account for symmetries.  Symmetries
    // are handled by specializations below.
    static int get(const TIndexStructure_t&) { return 0; }
  };


  // Specialization: The first index **is** part of a tensor-symmetry,
  // and it **is** a TIndex:
  template<int pos2, class ... TailSymm,
	   int d, int l, int o, class ...Tail>
  struct LowerBoundFirstIndexHelper<
    TIndexStructure<TSymmetry<TInequality<0,pos2>,
			      TailSymm...>,
		    TIndexSlot<d,l,o>, Tail...>
    > {
    typedef TIndexStructure<TSymmetry<TInequality<0,pos2>,
				      TailSymm...>,
			    TIndexSlot<d,l,o>, Tail...> this_type;

    static int get(const this_type& /* t  don't need the instance anymore */ ) {
      // downcast the TIndexStructure to the TIndexStructure that begins
      // with the other index that is part of the TInequality (i.e. pos2):
      typedef typename GetNthBaseClass<pos2,this_type>::type base_type;

      // assert that the other index of this TInequality is also free:
      static_assert(base_type::FirstIndexIsFree,
		    "Encountered a tensor-symmetry between a free TIndex"
		    " and a fixed index.  This is not allowed.");

      // sanity check: make sure both TensorIndices have the same offset.
      // (otherwise, don't know what to do)
      static_assert(this_type::FirstSlotTIndex_t::Offset
		    ==base_type::FirstSlotTIndex_t::Offset,
		    "Attempting to enforce Tensor-Symmetry between indices\n"
		    "that have different Offset's.  I don't know what the\n"
		    "correct behavior should be in this case, so please \n"
		    "don't do this.");

      // the lower bound of the (first) index is the current value of the
      // second index in the pair:
      // FIXME: This function used to return loop-variable+offset.
      // has the chaneg to just 'loop-variable' been taken into
      // account in TLoopApply?
      return base_type::FirstSlotTIndex_t::Value();
    };
  };



  template<class ...Symm, class ...Tail>
  int LowerBoundFirstIndex(const TIndexStructure<TSymmetry<Symm...>,
			   Tail...>& t) {
    return LowerBoundFirstIndexHelper<TIndexStructure<TSymmetry<Symm...>,
						      Tail...> >::get(t);
  };

}

#endif
