///
/// \file
/// Defines MeshIndex.

#ifndef MeshIndex_hpp
#define MeshIndex_hpp

#include "IPoint.hpp" 
//#include "Utils/MyContainers/MyVector.hpp" 
class Mesh;


/// MeshIndex indexes a Mesh.
/// It can be used to iterate over the indices of a Mesh with the
/// following syntax: \code
///   for(MeshIndex index(aMesh);index;++index) {
///     do_something_at_index(index);
///   }
/// \endcode
class MeshIndex: public IPoint {
public:	
  explicit MeshIndex(const Mesh & rMesh);
  // default copy c'tor, operator= and destructor ok
public:

  /// Is the end reached? 
  operator void* () const { return mValid;}

  /// Advance to next gridpoint.
  MeshIndex & operator++();

  /// DataOffset of the current gridpoint. 
  int DataOffset() const { return mOffset; }
private:
  IPoint mExtents;
  const int mDim;
  int mOffset;
  void* mValid;
}; 

//============================================================================

inline MeshIndex & 
MeshIndex::operator++()
{
  if(0==mDim) mValid = 0; // Otherwise it would cycle forever!
  for(size_t d=0;d<mDim;++d) {
    ++((*this)[d]);
    if((*this)[d]<mExtents[d]) break;
    (*this)[d] = 0;
    if(mDim-1==d) mValid = 0; // Otherwise it would cycle forever!
  }
  ++mOffset;
  return *this;
}

//============================================================================

// GetMeshIndexFromOffset return the Index of a point given its offset
// and the extents of the mesh it is a part of.
IPoint GetMeshIndexFromOffset(const int offset,const IPoint MeshExtent);



#endif  // MeshIndex_hpp
