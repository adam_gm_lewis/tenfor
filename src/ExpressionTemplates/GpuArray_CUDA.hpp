///
/// \file
/// Defines class GpuArray.

#ifndef GpuArray_CUDA_hpp
#define GpuArray_CUDA_hpp
#include "Mesh.hpp"
#include <ostream>
#include <vector>
/// Holds an array of doubles of size sz, and synchronizes it
/// between Host and Device as needed.
///
/// if NONACCEL or ACCEL_CPU, keeps only one copy of data, and returns 
/// it through HostData() and DeviceData().  Otherwise, keeps
/// two separate copies, and synchronizes back and forth.
class GpuArray {
public:
  GpuArray();
  /// initializes device-data to zero, does not allocate host-data
  GpuArray(const int sz);

  /// copies into device-data, does not set host-data.
  GpuArray(const GpuArray& rhs);

  //swap!
  friend void swap(GpuArray& first, GpuArray& second);

  //Move constructor.
  GpuArray(GpuArray&& rhs);
  /// Assignment operator - calls the copy constructor for lvalues,
  // move constructor for rvalues.
  GpuArray & operator=(GpuArray rhs);
  
  
  ~GpuArray();

  // resizes device-data, and sets the data to zero.  If size changes,
  // de-allocated host-data, otherwise marks host-data as invalid and
  // leaves it.
  void Resize(const int new_size);

  int Size() const { return mSz; }
  /*
  const double* HostData() const {
    if(mHostOutdated) CopyToHost();
    return mpHostData;
  }
  
  double* HostDataNonConst() {
    if(mHostOutdated) CopyToHost();
    mDeviceOutdated=true; // non-const access invalidates other side
    return mpHostData;
  }

  const double* DeviceData() const {
    if(mDeviceOutdated) CopyToDevice();
    return mpDeviceData;
  }
  double* DeviceDataNonConst() {
    if(mDeviceOutdated) CopyToDevice();
    mHostOutdated=true; // non-const access invalidates other side
    return mpDeviceData;
  }
  */
  
  const double* HostData() const;
  double* HostDataNonConst();
  const double* DeviceData() const;
  double* DeviceDataNonConst();

  // this function is called in assignments LHS=...; 
  // in such a case there is no need to synchronize data
  // from the host. 
  double* PossiblyInvalidDeviceDataToOverwrite() {
    mHostOutdated=true; // non-const access invalidates other side
    mDeviceOutdated=false;
    return mpDeviceData;
  }

  /// Returns an element of the array on the host without
  /// checking whether HostData is synchronized, and without
  /// access-checking.  To be used inside checked expression-templates,
  /// to avoid point-by-point asserts.
  const double& UncheckedHostAccess(const int i) const {
    return mHostData[i];
  }


  std::ostream& operator<<(std::ostream& out) const {
    out << "sz=" << mSz;
    out << " HostData " << (mHostOutdated ? "outdated" : "up-to-date")
	<< ", DeviceData " << (mDeviceOutdated ? "outdated" : "up-to-date");
    if(!mHostOutdated)
      out << ", HostData=(" << mHostData[0] << ", ..., " << mHostData[mSz-1]
	  << ")";
    return out;
  }
  void UseANewPointerAsDeviceData(double* new_pointer, const int size); 
  void Sync() const; //Ensures host and device data are sync'd
 
  //Interface with DM = double
  void AssignConstant(const double s);
  void AddConstant(const double s);
  void SubtractConstant(const double s);
  void MultiplyByConstant(const double s);
  void DivideByConstant(const double s);

private:
  void CopyToDevice() const;
  void CopyToHost() const;
  int mSz; // size TODO: make this a size_t 
  //mutable double* mpHostData; // pointer=0 means data NOT allocated on host
  //mutable std::unique_ptr<double[]> mpHostData;
  mutable std::vector<double> mHostData;
  mutable double* mpDeviceData; 
  mutable bool mHostOutdated, mDeviceOutdated;
};  


#endif  
