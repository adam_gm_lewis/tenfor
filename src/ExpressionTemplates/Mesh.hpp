///
/// \file
/// Defines Mesh.

#ifndef Mesh_hpp
#define Mesh_hpp

//#include <iosfwd>
#include <ostream>
#include <vector>
#include "OutputVector.hpp"
#include "IPoint.hpp"
#include "NameOf.hpp"

/// Represents a regular grid in d dimensions.
/// 
/// Mesh represents a regular grid in d dimensions.  
///
/// A Mesh is constructed from an IPoint representing the extents, and knows
///  -# Its dimension.
///  -# Its extents.
///  -# Its total number of points.
/// 
///
/// ASSUMPTION:  In each dimension, the indices run from [0,extent(d)-1]
/// We could generalize Mesh by allowing arbitrary lower and upper bounds.
class Mesh {
public:	
  // Constructors and destructor:
  explicit Mesh(const IPoint& rExtents);
  // default copy constructor and operator= ok

  // There's some argument still whether the destructor should be
  // virtual.  We never use Mesh to act "virtually" on derived
  // classes.  Harald believes the situation we have here in SpEC represents
  // the exception to the "base-class destructors should be
  // private"-rule, listed in Alexandescu & Sutter, "C++ coding
  // standards", and so should not be virtual
  virtual ~Mesh() {};

  // Public Access and Inquiry:
  const IPoint& Extents() const {return mExtents;}
  int Dim() const {return mDim;}
  int Size() const {return mSize;}
  /// If the data on the Mesh is stored in C-order, what is the offset of 
  /// the data-point pt relative to the first element?  
  int DataOffset(const IPoint&pt) const;
private:
  // Private Member Data:
  IPoint mExtents;
  int mDim;
  int mSize;
};  // class Mesh

REGISTER_NAME(Mesh);

inline bool operator==(const Mesh& lhs, const Mesh& rhs) {
  return lhs.Extents()==rhs.Extents();
} 

inline bool operator!=(const Mesh& lhs, const Mesh& rhs) {
  return !(lhs==rhs);
} 

inline std::ostream& operator<<(std::ostream& os, const Mesh& d) {
  OutputVector(os, d.Extents());
  //const IPoint& ext = d.Extents();
  //const std::size_t sz = ext.size();
  //os << "(";
  //if (sz>=1) {
    //os << ext[0];
    //for(std::size_t i=1; i<sz; ++i) os << "," << ext[i];
  //}
  //os << ")";
  return os;
}


//============================================================================

#endif  // Mesh_hpp
