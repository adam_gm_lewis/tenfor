#ifndef PartialSum_hpp
#define PartialSum_hpp

namespace ImplicitTensorLoops {

  //--------------------------------
  // Tag for a Sum
  //--------------------------------
  struct SumOp { };


  //--------------------------------
  // Helper-class PartialSum
  //--------------------------------

  // Compile-time loop unrolling within Sum(i_, expression), by
  // recursively evaluating the Sum-TIndex.
  //
  // curr_dim    -- current value of the Sum-TIndex
  // TIndex_t    -- the TIndex being summed over
  // iBinaryOp_t -- the expression being summed
  //
  // This template is used for two distinct purposes:
  //
  // (1) PartialSum<...>::ExpandIndices_t
  //     is the type of the unrolled Sum.  This type is used
  //     in iBinaryOp.
  //
  // (2) An instance PartialSum<...> theSum;
  //     during construction, theSum() will construct internal
  //     helper-variables to capture the entire expanded BinaryOp.
  //     This is functionality used by BinaryOpHolder.
  //

  // Recursion
  //
  // handle TIindex<dim,label>::Value()==curr_dim, then recurse to
  // curr_dim+1.
  template<int curr_dim, class TIndex_t, class iBinaryOp_t>
  struct PartialSum: public PartialSum<curr_dim-1,TIndex_t,iBinaryOp_t> {

    // preceding terms (ie. lower values of curr_dim)
    typedef PartialSum<curr_dim-1,TIndex_t,iBinaryOp_t> BASE;

    // placing BASE (i.e. terms with lower values of curr_dim) first
    // gives consistency with C++ associativity rules
    typedef BinaryOp<typename BASE::ExpandIndices_t,
		     AddOp,
		     typename iBinaryOp_t::ExpandIndices_t> ExpandIndices_t;

    // constructor constructs BinaryOpHolders, which also expand
    // all free TIndex'es to DataMesh'es for subsequent
    // evaluation of the BinaryOp 'partial_sum'
    PartialSum(const iBinaryOp_t& summand):
      BASE(summand), // fill preceding terms
      this_term(
		// comma operator sets TIndex
		// before capture into 'this_term'
		(TIndex_t::Reset(curr_dim), summand)
		),
      partial_sum(BASE::partial_sum,this_term.op)
    { };

    const BinaryOpHolder<iBinaryOp_t> this_term;
    const ExpandIndices_t partial_sum;
  };


  // break recursion:  first term, curr_dim=0
  template<class TIndex_t, class iBinaryOp_t>
  struct PartialSum<0, TIndex_t, iBinaryOp_t> {
    typedef typename iBinaryOp_t::ExpandIndices_t ExpandIndices_t;

    PartialSum(const iBinaryOp_t& summand):
      this_term(
		// comma operator sets TIndex
		// before capture into 'this_term'
		(TIndex_t::Reset(0), summand)
		),
      partial_sum(this_term.op)
    { };

    const BinaryOpHolder<iBinaryOp_t> this_term;
    const ExpandIndices_t& partial_sum; // ref ok, BinaryOpHolder has instance
  };


  //================================================================
  // iBinaryOp specialization for a Sum
  //================================================================
  //
  //
  // The first template argument L=TIndex<dim,value> indicates
  // the TIndex<> to be summed over
  //
  template<int dim, int label, class L, class Op, class R>
  struct iBinaryOp<TIndex<dim,label>, SumOp,  iBinaryOp<L,Op,R>> {
    typedef
      typename RemoveFromTIndexSet<TIndex<dim,label>,
				   typename iBinaryOp<L,Op,R>::TIndexSet_t
				   >::type TIndexSet_t;

    // when indexing, return the unrolled loop
    // A(i_)*B(i_) -> A(0)*B(0)+A(1)*B(1)+A(2)*B(2)
    typedef
      typename PartialSum<dim-1, TIndex<dim, label>,
                         iBinaryOp<L,Op,R> >::ExpandIndices_t ExpandIndices_t;

    iBinaryOp(const iBinaryOp<L,Op,R>& op):
      rhs(op) { };
    ~iBinaryOp() { }

    const iBinaryOp<L,Op,R>& rhs;
  };

  //================================================================
  // BinaryOpHolder specialization for a Sum
  //================================================================
  //
  template<int dim, int label, class L, class Op, class R>
  struct BinaryOpHolder<iBinaryOp<TIndex<dim,label>,
				  SumOp, iBinaryOp<L,Op,R> > > {
    typedef iBinaryOp<TIndex<dim, label>,
		      SumOp, iBinaryOp<L,Op,R> > iBinaryOp_t;

    BinaryOpHolder(const iBinaryOp_t& iop):
      // instantiation of PartialSum automatically constructs
      // sequence of BinaryOpHolders inside PartialSum
      sum(iop.rhs),
      // capture the complete unrolled sum into standard name
      op(sum.partial_sum)
    { };

    const PartialSum<dim-1, TIndex<dim, label>, iBinaryOp<L, Op, R> > sum;
    const typename iBinaryOp_t::ExpandIndices_t& op;
  };


  //================================================================
  // Sum(i_, iBinaryOp<...>)
  //================================================================
  //
  // function to create a Sum in user code
  // i.e. this causes the user code "Sum i_, iBinaryOp<...>" to construct and
  // return an iBinaryOp<TIndex<dim,label>, SumOp,iBinaryOp<L,Op,R>>(op);
  // which is the argument to the correct CodeWritten function performing the
  // sum.
  template<int dim, int label, class L, class Op, class R>
  iBinaryOp<TIndex<dim,label>, SumOp,  iBinaryOp<L,Op,R>>
    Sum(const TIndex<dim, label>&, const iBinaryOp<L, Op, R>& op) {
    static_assert(IsTIndexInTIndexSet<TIndex<dim, label>,
		  typename iBinaryOp<L, Op, R>::TIndexSet_t>::value,
		  "Invalid summation index ");
    return iBinaryOp<TIndex<dim,label>, SumOp,  iBinaryOp<L,Op,R>>(op);
  }


  // // ==== Sum over two indices ====
  // template<int dim1, int label1, int dim2, int label2,
  // 	   class L, class Op, class R>
  // iBinaryOp<TIndex<dim1,label1>, SumOp,
  // 	    iBinaryOp<TIndex<dim2,label2>, SumOp, iBinaryOp<L,Op,R> > >
  // Sum(const TIndex<dim1, label1>& i1,
  //     const TIndex<dim2, label2>& i2,
  //     const iBinaryOp<L, Op, R>& op) {
  //  ERROR - ERROR - ERROR
  // This fails, because the inner Sum() returns a temporary, so the outer
  // Sum() keeps a reference to a temporary.
  //   return Sum(i1, Sum(i2, op));
  // }


}

#endif
