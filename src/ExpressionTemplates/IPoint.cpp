#include "IPoint.hpp"
#include "Assert.hpp"
//============================================================================
int ComputeIndex(const IPoint& Index, const IPoint& Extents)
{
  const int Dim = Index.size();
  ASSERT(Dim==Extents.size(),"Dim of Index = " << Dim << 
         ", Dim of Extents = " << Extents.size());

  // Return zero for an empty Index.
  if(Dim==0) return 0;

  ASSERT(0<=Index.back()&& Index.back()<Extents.back(),
	 "ComputeIndex: Given index outside of Extents. Index.back()=" 
	 << Index.back() << ", Extents.back()=" << Extents.back() );

  int Offset = Index[Dim-1];
  for(int d=Dim-2;d>=0;--d) {
    ASSERT(0<=Index[d]&&Index[d]<Extents[d],
	   "ComputeIndex: Given index outside of Extents. Index[d]=" 
	   << Index[d] << ", Extents[d]=" << Extents[d] << ", d=" << d );
    Offset *= Extents[d];
    Offset += Index[d];
  }
  return Offset;
}
//============================================================================
const IPoint ComputeIndex(const int offset, const IPoint& Extents) {
  IPoint i(Extents.size(),0);
  int o0=offset;
  for(int d=0;d<Extents.size()-1;++d) {
    i[d] = o0 % Extents[d];
    o0 -= i[d];
    o0 /= Extents[d];
  }
  i.back()=o0;
  //COMMENTED_OUT_<<
  //ASSERT(offset==ComputeIndex(i,Extents),
      //"wrong index construction: offset="
      //<<offset << ", got " << ComputeIndex(i,Extents)<<", i="<<i<<", extents="<<Extents);
  return i;
}
//============================================================================
bool IsComputeIndexValid(const IPoint& Index, const IPoint& Extents) {
  const int Dim = Index.size();
  if(Dim!=Extents.size()) return false;
  if(Dim==0) return true;
  
  if(0>Index.back() || Index.back()>=Extents.back()) return false;
  
  for(int d=Dim-2;d>=0;--d) {
    if(0>Index[d]||Index[d]>=Extents[d]) return false;
  }
  return true;
}
//============================================================================
