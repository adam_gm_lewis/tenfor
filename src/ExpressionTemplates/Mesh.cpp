#include "Mesh.hpp" 
#include "Assert.hpp"

//============================================================================

Mesh::Mesh(const IPoint & rExtents) 
  : mExtents(rExtents), 
    mDim(rExtents.size()), 
    mSize(1)
{
  for(int d=0;d<mDim;++d) {
    ASSERT(0<rExtents[d],"Invalid Extents[" << d << "] = " << rExtents[d]);
    mSize *= rExtents[d];
  }
}

//============================================================================

int Mesh::DataOffset(const IPoint& pt) const 
{
  ASSERT(pt.size()==mDim, 
	 "Dimension mismatch, pt.Dim()=" << pt.size() << ", mDim=" << mDim);
	 
  return ComputeIndex(pt, mExtents); 
}
