///
/// \file
/// Defines class GpuArray.

#ifndef GpuArray_hpp
#define GpuArray_hpp


#if defined(ACCEL_CUDA) 
  #include "GpuArray_CUDA.hpp"  
#else
  #include "GpuArray_CPU.hpp"
#endif



#endif  
