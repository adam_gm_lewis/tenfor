#include "CudaMemoryHandler.hpp"
// #include "GpuStatistics.hpp"
#include <cstring>
#include <cmath>
#include "Mesh.hpp"
#include "cuda_wrapper.hpp"
#include "GpuArray_CUDA_kernels.hpp"
#include "GpuArray_CUDA.hpp"
#include "Assert.hpp"
#include "Require.hpp"
//=================================================================
GpuArray::GpuArray()
  : mSz(0), 
    mHostData(0), 
    mpDeviceData(0),
    //AllocateMemOnDevice(sz)),
    mHostOutdated(true), mDeviceOutdated(false) //setting both sides false
                                                 //avoids copying uninitialized
                                                 //data around
{
  // GpuStatisticsNew(0, sz);
}
//=================================================================
//TODO: sz should be a size_t
GpuArray::GpuArray(const int sz)
  : mSz(sz), 
    mHostData(), 
    mpDeviceData(static_cast<double*>((CudaMemAllocate(sz*sizeof(double)))) ),
    //AllocateMemOnDevice(sz)),
    mHostOutdated(true), mDeviceOutdated(false) //setting both sides false
                                                 //avoids copying uninitialized
                                                 //data around
{
  // GpuStatisticsNew(0, sz);
}

//=================================================================

GpuArray::GpuArray(const GpuArray & rhs)
  : mSz(rhs.mSz), 
    mHostData(), 
    mpDeviceData(static_cast<double*>(CudaMemAllocate(rhs.mSz*sizeof(double)))),
    //mpDeviceData(AllocateMemOnDevice(rhs.mSz)),
    mHostOutdated(true), 
    mDeviceOutdated(rhs.mDeviceOutdated)
{
  // if rhs is only available on host, then copy to device.
  // This happens if rhs was filled via DataMesh::NonConstData()
  // (basic philosphy: Move data to GPU whenever suitable.  If
  // we didn't do this copy, and rhs is used multiple times,
  // it would always be copied on the host only). 
  if(rhs.mDeviceOutdated && (!rhs.mHostOutdated)) {
    rhs.CopyToDevice();
  }

  // copy device-data, if up-to-date
  if(!rhs.mDeviceOutdated) {
    std::string& errstring = GPU::errstring(); 
    bool err=GPU::dMemcpyDeviceToDevice(mpDeviceData,
	       rhs.mpDeviceData, mSz*sizeof(double), errstring);
    REQUIRE(err,errstring);
    mDeviceOutdated=false;
  }

  // do NOT allocate host-data.  Do so only on demand when data is
  // retrieved.
  
  //  GpuStatisticsNew(0, mSz);
}



//=================================================================
//copy and swap
//=================================================================
void swap(GpuArray& first, GpuArray& second){
  using std::swap;
  swap(first.mSz,second.mSz);
  swap(first.mHostData,second.mHostData);
  swap(first.mpDeviceData,second.mpDeviceData);
  swap(first.mHostOutdated,second.mHostOutdated);
  swap(first.mDeviceOutdated,second.mDeviceOutdated);
}

GpuArray& GpuArray::operator=(GpuArray rhs) {
  swap(*this,rhs);
  return(*this);
} 


//move constructor
GpuArray::GpuArray(GpuArray&& rhs)
  :GpuArray()
{
  swap(*this,rhs);
} 
//================================================================

GpuArray::~GpuArray() {
  //GPU::dSynchronize();
  CudaMemRelease(mpDeviceData, mSz*sizeof(double));
  mHostData.clear();
  //  FreeMemOnDevice(mpDeviceData);
  //if(mpHostData!=0) {
  //  std::cout << "if" << std::endl;
  //  delete[] mpHostData;
  //  std::cout << "after if" << std::endl;
  //  mpHostData=0;
    // GpuStatisticsDestroy(mSz, mSz);
  //} else {
    // GpuStatisticsDestroy(0, mSz);
  //}
   
}

//================================================================

void GpuArray::Resize(const int new_size) {
  if(new_size!=mSz) {
    // do statistics before reset of mSz
    // GpuStatisticsResizeHost(mSz, 0);
    // GpuStatisticsResizeDevice(mSz, new_size);
    //if(mpHostData!=0){
      //delete[]  mpHostData;  
    //  mpHostData=0;
    //}
    //mpHostData = std::unique_ptr<double[]>(0);
    mHostData = std::vector<double>();
    CudaMemRelease(mpDeviceData, mSz*sizeof(double));
    //FreeMemOnDevice(mpDeviceData);
    mSz=new_size;
    mpDeviceData=static_cast<double*>(CudaMemAllocate(mSz*sizeof(double)));
    //mpDeviceData=AllocateMemOnDevice(mSz);
    mDeviceOutdated=false;
    mHostOutdated=true;
  }
}

//================================================================

void GpuArray::CopyToDevice() const {
  ASSERT(mHostData.size()!=0, 
	 "Attempting to copy data to Device which was NOT allocated on Host");
  ASSERT(!mHostOutdated, 
	 "Attempting to copy outdated data to Device");

  bool err;
  std::string& errstring = GPU::errstring();
  #ifdef ACCEL_DEBUG
  GPU::dSynchronize();
  err = GPU::dLastError(errstring);
  REQUIRE(err, errstring);
  #endif 
  
  err = GPU::dMemcpyHostToDevice(mpDeviceData,mHostData.data(),
                    mSz*sizeof(double), errstring);
  REQUIRE(err,errstring);
  mDeviceOutdated=false; 
  //std::cout << "CopyToDevice, sz=" << mSz << ", data[0]=" << mpHostData[0]
  //  	    << std::endl;
  //REQUIRE(fabs(mpHostData[0])<1e20, "big number");  // gdb> break MyAbort 
  
  // GpuStatisticsCopyToDevice(mSz);
}

//================================================================

void GpuArray::CopyToHost() const {
  ASSERT(!mDeviceOutdated, 
	 "Attempting to copy outdated data to Device");
  if(mHostData.size()==0) {
    mHostData=std::vector<double>(mSz,0.);
    // GpuStatisticsResizeHost(0, mSz);
  }
  bool& err = GPU::err(); 
  std::string& errstring = GPU::errstring(); 
  
  #ifdef ACCEL_DEBUG
  GPU::dSynchronize();
  err = GPU::dLastError(errstring);
  REQUIRE(err, errstring);
  #endif 

  //mpHostData[mSz-2] = 4.;
  //CudaMemRelease(mpDeviceData, mSz*sizeof(double));
  //mpDeviceData=static_cast<double*>((CudaMemAllocate(mSz*sizeof(double))));
  
  err = GPU::dMemcpyDeviceToHost(mHostData.data(),mpDeviceData,
                mSz*sizeof(double), errstring);
  REQUIRE(err,errstring);

  mHostOutdated=false;	
  //std::cout << "CopyToHost, sz=" << mSz << ", data[0]=" << mpHostData[0]
  //          << std::endl;
  // GpuStatisticsCopyToHost(mSz);
}

void GpuArray::Sync() const {
  if (mHostOutdated) CopyToHost();
  if (mDeviceOutdated) CopyToDevice();
}


//Changes the value of mpDeviceData.
//This allows GPU data to be manually copied in-place.
//Can dramatically improve performance but USE WITH EXTREME CAUTION

void GpuArray::UseANewPointerAsDeviceData(double* new_pointer, 
        const int size)
{   
     REQUIRE(size==mSz, 
        "Error: tried to make a GpuArray's pointer to device"
        <<" point to an array of the wrong size!");
     CudaMemRelease(mpDeviceData, mSz*sizeof(double));
     GPU::dSynchronize();
     mpDeviceData = new_pointer;
     mHostOutdated=true;
     mDeviceOutdated=false;    
}

//============================================================================== 
//ACCESS
//============================================================================== 
const double* GpuArray::HostData() const {
  if(mHostOutdated) CopyToHost();
  return mHostData.data();
}

double* GpuArray::HostDataNonConst() {
  if(mHostOutdated) CopyToHost();
  mDeviceOutdated=true; // non-const access invalidates other side
  return mHostData.data();
}

const double* GpuArray::DeviceData() const {
  if(mDeviceOutdated) CopyToDevice();
  return mpDeviceData;
}
double* GpuArray::DeviceDataNonConst() {
  if(mDeviceOutdated) CopyToDevice();
  mHostOutdated=true; // non-const access invalidates other side
  return mpDeviceData;
}
//============================================================================== 

//================================================================
//sets the device to some value
void GpuArray::AssignConstant(const double s) {
    
  //std::cout << "SetToValue" << std::endl;
    const int threadsperblock = 192;
    int nblocks = mSz/threadsperblock;
    if (mSz % threadsperblock) ++nblocks;
    
    AssignConstantWrapper(mpDeviceData, s, mSz,nblocks,threadsperblock);
    
    #ifdef ACCEL_DEBUG
    std::string& errstring = GPU::errstring(); 
    GPU::dSynchronize();
    bool err = GPU::dLastError(errstring);
    REQUIRE(err,errstring);
    #endif
    mHostOutdated = true;
    mDeviceOutdated = false;  
}



void GpuArray::AddConstant(const double s) {
    
  //std::cout << "SetToValue" << std::endl;
    if(mDeviceOutdated) CopyToDevice();
  
    const int threadsperblock = 192;
    int nblocks = mSz/threadsperblock;
    if (mSz % threadsperblock) ++nblocks;
    
    AddConstantWrapper(mpDeviceData, s, mSz,nblocks,threadsperblock);
    
    #ifdef ACCEL_DEBUG
    std::string& errstring = GPU::errstring(); 
    GPU::dSynchronize();
    bool err = GPU::dLastError(errstring);
    REQUIRE(err,errstring);
    #endif
    mHostOutdated = true;
    mDeviceOutdated = false;  
}


void GpuArray::SubtractConstant(const double s) {
    
  //std::cout << "SetToValue" << std::endl;
    if(mDeviceOutdated) CopyToDevice();
    const int threadsperblock = 192;
    int nblocks = mSz/threadsperblock;
    if (mSz % threadsperblock) ++nblocks;
    
    SubtractConstantWrapper(mpDeviceData, s, mSz,nblocks,threadsperblock);
    
    #ifdef ACCEL_DEBUG
    std::string& errstring = GPU::errstring(); 
    GPU::dSynchronize();
    bool err = GPU::dLastError(errstring);
    REQUIRE(err,errstring);
    #endif
    mHostOutdated = true;
    mDeviceOutdated = false;  
}

void GpuArray::MultiplyByConstant(const double s) {
    
  //std::cout << "SetToValue" << std::endl;
    if(mDeviceOutdated) CopyToDevice();
    const int threadsperblock = 192;
    int nblocks = mSz/threadsperblock;
    if (mSz % threadsperblock) ++nblocks;
    
    MultiplyByConstantWrapper(mpDeviceData, s, mSz,nblocks,threadsperblock);
    
    #ifdef ACCEL_DEBUG
    std::string& errstring = GPU::errstring(); 
    GPU::dSynchronize();
    bool err = GPU::dLastError(errstring);
    REQUIRE(err,errstring);
    #endif
    mHostOutdated = true;
    mDeviceOutdated = false;  
}

void GpuArray::DivideByConstant(const double s) {
    
  //std::cout << "SetToValue" << std::endl;
    if(mDeviceOutdated) CopyToDevice();
    const int threadsperblock = 192;
    int nblocks = mSz/threadsperblock;
    if (mSz % threadsperblock) ++nblocks;
    ASSERT(s!=0, "Divide by zero error!"); 
    AddConstantWrapper(mpDeviceData, s, mSz,nblocks,threadsperblock);
    
    #ifdef ACCEL_DEBUG
    std::string& errstring = GPU::errstring(); 
    GPU::dSynchronize();
    bool err = GPU::dLastError(errstring);
    REQUIRE(err,errstring);
    #endif
    mHostOutdated = true;
    mDeviceOutdated = false;  
}
