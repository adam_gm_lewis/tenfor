#ifndef TIndex_hpp
#define TIndex_hpp


#include <string>

namespace ImplicitTensorLoops {

  // represents a tensor-index, which ranges from 0 up to value
  // 'dim-1'.  The second template argument 'label' distinguishes
  // different indices of the same dim.
  template<int dim, int label>
  class TIndex {
  public:
    // default constructors/destructors etc ok.

    static int Value() { return mValue; }
    // ctr!=0 needed for looping over symmetric variables
    static void Reset(const int ctr=0) { mValue=ctr; }

    static void Increment() { ++mValue; }

    // iteration over all values complete?
    static bool Done() {
      return mValue>=dim;
    };

    // TCodeWriter interface
    static std::string VarName();


  private:
    static int mValue;
  };


  template<int dim, int label>
  int TIndex<dim,label>::mValue;

}


#endif
