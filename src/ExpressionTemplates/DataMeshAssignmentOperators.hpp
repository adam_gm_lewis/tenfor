#ifndef DATA_MESH_ASSIGNMENT_OPERATORS_HPP
#define DATA_MESH_ASSIGNMENT_OPERATORS_HPP

//This file contains assignment operators for
//lhs: DataMesh, rhs: DataMesh, double, BinaryOp, iBinaryOp.
//All operators call ImplicitTensorLoops::TLoopApply, simplifying the automatic
//generation of expression template code.
//It is to be #included directly from *inside* the public member functions
//of DataMesh.hpp
//===============================================================
//Construct from double
//===============================================================
DataMesh(const Mesh & rMesh, const double s)
  : Mesh(rMesh),
    mData(rMesh.Size())
{
  //mData.AssignConstant(s);
  if (!IsEmpty())
    ImplicitTensorLoops::TLoopApply(*this,ImplicitTensorLoops::TSetEqualOp(),s);

  AddToSizeCounter(rMesh.Size());
}

//These catch calls that otherwise would be misinterpreted as functors
DataMesh(const Mesh & rMesh, const int s)
  : Mesh(rMesh),
    mData(rMesh.Size())
{
  
  if (!IsEmpty())
    ImplicitTensorLoops::TLoopApply(*this,ImplicitTensorLoops::TSetEqualOp(),static_cast<double>(s));
  //mData.AssignConstant(static_cast<double>(s));
  AddToSizeCounter(rMesh.Size());
}

DataMesh(const Mesh & rMesh, const float s)
  : Mesh(rMesh),
    mData(rMesh.Size())
{
  
  if (!IsEmpty())
    ImplicitTensorLoops::TLoopApply(*this,ImplicitTensorLoops::TSetEqualOp(),static_cast<double>(s));
  //mData.AssignConstant(static_cast<double>(s));
  AddToSizeCounter(rMesh.Size());
}

//================================================================
// Construct from a functor
//================================================================
template<typename func>
DataMesh(const Mesh& rMesh, func& functor):
  Mesh(rMesh),
  mData(rMesh.Size())
{
  // initialize each element with a call to fun():
  double* ptr=mData.HostDataNonConst();
  for(int a=0; a<Size(); ++a)
    ptr[a] = functor();
  AddToSizeCounter(rMesh.Size());
}


//===============================================================
// operator =
//===============================================================

DataMesh& operator=(const double s) {
  ASSERT(!IsEmpty(), "Assigned double to empty DM");
  
  ImplicitTensorLoops::TLoopApply(*this,ImplicitTensorLoops::TSetEqualOp(),s);
  //mData.AssignConstant(s);
  return *this;
}

template<class LHS, class OP, class RHS>
DataMesh& operator=(const BinaryOp<LHS,OP,RHS>& op) {
  const std::vector<int>& extents=ExtentsFromBinaryOp(op);
  if(extents!=Mesh::Extents() || IsEmpty()) {
    const Mesh newmesh(extents);
    const int oldSize=mData.Size(); // Differs from Mesh::Size() for empty DM
    AddToSizeCounter(newmesh.Size()-oldSize);
    Mesh::operator=(newmesh);
    mData.Resize(Mesh::Size());
  }
  if (!IsEmpty())
    ImplicitTensorLoops::TLoopApply(*this, ImplicitTensorLoops::TSetEqualOp(), op);
  return *this;
}

template<class L, class O, class R>
DataMesh& operator=(const ImplicitTensorLoops::iBinaryOp<L,O,R>& op) {
  
  static_assert(ImplicitTensorLoops::iBinaryOp<L,O,R>::TIndexSet_t::Size==0,
      "Error: tried to assign rank>0 Tensor to DataMesh.");
  ImplicitTensorLoops::TLoopApply(*this, ImplicitTensorLoops::TSetEqualOp(), op);
  return *this;
}

//================================================================
// operator+=
//================================================================
DataMesh& operator+=(const DataMesh& rhs) {
  CheckExtentsWithASSERT(rhs.Extents(), "operator+=");
  ImplicitTensorLoops::TLoopApply(*this, ImplicitTensorLoops::TPlusEqualOp(),rhs);
  return *this;
}

DataMesh& operator+=(const double s) {
  ASSERT(!IsEmpty(), "Assigned double to empty DM");
  
  ImplicitTensorLoops::TLoopApply(*this, ImplicitTensorLoops::TPlusEqualOp(),s);
  //mData.AddConstant(s);
  return *this;
}

template<class LHS, class OP, class RHS>
DataMesh& operator+=(const BinaryOp<LHS,OP,RHS>& op) {
  
  CheckExtentsWithASSERT(ExtentsFromBinaryOp(op), "operator+=BinaryOp");
  ImplicitTensorLoops::TLoopApply(*this,ImplicitTensorLoops::TPlusEqualOp(),op);
  return *this;
}
template<class L, class O, class R>
DataMesh& operator+=(const ImplicitTensorLoops::iBinaryOp<L,O,R>& op) {
  
  static_assert(ImplicitTensorLoops::iBinaryOp<L,O,R>::TIndexSet_t::Size==0,
      "Error: tried to assign rank>0 Tensor to DataMesh.");
  ImplicitTensorLoops::TLoopApply(*this, ImplicitTensorLoops::TPlusEqualOp(), op);
  return *this;
}
//================================================================
// operator-=
//================================================================
DataMesh& operator-=(const DataMesh& rhs) {
  
  CheckExtentsWithASSERT(rhs.Extents(), "operator-=");
  ImplicitTensorLoops::TLoopApply(*this, ImplicitTensorLoops::TMinusEqualOp(),rhs);
  return *this;
}

DataMesh& operator-=(const double s) {
  ASSERT(!IsEmpty(), "Assigned double to empty DM");
  
  ImplicitTensorLoops::TLoopApply(*this, ImplicitTensorLoops::TMinusEqualOp(),s);
  //mData.SubtractConstant(s);
  return *this;
}

template<class LHS, class OP, class RHS>
DataMesh& operator-=(const BinaryOp<LHS,OP,RHS>& op) {
  CheckExtentsWithASSERT(ExtentsFromBinaryOp(op), "operator-=BinaryOp");
  
  ImplicitTensorLoops::TLoopApply(*this,ImplicitTensorLoops::TMinusEqualOp(),op);
  return *this;
}
template<class L, class O, class R>
DataMesh& operator-=(const ImplicitTensorLoops::iBinaryOp<L,O,R>& op) {
  
  static_assert(ImplicitTensorLoops::iBinaryOp<L,O,R>::TIndexSet_t::Size==0,
      "Error: tried to assign rank>0 Tensor to DataMesh.");
  ImplicitTensorLoops::TLoopApply(*this, ImplicitTensorLoops::TMinusEqualOp(), op);
  return *this;
}
//================================================================
// operator*=
//================================================================
DataMesh& operator*=(const DataMesh& rhs) {
  
  CheckExtentsWithASSERT(rhs.Extents(), "operator*=");
  ImplicitTensorLoops::TLoopApply(*this, ImplicitTensorLoops::TMultEqualOp(),rhs);
  return *this;
}

DataMesh& operator*=(const double s) {
  ASSERT(!IsEmpty(), "Assigned double to empty DM");
  
  ImplicitTensorLoops::TLoopApply(*this, ImplicitTensorLoops::TMultEqualOp(),s);
  //mData.MultiplyByConstant(s);
  return *this;
}

template<class LHS, class OP, class RHS>
DataMesh& operator*=(const BinaryOp<LHS,OP,RHS>& op) {
  CheckExtentsWithASSERT(ExtentsFromBinaryOp(op), "operator*=BinaryOp");
  
  ImplicitTensorLoops::TLoopApply(*this,ImplicitTensorLoops::TMultEqualOp(),op);
  return *this;
}
template<class L, class O, class R>
DataMesh& operator*=(const ImplicitTensorLoops::iBinaryOp<L,O,R>& op) {
  
  static_assert(ImplicitTensorLoops::iBinaryOp<L,O,R>::TIndexSet_t::Size==0,
      "Error: tried to assign rank>0 Tensor to DataMesh.");
  ImplicitTensorLoops::TLoopApply(*this, ImplicitTensorLoops::TMultEqualOp(), op);
  return *this;
}
//================================================================
// operator/=
//================================================================
DataMesh& operator/=(const DataMesh& rhs) {
  
  CheckExtentsWithASSERT(rhs.Extents(), "operator/=");
  ImplicitTensorLoops::TLoopApply(*this, ImplicitTensorLoops::TDivEqualOp(),rhs);
  return *this;
}

DataMesh& operator/=(const double s) {
  ASSERT(!IsEmpty(), "Assigned double to empty DM");
  
  ImplicitTensorLoops::TLoopApply(*this, ImplicitTensorLoops::TDivEqualOp(),s);
  //mData.DivideByConstant(s);
  return *this;
}

template<class LHS, class OP, class RHS>
DataMesh& operator/=(const BinaryOp<LHS,OP,RHS>& op) {
  CheckExtentsWithASSERT(ExtentsFromBinaryOp(op), "operator/=BinaryOp");
  
  ImplicitTensorLoops::TLoopApply(*this,ImplicitTensorLoops::TDivEqualOp(),op);
  return *this;
}

template<class L, class O, class R>
DataMesh& operator/=(const ImplicitTensorLoops::iBinaryOp<L,O,R>& op) {
  
  static_assert(ImplicitTensorLoops::iBinaryOp<L,O,R>::TIndexSet_t::Size==0,
      "Error: tried to assign rank>0 Tensor to DataMesh.");
  ImplicitTensorLoops::TLoopApply(*this, ImplicitTensorLoops::TDivEqualOp(), op);
  return *this;
}
#endif
