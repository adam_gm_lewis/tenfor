/// \file 
#ifndef INCLUDED_CudaMemoryHandler_hpp
#define INCLUDED_CudaMemoryHandler_hpp

/// output a list of sizes and how many reserve pointers of each
void CudaOutputMemoryHandlerContents();

/// return a piece of memory allocated with cudaMalloc
void* CudaMemAllocate(const long unsigned int size); 

/// release a chunk of memory, originally allocated with
/// CudaMemAllocate.
void CudaMemRelease(void* ptr, const long unsigned int size);

//Actually delete memory.
void CudaMemPermanentlyDelete(void* ptr, const long unsigned int size);

void CudaMemReleaseReserved();

/// return total size of GPU memory that's allocated, but
/// currently not in use.
long unsigned int CudaMemoryHandlerReservedSize();

/// returns total size of GPU memory that's presently in use.
long unsigned int CudaMemoryHandlerActiveSize();


/// returns maximal active GPU memory durnig execution.
long unsigned int CudaMemoryHandlerMaxActiveSize();
#endif
