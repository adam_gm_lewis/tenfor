// -*- c++ -*-
// documentation in ExpressionTemplates.tex

#ifndef _ExpressionTemplates_H_
#define _ExpressionTemplates_H_

#include <cmath>
#include <algorithm>    // for std::min, std::max
#include "RoundOps.hpp"
#include "StepFunction.hpp"
#include "IPow.hpp"

// tags to distinguish different purposes of BinaryOp's:
// - BinaryOp<EmptyType, UnaryOP, RHS> represents an unary operator
//   (like sin) acting on RHS.
struct EmptyType { }; 

// ==== Assignment operators ====

struct SetEqualOp {
  static inline void modify(double& lhs, const double& rhs) {
    lhs=rhs;
  }
};
struct PlusEqualOp {
  static inline void modify(double& lhs, const double& rhs) {
    lhs+=rhs;
  }
};
struct MinusEqualOp {
  static inline void modify(double& lhs, const double& rhs) {
    lhs-=rhs;
  }
};
struct MultEqualOp {
  static inline void modify(double& lhs, const double& rhs) {
    lhs*=rhs;
  }
};
struct DivEqualOp {
  static inline void modify(double& lhs, const double& rhs) {
    lhs/=rhs;
  }
};


// *** Binary operators ***
struct AddOp {
  static inline double apply(const double& x, const double& y)
  {
    return x+y;
  }
};

struct SubOp {
  static inline double apply(const double& x, const double& y)
  {
    return x-y;
  }
};

struct MultOp {
  static inline double apply(const double& x, const double& y) 
  { 
    return x*y;
  }
};

struct DivOp {
  static inline double apply(const double& x, const double& y) 
  {
    return x/y;
  }
};

// power
struct PowOp {
  static inline double apply(const double& x, const double& y) 
  {
    return pow(x,y);
  }
};

struct IPowOp {
  static inline double apply(const double& x, const int& y)
  {
    return IPow(x,y);
  }
};

// atan2(x,y) = atan(x/y)
struct Atan2Op {
  static inline double apply(const double& x, const double& y) 
  {
    return atan2(x,y);
  }
};

struct MinOp {
  static inline double apply(const double& x, const double& y)
  {
    return std::min(x,y);
  }
};

struct MaxOp {
  static inline double apply(const double& x, const double& y)
  {
    return std::max(x,y);
  }
};

// *** Unary Operators ***

struct doNothingOp {
  // is used to allow DataMesh+=double to use ExpressionTemplates
  static inline double apply(const double& x) { return x; }
};
struct negateOp {
  static inline double apply(const double& x) { return -x; }
};
struct sqrtOp {
  static inline double apply(const double& x) { return sqrt(x); }
};
struct sqrOp {
  static inline double apply(const double& x) { return x*x;  }
};
struct cubeOp {
  static inline double apply(const double& x) { return x*x*x;  }
};
struct pow4Op {
  static inline double apply(const double& x) { double x2 = x*x; return x2*x2;}
};
struct fabsOp {
  static inline double apply(const double& x) { return fabs(x);  }
};
struct expOp {
  static inline double apply(const double& x) { return exp(x); }
};
struct logOp {
  static inline double apply(const double& x) { return log(x); }
};
struct log10Op {
  static inline double apply(const double& x) { return log10(x); }
};
struct cosOp {
  static inline double apply(const double& x) { return cos(x); }
};
struct acosOp {
  static inline double apply(const double& x) { return acos(x); }
};
struct sinOp {
  static inline double apply(const double& x) { return sin(x); }
};
struct asinOp {
  static inline double apply(const double& x) { return asin(x); }
};
struct coshOp {
  static inline double apply(const double& x) { return cosh(x); }
};
struct sinhOp {
  static inline double apply(const double& x) { return sinh(x); }
};
struct tanhOp {
  static inline double apply(const double& x) { return tanh(x); }
};
struct tanOp {
  static inline double apply(const double& x) { return tan(x); }
};
struct atanOp {
  static inline double apply(const double& x) { return atan(x); }
};
struct RoundOp {
  static inline double apply(const double& x) { return Round(x); }
};
struct StepFunctionOp {
  static inline double apply(const double& x) { return StepFunction(x); }
};
struct floorOp {
  static inline double apply(const double& x) { return floor(x); }
};
struct ceilOp {
  static inline double apply(const double& x) { return ceil(x); }
};


//================================================================
// BinaryOp<LHS, OP, RHS>
//
// basic building block to encode an expression _and_ references
// to the data involved.
//================================================================


template<class LHS, class OP, class RHS> 
struct BinaryOp {
  BinaryOp(const LHS& left, const RHS& right):  lhs(left), rhs(right) {  };
  inline double operator[](const int& i) const 
  {
    return OP::apply(lhs[i], rhs[i]);
  }
  const LHS& lhs;
  const RHS& rhs;
};

// specializations for double
template<class OP, class RHS> 
struct BinaryOp<double, OP, RHS> {
  BinaryOp(const double& left, const RHS& right):  lhs(left), rhs(right) { };
  inline double operator[](const int& i) const 
  {
    return OP::apply(lhs, rhs[i]);
  }
  const double& lhs;
  const RHS& rhs;
};
template<class LHS, class OP> 
struct BinaryOp<LHS, OP, double> {
  BinaryOp(const LHS& left, const double& right):  lhs(left), rhs(right) { };
  inline double operator[](const int& i) const 
  {
    return OP::apply(lhs[i], rhs);
  }
  const LHS& lhs;
  const double& rhs;
};

// specialization for int
template<class LHS, class OP>
struct BinaryOp<LHS, OP, int> {
  BinaryOp(const LHS& left, const int& right):  lhs(left), rhs(right) { };
  inline double operator[](const int& i) const
  {
    return OP::apply(lhs[i], rhs);
  }
  const LHS& lhs;
  const int& rhs;
};


// Specializations for unary operators (e.g. sqrtOp), that have LHS==EmptyType
template<class OP, class RHS> 
struct BinaryOp<EmptyType, OP, RHS> {
  BinaryOp(const RHS& right):  rhs(right) {  };
  inline double operator[](const int& i) const 
  {
    return OP::apply(rhs[i]);
  }
  const RHS& rhs;
};

// specialization to allow routing of DataMesh::operator??(const double)
// into templated DataMesh::Apply
template<> 
struct BinaryOp<EmptyType, doNothingOp, double> {
  explicit BinaryOp(const double& right):  rhs(right) { };
  inline double operator[](const int& ) const 
  {
    return rhs;
  }
  const double& rhs;
};





// ****************************************************************
// Exchange the type in a BinaryOp
// ****************************************************************
// EXPR is a BinaryOp<...> . Inside this expression, replace the type X
// by the type Y whenever it appears. 
// Example:
// BinaryOp_ReplaceType<BinaryOp<const Ddata, MultOp, const double>, 
//                      const Ddata, const SDdata>::result
// equals BinaryOp<const SDdata, MultOp, const double>.
// ****************************************************************

// general template not defined
template<class EXPR, class X, class Y>
struct BinaryOp_ReplaceType;

// in leaf, replace X by Y, and don't touch const double or EmptyType
template<class X, class Y>
struct BinaryOp_ReplaceType<X, X, Y> {
  typedef Y result;
};
template<class X, class Y>
struct BinaryOp_ReplaceType<const double, X, Y> {
  typedef const double result;
};
template<class X, class Y>
struct BinaryOp_ReplaceType<EmptyType, X, Y> {
  typedef EmptyType result;
};

// in a BinaryOp, recursively go further down
template<class LHS, class OP, class RHS, class X, class Y>
struct BinaryOp_ReplaceType<BinaryOp<LHS, OP, RHS>, X, Y> {
  typedef BinaryOp<typename BinaryOp_ReplaceType<LHS, X, Y>::result, 
                   OP, 
                   typename BinaryOp_ReplaceType<RHS, X, Y>::result
                   > result;
};

/****************************************************************
 * BinaryOp_Indexed
 *
 * Given a BinaryOp 'EXPR' which is in terms of 'X' (e.g. 
 * BinaryOp<const Ddata, AddOp, const Ddata>), create a BinaryOp
 * in terms of 'Y' (e.g. BinaryOp<const SDdata, AddOp, const SDdata>),
 * where each element Y is initialized by indexing the corresponding
 * 'X' with const Y& X::operator[](const IDX& i) const; 
 *
 * The BinaryOp is stored in the const member variable BinOpRef. 
 *
 * BinaryOp_Indexed was removed in SVN r18179.
 ****************************************************************/


// ****************************************************************
// ****************************************************************
// ****************      O P E R A T  O R S        ****************
// ****************************************************************
// ****************************************************************

// Once we have expressions involving a BinaryOp< >, we can use these
// general templates to further combine them.  The entry level
// operators, which turn, say A*B into a BinaryOp< >, are not defined
// as general templates.  This is to avoid hard to understand error
// messages.  (If we had templatized entry level operators, then
// double* a, *b, *c; c=a*b; would generate an incomprehensible error
// message)

// ****************************************************************
//    s OP BinaryOp
// ****************************************************************
// These functions cannot be templates for arbitrary types X, 
// since then 5*A will instantiate a BinaryOp<const int, ...>, instead
// of a BinaryOp<const double, ... >
template<class LHS, class OP, class RHS>
inline BinaryOp<double, AddOp, BinaryOp<LHS, OP, RHS> >
operator+(const double& s, const BinaryOp<LHS, OP, RHS>& rhs) 
{
  return BinaryOp<double, AddOp, BinaryOp<LHS,OP,RHS> >(s, rhs);
}  

template<class LHS, class OP, class RHS>
inline BinaryOp<double, SubOp, BinaryOp<LHS,OP,RHS> >
operator-(const double& s, const BinaryOp<LHS,OP,RHS>& rhs) 
{
  return BinaryOp<double, SubOp, BinaryOp<LHS,OP,RHS> >(s, rhs);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<double, MultOp, BinaryOp<LHS,OP,RHS> >
operator*(const double& s, const BinaryOp<LHS,OP,RHS>& rhs) 
{
  return BinaryOp<double,MultOp, BinaryOp<LHS,OP,RHS> >(s, rhs);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<double, DivOp, BinaryOp<LHS,OP,RHS> >
operator/(const double& s, const BinaryOp<LHS,OP,RHS>& rhs)
{
  return BinaryOp<double, DivOp, BinaryOp<LHS,OP,RHS> >(s, rhs);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<double, PowOp, BinaryOp<LHS,OP,RHS> >
pow(const double& s, const BinaryOp<LHS,OP,RHS>& rhs)
{
  return BinaryOp<double, PowOp, BinaryOp<LHS,OP,RHS> >(s, rhs);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<double, Atan2Op, BinaryOp<LHS,OP,RHS> >
atan2(const double& s, const BinaryOp<LHS,OP,RHS>& rhs)
{
  return BinaryOp<double, Atan2Op, BinaryOp<LHS,OP,RHS> >(s, rhs);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<double, MinOp, BinaryOp<LHS,OP,RHS> >
min(const double& s, const BinaryOp<LHS,OP,RHS>& rhs)
{
  return BinaryOp<double, MinOp, BinaryOp<LHS,OP,RHS> >(s, rhs);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<double, MaxOp, BinaryOp<LHS,OP,RHS> >
max(const double& s, const BinaryOp<LHS,OP,RHS>& rhs)
{
  return BinaryOp<double, MaxOp, BinaryOp<LHS,OP,RHS> >(s, rhs);
}

// BinaryOp OP s
template<class LHS, class OP, class RHS>
inline BinaryOp<BinaryOp<LHS,OP,RHS>, AddOp, double>
operator+(const BinaryOp<LHS,OP,RHS>& lhs_op, const double& s) 
{
  return BinaryOp<BinaryOp<LHS,OP,RHS>, AddOp, double>(lhs_op,s);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<BinaryOp<LHS,OP,RHS>, SubOp, double>
operator-(const BinaryOp<LHS,OP,RHS>& lhs_op, const double& s) {
  return BinaryOp<BinaryOp<LHS,OP,RHS>, SubOp, double>(lhs_op,s);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<BinaryOp<LHS,OP,RHS>, MultOp, double>
operator*(const BinaryOp<LHS,OP,RHS>& lhs_op, const double& s) 
{
  return BinaryOp<BinaryOp<LHS,OP,RHS>, MultOp, double>(lhs_op,s);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<BinaryOp<LHS, OP, RHS>, DivOp, double>
operator/(const BinaryOp<LHS,OP,RHS>& lhs_op, const double& s) 
{
  return BinaryOp<BinaryOp<LHS,OP,RHS>, DivOp, double>(lhs_op,s);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<BinaryOp<LHS, OP, RHS>, PowOp, double>
pow(const BinaryOp<LHS,OP,RHS>& lhs_op, const double& s) 
{
  return BinaryOp<BinaryOp<LHS,OP,RHS>, PowOp, double>(lhs_op,s);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<BinaryOp<LHS, OP, RHS>, IPowOp, int>
IPow(const BinaryOp<LHS,OP,RHS>& lhs_op, const int& s)
{
  return BinaryOp<BinaryOp<LHS,OP,RHS>, IPowOp, int>(lhs_op,s);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<BinaryOp<LHS, OP, RHS>, Atan2Op, double>
atan2(const BinaryOp<LHS,OP,RHS>& lhs_op, const double& s) 
{
  return BinaryOp<BinaryOp<LHS,OP,RHS>, Atan2Op, double>(lhs_op,s);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<BinaryOp<LHS, OP, RHS>, MinOp, double>
min(const BinaryOp<LHS,OP,RHS>& lhs_op, const double& s)
{
  return BinaryOp<BinaryOp<LHS,OP,RHS>, MinOp, double>(lhs_op,s);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<BinaryOp<LHS, OP, RHS>, MaxOp, double>
max(const BinaryOp<LHS,OP,RHS>& lhs_op, const double& s)
{
  return BinaryOp<BinaryOp<LHS,OP,RHS>, MaxOp, double>(lhs_op,s);
}


// BinaryOp OP BinaryOP
template<class LHS1, class OP1, class RHS1, 
         class LHS2, class OP2, class RHS2>
inline BinaryOp<BinaryOp<LHS1,OP1,RHS1>, AddOp, 
                BinaryOp<LHS2,OP2,RHS2> >
operator+(const BinaryOp<LHS1,OP1,RHS1>& op1, 
	  const BinaryOp<LHS2,OP2,RHS2>& op2) 
{
  return BinaryOp<BinaryOp<LHS1,OP1,RHS1>,AddOp, 
                  BinaryOp<LHS2,OP2,RHS2> >(op1, op2);
}
template<class LHS1, class OP1, class RHS1, 
         class LHS2, class OP2, class RHS2> 
inline BinaryOp<BinaryOp<LHS1,OP1,RHS1>, SubOp, 
                BinaryOp<LHS2,OP2,RHS2> >
operator-(const BinaryOp<LHS1, OP1, RHS1>& op1, 
	  const BinaryOp<LHS2, OP2, RHS2>& op2) 
{
  return BinaryOp<BinaryOp<LHS1,OP1,RHS1>, SubOp, 
                  BinaryOp<LHS2,OP2,RHS2> >(op1, op2);
}
template<class LHS1, class OP1, class RHS1,
         class LHS2, class OP2, class RHS2>
inline BinaryOp<BinaryOp<LHS1, OP1, RHS1>, MultOp, 
                BinaryOp<LHS2, OP2, RHS2> >
operator*(const BinaryOp<LHS1, OP1, RHS1>& op1, 
	  const BinaryOp<LHS2, OP2, RHS2>& op2) 
{
  return BinaryOp<BinaryOp<LHS1, OP1, RHS1>, MultOp, 
                  BinaryOp<LHS2, OP2, RHS2> >(op1, op2);
}
template<class LHS1, class OP1, class RHS1, 
         class LHS2, class OP2, class RHS2> 
inline BinaryOp<BinaryOp<LHS1, OP1, RHS1>, DivOp, 
                BinaryOp<LHS2, OP2, RHS2> >
operator/(const BinaryOp<LHS1, OP1, RHS1>& op1, 
	  const BinaryOp<LHS2, OP2, RHS2>& op2) 
{
  return BinaryOp<BinaryOp<LHS1, OP1, RHS1>, DivOp, 
                  BinaryOp<LHS2, OP2, RHS2> >(op1, op2);
}
template<class LHS1, class OP1, class RHS1, 
         class LHS2, class OP2, class RHS2> 
inline BinaryOp<BinaryOp<LHS1, OP1, RHS1>, PowOp, 
                BinaryOp<LHS2, OP2, RHS2> >
pow(const BinaryOp<LHS1, OP1, RHS1>& op1, 
    const BinaryOp<LHS2, OP2, RHS2>& op2) 
{
  return BinaryOp<BinaryOp<LHS1, OP1, RHS1>, PowOp, 
                  BinaryOp<LHS2, OP2, RHS2> >(op1, op2);
}
template<class LHS1, class OP1, class RHS1, 
         class LHS2, class OP2, class RHS2> 
inline BinaryOp<BinaryOp<LHS1, OP1, RHS1>, Atan2Op, 
                BinaryOp<LHS2, OP2, RHS2> >
atan2(const BinaryOp<LHS1, OP1, RHS1>& op1, 
    const BinaryOp<LHS2, OP2, RHS2>& op2) 
{
  return BinaryOp<BinaryOp<LHS1, OP1, RHS1>, Atan2Op, 
                  BinaryOp<LHS2, OP2, RHS2> >(op1, op2);
}
template<class LHS1, class OP1, class RHS1,
         class LHS2, class OP2, class RHS2>
inline BinaryOp<BinaryOp<LHS1, OP1, RHS1>, MinOp,
                BinaryOp<LHS2, OP2, RHS2> >
min(const BinaryOp<LHS1, OP1, RHS1>& op1,
    const BinaryOp<LHS2, OP2, RHS2>& op2)
{
  return BinaryOp<BinaryOp<LHS1, OP1, RHS1>, MinOp,
                  BinaryOp<LHS2, OP2, RHS2> >(op1, op2);
}
template<class LHS1, class OP1, class RHS1,
         class LHS2, class OP2, class RHS2>
inline BinaryOp<BinaryOp<LHS1, OP1, RHS1>, MaxOp,
                BinaryOp<LHS2, OP2, RHS2> >
max(const BinaryOp<LHS1, OP1, RHS1>& op1,
    const BinaryOp<LHS2, OP2, RHS2>& op2)
{
  return BinaryOp<BinaryOp<LHS1, OP1, RHS1>, MaxOp,
                  BinaryOp<LHS2, OP2, RHS2> >(op1, op2);
}


// ****************************************************************
// macro DEFINE_EXPRESSION_TEMPLATE_OPERATORS
// ****************************************************************
// this macro defines the entry operators into expression templates
// for a certain type.  For example, 
// DEFINE_EXPRESSION_TEMPLATE_ENTRY_OPERATORS(Ddata)
// ****************************************************************

#define DEFINE_EXPRESSION_TEMPLATE_ENTRY_OPERATORS_single_op(TYPE, op__, clss)\
/**** A OP B ****/                                                     \
inline BinaryOp<TYPE, clss, TYPE>                                      \
op__ (const TYPE& A, const TYPE& B) {                                  \
  return BinaryOp<TYPE, clss, TYPE>(A, B);                             \
}                                                                      \
/**** s OP A ****/                                                     \
inline BinaryOp<double, clss, TYPE>                                    \
op__ (const double& s, const TYPE& B) {                                \
  return BinaryOp<double, clss, TYPE>(s, B);                           \
}                                                                      \
/**** A OP s ****/                                                     \
inline BinaryOp<TYPE, clss, double>                                    \
op__(const TYPE& A, const double& s) {                                 \
  return BinaryOp<TYPE, clss, double>(A, s);                           \
}                                                                      \
/**** A OP BinaryOp ****/                                              \
template<class LHS, class OP, class RHS>                               \
inline BinaryOp<TYPE, clss, BinaryOp<LHS, OP, RHS> >                   \
op__(const TYPE& A, const BinaryOp<LHS, OP, RHS>& rhs_op) {            \
  return BinaryOp<TYPE, clss, BinaryOp<LHS,OP,RHS> >                   \
 (A, rhs_op);                                                          \
}                                                                      \
/**** BinaryOp OP A ****/                                              \
template<class LHS, class OP, class RHS>                               \
inline BinaryOp<BinaryOp<LHS, OP, RHS>, clss, TYPE>                    \
op__(const BinaryOp<LHS, OP, RHS>& lhs_op, const TYPE& A) {            \
  return BinaryOp<BinaryOp<LHS,OP,RHS>, clss, TYPE>                    \
 (lhs_op, A);                                                          \
}                                                                      

#define DEFINE_EXPRESSION_TEMPLATE_ENTRY_OPERATORS(TYPE)               \
DEFINE_EXPRESSION_TEMPLATE_ENTRY_OPERATORS_single_op(TYPE, operator+, AddOp)  \
DEFINE_EXPRESSION_TEMPLATE_ENTRY_OPERATORS_single_op(TYPE, operator-, SubOp)  \
DEFINE_EXPRESSION_TEMPLATE_ENTRY_OPERATORS_single_op(TYPE, operator*, MultOp) \
DEFINE_EXPRESSION_TEMPLATE_ENTRY_OPERATORS_single_op(TYPE, operator/, DivOp)  \
DEFINE_EXPRESSION_TEMPLATE_ENTRY_OPERATORS_single_op(TYPE, pow, PowOp)        \
DEFINE_EXPRESSION_TEMPLATE_ENTRY_OPERATORS_single_op(TYPE, atan2, Atan2Op)    \
DEFINE_EXPRESSION_TEMPLATE_ENTRY_OPERATORS_single_op(TYPE, min, MinOp)        \
DEFINE_EXPRESSION_TEMPLATE_ENTRY_OPERATORS_single_op(TYPE, max, MaxOp)


#ifdef DEFINE_EXPRESSION_TEMPLATE_ENTRY_OPERATORS_FANCY
ArithStruct.h must be able to define a macro 
DEFINE_EXPRESSION_TEMPLATE_ENTRY_OPERATORS_FANCY
#endif
#define DEFINE_EXPRESSION_TEMPLATE_ENTRY_OPERATORS_FANCY_single_op(     \
   TYPE, op__, clss, X, EXTRACT)                                        \
/**** A OP B ****/                                                      \
inline BinaryOp<TYPE, clss, TYPE>                                       \
op__ (const X& A, const X& B) {                                         \
  return BinaryOp<TYPE, clss, TYPE>(A EXTRACT, B EXTRACT);              \
}                                                                       \
/**** s OP A ****/                                                      \
inline BinaryOp<const double, clss, TYPE>                               \
op__ (const double& s, const X& B) {                                    \
  return BinaryOp<const double, clss, TYPE>(s, B EXTRACT);              \
}                                                                       \
/**** A OP s ****/                                                      \
inline BinaryOp<TYPE, clss, const double>                               \
op__(const X& A, const double& s) {                                     \
  return BinaryOp<TYPE, clss, const double>(A EXTRACT, s);              \
}                                                                       \
/**** A OP BinaryOp ****/                                               \
template<class LHS, class OP, class RHS>                                \
inline BinaryOp<TYPE, clss, BinaryOp<LHS, OP, RHS> >                    \
op__(const X& A, const BinaryOp<LHS, OP, RHS,D>& rhs_op) {              \
  return BinaryOp<TYPE, clss, BinaryOp<LHS,OP,RHS>>                     \
 (A EXTRACT, rhs_op);                                                   \
}                                                                       \
/**** BinaryOp OP A ****/                                               \
template<class LHS, class OP, class RHS>                                \
inline BinaryOp<BinaryOp<LHS, OP, RHS>, clss, TYPE>                     \
op__(const BinaryOp<LHS, OP, RHS>& lhs_op, const X& A) {                \
  return BinaryOp<BinaryOp<LHS,OP,RHS>, clss, TYPE>                     \
 (lhs_op, A EXTRACT);                                                   \
}                                                                       \

#define DEFINE_EXPRESSION_TEMPLATE_ENTRY_OPERATORS_FANCY(TYPE, X, E)          \
DEFINE_EXPRESSION_TEMPLATE_ENTRY_OPERATORS_FANCY_single_op(TYPE, operator+,AddOp,X,E);\
DEFINE_EXPRESSION_TEMPLATE_ENTRY_OPERATORS_FANCY_single_op(TYPE, operator-,SubOp,X,E);\
DEFINE_EXPRESSION_TEMPLATE_ENTRY_OPERATORS_FANCY_single_op(TYPE, operator*,MultOp,X,E);\
DEFINE_EXPRESSION_TEMPLATE_ENTRY_OPERATORS_FANCY_single_op(TYPE, operator/, DivOp,X,E);\
DEFINE_EXPRESSION_TEMPLATE_ENTRY_OPERATORS_FANCY_single_op(TYPE, pow, PowOp,X,E); \
DEFINE_EXPRESSION_TEMPLATE_ENTRY_OPERATORS_FANCY_single_op(TYPE, atan2, Atan2Op,X,E); \
DEFINE_EXPRESSION_TEMPLATE_ENTRY_OPERATORS_FANCY_single_op(TYPE, min, MinOp,X,E); \
DEFINE_EXPRESSION_TEMPLATE_ENTRY_OPERATORS_FANCY_single_op(TYPE, max, MaxOp,X,E);


/****************************************************************
 * unary operators like sqrt
 ****************************************************************/
// for each unary operator, one needs two overloaded instances:
// 1) one that takes a BinaryOp
// 2) one that takes a const TYPE&.
// The 2nd one is #defined so it's easy to apply these to whatever
// types we want (Ddata, SDdata, ...)

template<class LHS, class OP, class RHS>
inline const BinaryOp<LHS, OP, RHS>
operator+(const BinaryOp<LHS, OP, RHS>& op) {
  return op;
}

template<class LHS, class OP, class RHS>
inline BinaryOp<EmptyType, negateOp, BinaryOp<LHS, OP, RHS> > 
operator-(const BinaryOp<LHS, OP, RHS>& op) {
  return BinaryOp<EmptyType, negateOp, BinaryOp<LHS, OP, RHS> >(op);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<EmptyType, sqrtOp, BinaryOp<LHS, OP, RHS> > 
sqrt(const BinaryOp<LHS, OP, RHS>& op) {
  return BinaryOp<EmptyType, sqrtOp, BinaryOp<LHS, OP, RHS> >(op);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<EmptyType, sqrOp, BinaryOp<LHS, OP, RHS> > 
sqr(const BinaryOp<LHS, OP, RHS>& op) {
  return BinaryOp<EmptyType, sqrOp, BinaryOp<LHS, OP, RHS> >(op);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<EmptyType, cubeOp, BinaryOp<LHS, OP, RHS> > 
cube(const BinaryOp<LHS, OP, RHS>& op) {
  return BinaryOp<EmptyType, cubeOp, BinaryOp<LHS, OP, RHS> >(op);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<EmptyType, pow4Op, BinaryOp<LHS, OP, RHS> >
pow4(const BinaryOp<LHS, OP, RHS>& op) {
  return BinaryOp<EmptyType, pow4Op, BinaryOp<LHS, OP, RHS> >(op);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<EmptyType, fabsOp, BinaryOp<LHS, OP, RHS> > 
fabs(const BinaryOp<LHS, OP, RHS>& op) {
  return BinaryOp<EmptyType, fabsOp, BinaryOp<LHS, OP, RHS> >(op);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<EmptyType, expOp, BinaryOp<LHS, OP, RHS> > 
exp(const BinaryOp<LHS, OP, RHS>& op) {
  return BinaryOp<EmptyType, expOp, BinaryOp<LHS, OP, RHS> >(op);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<EmptyType, logOp, BinaryOp<LHS, OP, RHS> > 
log(const BinaryOp<LHS, OP, RHS>& op) {
  return BinaryOp<EmptyType, logOp, BinaryOp<LHS, OP, RHS> >(op);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<EmptyType, log10Op, BinaryOp<LHS, OP, RHS> > 
log10(const BinaryOp<LHS, OP, RHS>& op) {
  return BinaryOp<EmptyType, log10Op, BinaryOp<LHS, OP, RHS> >(op);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<EmptyType, cosOp, BinaryOp<LHS, OP, RHS> > 
cos(const BinaryOp<LHS, OP, RHS>& op) {
  return BinaryOp<EmptyType, cosOp, BinaryOp<LHS, OP, RHS> >(op);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<EmptyType, acosOp, BinaryOp<LHS, OP, RHS> > 
acos(const BinaryOp<LHS, OP, RHS>& op) {
  return BinaryOp<EmptyType, acosOp, BinaryOp<LHS, OP, RHS> >(op);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<EmptyType, sinOp, BinaryOp<LHS, OP, RHS> > 
sin(const BinaryOp<LHS, OP, RHS>& op) {
  return BinaryOp<EmptyType, sinOp, BinaryOp<LHS, OP, RHS> >(op);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<EmptyType, asinOp, BinaryOp<LHS, OP, RHS> > 
asin(const BinaryOp<LHS, OP, RHS>& op) {
  return BinaryOp<EmptyType, asinOp, BinaryOp<LHS, OP, RHS> >(op);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<EmptyType, coshOp, BinaryOp<LHS, OP, RHS> > 
cosh(const BinaryOp<LHS, OP, RHS>& op) {
  return BinaryOp<EmptyType, coshOp, BinaryOp<LHS, OP, RHS> >(op);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<EmptyType, sinhOp, BinaryOp<LHS, OP, RHS> > 
sinh(const BinaryOp<LHS, OP, RHS>& op) {
  return BinaryOp<EmptyType, sinhOp, BinaryOp<LHS, OP, RHS> >(op);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<EmptyType, tanhOp, BinaryOp<LHS, OP, RHS> > 
tanh(const BinaryOp<LHS, OP, RHS>& op) {
  return BinaryOp<EmptyType, tanhOp, BinaryOp<LHS, OP, RHS> >(op);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<EmptyType, tanOp, BinaryOp<LHS, OP, RHS> > 
tan(const BinaryOp<LHS, OP, RHS>& op) {
  return BinaryOp<EmptyType, tanOp, BinaryOp<LHS, OP, RHS> >(op);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<EmptyType, atanOp, BinaryOp<LHS, OP, RHS> > 
atan(const BinaryOp<LHS, OP, RHS>& op) {
  return BinaryOp<EmptyType, atanOp, BinaryOp<LHS, OP, RHS> >(op);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<EmptyType, RoundOp, BinaryOp<LHS, OP, RHS> > 
Round(const BinaryOp<LHS, OP, RHS>& op) {
  return BinaryOp<EmptyType, RoundOp, BinaryOp<LHS, OP, RHS> >(op);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<EmptyType, StepFunctionOp, BinaryOp<LHS, OP, RHS> > 
StepFunction(const BinaryOp<LHS, OP, RHS>& op) {
  return BinaryOp<EmptyType, StepFunctionOp, BinaryOp<LHS, OP, RHS> >(op);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<EmptyType, floorOp, BinaryOp<LHS, OP, RHS> > 
floor(const BinaryOp<LHS, OP, RHS>& op) {
  return BinaryOp<EmptyType, floorOp, BinaryOp<LHS, OP, RHS> >(op);
}

template<class LHS, class OP, class RHS>
inline BinaryOp<EmptyType, ceilOp, BinaryOp<LHS, OP, RHS> > 
ceil(const BinaryOp<LHS, OP, RHS>& op) {
  return BinaryOp<EmptyType, ceilOp, BinaryOp<LHS, OP, RHS> >(op);
}


#define DEFINE_EXPRESSION_TEMPLATE_UNARY_OPERATORS(TYPE)\
inline const TYPE& operator+(const TYPE& A) {                          \
  return A;                                                            \
}                                                                      \
inline BinaryOp<EmptyType, negateOp, TYPE> operator-(const TYPE& A) {  \
  return BinaryOp<EmptyType, negateOp, TYPE>(A);                       \
}                                                                      \
inline BinaryOp<EmptyType, sqrtOp, TYPE> sqrt(const TYPE& A) {         \
  return BinaryOp<EmptyType, sqrtOp, TYPE>(A);                         \
}                                                                      \
inline BinaryOp<EmptyType, sqrOp, TYPE> sqr(const TYPE& A){            \
  return BinaryOp<EmptyType, sqrOp, TYPE>(A);                          \
}                                                                      \
inline BinaryOp<EmptyType, cubeOp, TYPE> cube(const TYPE& A){          \
  return BinaryOp<EmptyType, cubeOp, TYPE>(A);                         \
}                                                                      \
inline BinaryOp<EmptyType, pow4Op, TYPE> pow4(const TYPE& A){          \
  return BinaryOp<EmptyType, pow4Op, TYPE>(A);                         \
}                                                                      \
inline BinaryOp<EmptyType, fabsOp, TYPE> fabs(const TYPE& A) {         \
  return BinaryOp<EmptyType, fabsOp, TYPE>(A);                         \
}                                                                      \
inline BinaryOp<EmptyType, expOp, TYPE> exp(const TYPE& A) {           \
  return BinaryOp<EmptyType, expOp, TYPE>(A);                          \
}                                                                      \
inline BinaryOp<EmptyType, logOp, TYPE> log(const TYPE& A) {           \
  return BinaryOp<EmptyType, logOp, TYPE>(A);                          \
}                                                                      \
inline BinaryOp<EmptyType, log10Op, TYPE> log10(const TYPE& A) {       \
  return BinaryOp<EmptyType, log10Op, TYPE>(A);                        \
}                                                                      \
inline BinaryOp<EmptyType, cosOp, TYPE> cos(const TYPE& A) {           \
  return BinaryOp<EmptyType, cosOp, TYPE>(A);                          \
}                                                                      \
inline BinaryOp<EmptyType, acosOp, TYPE> acos(const TYPE& A) {         \
  return BinaryOp<EmptyType, acosOp, TYPE>(A);                         \
}                                                                      \
inline BinaryOp<EmptyType, sinOp, TYPE> sin(const TYPE& A) {           \
  return BinaryOp<EmptyType, sinOp, TYPE>(A);                          \
}                                                                      \
inline BinaryOp<EmptyType, asinOp, TYPE> asin(const TYPE& A) {         \
  return BinaryOp<EmptyType, asinOp, TYPE>(A);                         \
}                                                                      \
inline BinaryOp<EmptyType, coshOp, TYPE> cosh(const TYPE& A) {         \
  return BinaryOp<EmptyType, coshOp, TYPE>(A);                         \
}                                                                      \
inline BinaryOp<EmptyType, sinhOp, TYPE> sinh(const TYPE& A) {         \
  return BinaryOp<EmptyType, sinhOp, TYPE>(A);                         \
}                                                                      \
inline BinaryOp<EmptyType, tanhOp, TYPE> tanh(const TYPE& A) {         \
  return BinaryOp<EmptyType, tanhOp, TYPE>(A);                         \
}                                                                      \
inline BinaryOp<EmptyType, tanOp, TYPE> tan(const TYPE& A) {           \
  return BinaryOp<EmptyType, tanOp, TYPE>(A);                          \
}                                                                      \
inline BinaryOp<EmptyType, atanOp, TYPE> atan(const TYPE& A) {         \
  return BinaryOp<EmptyType, atanOp, TYPE>(A);                         \
}                                                                      \
inline BinaryOp<EmptyType, RoundOp, TYPE> Round(const TYPE& A) {       \
  return BinaryOp<EmptyType, RoundOp, TYPE>(A);                        \
}                                                                      \
inline BinaryOp<EmptyType, StepFunctionOp, TYPE> StepFunction(const TYPE& A) {\
  return BinaryOp<EmptyType, StepFunctionOp, TYPE>(A);                 \
}                                                                      \
inline BinaryOp<EmptyType, floorOp, TYPE> floor(const TYPE& A) {       \
  return BinaryOp<EmptyType, floorOp, TYPE>(A);                        \
}                                                                      \
inline BinaryOp<EmptyType, ceilOp, TYPE> ceil(const TYPE& A) {         \
  return BinaryOp<EmptyType, ceilOp, TYPE>(A);                         \
}                                                                      \
inline BinaryOp<TYPE, IPowOp, int> IPow(const TYPE& A, const int& B) { \
  return BinaryOp<TYPE, IPowOp, int>(A, B);                            \
}                                                                      \

#endif
