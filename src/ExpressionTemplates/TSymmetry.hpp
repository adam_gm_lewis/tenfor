#ifndef TIndexSymmetries_hpp
#define TIndexSymmetries_hpp

#include "Require.hpp"

namespace ImplicitTensorLoops {

  //================================================================
  // TInequality<pos1, pos2> 
  //================================================================

  // represents a pair of indices into a tensor (i.e. the 2nd and the
  // 4th), and indicates that these two indices are related by an 
  // inequality  first_index.value >= second_index.value.
  //
  // This relation allows to iterate over symmetric indices.  Example:
  //
  // "11"   -> TInequality<0,1>
  // "122"  -> TInequality<1,2>
  // "212"  -> TInequality<0,2>
  //
  // class-invariant:  pos1<pos2

  template<int pos1, int pos2> 
  struct TInequality {
    static_assert(pos1<pos2, "TInequalities must satisfy pos1<pos2");
  };


  //================================================================
  // TSymmetry<TInequality<pos1,pos2>, TInequality<pos3,pos4>, ...>
  //================================================================
  //
  // Template class to represent the entire symmetries of a tensor

  // "12"   -> TSymmetry<>
  // "11"   -> TSymmetry<TInequality<0,1>>
  // "1122" -> TSymmetry<TInequality<0,1>,TInequality<2,3>>
  // "111"  -> TSymmetry<TInequality<0,1>,TInequality<1,2>>
  //
  // Note that symmetrization over 3 or more indices can be expressed with
  // multiple linked TInequality'es, as in the last line.
  //
  // class-invariants:
  // 1) pos1 of each TInequality must be monotonically increasing
  // 2) for each TInequality:  pos1<pos2 
  //
  // The generic template is not defined.  The specialized templates
  // define:
  //
  //   - typename Shift_t -- reduce all positions by 1.  If the first
  //                         TInequalty's pos1 would become negative,
  //                         remove it.  This operation is needed during
  //                         the recursive TIndexStructure::operator++ 
  //                         evaluation, to obtain the symmetries still 
  //                         relevant for the remaining indices. 
  template<class ...TInequalities> struct TSymmetry;


  //--------------------------------
  // Helper:
  //--------------------------------

  // place new TInequality at front of TSymmetry, without checking or
  // enforcing invariant of increasing pos1's.  
  //
  // {i1<i2}+TSymmetry<{i3<i4},...> = TSymmetry<{i1<i2},{i3<i4},...>
  //
  template<class TInequality_t, class TSymmetry_t>
  struct PrependToTSymmetry; 
  
  template<int pos1, int pos2, class ...Tail>
  struct PrependToTSymmetry<TInequality<pos1, pos2>, 
			    TSymmetry<Tail...> > {
    typedef TSymmetry<TInequality<pos1,pos2>, Tail...> type;
  };
    
  
  //--------------------------------
  // TSymmetry Specialization 1  (pos1\neq 0)
  //--------------------------------
  template<int pos1, int pos2, class ...Tail> 
  struct TSymmetry<TInequality<pos1,pos2>, Tail...> {

    typedef
      typename PrependToTSymmetry< TInequality<pos1-1,pos2-1>,
				   typename TSymmetry<Tail...>::Shift_t
				   >::type Shift_t;

    static void ReplaceTInequalitiesInString(std::string& out) {
      REQUIRE(pos2<out.size(), "TSymmetry has larger rank than Tensor");
      out[pos2] = out[pos1];
      TSymmetry<Tail...>::ReplaceTInequalitiesInString(out);
    }
  };

  //--------------------------------
  // TSymmetry Specialization 2 (pos1==0)
  //--------------------------------
  template<int pos2, class ...Tail> 
  struct TSymmetry<TInequality<0,pos2>, Tail...> {
    typedef typename TSymmetry<Tail...>::Shift_t Shift_t;

    static void ReplaceTInequalitiesInString(std::string& out) {
      REQUIRE(pos2<out.size(), "TSymmetry has larger rank than Tensor");
      out[pos2] = out[0];
      TSymmetry<Tail...>::ReplaceTInequalitiesInString(out);
    }
  };
  //--------------------------------
  // TSymmetry Specialization 3:  no symmetries
  //--------------------------------
  template<> struct TSymmetry<> { 
    typedef TSymmetry<> Shift_t;

    static void ReplaceTInequalitiesInString(std::string& ) { }
  };
    
  
  //================================================================
  // Create TSymmetry<>'s with sym<1,2>(), sym<1,2,3>(), sym<1,2,3,4>()
  //================================================================

  template<int pos1, int pos2>
  TSymmetry<TInequality<pos1, pos2> > sym() { 
    static_assert(pos1<pos2 && pos1>=0,
		  "positions must be increasing, and non-negative");
    return   TSymmetry<TInequality<pos1, pos2> >();
  }

  template<int pos1, int pos2, int pos3>
  TSymmetry<TInequality<pos1, pos2>,
	    TInequality<pos2, pos3> > sym() { 
    static_assert(pos1<pos2 && pos2<pos3 && pos1>=0,
		  "positions must be increasing, and non-negative");
    return   TSymmetry<TInequality<pos1, pos2>,
		       TInequality<pos2, pos3> >();
  }

  template<int pos1, int pos2, int pos3, int pos4>
  TSymmetry<TInequality<pos1, pos2>,
	    TInequality<pos2, pos3>,
	    TInequality<pos3, pos4> > sym() { 
    static_assert(pos1<pos2 && pos2<pos3 && pos3 < pos4 && pos1>=0,
		  "positions must be increasing, and non-negative");
    return   TSymmetry<TInequality<pos1, pos2>,
		       TInequality<pos2, pos3>,
		       TInequality<pos3, pos4> >();
  }

  


  //================================================================
  // JoinTSymmetry, operator&&
  //================================================================
  //
  // Joins two distinct TSymmetry's.  Needed for tensors with disjoint
  // pairs of symmetries, e.g. "1122", "1212", "12323".
  // 
  // Implemented recursively to preserve TSymmetry invariant that pos1 
  // arguments must be monotonically increasing.
  

  //--------------------------------
  // Helper:
  //--------------------------------
  // Inserts one new TInequality into TSymmetry, respecting
  // the invariant that pos1's are monontonically increasing.
  //
  template<class TInequality_t, class TSymmetry_t>
  struct InsertIntoTSymmetry; 

  template<int pos1, int pos2>
  struct InsertIntoTSymmetry<TInequality<pos1, pos2>, TSymmetry<> > {
    typedef TSymmetry<TInequality<pos1,pos2> > type;
  };

  template<int pos1, int pos2, int pos3, int pos4, class ...Tail>
  struct InsertIntoTSymmetry<TInequality<pos1, pos2>, 
			     TSymmetry<TInequality<pos3,pos4>, Tail...> > {
    static_assert(pos1!=pos3,
		  "TSymmetry:  First indices must be distinct");
    typedef typename 
      std::conditional
      < (pos1<pos3),
	// put the new TInequality first
      TSymmetry<TInequality<pos1,pos2>, TInequality<pos3,pos4>, Tail...>,
	// else: insert new TInequality into Tail, after pos1,pos2 
      typename 
      PrependToTSymmetry<TInequality<pos3,pos4>,
			 typename 
			 InsertIntoTSymmetry<TInequality<pos1,pos2>, 
					     TSymmetry<Tail...> >::type
			 >::type
      >::type type;
  };

  //--------------------------------
  // JoinTSymmetry<TSym1, TSym2> 
  //--------------------------------
  //
  // strategy: Move one element at a time from the second to the first
  // TSymmetry.
  //
  template<class TSymmetry1, class TSymmetry2> struct JoinTSymmetry; 
  
  template<class ...Tail1, int pos1, int pos2, class ...Tail2>
  struct JoinTSymmetry<TSymmetry<Tail1...>, 
		       TSymmetry<TInequality<pos1,pos2>,Tail2...> > {
    typedef typename 
      JoinTSymmetry<typename InsertIntoTSymmetry<TInequality<pos1,pos2>,
						 TSymmetry<Tail1...> 
						 >::type,
		    TSymmetry<Tail2...> 
		    >::type type;
  };

  template<class ...Tail1>
  struct JoinTSymmetry<TSymmetry<Tail1...>, 
		       TSymmetry< > > {
    typedef TSymmetry<Tail1...> type;
  };



  //--------------------------------
  // operator &&  combines separate TSymmetries
  //--------------------------------
  template<class ...Tail1, class ...Tail2> 
  typename JoinTSymmetry<TSymmetry<Tail1...>, TSymmetry<Tail2...> >::type
  operator&&(const TSymmetry<Tail1...>&, const TSymmetry<Tail2...>&) {
    return 
      typename JoinTSymmetry<TSymmetry<Tail1...>, TSymmetry<Tail2...> >::type();
  };


  //--------------------------------
  // SymmetryString()
  // Translate a TSymmetry<> into an equivalent symmetry-string
  // for SpEC tensors.  This is used at run-time for consistenty
  // checks.

  template<class TSymmetry_t>
  std::string SymmetryString(const int rank) {
    char A='a';
    std::string out="";
    // make string without symmetries, then replace for each
    // TInequality<>
    for(int i=0; i<rank; ++i) {
      out+=A;
      ++A;
    }
    TSymmetry_t::ReplaceTInequalitiesInString(out);
    return out;
  }

}

#endif
