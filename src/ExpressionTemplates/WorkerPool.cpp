#include "WorkerPool.hpp"

#if defined(__INTEL_COMPILER)
#  if __INTEL_COMPILER >= 1500
#    define HAVE_THREAD_LOCAL
#  endif
#elif defined(__clang_major__)
#  if __clang_major__ > 3 || (__clang_major__ == 3 && __clang_minor__ >= 3)
//   this protects against a bug in Apple's clang 6.1 (based on LLVM clang 3.6)
//   Edit: Apple still appears to not support thread_local
#    if !(defined(__APPLE__))//&& __clang_major__ == 6 && __clang_minor__ == 1)
#     define HAVE_THREAD_LOCAL
#    endif
#  endif
#else /* gcc */
#  if __GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 8)
#    define HAVE_THREAD_LOCAL
#  endif
#endif

#ifndef HAVE_THREAD_LOCAL
#  include <cstdint>
#endif

namespace {
#ifdef HAVE_THREAD_LOCAL
  thread_local int thread_number = 0;
#else
  class IntPerThread {
    public:
    IntPerThread() { pthread_key_create(&mKey, nullptr); }
    ~IntPerThread() { pthread_key_delete(mKey); }

    void Set(int value) {
      pthread_setspecific(mKey, reinterpret_cast<void*>(static_cast<std::intptr_t>(value)));
    }
    int Get() {
      return static_cast<int>(reinterpret_cast<std::intptr_t>(pthread_getspecific(mKey)));
    }
  private:
    pthread_key_t mKey;
  };
  IntPerThread& ThreadNumberContainer() {
    // This has to be done as a static in a function because it can be
    // needed very early in initialization.  According to the
    // standard, this is thread-safe.
    static IntPerThread thread_number_container;
    return thread_number_container;
  }
#endif
  int num_threads = 1;
};

#ifdef HAVE_THREAD_LOCAL
int WorkerPoolGetThisThreadNum() { return thread_number; }
void WorkerPoolSetThisThreadNum(int num) { thread_number = num; }
#else
int WorkerPoolGetThisThreadNum() { return ThreadNumberContainer().Get(); }
void WorkerPoolSetThisThreadNum(int num) { ThreadNumberContainer().Set(num); }
#endif

int WorkerPoolGetNumThreads() { return num_threads; }
void WorkerPoolSetNumThreads(int num) { num_threads = num; }
