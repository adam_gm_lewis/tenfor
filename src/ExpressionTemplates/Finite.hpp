//=============================================================================
/// \file
/// Defines Finite().
//=============================================================================
#ifndef Finite_hpp
#define Finite_hpp
//=============================================================================
/// Returns true if x is a finite number and false if x is Nan, Inf, or -Inf.
/// \note We use the standard POSIX macro isfinite in cases where it is
/// available directly. The GNU C++ library headers place a wrapper for it
/// into the std namespace instead, so that is used.
///
/// The test
///   return (x-x == 0.0 );
/// should work if the compiler is IEEE compliant, however
/// the intel compiler INCORRECTLY optimizes this test to return true! 
bool Finite(const double x);

namespace MyLimits {
  double NaN();
}
//=============================================================================
// This template defines Finite(x) for containers.  Returns true, iff
// Finite() applied to each element returned true.  
//
// NOTE: Currently, this template-function applies only to containers
// with ONE template-argument, like MyVector, Tensor and MyList.  It 
// does not match STL-containers which take further template arguments 
// with default values.  Also, MyMap has two
// template arguments, and isn't captured by this template.  Presumably,
// one could come up with a smarter calling sequence to capture those
// containers, too.  
//     I chose nevertheless this calling sequence, because it leads to
// source-code which is easier to understand.  Furthermore, because
// the template does NOT match erroneous calls like Finite("string"),
// such calls give simple compiler errors, rather than template
// gibberish.  Feel free to improve!  Harald
template<template<class> class CONT, class X>
bool Finite(const CONT<X>& container) {
  for(typename CONT<X>::const_iterator it=container.begin();
      it!=container.end(); ++it) {
    if(!Finite(*it)) return false;
  }
  return true;
}
//=============================================================================

#endif // Finite_hpp
