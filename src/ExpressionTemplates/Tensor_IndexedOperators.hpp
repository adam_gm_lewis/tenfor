// this file is #include'd within the class-definition of Tensor.
//
// It contains all TensorIndexing operators, which are separated in
// here to maximize separation of ImplicitTensorLoops from the
// regular Tensor<X> code.



//Classes intended to reference a Tensor independently of the
//ImplicitTensorLoops code must have their () operators
//defined here. Otherwise the templates below will take over, causing an
//error.
X& operator()(const MeshIndex& idx)
{
  return mData[mStructure.Index(idx)];
}

const X& operator()(const MeshIndex& idx) const{
  return mData[mStructure.Index(idx)];
}


//================================================================
// non-const implicit-tensor-loop indexing
//================================================================
//
// This operator() is **only** defined for X=DataMesh and
// Tensor<DataMesh>.  It handles the indexing on the right-hand-side
// of expressions, where both TDm and TTDm's may occur.
/*
template<class ...Indices>
ImplicitTensorLoops::iBinaryOp
  <ImplicitTensorLoops::TIndexStructure<ImplicitTensorLoops::TSymmetry<>,
    Indices...>, EmptyType, X>
    operator()(const Indices... indices) const
  {
    return operator()(indices...,
                std::is_same<X,DataMesh>::value ||
                 std::is_same<X,Tensor<DataMesh>>::value);
  }

  template<class ...Indices>
  ImplicitTensorLoops::iBinaryOp
  <ImplicitTensorLoops::TIndexStructure<ImplicitTensorLoops::TSymmetry<>,
    Indices...>, EmptyType, X>
  operator()(const Indices... indices, std::true_type) const{
    using namespace ImplicitTensorLoops;
    typedef TIndexStructure<TSymmetry<>, Indices...> TIndexStructure_t;
    REQUIRE(TIndexStructure_t::Rank == Rank(),
            "Erroneous tensor-indexing (rank-missmatch)\n"
            << "The tensor has rank=" << Rank() << ", but the user"
            << " passed in " << TIndexStructure_t::Rank
            << " indices.");
    return iBinaryOp<TIndexStructure_t, EmptyType, X>
      (*this, indices...);
  }
*/

template<class ...Indices>
typename std::enable_if< std::is_same<X, DataMesh>::value
			 || std::is_same<X, Tensor<DataMesh>>::value,
			 ImplicitTensorLoops::iBinaryOp
			    <ImplicitTensorLoops::TIndexStructure
			       <ImplicitTensorLoops::TSymmetry<>, Indices...>,
			     EmptyType, X
			     >
			 >::type

operator()(const Indices... indices) const {
  using namespace ImplicitTensorLoops;
  typedef TIndexStructure<TSymmetry<>, Indices...> TIndexStructure_t;
  REQUIRE(TIndexStructure_t::Rank == Rank(),
	  "Erroneous tensor-indexing (rank-missmatch)\n"
	  << "The tensor has rank=" << Rank() << ", but the user"
	  << " passed in " << TIndexStructure_t::Rank
	  << " indices.");
  return iBinaryOp<TIndexStructure_t, EmptyType, X>
    (*this, indices...);
}


//================================================================
// non-const implicit-tensor-loop indexing
//================================================================
//
// This operator() is **only** defined for X=DataMesh
//
// T<T<DM> > can be used on the right-hand-side of an
// assignment; they are handled by operator(Indices...) const
// defined above.
//

template<class ...Indices>
typename std::enable_if< std::is_same<X, DataMesh>::value,
			 ImplicitTensorLoops::NonConstiBinaryOp
			    <ImplicitTensorLoops::TIndexStructure
			       <ImplicitTensorLoops::TSymmetry<>, Indices...>,
			     EmptyType, X
			     >
			 >::type
operator()(const Indices... indices) {
   using namespace ImplicitTensorLoops;

   typedef TIndexStructure<TSymmetry<>, Indices...> TIndexStructure_t;
   REQUIRE(TIndexStructure_t::Rank == Rank(),
	   "Erroneous tensor-indexing (rank-missmatch)\n"
	   << "The tensor has rank=" << Rank() << ", but the user"
	   << " passed in " << TIndexStructure_t::Rank
	   << " indices.");

   // this operator() may be called on the right-hand-side of an
   // assignment, where symmetry-specifiers are not needed.
   // Therefore, defer checking of symmetries until the assignment.
   return NonConstiBinaryOp<TIndexStructure_t, EmptyType, X>
     (*this, indices...);
 }

/*
template<class ...Indices>
ImplicitTensorLoops::NonConstiBinaryOp
  <ImplicitTensorLoops::TIndexStructure<ImplicitTensorLoops::TSymmetry<>,
    Indices...>, EmptyType, X>
    operator()(const Indices... indices)
  {
    return operator()(indices...,
                std::is_same<X,DataMesh>::value);
  }

  template<class ...Indices>
  ImplicitTensorLoops::NonConstiBinaryOp
  <ImplicitTensorLoops::TIndexStructure<ImplicitTensorLoops::TSymmetry<>,
    Indices...>, EmptyType, X>
  operator()(const Indices... indices, std::true_type) const{
    using namespace ImplicitTensorLoops;
    typedef TIndexStructure<TSymmetry<>, Indices...> TIndexStructure_t;
    REQUIRE(TIndexStructure_t::Rank == Rank(),
            "Erroneous tensor-indexing (rank-missmatch)\n"
            << "The tensor has rank=" << Rank() << ", but the user"
            << " passed in " << TIndexStructure_t::Rank
            << " indices.");
    return NonConstiBinaryOp<TIndexStructure_t, EmptyType, X>
      (*this, indices...);
  }
*/
//================================================================
// non-const implicit-tensor-loop indexing with symmetry specifier
//================================================================
//
// This operator() is defined **only** for X=DataMesh
//
// T<T<DM> > can be used on the right-hand-side of an assignment, but
// symmetry-specifiers are only specified on the left-hand-side, so
// this function is never needed for T<T<DM>>.
//

template<class ...Symm, class ...Indices>
typename std::enable_if< std::is_same<X, DataMesh>::value,
			 ImplicitTensorLoops::NonConstiBinaryOp
			    <ImplicitTensorLoops::TIndexStructure
			       <ImplicitTensorLoops::TSymmetry<Symm...>,
				Indices...>,
			     EmptyType, X
			     >
			 >::type
 operator()(const ImplicitTensorLoops::TSymmetry<Symm...>&,
	    const Indices... indices) {
   using namespace ImplicitTensorLoops;
   typedef TIndexStructure<TSymmetry<Symm...>, Indices...> TIndexStructure_t;

   REQUIRE(TIndexStructure_t::Rank == Rank(),
	   "Erroneous tensor-indexing (rank-missmatch)\n"
	   << "The tensor has rank=" << Rank() << ", but the user"
	   << " passed in "
	   << TIndexStructure_t::Rank
	   << " indices.");

   const std::string tsym=SymmetryString<TSymmetry<Symm...>>(Rank());
   REQUIRE(Structure()==TensorStructure(Dim(), tsym),
	   "Wrong symmetry specifier when indexing a Tensor<DataMesh>\n"
	    "   TensorStructure=" << Structure() << "\n"
	    "   TSymmetry<...> =   \"" << tsym << "\""
	   );

   return NonConstiBinaryOp<TIndexStructure_t, EmptyType, X>
     (*this, indices...);
 }

/*
template<class ...Symm, class ...Indices>
ImplicitTensorLoops::NonConstiBinaryOp
  <ImplicitTensorLoops::TIndexStructure<
    ImplicitTensorLoops::TSymmetry<Symm...>,
    Indices...>,
    EmptyType, X>
    operator()(const ImplicitTensorLoops::TSymmetry<Symm...>& TSym,
               const Indices... indices)
  {
    return operator()(TSym, indices...,
                std::is_same<X,DataMesh>::value);
  }

  template<class ...Symm, class ...Indices>
  ImplicitTensorLoops::NonConstiBinaryOp
  <ImplicitTensorLoops::TIndexStructure<
  ImplicitTensorLoops::TSymmetry<Symm...>,
    Indices...>, EmptyType, X>
  operator()(const ImplicitTensorLoops::TSymmetry<Symm...>&,
      const Indices... indices, std::true_type) {

   using namespace ImplicitTensorLoops;
   typedef TIndexStructure<TSymmetry<Symm...>, Indices...> TIndexStructure_t;

   REQUIRE(TIndexStructure_t::Rank == Rank(),
	   "Erroneous tensor-indexing (rank-missmatch)\n"
	   << "The tensor has rank=" << Rank() << ", but the user"
	   << " passed in "
	   << TIndexStructure_t::Rank
	   << " indices.");

   const std::string tsym=SymmetryString<TSymmetry<Symm...>>(Rank());
   REQUIRE(Structure()==TensorStructure(Dim(), tsym),
	   "Wrong symmetry specifier when indexing a Tensor<DataMesh>\n"
	    "   TensorStructure=" << Structure() << "\n"
	    "   TSymmetry<...> =   \"" << tsym << "\""
	   );

   return NonConstiBinaryOp<TIndexStructure_t, EmptyType, X>
     (*this, indices...);

  }
*/
