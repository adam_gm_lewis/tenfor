// -*- c++ -*- 

//#include "GpuEvent.hpp"
#include "cuda_wrapper.hpp"
#include "Require.hpp"
#include <vector>
#include <iostream>
#include <map>
#include <stdexcept>
#include <cmath> //std::trunc
#include "CudaMemoryHandler.hpp"

/// This class implements a memory handler.  It returns device-memory
/// allocated with cudaMalloc.  When memory is released, the 
/// memory is **not** de-allocated, but rather kept for future use. 
///
/// This is useful, because SpEC allocates and deallocates memory
/// often and cudaMalloc is very slow.
class CudaMemoryHandler {
public:
  CudaMemoryHandler(): mReservedSize(0), mActiveSize(0), mMaxActiveSize(0),
	 mWarnIfAbove(1e8), mMemStep(1e8), err(GPU::err()),
         mErrstring(GPU::errstring())
  { 
      
        size_t free=0;
        size_t total=0;
        err = GPU::dCudaMemGetInfo(&free,&total,mErrstring);
        REQUIRE(err,mErrstring); 
        std::cout << "CUDA: Initializing CudaMemoryHandler. \n" 
            << free << "bytes device memory free, \n"
            << total << "bytes device memory total. \n";

      };
  ~CudaMemoryHandler() {
    ReleaseReservedMemory();
  }

  /// Return a chunk of memory of size 'size', allocated with
  /// cudaMalloc
  void* Allocate(size_t size);

  /// Release a chunk of memory (originally allocated by Allocate)
  /// This memory will go into a 'pool', to be returned by future
  /// Allocate calls of the same size. 
  void Release(void* ptr, size_t size);
  void ReleaseReservedMemory(); 
  void PermanentlyDelete(void* ptr, size_t the_size);
  size_t ReservedSize() const {
    return mReservedSize;
  }
  size_t ActiveSize() const {
    return mActiveSize;
  }

  size_t MaxActiveSize() const {
    return mMaxActiveSize;
  }
  void OutputContents() const;
private:
  // mUnusedMemory[k] holds pointers to memory-chunks allocated with
  // cudaMalloc with size=k
  std::map<size_t, std::vector<void*> > mUnusedMemory; 
  long unsigned int mReservedSize; 
    // all *unused* memory allocated with CudaMemAlloc
  long unsigned int mActiveSize;   
    // all actively *used* memory allocated with CudaMemAlloc
  long unsigned int mMaxActiveSize;  
    // max of mActiveSize during the execution so far
  long unsigned int mWarnIfAbove; 
    // if ReservedSize+ActiceSize>WarnAbove, print warning
  const long unsigned int mMemStep; // the amount to increment mWarnIfAbove by
  bool err;
  std::string mErrstring;

  //stores sizes and how many entries exist of each
  //std::map<int, int> mRegistry;

};


void* CudaMemoryHandler::Allocate(size_t the_size) {
  void *ptr;
  ////GpuEvent"Allocate", static_cast<int>(the_size));
  //If we have a pointer of this the_size reserved, return it, 
  //pop it out of the pool,
  //and subtract its the_size from mReservedthe_size.
   
  bool key_non_null(false);
  auto key = mUnusedMemory.find(the_size); 
  if (key==mUnusedMemory.end()) 
    key_non_null = mUnusedMemory[the_size].size()>0;
  
  //int int_size = static_cast<int>(the_size);
  //{
    //bool key_exists = mUnusedMemory.KeyExists(int_size);
    //if(key_exists) key_non_null = mUnusedMemory[int_size].size()>0;
  //}
  
  if(key_non_null) {
    std::vector<void*>& tmp=mUnusedMemory[the_size];
    ptr=tmp.back();
    tmp.pop_back();
    mReservedSize -= the_size;
  } else {
    //If we don't have a pointer of this size reserved, allocate one,
    //add its size to mActiveSize, and then return it.
    err = GPU::dMalloc(&ptr, the_size, mErrstring);
    GPU::dSynchronize();
    //If cudaMalloc failed, try releasing all the reserved memory to
    //make room for new allocations.
    if (!err)
    {
        size_t freesz=0;
        size_t totalsz=0;
        std::string string_copy = mErrstring;
        err = GPU::dCudaMemGetInfo(&freesz,&totalsz,mErrstring);
        REQUIRE(err,mErrstring); 
        
        std::cerr << "CUDA: CudaMemoryHandler::Allocate ran out of GPU memory.\n"
     	      << "Error message was: " << string_copy << "\n" 
              << "CudaMemoryHandler::ActiveSize() = "
     	      << ActiveSize()
     	      << ", CudaMemoryHandler::ReservedSize() = "
     	      << ReservedSize() << ".\n"
              << "cudaMemGetInfo reports " << free << " bytes free of "
              << totalsz << "bytes total. \n"
     	      << "Releasing all reserved memory in an attempt to salvage\n"
     	      << "the run.  Please think about a more controlled way of \n"
     	      << "dealing with GPU-memory exhaustion." << std::endl;
     	ReleaseReservedMemory();
        
        err = GPU::dCudaMemGetInfo(&freesz, &totalsz, mErrstring);
        REQUIRE(err,mErrstring); 

        //If even after releasing the reserved memory cudaMalloc failed, 
        //end the run and output an error.
        err = GPU::dMalloc(&ptr, the_size, mErrstring);
        REQUIRE(err,
        "Couldn't allocate GPU memory, even after all unusued memory was \n"
        << "released. \n" << "CudaMemoryHandler::ActiveSize() = " << ActiveSize()
        << "\n CudaMemoryHandler::ReservedSize() = " << ReservedSize() << ".\n"
        << "cudaMemGetInfo reports " << freesz << " bytes free of "
        << totalsz << "bytes total. \n" 
        << "Cuda mError string was: " << mErrstring);
    }
    
  }

  // bookkeeping of sizes
  mActiveSize += the_size;
  if(mActiveSize>mMaxActiveSize) mMaxActiveSize=mActiveSize;

  //Keep track of the total amount of memory we've allocated
  if((mActiveSize+mReservedSize) > mWarnIfAbove) {
    std::cerr << "CUDA: CudaMemoryHandler exceeded " << mWarnIfAbove/mMemStep
	  << " increment(s) of " << static_cast<double>(mMemStep)/1e9 
          << " GB allocated memory" << std::endl;
    mWarnIfAbove += mMemStep;
  }

  return ptr;
}

//This "deletes" a pointer by returning it to the memory pool.
void CudaMemoryHandler::Release(void* ptr, size_t the_size) {
  //GpuEvent"Release", the_size);
  //Add this pointer to the list of unused pointers
  auto&& key = mUnusedMemory.find(the_size);
  if (key == mUnusedMemory.end()){
    mUnusedMemory.emplace(the_size, std::vector<void*>({ptr}));
  }else{
    key->second.push_back(ptr);
  }
  mReservedSize += the_size;
  mActiveSize -= the_size;
}

//This actually frees the GPU memory. Sometimes necessary, but avoid if 
//possible.
void CudaMemoryHandler::PermanentlyDelete(void* ptr, size_t the_size) {
  const int int_size = static_cast<int>(the_size);
  //GpuEvent"PermanentlyDelete", int_size);
  err = GPU::dFree(ptr,mErrstring);
  REQUIRE(err,"cudaFree failed; error was: " << mErrstring);
  mActiveSize -= the_size;
}

void CudaMemoryHandler::ReleaseReservedMemory() {
  std::cout << "CUDA: RELEASE RESERVED" << std::endl;
  for (auto&& mapit: mUnusedMemory){
    const size_t sz = mapit.first; 
    std::vector<void*>& vec = mapit.second;
    for (auto&& vecit: vec){
      err = GPU::dFree(vecit, mErrstring);
      REQUIRE(err, "cudaFree failed; error was: " << mErrstring);
      mReservedSize -= sz;
    }
    vec.clear();
    vec.shrink_to_fit();
    REQUIRE(vec.empty(), "inconsistency");
 }
 REQUIRE(mReservedSize==0,
	  "Inconsistency in CudaMemoryHandler::ReleaseReservedMemory()\n"
	  << "mReservedSize=" << mReservedSize << " is non-zero, despite\n"
	  << "having released all memory");

  {
    double double_size = static_cast<double>(mActiveSize);
    double double_step = static_cast<double>(mMemStep);
    mWarnIfAbove = static_cast<size_t>(
          std::trunc((double_size/double_step)+1.)*double_step); 

  }
  std::cerr << "CUDA: RELEASE RESERVED SUCCESSFUL - but " 
    << static_cast<double>(mActiveSize)/1e9 << "GB remain checked out."
    << std::endl;
}

void CudaMemoryHandler::OutputContents() const{
  std::cout << "CUDA: *************************************" <<std::endl;
  std::cout << " *Outputting memory handler contents..." <<std::endl;
  for (auto&& it: mUnusedMemory){ 
    std::cout << "*";
    const int sz=it.first;
    const std::vector<void*>& vec = it.second;
    size_t number_of_entries = 0; 
    if(!vec.empty()){
      number_of_entries=vec.size();
    }
    std::cout << number_of_entries << " entries of size " << sz << std::endl; 
  }

  std::cout << "*************************************" <<std::endl;
}


//================================================================
// Interface functions from the header file
// these functions call the class CudaMemoryHandler, but in
// such a way that the user never sees the class, and
// never get's to interact with the class.
//================================================================


CudaMemoryHandler theHandler; 

void CudaOutputMemoryHandlerContents() {
  theHandler.OutputContents();
}

//Syntax: 
//POINTER = static_cast<TYPE*>(CudaMemAllocate(size*static_cast<int>(
//              sizeof(TYPE))));
void* CudaMemAllocate(size_t size) {
  //static CudaMemoryHandler* theHandler;
  if (size<=0) return 0; 
  return theHandler.Allocate(size);
}

void CudaMemRelease(void* ptr, size_t size) {
  //static CudaMemoryHandler* theHandler;
  if (size>0) theHandler.Release(ptr, size);
}

//Actually delete memory.
void CudaMemPermanentlyDelete(void* ptr, const long unsigned int size){
  if (size>0) theHandler.PermanentlyDelete(ptr,size);
}

void CudaMemReleaseReserved(){
  theHandler.ReleaseReservedMemory();
}

long unsigned int CudaMemoryHandlerReservedSize() {
  //static CudaMemoryHandler* theHandler;
  return theHandler.ReservedSize();
}

long unsigned int CudaMemoryHandlerActiveSize() {
  //static CudaMemoryHandler* theHandler;
  return theHandler.ActiveSize();
}
