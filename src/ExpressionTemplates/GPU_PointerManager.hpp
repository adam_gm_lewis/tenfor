#ifndef GPUPOINTERMANAGER_HPP
#define GPUPOINTERMANAGER_HPP

//Store the GPU_Pointers for a given Tensor<DataMesh> or T
//Tensor<Tensor<DataMesh> >. There should be one instance of this class per
//Tensor. Multiple GPU_Pointers are necessary to deal with implicit indexing
//by an integer; i.e. T(i_,0,k_) and T(i_,j_,k_) refer to different sets of
//pointers. 
//
//New pointer lists are constructed and sent to the GPU in the following cases:
//   -when a Tensor is indexed in a particular way for the first time
//   -when the pointers in the list no longer point to the beginnings of the
//    relevant DataMeshes (this would happen if the DMs were resized, for 
//    example).
//
//Otherwise they are simply retrieved, which greatly improves performance, 
//since even small CPU-GPU memcopies are expensive (in realistic cases, the 
//copies take about as much time as the kernels themselves).

template<class X> class Tensor;
class DataMesh;

#include "NonCopyable.hpp"
#include "Tensor_GPUPointers.hpp"
//For Tensor<double>, Tensor<std::vector<double>>, etc, we want this
//class to exist but not to do anything (because we prefer not to
//specialize Tensor itself).
template<class X>
class GPU_PointerManager: private NonCopyable{
};

#ifdef ACCEL_CUDA
#include <map>
#include <memory>

//Therefore we create explicit specializations for all cases in which this
//class should be used.
template<>
class GPU_PointerManager<Tensor<DataMesh> >: private NonCopyable{
  friend class Tensor<DataMesh>;
  
  private:
    //Default constructor ok; don't need = or copy.
    //Default destructor OK because we use unique_ptr.

    //Methods to retrieve the GPUPointers for a particular indexing structure, 
    //constructing them as necessary.
    //Note this returns a *reference*.
    Tensor_GPUPointers<Tensor<DataMesh > >& 
      GPUPointers(Tensor<DataMesh>& me, 
          const int nfree,
          const std::vector<int>& idx_key); 

    Tensor_GPUPointers<Tensor<DataMesh > >& 
      GPUPointers(const Tensor<DataMesh>& me, 
          const int nfree,
          const std::vector<int>& idx_key); 
    
    std::map<int,
      std::unique_ptr<Tensor_GPUPointers<Tensor<DataMesh > > > > mGPUPointers;

};

//Therefore we create explicit specializations for all cases in which this
//class should be used.
template<>
class GPU_PointerManager<Tensor<Tensor<DataMesh> > >: private NonCopyable{
  friend class Tensor<Tensor<DataMesh> >;
  
  private:
    //Default constructor ok; don't need = or copy.
    //Default destructor OK because we use unique_ptr.

    //Methods to retrieve the GPUPointers for a particular indexing structure, 
    //constructing them as necessary.
    //Note this returns a *reference*.
    Tensor_GPUPointers<Tensor<Tensor<DataMesh > > >& 
      GPUPointers(Tensor<Tensor<DataMesh> >& me, 
          const int nfreeL,
          const std::vector<int>& idx_key,
          const int nfreeR);

    Tensor_GPUPointers<Tensor<Tensor<DataMesh > > >& 
      GPUPointers(const Tensor<Tensor<DataMesh> >& me, 
          const int nfreeL,
          const std::vector<int>& idx_key,
          const int nfreeR);
    
    std::map<int,
      std::unique_ptr<Tensor_GPUPointers<Tensor<Tensor<DataMesh > > > > >
        mGPUPointers;

};
#endif

#endif
