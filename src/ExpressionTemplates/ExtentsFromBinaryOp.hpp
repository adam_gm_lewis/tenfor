//#ifndef EXTENTS_FROM_BINARY_OP_HPP
//#define EXTENTS_FROM_BINARY_OP_HPP
#include "OutputVector.hpp"


//================================================================
// ExtentsFromBinaryOp member functions 
// Used to construct DataMesh from expression and for error checking
//
// NOTE:  This code-fragment must be #included INSIDE class DataMesh { ... }
  //================================================================


  // DataMesh OP double
  template<class OP>
  inline const IPoint& ExtentsFromBinaryOp(
		     const BinaryOp<DataMesh, OP, double>& op) {
    return op.lhs.Extents();  }
  // DataMesh OP int
  template<class OP>
  inline const IPoint& ExtentsFromBinaryOp(
		     const BinaryOp<DataMesh, OP, int>& op) {
    return op.lhs.Extents();  }
  // double OP DataMesh 
  template<class OP>
  inline const IPoint& ExtentsFromBinaryOp(
			const BinaryOp<double, OP, DataMesh>& op) {
    return op.rhs.Extents(); }
  // DataMesh OP DataMesh
  template<class OP>
  inline const IPoint& ExtentsFromBinaryOp(
		      const BinaryOp<DataMesh, OP, DataMesh>& op) {
    ASSERT(op.lhs.Extents()==op.rhs.Extents(), 
	   "Extents mismatch WITHIN expression.\n"
	   "op.lhs.Extents()=" << VecToString(op.lhs.Extents())
	   << ", op.rhs.Extents()=" << VecToString(op.rhs.Extents()));
  return op.lhs.Extents(); }
  // DataMesh OP BinaryOp
  template<class OP, class RHS1, class RHSOP, class RHS2>
  inline const IPoint& ExtentsFromBinaryOp(
	const BinaryOp<DataMesh, OP, BinaryOp<RHS1, RHSOP, RHS2> >& op) {
    ASSERT(op.lhs.Extents()==ExtentsFromBinaryOp(op.rhs), 
	   "Extents mismatch WITHIN expression.\n"
	   << "op.lhs.Extents()=" << VecToString(op.lhs.Extents())
	   << ", ExtentsFromBinaryOp(op.rhs)=" << VecToString(ExtentsFromBinaryOp(op.rhs)));
    return op.lhs.Extents(); }
  // BinaryOp OP DataMesh 
  template<class LHS1, class LHSOP, class LHS2, class OP>
  inline const IPoint& ExtentsFromBinaryOp(
        const BinaryOp<BinaryOp<LHS1, LHSOP, LHS2>, OP, DataMesh>& op) {
    ASSERT(ExtentsFromBinaryOp(op.lhs)==op.rhs.Extents(), 
	   "Extents mismatch WITHIN expression.\n"
	   << "ExtentsFromBinaryOp(op.lhs)=" << VecToString(ExtentsFromBinaryOp(op.lhs))
	   << ", op.rhs.Extents()=" << VecToString(op.rhs.Extents()));
    return op.rhs.Extents(); }
  // double OP BinaryOp
  template<class OP, class RHS1, class RHSOP, class RHS2>
  inline const IPoint& ExtentsFromBinaryOp(
	  const BinaryOp<double, OP, BinaryOp<RHS1, RHSOP, RHS2> >& op) {
    return(ExtentsFromBinaryOp(op.rhs)); }
  // BinaryOp OP double
  template<class LHS1, class LHSOP, class LHS2, class OP>
  inline const IPoint& ExtentsFromBinaryOp(
 	   const BinaryOp<BinaryOp<LHS1, LHSOP, LHS2>, OP, double>& op) {
   return ExtentsFromBinaryOp(op.lhs); }
  // BinaryOp OP int
  template<class LHS1, class LHSOP, class LHS2, class OP>
  inline const IPoint& ExtentsFromBinaryOp(
	   const BinaryOp<BinaryOp<LHS1, LHSOP, LHS2>, OP, int>& op) {
   return ExtentsFromBinaryOp(op.lhs); }
  // EmptyType OP DataMesh
  template<class OP>
  inline const IPoint& ExtentsFromBinaryOp(
		  const BinaryOp<EmptyType, OP, DataMesh>& op) {
  return op.rhs.Extents(); }

  // EmptyType OP BinaryOp
  template<class OP, class RHS1, class RHSOP, class RHS2>
  inline const IPoint& ExtentsFromBinaryOp(
	     const BinaryOp<EmptyType, OP, BinaryOp<RHS1, RHSOP, RHS2> >& op) {
    return ExtentsFromBinaryOp(op.rhs); }
  // BinaryOp OP BinaryOP
 template<class L1, class LO, class L2, class OP, class R1, class RO, class R2>
 inline const IPoint& ExtentsFromBinaryOp(
	   const BinaryOp<BinaryOp<L1,LO,L2>, OP, BinaryOp<R1,RO,R2> >& op) {
   const IPoint& result = ExtentsFromBinaryOp(op.lhs);
   ASSERT(result==ExtentsFromBinaryOp(op.rhs),
	  "Extents mismatch WITHIN expression.\n"
	  << ", ExtentsFromBinaryOp(op.lhs)=" << VecToString(ExtentsFromBinaryOp(op.lhs))
	  << ", ExtentsFromBinaryOp(op.rhs)=" << VecToString(ExtentsFromBinaryOp(op.rhs)));
   return result; }
//#endif
