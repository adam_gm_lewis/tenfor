

// convenience header to #include all needed files in the correct order
//
// This file lists headers that can be included before Tensor<T> is
// defined.


#include "ExpressionTemplates.hpp"
#include "TIndexStructure.hpp"
#include "TensorStructure.hpp"
#include "TAssignmentHelpers.hpp"
#include "TLoopApply.hpp"
#include "iBinaryOp.hpp"
#include "BinaryOpHolder.hpp"
#include "PartialSum.hpp"
