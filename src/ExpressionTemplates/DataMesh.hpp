/// \file
/// Defines class DataMesh.

#ifndef DataMesh_hpp
#define DataMesh_hpp

#include <iosfwd>
#include <iostream>
#include <cstddef>

#include "Mesh.hpp"
#include "Assert.hpp"

#ifdef ACCEL_CUDA
#include <cuda_runtime.h>
#endif

#include "GpuArray.hpp"
#include "ExpressionTemplates.hpp"

// This avoids incomprehensible error message when using sqr() on
// doubles after a #include "DataMesh.hpp" but WITHOUT #include
// "Exponents.hpp"
#include "Exponents.hpp"
#include "OutputVector.hpp"
#include "NameOf.hpp"
#include "MemsetToZero.hpp"

#include "ImplicitTensorLoops.hpp"
//================================================================
// Generating CUDA source code: set -DCODE_WRITER
// ================================================================

#ifdef CODE_WRITER
  //#include "ConcreteBinaryOpInfoHolder.hpp"
  //#include "Utils/DataMesh/CodeWriter/ConcreteBinaryOpInfoHolder.hpp"
  //#include "TCodeWriter/lib/ConcreteBinaryOpInfoHolder.hpp"
#endif

//#include "Utils/Tensor/compare.hpp"
//#include "ImplicitTensorLoops.hpp"
//#include "ImplicitTensorLoops-impl.hpp"

//#include "Utils/Tensor/ImplicitTensorLoops/TLoopApply.hpp"
#define CheckExtentsWithASSERT(kExtents, kFunc) \
    ASSERT(this->Extents() == kExtents, "Extents mismatch.");
          //"Extents mismatch between lhs and rhs of " << kFunc << "\n" <<
          //"this->Extents()=" << OutputVector(this->Extents()) << ",\n" <<
          //"Extents passed to op=" << OutputVector(kExtents))

//template<class T> class Tensor;
class DataMesh;

//typedef DataMesh DataSlice;

/// A Mesh holding one double at each grid-point (a grid-function).
///
/// A DataMesh knows how to
/// -# index the data with an integer.
/// -# return a pointer to its data
///
/// Since a DataMesh is a Mesh, it knows
/// -# Its dimension.
/// -# Its extents.
/// -# Its total number of points.
///
/// DataMesh uses expression templates to distribute mathematical functions
/// to all of its points.
///
/// NOTE on EMPTY DataMeshes:
///   A DataMesh can be 'empty', i.e. not being completely constructed.
///   This is indicated by DataMesh::IsEmpty()==True.  In this case,
///     Mesh::Extents() = (),
///     Mesh::Size()==1    (i.e., *not* zero),
///     mData.Size()==0    (i.e. correct, but inconsistent with Mesh::Size()
class DataMesh : public Mesh {
public:
  /// \name Constructors, operator=, destructor.
  //@{
  /// Construct to a given size, data will be uninitialized.
  explicit DataMesh(const Mesh& rMesh);

  enum eEmptyConstructorTag { Empty };

  /// Constructor to make an empty DataMesh.
  /// Mesh is set to that of a point, data pointer is set to null pointer.
  /// In class-initializer lists, one sometimes needs to initialize a DataMesh
  /// or Tensor<DataMesh> without knowing yet the final extents of the DataMesh.
  /// In such situations, this constructor comes in handy, which can be used as
  /// \code
  ///   DataMesh empty(DataMesh::Empty)
  /// \endcode
  /// This is not just the default constructor to encourage use of the
  /// DataMesh(const Mesh&) constructor for proper
  /// creation-is-initialization, which is almost always possible.
  explicit DataMesh(const eEmptyConstructorTag&);

  /// Create and set each datapoint to the value 's'
  //Set in DataMeshAssignmentOperators.hpp
  //DataMesh(const Mesh& rMesh, const double s);

  /// Copy constructor
  DataMesh(const DataMesh& rDataMesh);


public:
  /// Destructor
  ~DataMesh() {
    AddToSizeCounter(-mData.Size());  // differs from Mesh::Size() when empty
  }
  //@}

  /// Query if the DataMesh is in the state DataMesh::Empty
  /// (in that case Mesh::Size() still returns 1)
  bool IsEmpty() const { return mData.Size()==0; }

  /// \name Access to data
  // @{
  /// \name operator[]
  /// operator[] returns data on the host.  For ACCEL*, this requires
  /// checking for every single call, and potentially a synchronization.
  /// AVOID operator[] WHENEVER POSSIBLE!
  // @{
  double & operator[](const int i) {
    ASSERT(!IsEmpty(),"DataMesh is DataMesh::Empty");
    ASSERT(i>=0 && i<Size(), "i=" << i << ", Size()=" << Size());
    return mData.HostDataNonConst()[i];
  }
  const double & operator[](const int i) const {
    ASSERT(!IsEmpty(),"DataMesh is DataMesh::Empty");
    ASSERT(i>=0 && i<Size(), "i=" << i << ", Size()=" << Size());
    return mData.HostData()[i];
  }
  //@}

  /// \name double* Data()
  /// Returns data on the host.  For ACCEL*, this requires
  /// an if-statement, and potentially a synchronization.
  /// It is ok to retrieve the pointer and hold it during a loop
  /// over grid-points.  But while doing so, one cannot use
  /// the DataMesh elsewhere.
  // @{
  double * Data() {
    ASSERT(!IsEmpty(),"DataMesh is DataMesh::Empty");
    return mData.HostDataNonConst();
  }


  const double * Data() const {
    ASSERT(!IsEmpty(),"DataMesh is DataMesh::Empty");
    return mData.HostData();
  }

  double *DeviceDataNonConst() {return mData.DeviceDataNonConst(); }
  const double * DeviceData() const {return mData.DeviceData(); }
  //Used by the old GPU_Array streaming code
  //#ifdef ACCEL_CUDA
  //void ChangeDeviceData(double* new_pointer, const int size)
  //   {mData.UseANewPointerAsDeviceData(new_pointer,size);}
  //#endif
  
  //@}
  // Returns an element of the array on the host without
  // checking whether HostData is synchronized, and without
  // access-checking.
  const double& UncheckedHostAccess(const int i) const {
    return mData.UncheckedHostAccess(i);
  }
  static void DisableSizeCounters() { mSizeCountersDisabled = true; }

  /// STL typedefs and interface
  //@{
  typedef double value_type;
  typedef double& reference;
  typedef const double& const_reference;
  typedef double *iterator;
  typedef const double *const_iterator;
  typedef ptrdiff_t difference_type;
  typedef size_t size_type;

  double *begin() { return Data(); }
  const double *begin() const { return Data(); }
  double *end() { return Data()+mData.Size(); }
  const double *end() const { return Data()+mData.Size(); }
  //@}

  /// returns the number of doubles allocated in all DataMeshes on thread 0.
  static void GetSizeCounters(size_t& currSize, size_t& maxSize) {
    currSize=mSizeCounter;
    maxSize=mMaxSizeCounter;
  }

  void GpuDebug(const std::string& name) const {
    std::cout << "DataMesh " << name << ":";
    mData.operator<<(std::cout);
    std::cout << std::endl;
  }
  
  #ifdef ACCEL_CUDA
  //Ensures host and device data are in sync
  void Sync() {
    mData.Sync();
  }
  #endif
  //Implement copy-swap idiom to avoid repeated code and permit copy elision. 
  friend void swap(DataMesh& first, DataMesh& second);
  
  //This is just a memcopy so we let GPUArray handle it (i.e. we don't use
  //ImplicitTensorLoops::TLoopApply)
  //Pass by value to permit copy elision
  DataMesh& operator=(DataMesh rhs);  

  //Add a move constructor since we are implementing C++11
  DataMesh(DataMesh&& rhs);


  ////METHODS INVOLVING TLOOPAPPLY-IMPL
  #include "ExtentsFromBinaryOp.hpp"
  #include "DataMeshAssignmentOperators.hpp"

  /// Construct from expression.  (we want to be able to do this
  /// implicitly, e.g. DataMesh B=sin(A), so no 'explicit' here. )
  // Both DMs will be synced on the *DEVICE* at the end of this
  template<class LHS, class OP, class RHS>
  DataMesh(const BinaryOp<LHS,OP,RHS>& op):
    Mesh(ExtentsFromBinaryOp(op)),
    mData(Mesh::Size())
  {
    AddToSizeCounter(Mesh::Size());
    ////route through TLoopApply to get expression tagging
    TLoopApply(*this,ImplicitTensorLoops::TSetEqualOp(),op);
  }

  
private:
  GpuArray mData;

  static bool mSizeCountersDisabled;
  static void AddToSizeCounter(const ssize_t diff);
  static size_t mSizeCounter;
  static size_t mMaxSizeCounter;
  // Code for ExtentsFromBinaryOp() member-functions
  // Moved into separate file to keep DataMesh.hpp shorter.
};  // class DataMesh

//================================================================
// FREE FUNCTIONS

//================================================================
std::ostream& operator<<(std::ostream& os, const DataMesh& d);

bool operator==(const DataMesh& lhs, const DataMesh& rhs);
inline bool operator!=(const DataMesh& lhs, const DataMesh& rhs) {
  return !(lhs==rhs);
}

/// Returns whether all numbers in the DataMesh are finite.
/// The header-file Finite.hpp provides Finite() for containers like
/// Tensor<DataMesh>.
bool Finite(const DataMesh& data);
//================================================================
// Operators for expression templates
//================================================================
DEFINE_EXPRESSION_TEMPLATE_ENTRY_OPERATORS(DataMesh)
DEFINE_EXPRESSION_TEMPLATE_UNARY_OPERATORS(DataMesh)

//================================================================
//#include "Utils/LowLevelUtils/NameOf.hpp"
REGISTER_NAME(DataMesh);

#undef CheckExtentsWithASSERT

#endif  // DataMesh_hpp
