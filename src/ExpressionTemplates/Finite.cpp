//============================================================================
#include "Finite.hpp" // should be first include
//============================================================================
#include <limits>
#include <cmath>
//============================================================================

// GNU versions of C++ headers do not expose the standard POSIX isfinite
// macro, because of its association with C99. Instead it gets a wrapper
// placed into the std namespace.
bool Finite(const double x) {
#ifdef __GLIBCXX__
  return std::isfinite(x);
#else
  return isfinite(x);
#endif
}
//============================================================================

namespace MyLimits {
  double NaN() {
    if(std::numeric_limits<double>::has_quiet_NaN) {
      return std::numeric_limits<double>::quiet_NaN();
    }
    return sqrt(-1.0);
  }
}

//============================================================================
