
#ifndef TENSOR_GPUPOINTERS_IMPL_HPP
#define TENSOR_GPUPOINTERS_IMPL_HPP
//This should be included *after* Tensor is defined. It contains code from 
//Tensor needed by Tensor_GPUPointers.

//We only want to deal with the device pointers when ACCEL_CUDA is defined
#ifdef ACCEL_CUDA

#include "DataMesh.hpp"
#include "OutputVector.hpp" //ConcatVectors
#include <vector>
//Full specialization for Tensor<DataMesh>
    //Retrieve all the DataMesh device pointers and bundle them into an
    //array on the device.

namespace PointerGlobals{
  /*
  inline void AssignOneIndex(IPoint& IdxList, int& FR, int i, const int dim,
      const IPoint& idx_key, const IPoint& freeIds){
        if(idx_key[i]==-1){ 
          IdxList[i]=freeIds[FR]; //free index
          ++FR;
        }
        else IdxList[i]=idx_key[i]; //fixed index
      }
  */
  inline int intpow(const int a, const int b){
    int res=a;
    for(int i=0;i<(b-1);++i) res*=a;
    return res;
  }

  //set to the maximum Tensor dimension SpEC will work with.
  inline int magic_dim(){
    return 4;
  }
}

inline Tensor_GPUPointers<Tensor<DataMesh> >::
    Tensor_GPUPointers(Tensor<DataMesh>& me, const int nfree,
        const std::vector<int>& idx_key)
  :isOutOfSync(true),
   mTensor(me),
   //mDim(mTensor.Dim()),
   mDim(PointerGlobals::magic_dim()),
   mTDim(mTensor.Dim()),
   mSize(PointerGlobals::intpow(mDim,nfree)),
   //mRealSize(PointerGlobals::intpow(mTensor.Dim(),nfree)),
   mp_hstPointers(new const double*[mSize]),
   mp_devPointers((double**)CudaMemAllocate(mSize*sizeof(double*))),
   mIdxList(std::vector<IPoint>(mSize,IPoint(mTensor.Rank()))),
   mErr(false), mErrstring("")
{
  BuildIndexList(nfree,idx_key);
  SynchronizePointers();
  //isOutOfSync=true;
  //SynchronizePointersNonConst();
}

inline void Tensor_GPUPointers<Tensor<DataMesh> >::BuildIndexList(
    const int nfree,
    const std::vector<int>& IdxKey)
{
  //This constructs mIdxList, a vector of IPoints to the correct Tensor elements
  //accounting for possibly fixed indices.
  //for(auto i:IdxKey) ASSERT(i>=0 && i<=mDim, "Invalid IdxKey!");
  
  //std::cout << "\nmDim: " << mDim << " nfree: " << nfree << std::endl;
  std::vector<IPoint> TempIdxList(mSize,IPoint(nfree));
  
  //Build an index list ranging over the free indices only.
  {
    int idx=0;
    Mesh FreeMesh(std::vector<int>(nfree,mDim));
    for (MeshIndex jj(FreeMesh);jj;++jj){
      TempIdxList[idx]=jj;
      ++idx;
    }
  }
  
  //Now we have e.g. (0,0),(1,0),(2,0),(0,1),(1,1)...
  //We need (0,A,0,B),(1,A,0,B),(2,A,0,B),(0,A,1,B) for fixed indices
  //A and B.
  {
    for(unsigned long int a=0; a<TempIdxList.size(); ++a){
      int nFixed = 0;
      for(unsigned long int j=0; j<mIdxList[a].size();++j){
        //Test if this is a free index. If it isn't, substitute the value
        //of IdxKey and increment the counter.
        if (-1 != IdxKey[j]){
          mIdxList[a][j] = IdxKey[j];
          ++nFixed;
        
        //It if is, substitute the value of the free index list.
        }else{
          mIdxList[a][j] = TempIdxList[a][j-nFixed];
        }
        if(mIdxList[a][j] >= mTDim) mIdxList[a][j] = (mTDim-1);
      }
    }
  }  
}

inline Tensor_GPUPointers<Tensor<DataMesh> >::~Tensor_GPUPointers(){
    delete[] mp_hstPointers;
    CudaMemRelease(mp_devPointers,mSize*sizeof(double*));
    //CudaMemRelease(mp_devIndex,mIndSize);
}

inline const double** Tensor_GPUPointers<Tensor<DataMesh> >::GetPointers() 
{
  SynchronizePointers();
  return const_cast<const double**>(mp_devPointers);
}

inline double** Tensor_GPUPointers<Tensor<DataMesh> >::GetPointersNonConst() 
{
  SynchronizePointersNonConst();
  return mp_devPointers;
}


//Check all the pointers to make sure they haven't changed. If they have,
//update the device to reflect this.
inline void Tensor_GPUPointers<Tensor<DataMesh> >::SynchronizePointers() 
{
  for(int a=0; a<mSize; ++a){
    const double* thisPointer = mTensor(mIdxList[a]).DeviceData();
    if(!isOutOfSync)
      if(mp_hstPointers[a]!= thisPointer) isOutOfSync=true;
    if(isOutOfSync) mp_hstPointers[a]=thisPointer;
  }
   
  if(isOutOfSync){ 
    mErr=GPU::dMemcpyHostToDevice(mp_devPointers, mp_hstPointers,
                                      mSize*sizeof(double*), mErrstring);
    REQUIRE(mErr,mErrstring);
  }
  isOutOfSync=false;
}

inline void Tensor_GPUPointers<Tensor<DataMesh> >::SynchronizePointersNonConst() 
{
  for(int a=0; a<mSize; ++a){
    double* thisPointer = mTensor(mIdxList[a]).DeviceDataNonConst();
    if(!isOutOfSync){ 
      if(mp_hstPointers[a]!= thisPointer){
        isOutOfSync=true;
      }
    }
    if(isOutOfSync){
      mp_hstPointers[a]=const_cast<const double*>(thisPointer);
    }
  }
  
  if(isOutOfSync){ 
    //the GPU doesn't care about the const flag
    mErr=GPU::dMemcpyHostToDevice(mp_devPointers, mp_hstPointers,
                                      mSize*sizeof(double*), mErrstring);
    REQUIRE(mErr,mErrstring);
  }
  isOutOfSync=false;
}
  


//Full specialization for Tensor<Tensor<DataMesh> >
inline Tensor_GPUPointers<Tensor<Tensor<DataMesh> > >::
  Tensor_GPUPointers(Tensor<Tensor<DataMesh> >& me,
      const int nfreeL, const std::vector<int>& idx_key, const int nfreeR)
  :isOutOfSync(true),
   mTensor(me),
   mDimL(PointerGlobals::magic_dim()),
   mDimR(PointerGlobals::magic_dim()),
   mTDimL(me.Dim()),
   mTDimR(me.begin()->Dim()),
   mSize(PointerGlobals::intpow(mDimL,nfreeL)
       *PointerGlobals::intpow(mDimR,nfreeR)), 
   mp_hstPointers(new const double* [mSize]),
   mp_devPointers((double**)CudaMemAllocate(mSize*sizeof(double*))),
   mIdxListL(std::vector<IPoint>(mSize,IPoint(me.Rank()))),
   mIdxListR(std::vector<IPoint>(mSize,IPoint(me.begin()->Rank()))),
   mErr(false), mErrstring("")
{
  
  //send the index structure to device
  //const int* iVec = mTensor.Structure().IndexVector().Data();
  //mErr=GPU::dMemcpyHostToDevice(mp_devIndex, iVec,mIndSize,mErrstring);
  //REQUIRE(mErr,mErrstring);
  BuildIndexList(nfreeL, nfreeR, idx_key);
  SynchronizePointers();
  isOutOfSync=true;
  SynchronizePointersNonConst();
}

inline void Tensor_GPUPointers<Tensor<Tensor<DataMesh> > >::BuildIndexList(
    const int nfreeL, const int nfreeR,
    const std::vector<int>& IdxKey)
{
  //This constructs mIdxList, a vector of IPoints to the correct Tensor elements
  //accounting for possibly fixed indices.

  //for(auto i:IdxKey ASSERT(i>=0 && i<=mDim, "Invalid IdxKey!");
  
  
  std::vector<IPoint> TempIdxList(mSize, IPoint(nfreeL+nfreeR));
  
  //Build lists of indices, breaking a vector ranging over all the free indices
  //into two pieces, one for each Tensor. Note the *left* Tensor should
  //oscillate most quickly.
  { 
    int idx=0;
    Mesh FreeMeshLeft(std::vector<int>(nfreeL, mDimL));
    Mesh FreeMeshRight(std::vector<int>(nfreeR,mDimR));
    for(MeshIndex kk(FreeMeshRight);kk;++kk){
      for(MeshIndex jj(FreeMeshLeft);jj;++jj){
        TempIdxList[idx]= ConcatVectors(jj, kk);
        ++idx;
      }
    }
  }
  //Now we have e.g. (0,0),(1,0),(2,0),(0,1),(1,1)...
  //We need (0,A,0,B),(1,A,0,B),(2,A,0,B),(0,A,1,B) for fixed indices
  //A and B.
  for(unsigned long int a=0;a<TempIdxList.size();++a){ 
    int nFixed=0;
    IPoint this_entry(IdxKey.size(),0);
    for(unsigned long int j=0; j<IdxKey.size();++j){ 
      //Test if this is a free index. If it isn't, substitute the value of 
      //IdxKey and increment the counter.
      if(-1 != IdxKey[j]){
        this_entry[j] = IdxKey[j];  
        ++nFixed;
      }else{
        this_entry[j] = TempIdxList[a][j-nFixed];
      }
    }
    //Now break the entry up into two vectors, one for the left and one for the
    //right.
    unsigned long int i = 0;
    const unsigned long int Lsz = mIdxListL[a].size();
    while(i<Lsz){
      if(this_entry[i] >= mTDimL) this_entry[i] = mTDimL-1;
      mIdxListL[a][i] = this_entry[i];
      ++i;
    }
    while(i<this_entry.size()){
      if(this_entry[i] >= mTDimR) this_entry[i] = mTDimR-1;
      mIdxListR[a][i-Lsz] = this_entry[i];
      ++i;
    }
  }

  
}  


inline Tensor_GPUPointers<Tensor<Tensor<DataMesh> > >::~Tensor_GPUPointers(){
    delete[] mp_hstPointers;
    //delete mp_hstPointersNC;
    CudaMemRelease(mp_devPointers,mSize*sizeof(double*));
    //CudaMemRelease(mp_devIndex,mIndSize);
}

inline const double** Tensor_GPUPointers<Tensor<Tensor<DataMesh> > >::GetPointers() 
{
  SynchronizePointers();
  return const_cast<const double**>(mp_devPointers);
}

inline double** Tensor_GPUPointers<Tensor<Tensor<DataMesh> > >::GetPointersNonConst() 
{
  SynchronizePointersNonConst();
  return mp_devPointers;
}

//Check all the pointers to make sure they haven't changed. If they have,
//update the device to reflect this.
inline void Tensor_GPUPointers<Tensor<Tensor<DataMesh> > >::SynchronizePointers() 
{
  for(int a=0; a<mSize; ++a){
    const double* thisPointer 
      = mTensor(mIdxListL[a])(mIdxListR[a]).DeviceData();
    if(!isOutOfSync)
      if(mp_hstPointers[a]!= thisPointer) isOutOfSync=true;
    if(isOutOfSync) mp_hstPointers[a]=thisPointer;
  }
   
  if(isOutOfSync){ 
    mErr=GPU::dMemcpyHostToDevice(mp_devPointers, mp_hstPointers,
                                      mSize*sizeof(double*), mErrstring);
    REQUIRE(mErr,mErrstring);
  }
  isOutOfSync=false;
}

inline void Tensor_GPUPointers<Tensor<Tensor<DataMesh> > >::
  SynchronizePointersNonConst() 
{
  for(int a=0; a<mSize; ++a){
    double* thisPointer 
      = mTensor(mIdxListL[a])(mIdxListR[a]).DeviceDataNonConst();
    if(!isOutOfSync) 
      if(mp_hstPointers[a]!= thisPointer) isOutOfSync=true;
    if(isOutOfSync){
      mp_hstPointers[a]=const_cast<const double*>(thisPointer);
    }
  }
  
  if(isOutOfSync){ 
    //the GPU doesn't care about the const flag
    mErr=GPU::dMemcpyHostToDevice(mp_devPointers, mp_hstPointers,
                                      mSize*sizeof(double*), mErrstring);
    REQUIRE(mErr,mErrstring);
  }
  isOutOfSync=false;
}
#endif

#endif

