//=============================================================================
/// \file
/// Defines class Tensor<X>.
//=============================================================================
#ifndef Tensor_hpp
#define Tensor_hpp
//=============================================================================
//#include "std::vector.hpp"
#include "Assert.hpp"
#include "Require.hpp"
#include "NameOf.hpp"

// template declarations for implicit tensor loops
#include "ImplicitTensorLoops.hpp"

#ifdef CODE_WRITER
  #include "ConcreteTExpressionTreeHouse.hpp"
#endif

#include <memory> //std::unique_ptr
#include "TensorStructure.hpp"
#include "OutputVector.hpp"
#include "MeshIndex.hpp"

#include "GPU_PointerManager.hpp"
//=============================================================================
class TensorIter;
class DataMesh; // for typedefs and REGISTER_NAMES

template<class Tidx1> class IndexedTensor1;
template<class Tidx1, class Tidx2> class IndexedTensor2;

template<class Tidx1> class ConstIndexedTensor1;
template<class Tidx1, class Tidx2> class ConstIndexedTensor2;


//=============================================================================
/// Stores elements of a tensor \f$t_{ij\ldots}\f$.
/// -The tensor can have rank up to seven (this limit is easily extensible).
/// -Symmetric sets of indices are supported.
///
/// Rank and symmetries are specified by a string:
///  For example, "13132" indicates a rank 5 object,
///  symmetric on the 1st and 3rd and on the 2nd and 4th indices.
///
/// The \e structure of a tensor is the combination of dimension
/// (i.e. range of possible values for each index), and the
/// symmetry-string).  This information is stored in the class TensorStructure.
///
/// \nosubgrouping
template<class X>
class Tensor {
public:

  explicit Tensor(const TensorStructure::eEmptyConstructorTag&):
    mStructure(), mData(),
    mpGPU_Ptrs(new GPU_PointerManager<Tensor<X> >()) { }

  /// @name Constructors
  /// All constructors take \e either a
  /// TensorStructure as first argument, \e or they take the dimension
  /// and the symmetry-string as first two arguments. The variadic arguments
  /// pack 'args' passes 0 or more arguments to the constructor of X
  /// (i.e. X::X(args...)) to construct X in-place in the Tensor.
  ///   For example, \code
  ///  Tensor<DataMesh> Gamma(3, "122", aSubdomain);  // constructs each DataMesh by DataMesh::DataMesh(aSubdomain)
  /// \endcode
  //@{
  template<typename... Args>
  Tensor(int dim, const std::string& symm, const Args& ...args):
    mStructure(dim, symm), 
    mData(mStructure.Size(), X(args...)) { }
  template<typename... Args>
  Tensor(const TensorStructure& s, const Args& ...args):
    mStructure(s), 
    mData(mStructure.Size(), X(args...)) { }

  // --- zero additional arguments (i.e. using default constructor X::X() )
  Tensor(int dim, const std::string& symm):
    mStructure(dim, symm), mData(mStructure.Size()),
    mpGPU_Ptrs() {}
  explicit Tensor(const TensorStructure& s):
    mStructure(s), mData(mStructure.Size()),
    mpGPU_Ptrs(new GPU_PointerManager<Tensor<X> >()) { }
  // --- one additional argument ---
  template<class A1>
  Tensor(int dim, const std::string& symm, const A1& a1): // uses X::X(arg)
    mStructure(dim, symm), 
    mData(mStructure.Size(), X(a1)),
    mpGPU_Ptrs(new GPU_PointerManager<Tensor<X> >()) { }
  template<class A1>
  Tensor(const TensorStructure& s, const A1& a1):
    mStructure(s), mData(mStructure.Size(), X(a1)),
    mpGPU_Ptrs(new GPU_PointerManager<Tensor<X> >()) { }
  // --- two additional arguments ---
  template<class A1,class A2>
  Tensor(int dim,const std::string& symm,const A1& a1,const A2 &a2):
    mStructure(dim, symm), mData(mStructure.Size(), X(a1, a2)),
    mpGPU_Ptrs(new GPU_PointerManager<Tensor<X> >()){ }
  template<class A1,class A2>
  Tensor(const TensorStructure& s,const A1& a1,const A2& a2):
    mStructure(s), mData(mStructure.Size(), X(a1, a2)),
    mpGPU_Ptrs(new GPU_PointerManager<Tensor<X> >()){ }
  // --- three additional arguments ---
  template<class A1,class A2, class A3>
  Tensor(int dim, const std::string& symm,
	 const A1& a1, const A2 &a2, const A3 &a3):
    mStructure(dim, symm), mData(mStructure.Size(), X(a1, a2, a3)),
    mpGPU_Ptrs(new GPU_PointerManager<Tensor<X> >()){ }
  template<class A1,class A2, class A3>
  Tensor(const TensorStructure& s, const A1& a1, const A2 &a2, const A3 &a3):
    mStructure(s), mData(mStructure.Size(), X(a1, a2, a3)),
    mpGPU_Ptrs(new GPU_PointerManager<Tensor<X> >()){ }

 virtual ~Tensor() {} // virtual because Tensor is sometimes a base-class

 //need copy constructor and = for the unique_ptr
 Tensor(const Tensor<X>& rhs):
   mStructure(rhs.mStructure), mData(rhs.mData),
   mpGPU_Ptrs(new GPU_PointerManager<Tensor<X> >()){};

 Tensor<X>& operator = (Tensor<X> rhs){
    swap(*this, rhs);
    return *this;
 }

 friend void swap(Tensor<X>& lhs, Tensor<X>& rhs){
    using std::swap;
    std::swap(lhs.mStructure, rhs.mStructure);
    std::swap(lhs.mData, rhs.mData);
    std::swap(lhs.mpGPU_Ptrs, rhs.mpGPU_Ptrs);
 }

 Tensor(Tensor<X>&& other) : Tensor<X>(TensorStructure::Empty)
 {
   swap(*this, other);
 }
 //@}

  //----------------------------------------|
  // *** assign-functions ***               |
  //----------------------------------------|
  /// \name assign-functions
  /// Any combination of arguments that can be passed into the constructor
  /// can also be passed into the assign function.  The Tensor will be
  /// reset to the same state as a Tensor constructed from those arguments.
  //@{

  /// copy another tensor
  void assign(const Tensor& other) {
    mStructure = other.mStructure;
    mData.assign(other.mData);
    mpGPU_Ptrs.reset(new GPU_PointerManager<Tensor<X> >());
  }
  template<typename... Args>
  void assign(int dim, const std::string& symm, const Args& ...args) {
    mStructure = TensorStructure(dim, symm);
    mData.assign(mStructure.Size(), X(&args...) );
    mpGPU_Ptrs.reset(new GPU_PointerManager<Tensor<X> >());
  }
  template<typename... Args>
  void assign(const TensorStructure& s, const Args& ...args) {
    mStructure = s;
    mData.assign(mStructure.Size(), X(&args...));
    mpGPU_Ptrs.reset(new GPU_PointerManager<Tensor<X> >());
  }
  // --- one additional argument ---
  template<class A1>
  void assign(int dim, const std::string& symm, const A1& a1) { // uses X::X(arg)
    mStructure = TensorStructure(dim, symm);
    mData.assign(mStructure.Size(), X(a1));
    mpGPU_Ptrs.reset(new GPU_PointerManager<Tensor<X> >());
  }
  template<class A1>
  void assign(const TensorStructure& s, const A1& a1) {
    mStructure = s;
    mData.assign(mStructure.Size(), X(a1));
    mpGPU_Ptrs.reset(new GPU_PointerManager<Tensor<X> >());
  }
  // --- two additional arguments ---
  template<class A1,class A2>
  void assign(int dim,const std::string& symm,const A1& a1,const A2 &a2) {
    mStructure = TensorStructure(dim, symm);
    mData.assign(mStructure.Size(), X(a1, a2));
    mpGPU_Ptrs.reset(new GPU_PointerManager<Tensor<X> >());
  }
  template<class A1,class A2>
  void assign(const TensorStructure& s,const A1& a1,const A2& a2) {
    mStructure = s;
    mData.assign(mStructure.Size(), X(a1, a2));
    mpGPU_Ptrs.reset(new GPU_PointerManager<Tensor<X> >());
  }
  // --- three additional arguments ---
  template<class A1,class A2, class A3>
  void assign(int dim, const std::string& symm,
	 const A1& a1, const A2 &a2, const A3 &a3) {
    mStructure = TensorStructure(dim, symm);
    mData.assign(mStructure.Size(), X(a1, a2, a3));
    mpGPU_Ptrs.reset(new GPU_PointerManager<Tensor<X> >());
  }
  template<class A1,class A2, class A3>
  void assign(const TensorStructure& s, const A1& a1, const A2 &a2, const A3 &a3) {
    mStructure = s;
    mData.assign(mStructure.Size(), X(a1, a2, a3));
    mpGPU_Ptrs.reset(new GPU_PointerManager<Tensor<X> >());
  }
  //@}

  /// @name STL-interface
  //@{
  typedef typename std::vector<X>::value_type value_type;
  typedef typename std::vector<X>::reference reference;
  typedef typename std::vector<X>::const_reference const_reference;
  typedef typename std::vector<X>::iterator iterator;
  typedef typename std::vector<X>::const_iterator const_iterator;
  typedef typename std::vector<X>::pointer pointer;
  typedef typename std::vector<X>::const_pointer const_pointer;

  iterator begin()             { return mData.begin(); }
  const_iterator begin() const { return mData.begin(); }
  iterator end()               { return mData.end(); }
  const_iterator end() const   { return mData.end(); }
  //@}

  /// @name Indexing functions
  /// The number of indices used when indexing the Tensor must equal
  /// its rank.
  //@{
  /// Indexing by an IPoint that holds (n1,n2,...)
  reference       operator()(const IPoint &index)       {
    //    std::cout << "Tensor::operator(" << index << ")       -> data["
    //<< mStructure.Index(index) << "]" << std::endl;
    return mData[mStructure.Index(index)]; }
  const_reference operator()(const IPoint &index) const {
    //std::cout << "Tensor::operator(" << index << ") const -> data["
    //<< mStructure.Index(index) << "]" << std::endl;
    return mData[mStructure.Index(index)]; }
  reference operator()() {
    return mData[mStructure.Index()]; }
  reference operator()(int n1){
    return mData[mStructure.Index(n1)]; }
  reference operator()(int n1,int n2){
    return mData[mStructure.Index(n1,n2)]; }
  reference operator()(int n1,int n2,int n3){
    return mData[mStructure.Index(n1, n2, n3)]; }
  reference operator()(int n1,int n2,int n3,int n4) {
    return mData[mStructure.Index(n1, n2, n3, n4)]; }
  reference operator()(int n1,int n2,int n3,int n4,int n5) {
    return mData[mStructure.Index(n1, n2, n3, n4, n5)]; }
  reference operator()(int n1,int n2,int n3,int n4,int n5,int n6) {
    return mData[mStructure.Index(n1, n2, n3, n4, n5, n6)]; }
  reference operator()(int n1,int n2,int n3,int n4,int n5,int n6,int n7) {
    return mData[mStructure.Index(n1, n2, n3, n4, n5, n6, n7)]; }
  const_reference operator()() const{
    return mData[mStructure.Index()];  }
  const_reference operator()(int n1) const{
    return mData[mStructure.Index(n1)];  }
  const_reference operator()(int n1,int n2) const{
    return mData[mStructure.Index(n1,n2)];  }
  const_reference operator()(int n1,int n2,int n3) const{
    return mData[mStructure.Index(n1,n2,n3)];  }
  const_reference operator()(int n1,int n2,int n3,int n4) const{
    return mData[mStructure.Index(n1,n2,n3,n4)];  }
  const_reference operator()(int n1,int n2,int n3,int n4,int n5) const{
    return mData[mStructure.Index(n1,n2,n3,n4,n5)];  }
  const_reference operator()(int n1,int n2,int n3,int n4,int n5,int n6) const{
    return mData[mStructure.Index(n1,n2,n3,n4,n5,n6)];  }
  const_reference operator()(int n1,int n2,int n3,int n4,int n5,int n6,
                             int n7) const{
    return mData[mStructure.Index(n1,n2,n3,n4,n5,n6,n7)];  }

  /// Return component pointed to by TensorIter
  const_reference operator[](const TensorIter& iter) const;
  /// Return component pointed to by TensorIter
  reference operator[](const TensorIter& iter);
  /// Return the number of elements in the tensor
  int Size() const { return mStructure.Size(); }
  /// Return the i-th element. DO NOT USE IT IF YOU DON'T HAVE TO.
  const_reference operator[](int i) const { return mData[i]; }
  reference operator[](int i) { return mData[i]; }

  //access to the GPU_Ptrs
  //The data can then be accessed as //mpGPU_Ptrs->GetPointers{NonConst}()
  //This one is for Tensor<DataMesh>
  Tensor_GPUPointers<Tensor<X> >& GPUPointers(
      const int nfree,
      const std::vector<int>& idx_key) const
  {
    Tensor_GPUPointers<Tensor<X> >& the_pointers 
      = mpGPU_Ptrs->GPUPointers(*this,nfree,idx_key);
    return the_pointers;
  
    return mpGPU_Ptrs->GPUPointers(*this,nfree,idx_key);
  }
  
  ////Also Tensor<DataMesh>
  Tensor_GPUPointers<Tensor<X> >& GPUPointersNonConst(
      const int nfree,
      const std::vector<int>& idx_key) 
  {
    return mpGPU_Ptrs->GPUPointers(*this,nfree,idx_key);
  }
 
  ////Tensor<Tensor<DataMesh>>
  Tensor_GPUPointers<Tensor<X> >& GPUPointers(
      const int nfreeL,
      const std::vector<int>& idx_key,
      const int nfreeR) const
  {
      return mpGPU_Ptrs->GPUPointers(*this,nfreeL,idx_key,nfreeR);
  }

  ////Tensor<Tensor<DataMesh>>
  Tensor_GPUPointers<Tensor<X> >& GPUPointersNonConst(
      const int nfreeL,
      const std::vector<int>& idx_key,
      const int nfreeR) 
  {
      return mpGPU_Ptrs->GPUPointers(*this,nfreeL,idx_key,nfreeR);
  }
  

  #include "Tensor_IndexedOperators.hpp"

  /// @name Further query functions
  //{@
  const TensorStructure& Structure() const { return mStructure; }
  int Rank()   const { return mStructure.Rank(); }
  int Dim()    const { return mStructure.Dim(); }
  bool Empty() const { return mData.size()==0; }
public:
  /// reverse lookup: given an iterator, what is one set of indices
  /// pointing to that data-element? (Note: For tensors with
  /// symmetries, there are multiple sets of indices which will yield
  /// the same element)
  const IPoint& Indices(const iterator& it) const {
    return mStructure.Indices(static_cast<int>(it-begin())); }
  const IPoint& Indices(const const_iterator& it) const {
    return mStructure.Indices(static_cast<int>(it-begin())); }

public:
  /// Given an iterator, how many sets of indices point to that
  /// data element?
  int Multiplicity(const iterator&it) const {
    return mStructure.Multiplicity(static_cast<int>(it-begin())); }
  int Multiplicity(const const_iterator&it) const {
    return mStructure.Multiplicity(static_cast<int>(it-begin())); }

public:
  /// Outputs the TensorStructure and the elements.
  std::ostream& Output(std::ostream& s) const {
    mStructure.Output(s);  
    OutputVector(s, mData); 
    return s;
    
  }
  //}@

  //----------------------------------------|
  // Implementation details                 |
  //----------------------------------------|
private:
  TensorStructure mStructure; // description
  std::vector<X> mData; // storage
  
  //this needs to be mutable so the pointers can be updated even for const
  //iBinaryOps. We need a const and nonconst access function because iBinaryOp
  //stores only a reference to the Tensor which cannot be declared mutable,
  //so const correctness is important here.
  mutable std::unique_ptr<GPU_PointerManager<Tensor<X> > > mpGPU_Ptrs;
};


template<class X>
inline std::ostream& operator<<(std::ostream& os, const Tensor<X>& x) {
  return x.Output(os);
}

//------------------------------|
// operator==, operator!=       |
//------------------------------|
template<class X>
bool operator==(const Tensor<X>& lhs, const Tensor<X>& rhs) {
  if(lhs.Structure()!=rhs.Structure()) return false;
  typename Tensor<X>::const_iterator l=lhs.begin(), r=rhs.begin();
  while(l!=lhs.end()) {
    if(*l != *r) return false;
    ++l; ++r;
  }
  return true;
}
template<class X>
bool operator!=(const Tensor<X>& lhs,const Tensor<X>& rhs) {
  return !(lhs==rhs);
}


//================================================================

/// Iterates through all components of a Tensor.  You can index
/// mulitple Tensor's with the same TensorStructure using one
/// TensorIter.
class TensorIter {
public:
  template<class X>
  explicit TensorIter(const Tensor<X>& t):
    mSize(t.Structure().Size()), mIdx(0), mStruct(t.Structure()) { }
  TensorIter(const TensorStructure& Ts):
    mSize(Ts.Size()), mIdx(0), mStruct(Ts) { }

  typedef void (*PtrToFunc)(void);
  /// Conversion operator used inside for-loops.
  /// \code
  ///   for(TensorIter it(aTensor); it; ++it) {
  ///     ...
  ///   }
  /// \endcode
  /// The absurd return type works in the for-loop, however, it cannot
  /// be converted implicitly to an int, and therefore, this bug will
  /// not compile: \c aTensor(it).
  operator PtrToFunc() const {
    return reinterpret_cast<PtrToFunc>(mIdx!=mSize ? 1:0); }
  TensorIter& operator++() {
    ASSERT(mIdx<mSize, "operator++ beyond end of range");
    ++mIdx;
    return *this;
  }

  /// Number of component in Tensor currently pointed to.
  int Index() const { return mIdx; }

  /// The TensorStructure associated with this TensorIter.
  const TensorStructure& Structure() const { return mStruct; }
private:
  const int mSize;
  int mIdx;
  const TensorStructure mStruct; // TensorStructure is small, keep value.
};


//================================================================
// Functions of Tensor which index using TensorIter
//================================================================

template<class X>
typename Tensor<X>::const_reference
Tensor<X>::operator[](const TensorIter& it) const {
  ASSERT((it.Structure().Rank()==0 && Rank()==0) ||
         it.Structure()==Structure(), "TensorStructure mismatch");
  return mData[it.Index()];
}
template<class X>
typename Tensor<X>::reference
Tensor<X>::operator[](const TensorIter& it) {
  ASSERT((it.Structure().Rank()==0 && Rank()==0) ||
         it.Structure()==Structure(), "TensorStructure mismatch");
  return mData[it.Index()];
}



//================================================================
// ASSERT_TensorStructure, REQUIRE_TensorStructure macros
//
// These macros simplify the typing necessary to verify that a given
// Tensor has the expected structure.
//
// The template-functions FIRST_ARGUMENT... have the purpose of checking
// whether the first argument to the REQUIRE-macro is actually a
// Tensor.  If yes, the template-function matches and compiles (and
// should be optimized away).  If no, no matching template-function
// is found, and the compiler output mentions the name of that
// function, which serves as the diagnostic output.
// ================================================================

template<class X>
void FIRST_ARGUMENT_TO_REQUIRE_TensorStructure_MUST_BE_TENSOR(const Tensor<X>&)
{ }
template<class X>
void FIRST_ARGUMENT_TO_ASSERT_TensorStructure_MUST_BE_TENSOR(const Tensor<X>&)
{ }

/// Checks (always) that a tensor has a certain structure.
/// The first argument is the tensor, the second and third the dimension
/// and symmetry string the tensor should have.
#define REQUIRE_TensorStructure(theTensor, theDim, theSymm) {               \
   FIRST_ARGUMENT_TO_REQUIRE_TensorStructure_MUST_BE_TENSOR(theTensor);     \
   REQUIRE((theTensor).Structure() == TensorStructure(theDim, theSymm),     \
	   "(" << #theTensor << ").Structure()=" << (theTensor).Structure() \
	   << ", TensorStructure(" #theDim ", " #theSymm ")="               \
	   << TensorStructure(theDim, theSymm)); }

/// Checks (if -DDEBUG) that a tensor has a certain structure.
/// The first argument is the tensor, the second and third the dimension
/// and symmetry string the tensor should have.
#if defined DEBUG
#define ASSERT_TensorStructure(theTensor, theDim, theSymm) {                 \
   FIRST_ARGUMENT_TO_ASSERT_TensorStructure_MUST_BE_TENSOR(theTensor);       \
   ASSERT((theTensor).Structure() == TensorStructure(theDim, theSymm),       \
	    "(" << #theTensor << ").Structure()=" << (theTensor).Structure() \
	   << ", TensorStructure(" #theDim ", " #theSymm ")="                \
	   << TensorStructure(theDim, theSymm)); }
#else
#define ASSERT_TensorStructure(theTensor, theDim, theSymm) { }
#endif

/// Commonly used convenience typedef for Tensor<DataMesh>
typedef Tensor<DataMesh> TDm;
/// Commonly used convenience typedef for Tensor<Tensor<DataMesh> >
typedef Tensor<Tensor<DataMesh> > TTDm;

REGISTER_NAME(Tensor<double>);
REGISTER_NAME(Tensor<DataMesh>);
REGISTER_NAME(Tensor<Tensor<DataMesh> >);
REGISTER_NAME(std::vector<Tensor<Tensor<DataMesh> > >);
REGISTER_NAME(std::vector<Tensor<DataMesh> >);
REGISTER_NAME(Tensor<std::vector<DataMesh> >);
REGISTER_NAME(Tensor<std::vector<double> >);
// implementation for implicit tensor loops templates,
// that depend on the defintion of Tensor<T>
#include "GPU_PointerManager-impl.hpp"
#include "ImplicitTensorLoops-impl.hpp"

//#ifndef NONACCEL
//#ifndef CODE_WRITER
//#include "Utils/Tensor/AutoExpressions/AutoExpressions.hpp"
//#endif
//#endif

#endif
