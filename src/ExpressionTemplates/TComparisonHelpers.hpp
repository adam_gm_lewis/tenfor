#ifndef TComparison_hpp
#define TComparison_hpp

#include "UtilsForTesting.hpp"
#include "Assert.hpp"
#include "Require.hpp"
#include <vector>
#include <list>
#include <cstdlib>
namespace ImplicitTensorLoops {
  // helper classes to compare iBinaryOp data 
  //
  // Arguments are templated here to make the compiler happy
  // and to avoid nested #include dependencies, even though
  // the lhs and rhs types are always known.

  static inline double compareD_imp(const double A, const double B)
  {
    return (A-B)/(fabs(A)+fabs(B)+1);
  }

  //DATAMESH
  //Test whether two DataMeshes or TDMs are equal
  template<class L>  
  static inline double compareDM_imp(const L& A, const L& B)
  {
    REQUIRE(A.Size()==B.Size(), "Size mismatch.");
    double sum=0.0;
    for(int idx=0; idx<A.Size(); ++idx){
      sum += compareD_imp(A[idx],B[idx]);
    }
    return sum;
  }
  
  //Access
  template<class L>  
  static inline void compareDM(const L& A, const L& B, 
      const std::string& expstr="")
  {
    double sum=compareDM_imp(A,B); 
    IS_ZERO_EPS(sum,expstr);
  }

  //TENSOR<DATAMESH>
  template<class L>  
  static inline double compareTDM_imp(const L& A, const L& B)
  {
    REQUIRE(A.Structure()==B.Structure(), "Structure mismatch.");
    typename L::const_iterator Ait=A.begin();
    typename L::const_iterator Bit=B.begin();
    double sum=0.0;
    for(;Ait!=A.end();++Ait,++Bit){
      sum+=compareDM_imp(*Ait,*Bit);
    }
    return sum;
  }
 
  template<class L>
  static inline double comparelistDM_imp(const std::list<L>& A, const std::list<L>& B)
  {
    double sum=0.0;
    auto Ait = A.begin();
    auto Bit = B.begin();
    for (;Ait!=A.end();++Ait,++Bit){
      sum+=compareDM_imp(*Ait,*Bit);
    }
    return sum;
  }


  template<class L>
  static inline void comparelistDM(const std::list<L>& A, const std::list<L>& B,
      const std::string expstr="")
  {
    double sum = comparestd::listDM_imp(A,B);
    IS_ZERO_EPS(sum,expstr);
  }

  template<class L>  
  static inline void compareTDM(const L& A, const L& B, 
      const std::string expstr="")
  {
    double sum = compareTDM_imp(A,B);
    IS_ZERO_EPS(sum,expstr);
  }
  
  //TENSOR<TENSOR<DATAMESH>>
  template<class L>
  static inline double compareTTDM_imp(const L& A, const L& B) 
  {
    REQUIRE(A.Structure()==B.Structure(), "Structure mismatch.");
    typename L::const_iterator Ait=A.begin();
    typename L::const_iterator Bit=B.begin();
    double sum=0.0;
    for(;Ait!=A.end();++Ait,++Bit){
      sum+=compareTDM_imp(*Ait,*Bit);
    }
    return sum;
  }

  template<class L>
  static inline void compareTTDM(const L& A, const L& B, 
      const std::string expstr="") 
  {
    double sum = compareTTDM_imp(A,B);
    IS_ZERO_EPS(sum,expstr);
  }
}
#endif
