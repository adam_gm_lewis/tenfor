///
/// \file
/// Defines class GpuArray.

#ifndef GpuArray_NONACCEL_hpp
#define GpuArray_NONACCEL_hpp

#include "Mesh.hpp"         
#include <ostream>


/// Holds an array of doubles of size sz, and synchronizes it
/// between Host and Device as needed.
///
/// if NONACCEL or ACCEL_CPU, keeps only one copy of data, and returns 
/// it through HostData() and DeviceData().  Otherwise, keeps
/// two separate copies, and synchronizes back and forth.
class GpuArray {
public:
  GpuArray();
  /// initializes Host and Device to zero.  
  GpuArray(const int sz);
  /// Copy constructor - copies whichever of {Host, Device} is
  /// up-to-date. 
  GpuArray(const GpuArray& rhs);
  GpuArray & operator=(GpuArray rhs);
  friend void swap(GpuArray& first, GpuArray& second);
  //move constructor for rvalues
  GpuArray(GpuArray&& rhs);
  ~GpuArray();

  // resizes Host and Device, and sets the data to zero.  
  void Resize(const int new_size);

  int Size() const { return mSz; }

  const double* HostData() const   { return mpData; }
  double* HostDataNonConst()       { return mpData; }

  const double* DeviceData() const { return mpData; }
  double* DeviceDataNonConst()     { return mpData; }

  // this function is called in assignments LHS=...; 
  // in such a case there is no need to synchronize data
  // from the host. 
  double* PossiblyInvalidDeviceDataToOverwrite() { return mpData; }

  /// Returns an element of the array on the host without
  /// checking whether HostData is synchronized, and without
  /// access-checking.  To be used inside checked expression-templates,
  /// to avoid point-by-point asserts.
  const double& UncheckedHostAccess(const int i) const { return mpData[i]; }


  std::ostream& operator<<(std::ostream& out) const {
    out << "sz=" << mSz;
    out << " (no GPU-synchronization)";
    return out;
  }
  
private:
  int mSz; // size 
  double* mpData;
};  


#endif  
