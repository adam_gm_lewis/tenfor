#ifndef SPEC_UTILS_LOWLEVELUTILS_MEMSETTOZERO_HPP_
#define SPEC_UTILS_LOWLEVELUTILS_MEMSETTOZERO_HPP_

#include <cstring>
#include <limits>

/**
 * Set all elements in an array to zero.  If the zero value of T is
 * represented by a sequence of zero bytes, then memset will be used to
 * quickly zero the array.  Otherwise, each element will be explicitly
 * set to zero (which is 3x slower for doubles in Bela's experiments).
 *
 * Currently, memset is called regardless, so the array will be filled
 * with zero bytes before assignment is attempted (thus, as implemented,
 * this function is only safe for PODs).
 *
 * @param s Array whose elements should be set to zero
 * @param n Number of elements in the array
 */
template<typename T>
inline void MemsetToZero(T* s, size_t n) {
  if (n > 0) {
    memset(s, 0, n*sizeof(T));
    if (!std::numeric_limits<T>::is_iec559 &&
        (s[0] != static_cast<T>(0))) {
      for (size_t i=0; i<n; ++i) {
        s[i] = 0;
      }
    }
  }
}

#endif  // SPEC_UTILS_LOWLEVELUTILS_MEMSETTOZERO_HPP_

