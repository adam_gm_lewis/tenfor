#ifndef IPow_hpp
#define IPow_hpp

/// Raise b to the e power
int IPow(int b, int e);

/// Raise b to the e power
double IPow(double b, int e);

#endif
