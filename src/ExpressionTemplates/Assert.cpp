/// Defines the function AssertFail, called to handle ASSERT failure.

#include "Assert.hpp"
#include <stdexcept>

//#include "Breakpoint.hpp"
//#include "CodeError.hpp"

/// Handle an ASSERT failure. Print a useful diagnostic message and a
/// backtrace, and throw an exception to be caught in the termination handler.
void AssertFail(const char* expression, const char* file, const int line,
                std::string msg) {
  std::stringstream ss;
  ss << "\n#######################  ASSERT  FAILED  #######################\n"
     << "#### " << expression << "  violated\n"
     << "#### " << file << " (line " << line << ")\n"
     << msg
     << "\n################################################################\n";
  throw std::runtime_error(ss.str());
  // Debuggers will usually break when MyAbort() is called when the
  // CodeError is unhandled, but we want to ensure that we get a
  // breakpoint here if the CodeError is caught and rethrown.
  //Breakpoint();
  //throw CodeError(ss.str());
}
