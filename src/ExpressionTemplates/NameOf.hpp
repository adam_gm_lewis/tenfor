//=============================================================================
/// \file
/// Defines NameOf function template and specializations for standard types.
///
/// This function is used to obtain get an unmangled name of a C++-type.
/// The default implementation returns the result of typeid(T).name()
/// 
/// To overload the template for class Bla, add the following to Bla.hpp:
///
/// REGISTER_NAME(Bla);
//=============================================================================
#ifndef NameOf_hpp
#define NameOf_hpp
//=============================================================================
#include <string>
#include <typeinfo>
#include <vector>
//=============================================================================
/// Returns the name of a type when used as a string.
template <typename T> struct NameOf;

/// Automatically registers CLASS using NameOf with the correct string.
#define REGISTER_NAME(CLASS)                            \
  template<>                                            \
  struct NameOf<CLASS> : public std::string {           \
    NameOf() : std::string(#CLASS) { }                  \
  }
/// Automatically register CLASS<T> using NameOf.
#define REGISTER_TEMPLATE_NAME(CLASS)                               \
  template<typename T>                                              \
  struct NameOf<CLASS<T>> : public std::string {                    \
    NameOf() : std::string(#CLASS "<" + NameOf<T>() + ">") { }      \
  }
/// Automatically register CLASS<T,U> using NameOf.
#define REGISTER_TEMPLATE2_NAME(CLASS)                              \
  template<typename T, typename U>                                  \
  struct NameOf<CLASS<T,U>> : public std::string {                  \
    NameOf() : std::string(#CLASS "<" + NameOf<T>() + ","           \
                           + NameOf<U>() + ">") { }                 \
  }
//=============================================================================
template <typename T>
struct NameOf : public std::string {
  NameOf() : std::string(typeid(T).name()) { }
};


template <typename T>
struct NameOf<T*> : public std::string {
  NameOf() : std::string(NameOf<T>() + "*") { }
};
template <typename T>
struct NameOf<T&> : public std::string {
  NameOf() : std::string(NameOf<T>() + "&") { }
};
// Use postfix const so it works for pointers
template <typename T>
struct NameOf<T const> : public std::string {
  NameOf() : std::string(NameOf<T>() + " const") { }
};

//=============================================================================
// register built-in types
REGISTER_NAME(void);
REGISTER_NAME(bool);
REGISTER_NAME(int);
REGISTER_NAME(unsigned int);
REGISTER_NAME(double);
REGISTER_NAME(char);
REGISTER_NAME(long);
REGISTER_NAME(unsigned long);

// register types in namespace std
REGISTER_NAME(std::string);
REGISTER_TEMPLATE_NAME(std::vector);
//=============================================================================

#endif // NameOf_hpp
