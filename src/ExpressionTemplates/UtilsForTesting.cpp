//============================================================================
#include "UtilsForTesting.hpp" // should be first include
//============================================================================
#include <cmath>
//============================================================================

int UtilsForTesting::mNumberOfTests = 0;
int UtilsForTesting::mNumberOfTestsFailed = 0;
double UtilsForTesting::mMaximumError = 0.0;
double UtilsForTesting::mEps = 0.0;

// Query functions:
int UtilsForTesting::NumberOfTests() {return mNumberOfTests;}
int UtilsForTesting::NumberOfTestsPassed() {
  return mNumberOfTests-mNumberOfTestsFailed;}
int UtilsForTesting::NumberOfTestsFailed() { return mNumberOfTestsFailed; }
double UtilsForTesting::MaximumError() {return mMaximumError;}
double UtilsForTesting::Eps() {return mEps;}

// Set functions:
void UtilsForTesting::SetEps(const double NewEps) {
  mEps = NewEps;
}
void UtilsForTesting::ResetMaximumErrorToZero() {mMaximumError = 0.0;}

// Testing routines which actually do the work
double MaximalDifference(const long double lhs, const long double rhs) {
  return static_cast<double>(std::abs(lhs-rhs));
}
