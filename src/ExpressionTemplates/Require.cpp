 /// Defines the function RequireFail, called to handle REQUIRE failure.

#include "Require.hpp"
#include <stdexcept>
//#include "Breakpoint.hpp"
//#include "CodeError.hpp"

/// Handle a REQUIRE failure. Print a useful diagnostic message, print
/// a backtrace, and throw an exception to be caught in the termination handler.
void RequireFail(const char* expression, const char* file, const int line,
                 std::string msg) {
  std::stringstream ss;
  ss << "\n#######################  REQUIRE  FAILED  #######################\n"
     << "#### " << expression << "  violated\n"
     << "#### " << file << " (line " << line << ")\n"
     << msg
     << "\n#################################################################\n";

  throw std::runtime_error(ss.str());
  // Debuggers will usually break when MyAbort() is called when the
  // CodeError is unhandled, but we want to ensure that we get a
  // breakpoint here if the CodeError is caught and rethrown.
  //Breakpoint();
  //throw CodeError(ss.str());
}
