
#include "MeshIndex.hpp"
#include "Mesh.hpp"
#include "Assert.hpp"

//============================================================================

MeshIndex::MeshIndex(const Mesh & rMesh)
  : IPoint(rMesh.Dim(),0), 
    mExtents(rMesh.Extents()),
    mDim(rMesh.Dim()), mOffset(0),
    mValid(reinterpret_cast<void*>(1)) {}

//============================================================================


IPoint GetMeshIndexFromOffset(const int offset,const IPoint MeshExtent)
{
  IPoint mIdx=MeshExtent;
  //IPoint msize=MeshExtent;
  int dim=MeshExtent.size();
  //msize[dim-1]=1;
  //for(int i=dim-2; i>=0;i--)
  //  msize[i]=msize[i+1]*MeshExtent[i+1];

  ASSERT(MeshExtent.size()>0,"Must have nonzero size!");

  int Npt=MeshExtent[0];
  for(int i=1;i<MeshExtent.size();i++)
    Npt*=MeshExtent[i];

  ASSERT(offset>=0,
	 "offset should be positive in GetMeshIndexFromOffset");
  ASSERT(offset<Npt,
	 "offset out of bounds in GetMeshIndexFromOffset");  

  int curoffset=offset;
  for(int i=0;i<dim;i++)
    {
      mIdx[i]   = curoffset % MeshExtent[i];
      curoffset = (curoffset-mIdx[i])/MeshExtent[i];
    }

  return mIdx;
}

