/// Declares class NonCopyable.
#ifndef NonCopyable_hpp
#define NonCopyable_hpp
//============================================================================
/// Deletes default copy constructor and operator= for derived classes.
class NonCopyable {
protected:
  NonCopyable() {}
  ~NonCopyable() {}
public:
  NonCopyable(const NonCopyable&) = delete;
  NonCopyable& operator=(const NonCopyable&) = delete;
};
//============================================================================

#endif // NonCopyable_hpp
