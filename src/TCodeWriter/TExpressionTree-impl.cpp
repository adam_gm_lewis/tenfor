#include "ImplicitTensorLoops.hpp"
#include "TExpressionTree.hpp"

namespace TCodeWriter{
  std::string TExpressionTree::PrintComment() const{
    return mpRootNode -> PrintComment();
  }
  std::string TExpressionTree::PrintExpression() const{
    return mpRootNode -> PrintExpression();
  }

  std::string TExpressionTree::PrintCUDAExpression() const{
    return mpRootNode -> PrintCUDAExpression();
  }
}
