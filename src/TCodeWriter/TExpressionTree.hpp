#ifndef TLeafedTree_hpp
#define TLeafedTree_hpp

#include <vector>

#include "TExpressionNode.hpp"

namespace TCodeWriter {


  class TExpressionTree {
  public:
    typedef std::vector<TExpressionLeafBase*>::iterator iterator;
    typedef std::vector<TExpressionLeafBase*>::const_iterator const_iterator;
    const_iterator begin() const { return mLeafes.begin(); }
    const_iterator end() const { return mLeafes.end(); }

    // construct from a iBinaryOp
    // The implementation of this template is generally not #include'd,
    // to avoid compile-dependencies.
    template<class iBinaryOp_t> 
    TExpressionTree(const iBinaryOp_t& op, const std::string& prefix); 
 
  
    //Print out the expression formatted for the comment
    std::string PrintComment() const;

    //Print out the expression as executable c++ code
    //Meat of this in TExpressionNode.hpp
    std::string PrintExpression() const;  
    
    std::string PrintCUDAExpression() const;
  
  private:
    // root node of the expression.
    std::unique_ptr<TExpressionNode> mpRootNode;
    // vector of all leafes: By storing it as a vector, iteration over
    // all leafes is easy.
    std::vector<TExpressionLeafBase*> mLeafes;

  };
}



#endif
