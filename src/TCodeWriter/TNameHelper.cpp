#include "ImplicitTensorLoops.hpp"
#include "TNameHelper.hpp"

#include <string>
#include <vector>

namespace ImplicitTensorLoops {

  template<int dim, int label>
  std::string TNameHelper<TIndex<dim,label>>::ShortName() {
    return TIndex<dim,label>::VarName(); 
  }
  
  template<int dim, int label>
  std::string TNameHelper<TIndex<dim,label>>::CPlusPlusName() {
    return "TIndex<"+std::to_string(dim)+","+std::to_string(label)+">"; 
  }



  template struct TNameHelper<TIndex<3,0>>;
  template struct TNameHelper<TIndex<3,1>>;
  template struct TNameHelper<TIndex<3,2>>;
  template struct TNameHelper<TIndex<3,3>>;
  template struct TNameHelper<TIndex<3,4>>;
  template struct TNameHelper<TIndex<3,5>>;
  template struct TNameHelper<TIndex<3,6>>;
  template struct TNameHelper<TIndex<3,7>>;

  template struct TNameHelper<TIndex<4,0>>;
  template struct TNameHelper<TIndex<4,1>>;
  template struct TNameHelper<TIndex<4,2>>;
  template struct TNameHelper<TIndex<4,3>>;
  template struct TNameHelper<TIndex<4,4>>;
  template struct TNameHelper<TIndex<4,5>>;
  template struct TNameHelper<TIndex<4,6>>;
  template struct TNameHelper<TIndex<4,7>>;
}
