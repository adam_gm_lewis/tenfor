
#ifdef CODE_WRITER

#include "ImplicitTensorLoops.hpp"
#include "TExpressionTree.hpp"
#include "TExpressionTree-impl.hpp"
#include "TExpressionNode.hpp"
#include "ConcreteTExpressionTreeHouse.hpp"
#include "TCodeWriter-impl.hpp"
#include "Factory.hpp"
#include "FactoryHelperFunctions.hpp"
#include "LHSStrings.hpp"
#include "TabCounter.hpp"
#include "ToString.hpp"
#include <algorithm> //for std::remove
#include <fstream>
#include <list>
namespace TCodeWriter{
  using namespace ImplicitTensorLoops;


  void CreateAndPrepCodeWriterFiles(std::string& fname){
    //the .cpp file
    std::ofstream cppfile;
    cppfile.open(fname+".cpp");
    cppfile << "#ifndef CODE_WRITER\n";
    cppfile << "#include \"Tensor.hpp\"\n";
    cppfile << "#include \"DataMesh.hpp\"\n";
    cppfile << "#include \"UtilsForTesting.hpp\"\n";
    cppfile << "#include \"TLoopApply.hpp\"\n";
    cppfile << "#include \"" + fname + ".hpp\"\n\n";
    cppfile << "namespace ImplicitTensorLoops { \n\n\n\n";
    cppfile.close();
    
    //the .hpp file
    std::ofstream hppfile;
    hppfile.open(fname+".hpp");
    hppfile << "#ifndef TENSOR_EXPRESSION_TEMPLATES_HPP\n";
    hppfile << "#define TENSOR_EXPRESSION_TEMPLATES_HPP\n\n";
    hppfile << "#ifndef CODE_WRITER\n";
    hppfile << "namespace ImplicitTensorLoops { \n\n\n";
    
    //Needs advance definition of 
    //iBinaryOp, NonConstiBinaryOp, SumOp
    hppfile << "template<class LHS, class OP, class RHS>\n";
    hppfile << "struct iBinaryOp;\n\n";
    hppfile << "template<class LHS, class OP, class RHS>\n";
    hppfile << "struct NonConstiBinaryOp;\n\n";
    hppfile << "struct SumOp;\n\n\n";
    hppfile.close();

  }

  void FinalizeCodeWriterFiles(std::string& fname){
    std::ofstream cppfile;
    cppfile.open(fname+".cpp", std::ios::app);
    cppfile << "}\n";
    cppfile << "#endif"; //CODE_WRITER
    cppfile.close();

    std::ofstream hppfile;
    hppfile.open(fname+".hpp", std::ios::app);
    hppfile << "}\n";
    hppfile << "#endif\n";
    hppfile << "#endif"; //CODE_WRITER
    hppfile.close();
  }




  //Call the old TLoopApply code for comparison if ACCEL_DEBUG
  //is defined.
  void WriteAccelDebugCall(TExpressionTreeHouseBase* TreeHouse_ptr,
      LHS_Strings& lhs_strings, std::ofstream& cppfile)
  {
    cppfile<<
      "#ifdef ACCEL_DEBUG \n"<<
      "    " << TreeHouse_ptr->AssignOpCPlusPlusName() << " AssignOp;\n"<<
      "    " << lhs_strings.CPlusPlusName() << " lhs_temp=lhs;\n" <<
      "    " << TreeHouse_ptr->RHSCPlusPlusName() << " rhs_temp=rhs;\n" <<
      "    " << "NonaccelTLoopApply(lhs_temp, AssignOp, rhs_temp);\n" <<
      "#endif\n";
  }
  
  void WriteAccelDebugComparison
    (LHS_Strings& lhs_strings, const int gnumber, std::ofstream& cppfile)
  {
      //LHS_Strings lhs_strings(lhs_strings,"lhs_temp");
      cppfile << 
        "#ifdef ACCEL_DEBUG \n" 
        << lhs_strings.TempNonaccelDeclaration() << " = \n"
        << lhs_strings.TempNonaccelInitialization() << ";\n\n"<<
        "double sum=0;\n" <<
        lhs_strings.ForLoopsOverTensorIndices() <<
        "double this_sum=0;\n" <<
        "\n for(int idx=0; idx<sz; ++idx){ \n" <<
        "\n this_sum += " << lhs_strings.TempNonaccelIndexedExpression() << " - " <<
        lhs_strings.LoopIndexedExpression() << ";\n" <<
        "}\n" <<
        "sum += this_sum;\n" <<
        "IS_ZERO(this_sum, \"EXPRESSION" << std::to_string(gnumber) <<
          "\");\n" <<
        lhs_strings.ForLoopClosingBraces() <<
        "IS_ZERO(sum, \"EXPRESSION" << std::to_string(gnumber) << "\");\n"
        


        
        <<"#endif\n";
  }


  void WriteFunctionWrapper(TExpressionTreeHouseBase* TreeHouse_ptr,
      LHS_Strings& lhs_strings,
      const int& gnumber, std::ofstream& cppfile, std::ofstream& hppfile)
  {
    TabCounter tabs;
    ++tabs; //start with one indent since we're in a namespace
   
    //--------------------------------
    // function name
    //-----------
    std::string FunctionDefinition = "void TLoopApply(" +
       lhs_strings.CPlusPlusName() + "& lhs,"+
       "const " + TreeHouse_ptr-> AssignOpCPlusPlusName() + "&, "+
       "const " + TreeHouse_ptr-> RHSCPlusPlusName() + "& rhs)";
    cppfile << tabs.indent() << FunctionDefinition + "\n { \n";
    hppfile << FunctionDefinition + ";\n\n"; 
    ++tabs;
    
    WriteAccelDebugCall(TreeHouse_ptr, lhs_strings, cppfile);
    cppfile << tabs.indent() << "//--------------------------------\n"
	    << tabs.indent() << "// variable declarations\n"
	    << tabs.indent() << "//--------------------------------\n";

    //prints e.g. double* TDm00[3][3] =
    //{{
    //  lhs.GetComponent({0,0}).Data(),
    //  lhs.GetComponent({1,0}).Data(),
    //  lhs.GetComponent({2,0}).Data(),
    //},
    //{
    //  lhs.GetComponent({0,1}).Data(),...
    //...
    //}};
    cppfile << "\n"
	    << lhs_strings.VarDeclaration() << " =\n"
	    << lhs_strings.VarInitialization() << ";\n\n";

    //prints e.g. 
    //const double* TDm01[3] =
    //{
    //  rhs.lhs.GetComponent({0}).Data(),
    //  rhs.lhs.GetComponent({1}).Data(),
    //  rhs.lhs.GetComponent({2)}.Data(),
    //};
    //
    //const double* TDm02[3] = ...
    for(auto leaf: TreeHouse_ptr->Tree()) {
      cppfile << "\n"
	    << leaf->VarDeclaration() << " =\n"
	    << leaf->VarInitialization() << ";\n\n";
    }
    
    //const int sz=lhs.GetComponent({0,0,0..}).Size();
    cppfile << tabs.indent() << "const int sz=" << lhs_strings.Begin() << ".Size();\n\n"; 
    
    //g_0000(sz, TDm00, TDm01, TDm02...);
    cppfile << tabs.indent() << "g_";
    cppfile << IntToStringPaddedZero(gnumber,4); 
    
    cppfile << "(sz, " << lhs_strings.VarName();
    
    for(auto leaf:TreeHouse_ptr->Tree()) 
        cppfile << ", " << leaf->VarName(); //", TDm1, TDm2, TDm3, ...);"
    cppfile << ");\n\n";
   
    //Write out code calling the old expressions and comparing the results
    //when ACCEL_DEBUG is defined.
    WriteAccelDebugComparison(lhs_strings, gnumber, cppfile);
    
    cppfile << "}\n";
  }
  
  
  //Write out the comment above each expression
  void WriteComment(TExpressionTreeHouseBase* TreeHouse_ptr,
      LHS_Strings& lhs_strings,
      const int& gnumber, std::ofstream& cppfile)
  {
    //************************************************************
    // EXPRESSION xxxx 
    //------------------------------------------------------------
    // DM = DM - DM
    //************************************************************
    std::string stars(60,'*');
    std::string bars(60,'-');
    cppfile << "\n\n";
    cppfile << "//" << stars << "\n";
    cppfile << "//" << " EXPRESSION " << gnumber << ": \n";
    cppfile << "//" << bars << "\n";
    cppfile << "//" << lhs_strings.LoopIndexedComment() <<
        TreeHouse_ptr->AssignOpShortName() <<
        TreeHouse_ptr->Tree().PrintComment() << std::endl;
    cppfile << "//" << stars << "\n";
  }


  void WriteFunction(TExpressionTreeHouseBase* TreeHouse_ptr,
      LHS_Strings& lhs_strings,
      const int& gnumber, std::ofstream& cppfile)
  { 
    TabCounter tabs;
    ++tabs; //start with one indent since we're in a namespace

    WriteComment(TreeHouse_ptr, lhs_strings, gnumber, cppfile);

    //void g_000(const int sz, 
    cppfile << tabs.indent() << "void g_" << 
      IntToStringPaddedZero(gnumber,4) << "(const int sz, "; 
    // double* TDm1[3][3]
    cppfile << lhs_strings.VarDeclaration();
  
    //, const double* TDm2[3], const double* TDm3[3], ...
    for(auto leaf:TreeHouse_ptr->Tree()){
        cppfile << ", " << leaf->VarDeclaration();
    }
    //) {
    cppfile << ") {\n";
    ++tabs;
    //Now the for loops over tensor indices:
    //for(int j=0; j<3; ++j) {
    //  for(int i=0; i<3; ++i) {
    //  ...
    cppfile << lhs_strings.ForLoopsOverTensorIndices();

    //The for loop over the grid:
    //for(int idx=0; idx<sz; ++idx) {
    cppfile << tabs.indent() << "for(int idx=0; idx<sz; ++idx){ \n";
    ++tabs;

    //Print the left hand side
    //First get the LHS operator
    std::string lhs_op = TreeHouse_ptr->AssignOpShortName();
    cppfile << tabs.indent() << lhs_strings.LoopIndexedExpression() << lhs_op;  
    --tabs;

    //Print the RHS
    cppfile << TreeHouse_ptr->Tree().PrintExpression(); 
    cppfile << ";\n";

    //The closing braces
    cppfile << tabs.indent() << "}\n"; //closes the loop over idx
    --tabs;
    cppfile << lhs_strings.ForLoopClosingBraces();
    cppfile << tabs.indent() << "}\n"; //closes the function
  
  }
  void WriteEntry(TExpressionTreeHouseBase* TreeHouse_ptr,
      const int& gnumber, std::string& fname)
  {
    
    std::ofstream cppfile;
    cppfile.open(fname+".cpp", std::ios::app);
   
    std::ofstream hppfile;
    hppfile.open(fname+".hpp", std::ios::app);
  
    LHS_Strings lhs_strings = TreeHouse_ptr->lhs_strings(); 
 
    WriteFunction(TreeHouse_ptr, lhs_strings, gnumber, cppfile);
    WriteFunctionWrapper(TreeHouse_ptr, lhs_strings, gnumber, cppfile, hppfile);

    
    cppfile.close();
    hppfile.close();
  }
  
  
  void LoopOverExpressionsAndWriteFiles(std::string& fname)
  {
    //First we make the files we need - this creates the files and
    //writes their headers.
    CreateAndPrepCodeWriterFiles(fname);
    
    int count=0;
    const std::list<std::string> Expressions
                =Factory::RegisteredClassIDs<TExpressionTreeHouseBase>();
    
    std::cout << "TCodeWriter_impl..." << std::endl;
    
    
    //loop over all the TExpressionTreeHouseBases in the Factory.
    //Each one represents one expression template to be generated.
    //The Factory allows us to create the derived class 
    //"ConcreteTExpressionTreeHouse" from the ExpressionTreeHouseBases,
    //which in turn tell us the equals operator and RHS ExpressionTree
    //we need. This is enough information to generate the expression.
    for(auto ExpTag:Expressions)
    {
      std::cout << "Expression number " << count << std::endl;
      std::cout << ExpTag << std::endl;
      ++count;
      TExpressionTreeHouseBase* TreeHouse_ptr = 
        TExpressionTreeHouseBase::CreateDerivedClass(ExpTag);
      WriteEntry(TreeHouse_ptr, count, fname); 
      delete TreeHouse_ptr; 
    }

    //Write the last parts of each file and close them.
    FinalizeCodeWriterFiles(fname);
  }
}
#endif 
