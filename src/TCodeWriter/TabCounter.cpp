#include "TabCounter.hpp"
#include "Require.hpp"

TabCounter::TabCounter(int SpacesPerTab) //default argument 4 set in .hpp
  : mSpacesPerTab(SpacesPerTab), mNSpaces(0) 
{
  REQUIRE(SpacesPerTab>0, "Must set positive SpacesPerTab.");
}

std::string TabCounter::indent(){
  return "";
  //return std::string(" ", mNSpaces);  
  
}

size_t TabCounter::NTabs(){return (mNSpaces/mSpacesPerTab);}


TabCounter& TabCounter::operator++() {
  mNSpaces += mSpacesPerTab;
  return *this;
}

TabCounter& TabCounter::operator--() {
  REQUIRE(mNTabs>0, 
      "Tried to decrement TabCounter when the count was 0!");
  mNSpaces -= mSpacesPerTab;
  return *this;
}
