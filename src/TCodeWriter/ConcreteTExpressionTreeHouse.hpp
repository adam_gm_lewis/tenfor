#ifndef ConcreteTExpressionTreeHouse_hpp
#define ConcreteTExpressionTreeHouse_hpp

#include "TExpressionTree.hpp"
#include "TExpressionTreeHouseBase.hpp"
#include "TNameHelper.hpp"
#include "CreateTExpressionTree.hpp"
#include "LHSStrings.hpp"

//************************************************************************
//CheckNumberOfArguments
//************************************************************************
//Gives compiler error if i_BINARY_OP_MAX_ARGS is exceeded.
//************************************************************************
/*
//This struct doesn't do anything, but its name is printd in the error
//message so the user has some idea what is going on.
template<int> struct ERROR_Too_Many_Arguments;

//CheckNumberOfArguments_Helper: templated with two versions.
//Create this version only if there are too many arguments. 
//It holds an ERROR_Too_Many_Arguments as member, which is undefined,
//so the compiler will crash with the appropriate error message.
//crash). 
template<bool, int NARGS> struct CheckNumberOfArguments_Helper:
  ERROR_Too_Many_Arguments<NARGS> { };

//Create this version if a safe number of arguments is used. It 
//does nothing.
template<int NARGS> struct CheckNumberOfArguments_Helper<true, NARGS> {}

//This class implements one of the CheckNumberOfArguments_Helpers defined
//above. If NARGS<= i_BINARY_OP_MAX_ARGS, this definition will cause a 
//compiler error.
template<int NARGS> struct CheckNumberOfArguments:
  CheckNumberOfArguments_Helper< (NARGS <= i_BINARY_OP_MAX_ARGS), NARGS > { };
*/
//CheckNumberOfArguments******************************************************
//****************************************************************************

//************************************************************************
//ConcreteTExpressionTreeHouse
//************************************************************************
//Register expression template iBinaryOp RHS and assigment
//operator (=,+=,etc ASSIGNOP) with the Factory so that the
//Factory can iterate over all iBinaryOp's used in SpEC.
//************************************************************************
namespace TCodeWriter{

  template<class LHS, typename ASSIGNOP, class RHS>
  class ConcreteTExpressionTreeHouse:
    public TExpressionTreeHouseBase,
    //Deriving from this class lets the Factory know that a unique
    //ConcreteTExpressionTreeHouse exists for each ASSIGNOP and RHS
    //at compile time
    private Factory::Register<ConcreteTExpressionTreeHouse<LHS,ASSIGNOP,RHS> > 
  {
  
  public:
    ConcreteTExpressionTreeHouse<LHS, ASSIGNOP, RHS>()
    {
      Tree_Static();
    }

    //A unique ClassID() is required by the Factory
    static std::string ClassID()
    {
      return InfoString();
    }

    //Help also required by the Factory
    static std::string Help() {
      return
        "ConcreteTExpressionTreeHouse\n"
        " Represents Tensor expressions as trees of strings, accomplished \n"
        " via templatization by an iBinaryOp and an assignment operator.  \n"
        " This permits automatic generation of expression templates       \n"
        " using the Factory. \n                                             ";
    }

    //Allows a Tree to be created
    static const TExpressionTree& Tree_Static() {
      std::string prefix = "rhs";
      static TExpressionTree theTree
        =CreateTExpressionTree<RHS>::Create(prefix); 
      return theTree;
    }
   
    //Static interface for the Factory to create a unique ClassID
    static const std::string& InfoString() {
      static std::string TheInfoString;
      static ImplicitTensorLoops::TNameHelper<LHS> helpLhs; //for T(a_)=double
      static ImplicitTensorLoops::TNameHelper<ASSIGNOP> helpOp;
      static ImplicitTensorLoops::TNameHelper<RHS> helpRhs;
      TheInfoString = helpLhs.CPlusPlusName() + ", " +  
        helpOp.CPlusPlusName() + ", " + helpRhs.CPlusPlusName();
      return TheInfoString;
    }
  
    static const LHS_Strings& LHS_Strings_Static() {
      LHS* lhs_ptr = 0;
      static LHS_Strings theStrings(*lhs_ptr);
      return theStrings;   
    }
    
    const TExpressionTree& Tree() const {
      return Tree_Static(); 
    } 

    const std::string AssignOpShortName() const {
      ImplicitTensorLoops::TNameHelper<ASSIGNOP> helpOp;
      return helpOp.ShortName();
    }

    const std::string AssignOpCPlusPlusName() const {
      ImplicitTensorLoops::TNameHelper<ASSIGNOP> helpOp;
      return helpOp.CPlusPlusName();
    }

    const LHS_Strings& lhs_strings() const {
      return LHS_Strings_Static();
    }

    const std::string RHSCPlusPlusName() const {
      ImplicitTensorLoops::TNameHelper<RHS> helpRHS;
      return helpRHS.CPlusPlusName();
    } 
  }; 
  //ConcreteTExpressionTreeHouse************************************************
  //****************************************************************************
  
}
#endif
