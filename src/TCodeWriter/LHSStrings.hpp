#ifndef LHS_Strings_hpp
#define LHS_Strings_hpp
#include <map>
#include "TNameHelper.hpp"
#include "TExpressionLeaf.hpp"

//Helper class to generate all the strings we need from the LHS of a Tensor
//expression. This spares us from having to template the CodeWriter functions
//on the LHS.
namespace TCodeWriter {
  
  template<class T> class TExpressionLeaf; //this silences a compiler error
                                          //having
                                          //to do with the 2-phase lookup of
                                          //templated classes
  
  using namespace ImplicitTensorLoops;
  
  class LHS_Strings {
    public:
      template<class LHS>
      LHS_Strings(LHS&) 
      {
        TNameHelper<LHS> helper;
        
        mCPlusPlusName = helper.CPlusPlusName();
        mShortName = helper.ShortName();
        
        //This is just a temporary, so that the class itself
        //need not be templated.
        TExpressionLeaf<LHS> lhs_leaf(0, "lhs");
        
        mVarDeclaration = lhs_leaf.VarDeclaration();
        mCUDAVarDeclaration = lhs_leaf.CUDAVarDeclaration();
        mKernelVarDeclaration = lhs_leaf.KernelVarDeclaration();

        mVarInitialization = lhs_leaf.VarInitialization();
        mCUDAVarInitialization = lhs_leaf.CUDAVarInitialization();
        
        mLoopIndexedComment = lhs_leaf.LoopIndexedComment(); 
        mLoopIndexedExpression = lhs_leaf.LoopIndexedExpression();
        mCUDALoopIndexedExpression = lhs_leaf.CUDALoopIndexedExpression();
        
        mForLoopsOverTensorIndices = lhs_leaf.ForLoopsOverTensorIndices();
        mForLoopClosingBraces = lhs_leaf.ForLoopClosingBraces();
        mCUDAForLoopsOverTensorIndices = 
          lhs_leaf.CUDAForLoopsOverTensorIndices();
        mCUDAParallelIndexDeclaration = lhs_leaf.CUDAParallelIndexDeclaration();
        mCUDABlocksizeNBlocks = lhs_leaf.CUDABlocksizeNBlocks();

        mBegin = lhs_leaf.Begin();
        mVarName = lhs_leaf.VarName();
        mPointersName = lhs_leaf.PointersName();  
        //stuff to output the temporary strings for ACCEL_DEBUG
        mTempNonaccelDeclaration = lhs_leaf.VarDeclaration("nonaccel");
        mTempNonaccelInitialization = lhs_leaf.VarInitialization("lhs_temp");
        mTempNonaccelIndexedExpression = lhs_leaf.LoopIndexedExpression("nonaccel");
      
        mTempAccelDeclaration = lhs_leaf.VarDeclaration("accel");
        mTempAccelIndexedExpression = lhs_leaf.LoopIndexedExpression("accel");
      
      }
      //Access 
      std::string CPlusPlusName() const{return mCPlusPlusName;}
      std::string ShortName() const{return mShortName;}
      std::string VarDeclaration() const{return mVarDeclaration;}
      std::string CUDAVarDeclaration() const{return mCUDAVarDeclaration;}
      std::string KernelVarDeclaration() const{return mKernelVarDeclaration;}
      std::string VarInitialization() const{return mVarInitialization;}
      std::string CUDAVarInitialization() const{return mCUDAVarInitialization;}
      std::string LoopIndexedComment() const{return mLoopIndexedComment;}
      std::string LoopIndexedExpression() const{return mLoopIndexedExpression;}
      std::string CUDALoopIndexedExpression() const
                                            {return mCUDALoopIndexedExpression;}
      
      std::string CUDAForLoopsOverTensorIndices() const
                                        {return mCUDAForLoopsOverTensorIndices;} 
      std::string CUDABlocksizeNBlocks() const
                        {return mCUDABlocksizeNBlocks;}
      std::string CUDAParallelIndexDeclaration() const
                        {return mCUDAParallelIndexDeclaration;}
      std::string ForLoopsOverTensorIndices() const
                                            {return mForLoopsOverTensorIndices;}
      std::string ForLoopClosingBraces() const{return mForLoopClosingBraces;}
      std::string Begin() const{return mBegin;}
      std::string VarName() const{return mVarName;}
      std::string PointersName() const{return mPointersName;} 
      std::string TempNonaccelDeclaration() const{return mTempNonaccelDeclaration;}
      std::string TempNonaccelInitialization() const{return mTempNonaccelInitialization;}
      std::string TempNonaccelIndexedExpression() const{return mTempNonaccelIndexedExpression;}
    
      std::string TempAccelDeclaration() const{return mTempAccelDeclaration;}
      std::string TempAccelIndexedExpression() const{return mTempAccelIndexedExpression;}
    private:
      
      std::string mCPlusPlusName;
      std::string mShortName;
      std::string mVarDeclaration;
      std::string mCUDAVarDeclaration;
      std::string mKernelVarDeclaration;
      std::string mVarInitialization;
      std::string mCUDAVarInitialization;
      std::string mLoopIndexedComment;
      std::string mLoopIndexedExpression;
      std::string mCUDALoopIndexedExpression;
      std::string mForLoopsOverTensorIndices;
      std::string mCUDAForLoopsOverTensorIndices;
      std::string mCUDABlocksizeNBlocks;
      std::string mCUDAParallelIndexDeclaration;
      std::string mForLoopClosingBraces;
      std::string mBegin;
      std::string mVarName;
      std::string mPointersName;
      
      std::string mTempNonaccelDeclaration;
      std::string mTempNonaccelInitialization;
      std::string mTempNonaccelIndexedExpression;

      std::string mTempAccelDeclaration;
      std::string mTempAccelIndexedExpression;
  };
}
#endif
