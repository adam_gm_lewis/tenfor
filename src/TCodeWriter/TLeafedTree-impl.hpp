#include "TLeafedTree.hpp"
#include "Tensor.hpp"

namespace TCodeWriter {
  using namespace ImplictTensorLoops;


  template<class iBinaryOp_t>
  struct ConstructTExpressionNode;
  

  // SPECIALIZATIONS
  
  // Recursion 1: binary operators
  template<class L, class Op, class R>
  struct ConstructTExpressionNode<iBinaryOp<L,Op,R> > {
    
    using CL=ConstructTExpressionNode<L>;
    using CR=ConstructTExpressionNode<R>;
    
    static TExpressionNode construct() {
      return TExpressionNode(
			     CL.construct(),
			     Op::ShortName(),
			     CR.construct()
			     );
    }
  };

  // Recursion 2: unary operators
  template<class Op, class R>
  struct ConstructTExpressionNode<iBinaryOp<EmptyType,Op,R> > {
    
    using CR=ConstructTExpressionNode<R>;
    
    static TExpressionNode 
    construct() {
      return TExpressionNode(
			     Op::ShortName(),
			     CR.construct()
			     );
    }
  };
  
  // Recursion 3: SumOp
  template<class TIndex_t, class R>
  struct ConstruTExpressionNoder<iBinaryOp<TIndex_t, SumOp,R> > {  
    // to be coded 
  };
  
  
  // Leaf 1: double, DataMesh
  template<class T>
  struct ConstructTExpressionNode<T> {
    
    static TExpressionNode 
    construct(const double& /* op */ ) {
      return new TExpressionLeaf<T>();
    }
  };



  // Leaf 2: Tensor, Tensor<Tensor<DataMesh>>
  //
  // For Tensor, L = TIndecStructure, 
  // For T<T > , L = pair<TIndexStructure1, TIndexStructure2>
  template<class Structure>
  struct ConstructTExpressionNode<iBinaryOp<Structure,    
					    EmptyType,
					    DataMesh> 
				  > {
    using T=iBinaryOp<Structure, EmptyType, DataMesh>;
    static TExpressionNode construct() {
      return new TExpressionLeaf<T>();
    }
  };
  

  
  //================================================================
  // TExpressionTree
  //================================================================
  
  template<class iBinaryOp> 
  TExpressionTree::TExpressionTree(const iBinaryOp& op):
    mRootNode{ConstructTExpressionNode<iBinaryOp_t>::construct()},
    mLeafes{}
  {
    // iterate through all nodes, collect the leafes, set calling sequence
    int ctr=0;
    prefix="";
    mRootNode.AssignLocation(ctr, prefix, mLeafes);
    
  }
