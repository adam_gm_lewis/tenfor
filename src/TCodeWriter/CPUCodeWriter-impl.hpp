#ifdef CODE_WRITER
#ifndef CPUCodeWriter_impl_hpp
#define CPUCodeWriter_impl_hpp

#include "ImplicitTensorLoops.hpp"
#include "TExpressionTree.hpp"
#include "TExpressionTree-impl.hpp"
#include "TExpressionNode.hpp"
#include "ConcreteTExpressionTreeHouse.hpp"
#include <string>
namespace TCodeWriter{
  class CPUCodeWriter {
    public:
      void Write(); //this is the only one to be called from outside
    private:
      void Prep();
      void PrepAutoExpressions_cpp();
      void PrepAutoExpressions_hpp();
      void PrepAutoExpressionCPUPointerBundlers_cpp();
      void PrepAutoExpressionCPUPointerBundlers_hpp();
      void PrepAutoExpressionCPUFunctions_cpp();
      void PrepAutoExpressionCPUFunctions_hpp();

      void Finalize();
      void FinalizeAFile(const int number_of_endifs, const std::string& fname,
          bool closing_brace);
      
      std::string Comment(TExpressionTreeHouseBase* TreeHouse_ptr,
          const int& gnumber);
      std::string WriteAccelDebugCall(TExpressionTreeHouseBase* TreeHouse_ptr);
      std::string WriteAccelDebugComparison(
            TExpressionTreeHouseBase* TreeHouse_ptr,
            const int gnumber);
      std::string TLoopApplyString(TExpressionTreeHouseBase* TreeHouse_ptr);
      void AddEntryToAutoExpressions_cpp(TExpressionTreeHouseBase* TreeHouse_ptr, 
            const int gnumber);
      void AddEntryToAutoExpressions_hpp(TExpressionTreeHouseBase* TreeHouse_ptr, 
            const int gnumber);
     
      std::string CPUFunctionArguments
            (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber); 
      std::string PointerBundlerString
          (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber);
      void AddEntryToAutoExpressionCPUPointerBundlers_cpp
        (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber);
      void AddEntryToAutoExpressionCPUPointerBundlers_hpp
        (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber);
      
      std::string CPUFunction
            (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber); 
      std::string CPUWrapper_cu(TExpressionTreeHouseBase* TreeHouse_ptr, 
          const int gnumber); 
      void AddEntryToAutoExpressionCPUFunctions_cpp
        (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber);
      void AddEntryToAutoExpressionCPUFunctions_hpp
        (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber);
      
      void AddEntry(TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber);
  };
}
#endif
#endif
