#ifndef TExpressionLeaf_hpp
#define TExpressionLeaf_hpp

#include "TExpressionLeafBase.hpp"
#include "OutputVector.hpp"
#include "Tensor.hpp"
#include "ForLoopHelper.hpp"
#include "CUDAForLoopHelper.hpp"

namespace TCodeWriter {

  using namespace ImplicitTensorLoops;

  
  /// Abstracts writing of elements of source-code for TCodeWriter.
  /// This class represents a leaf of an expression, i.e. a certain 
  ///// variable used in an expression.
  ///
  /// There will be derived classes for specific types of leafes like
  /// a DataMesh or iBinaryOp<TIndexStructure_t, EmptyType, DataMesh> 

  template<class T>
  class TExpressionLeaf;

  class TensorOutputHelpers{
    public:
      
      //HELPER FUNCTIONS
      //This prints the indices in 'indices' 
      //from indices[left] to indices[right-1]
      std::string ComponentIndices(const std::vector<int>& indices, 
          const int left, const int right) 
      {
        std::ostringstream out;
        out << "{" << indices[left];
        for(int i=left+1; i<right;++i){
          out << "," << indices[i];
        }
        out << "}";
        return out.str();
      }
     
      std::string OutputVarInitGetComponentString(const std::vector<int>& indices, 
        const std::string& prefix, 
        const int Nfree1, const int Nfree2=0) 
      {
        std::ostringstream out;
          const int Nfree=Nfree1+Nfree2;
          REQUIRE(Nfree==indices.size(), "Internal error!");
          // Should work for TDm and TTDm
          // add the present component 
          // "prefix.GetComponent({i,j,k...})"
          out << std::string(2*Nfree,' ')
              << prefix << ".GetComponent(" <<
              ComponentIndices(indices,0,Nfree1);
          if (Nfree2!=0) 
          {
            out << ",";
            out << ComponentIndices(indices,Nfree1,Nfree2);
          }
          out << ")";
          return out.str();
      }

      void IncrementIndices(std::vector<int>& indices, bool& done,
          int& Nincr, const int Nfree, const std::vector<int>& range) {
          // Increment the values of indices[] to represent
          // the next component. Also increment Nincr, which tells
          // us how many curly braces to add later.
          Nincr=0;
          for(int d=(Nfree-1);d>=0;--d) {
            ++indices[d];
            if(indices[d]<range[d]) break;
            //else
              indices[d] = 0;
              ++Nincr;
              if(d==0) // have reset the last index, i.e. done!
              {  
                done=true;
                if(done){}; //Do nothing, to silence compiler warning 
              }

          }
      }
     
      std::string CloseBracesAndCheckIfDone(const int Nincr, const int Nfree,
          const bool done) {
          std::ostringstream out; 
          if(Nincr==0)  
            out << ",\n";
          else { 
            out << "\n" 
                << std::string(2*(Nfree-Nincr),' ') << std::string(Nincr,'}');
            if(!done) out << ",\n"; // add comma only if not done
          }
          return out.str();
      }
    
      std::string GetComponentStringForBegin(const int Nfree) {
          std::ostringstream out;
          out << "{0";
          //rank of the tensor - the number of zeros we must output
          for (int a = 1; a<Nfree; ++a)
              out << ",0";
          out << "}";
          return out.str();
      }
      
      //generate the string assigning variable names to the parallel indices 
      //should use
      //this ordering appears to improve performance
      std::string AddIndVar(std::string ind, int i){
        std::ostringstream strm;
        switch(i){
          case 0:
            strm << "const int " << ind << " = threadIdx.y;\n";
            break;
          case 1:
            strm << "const int " << ind << " = blockIdx.y;\n";
            break;
          case 2:
            strm << "const int " << ind << " = threadIdx.z;\n";
            break;
          case 3:
            strm << "const int " << ind << " = blockIdx.z;\n";
            break;
        }
        return strm.str();
      }
      
      std::string MakeIndVarNames(std::vector<std::string> ParallelInds, 
          int cnt){
        std::ostringstream strm;
        for(int a=0; a<cnt; ++a)
          strm << AddIndVar(ParallelInds[a], a);
        return strm.str();
      }
      //generate the string to declare how many blocks and threads the device
      //should use
      std::string AddBlocksizeString(int dim, int i){
        std::ostringstream strm;
        //change the GenerateBx product if you shuffle the order!
        switch(i){
          case 0:
            strm << "const int blocksize_y = " << dim << ";\n";
            break;
          case 1:
            strm << "const int nblocks_y = " << dim << ";\n";
            break;
          case 2:
            strm << "const int blocksize_z = " << dim << ";\n";
            break;
          case 3:
            strm << "const int nblocks_z = " << dim << ";\n";
            break;
          default:
            REQUIRE(false, "internal error");break;
        }
        return strm.str();
      }
      //how big should the blocks be along the x axis? 
      int GenerateBx(std::vector<int> ParallelDims){
        int TotalNumberOfBlocks_yz=ParallelDims[0]*ParallelDims[2]; //number of non-x blocks
        int bx=0;
        switch(TotalNumberOfBlocks_yz){
          case 1:
            bx=128;break;
          case 3:
            bx=96;break;
          case 4:
            bx=96;break;
          case 9:
            bx=64;break;
          case 12:
            bx=64;break;
          case 16:
            bx=32;break;
          default:
            REQUIRE(false, "internal error");break;
        }
        return bx;
      }

      std::string MakeBlocksizeNBlocks(const std::vector<int>& ParallelDims){
        std::ostringstream strm;
        const int bx = GenerateBx(ParallelDims);
        strm << "const int blocksize_x = " << bx << ";\n";
        strm << "const int nblocks_x = sz/blocksize_x + (sz%blocksize_x"
             << " == 0?0:1);\n";
        for(int a=0; a<4; ++a)
          strm << AddBlocksizeString(ParallelDims[a],a);
        strm << 
          "const dim3 blocksize(blocksize_x, blocksize_y, blocksize_z);\n" <<
          "const dim3 nblocks(nblocks_x, nblocks_y, nblocks_z);\n"; 
        return strm.str();
      }
  };


  //----------------------------------------------------------------
  // double
  //----------------------------------------------------------------
  template<>
  class TExpressionLeaf<double>:  public TExpressionLeafBase   {
    public:
      
      //d00
      std::string VarName() const {
        return "d"+mCtr;
      }
     
      std::string PointersName() const {
        return VarName();
      }

      //const double d00
      std::string VarDeclaration(const std::string& prefix="") const {
        return "const double "+prefix+VarName(); 
      }
      
      std::string CUDAVarDeclaration() const {
        return VarDeclaration();
      }

      std::string KernelVarDeclaration() const {
        return VarDeclaration();
      }
      
      //rhs.lhs.rhs
      std::string VarInitialization(std::string prefix="") const {
        (void)prefix; //silence unused variable warning
        return mPrefix;
      }
      
      std::string CUDAVarInitialization(std::string prefix="") const {
        return VarInitialization(prefix);
      }

      //d
      std::string LoopIndexedComment() const {
        return "d";
      }
      
      //d00
      std::string LoopIndexedExpression
        (const std::string& prefix="",
         const std::map<std::string, int>& FixedIndices
            = std::map<std::string,int>()) const {
        (void)FixedIndices; //silence unused variable warning
        return prefix+VarName();
      }
      
      std::string CUDALoopIndexedExpression(
          const std::map<std::string, int>& FixedIndices
            = std::map<std::string, int>()) const{
          return LoopIndexedExpression("",FixedIndices);
      }

      //
      std::string ForLoopsOverTensorIndices() const {
        return "";
      }
      
      //less for loops for CUDA because we parallelize over indices 
      std::string CUDAForLoopsOverTensorIndices() const {
        return ForLoopsOverTensorIndices();
      }

      std::string CUDAParallelIndexDeclaration() const{
        return "";
      }
      
      std::string CUDABlocksizeNBlocks() const{
        return "";
      }

      //
      std::string ForLoopClosingBraces() const {
        return "";
      }
  
      //
      std::string Begin() const {
        return "";
      }
      
      //temp_d00
      std::string TempDeclaration() const{
        return "temp_"+VarName();
      } 
      //temp_d00
      std::string TempIndexedExpression() const{
        return "temp_"+VarName();
      }
  };

  //----------------------------------------------------------------
  // DataMesh
  //----------------------------------------------------------------

  template<>
  class TExpressionLeaf<DataMesh>:  public TExpressionLeafBase  {
    public:
      TExpressionLeaf(){}
      
      //LHS needs a different constructor 
      TExpressionLeaf(int ctr, const std::string& prefix)
      {
        TExpressionLeafBase::SetLocation(ctr, prefix); 
      }
      
      //Dm00
      std::string VarName() const {
        return "Dm"+mCtr;
      }
      
      std::string PointersName() const {
        return VarName();
      }

      //(const) double* Dm00;
      std::string VarDeclaration(const std::string& prefix="") const {
        //detects whether this is the LHS
        if (mCtr==IntToStringPaddedZero(0,2)){
          return "double* "+prefix+VarName();
        }else{
          return "const double* " +prefix+VarName();
        }
      }
      
      std::string CUDAVarDeclaration() const {
        return VarDeclaration();
      }

      std::string KernelVarDeclaration() const {
        if (mCtr==IntToStringPaddedZero(0,2)){
          return "double* __restrict__ "+VarName();
        }else{
          return "const double* __restrict__ " +VarName();
        }
      }
      
      //rhs.lhs.rhs.Data();
      std::string VarInitialization(std::string prefix="") const {
        (void)prefix;
        return mPrefix+".Data()";    
      }
      
      //lhs.DeviceDataNonConst();
      //or rhs.lhs.rhs.DeviceData();
      std::string CUDAVarInitialization(std::string prefix="") const {
        (void)prefix;
        //detects whether this is the LHS
        if (mCtr==IntToStringPaddedZero(0,2)){
          return mPrefix+".DeviceDataNonConst()";
        }else{
          return mPrefix+".DeviceData()";
        }
      }

      //Dm
      std::string LoopIndexedComment() const {
        return "Dm";
      }
      
      //Dm00[idx]
      std::string LoopIndexedExpression
        (const std::string& prefix="",
         const std::map<std::string,int>& FixedIndices 
                = std::map<std::string,int>()) const {
        (void)FixedIndices; //hush the unsused variable warning
        return prefix+VarName()+"[idx]";
      }
      
      std::string CUDALoopIndexedExpression
        (const std::map<std::string,int>& FixedIndices 
                = std::map<std::string,int>()) const {
        return LoopIndexedExpression("",FixedIndices);  
      }
      
      //
      std::string ForLoopsOverTensorIndices() const {
        return "";
      }
    
      //less for loops for CUDA because we parallelize over indices 
      std::string CUDAForLoopsOverTensorIndices() const {
        return ForLoopsOverTensorIndices();
      }

      std::string CUDAParallelIndexDeclaration() const{
        return "";
      }
      
      std::string CUDABlocksizeNBlocks() const{
        std::ostringstream strm;
        strm << "const int blocksize = 256;\n";
        strm << "const int nblocks = sz/blocksize + (sz%blocksize"
             << " == 0?0:1);\n";
        return strm.str();
      }

      //
      std::string ForLoopClosingBraces() const {
        return "";
      }
      
      //rhs.lhs.rhs
      std::string Begin() const {
        return mPrefix;
      }
    
      //double* temp_Dm00
      std::string TempDeclaration() const{
        return "double* temp_"+VarName();
      }

      //temp_Dm00[idx]
      std::string TempIndexedExpression() const{
        return "temp_"+VarName()+"[idx]";
      }
  
  };
 
  //---------------------------------------------------------------
  // Tensor<Tensor<DataMesh> >
  //---------------------------------------------------------------
  template<class ...Sym1, class ...Ind1, class ...Sym2, class ...Ind2>
  class TExpressionLeaf<iBinaryOp<std::pair<TIndexStructure<TSymmetry<Sym1...>,
                                                            Ind1...>,
                                            TIndexStructure<TSymmetry<Sym2...>,
                                                            Ind2...> 
                                           >,
                                  EmptyType,
                                  DataMesh>
                       >:
    public TExpressionLeafBase {
  public:
    typedef TIndexStructure<TSymmetry<Sym1...>,Ind1...> TIndexStructure1_t;
    typedef iBinaryOp<TIndexStructure1_t,EmptyType,DataMesh> iBinaryOp1_t;
    typedef TExpressionLeaf<iBinaryOp1_t> TExpressionLeaf1_t;
    
    typedef TIndexStructure<TSymmetry<Sym2...>,Ind2...> TIndexStructure2_t;
    typedef iBinaryOp<TIndexStructure1_t,EmptyType,DataMesh> iBinaryOp2_t;
    typedef TExpressionLeaf<iBinaryOp1_t> TExpressionLeaf2_t;

    TExpressionLeaf(){}
    
    //TTDm00
    std::string VarName() const {
      return "TTDm"+mCtr;
    }

    //Goes inside the kernel wrapper
    std::string PointersName() const{
      return VarName()+".GetPointers()";
    }

    //const double* TTDm00[4][4][3]
    std::string VarDeclaration(const std::string& prefix="") const{
      return "const double* "+prefix+VarName()+
        TIndexStructure1_t::VarDeclaration()+
        TIndexStructure2_t::VarDeclaration(); //reversed?
    }

    //Tensor_GPUPointers<Tensor<DataMesh> >& TTDm00
    std::string CUDAVarDeclaration() const{
      return "const double** "+VarName();
    }

    //const double** TTDm00 (for inside the CUDA kernel)
    std::string KernelVarDeclaration() const{
      return "const double* __restrict__ * __restrict__  "+VarName();
    }


    
    //lhs.GetComponent({0,1},{0}).Data(),...
    std::string DoVarInitialization(
        std::string prefix,
        std::string datastring) const {
       
      if (prefix=="") prefix=mPrefix;
      std::ostringstream out;
      
      const int Nfree1=TIndexStructure1_t::NFree;
      const int Nfree2=TIndexStructure2_t::NFree;
      const int Nfree =Nfree1+Nfree2;
      //const std::vector<int> rng1(TIndexStructure1_t::Range());
      //const std::vector<int> rng2(TIndexStructure2_t::Range());
      const std::vector<int> range(ConcatVectors(TIndexStructure1_t::Range(), 
                                                 TIndexStructure2_t::Range()));
      //const std::vector<int> range(MV::concat,
          //TIndexStructure1_t::Range(), TIndexStructure2_t::Range());
      REQUIRE(range.size()==Nfree, "internal error");
      std::vector<int> indices(Nfree,0);
      int Nincr=Nfree;
      bool done=false;

      while(!done) {
        if(Nincr>0)
          out << std::string(2*(Nfree-Nincr),' ')
              << std::string(Nincr, '{') << "\n";
        //doesn't matter which of the two base leaves we use here
        {
          TensorOutputHelpers O; 
          out << O.OutputVarInitGetComponentString(indices,prefix,Nfree1,Nfree2);
          out << datastring;
          O.IncrementIndices(indices, done, Nincr, Nfree, range);
          out << O.CloseBracesAndCheckIfDone(Nincr, Nfree, done);
        }
      }
      return out.str();
    }

    std::string VarInitialization(std::string prefix="") const {
      return DoVarInitialization(prefix,".Data()");
    }
   

    std::string CUDAVarInitialization(std::string prefix="") const {
      (void) prefix;
      return  mPrefix + ".GPUPointers().GetPointers()"; 
    }

    //lhs.GetComponent({0,0},{0}) (for const int sz=)
    std::string Begin() const{
      const int Nfree1=TIndexStructure1_t::NFree;
      const int Nfree2=TIndexStructure2_t::NFree;
      std::ostringstream out;
      out << mPrefix << ".GetComponent(";
      out << TExpressionLeaf1_t::GetComponentStringForBegin(Nfree1);
      out << ",";
      out << TExpressionLeaf2_t::GetComponentStringForBegin(Nfree2);
      out << ")";
      return out.str();
    }
  
    //Sum{i}(TTDm[a][b][0])
    std::string LoopIndexedComment() const {
      return "TTDm" 
        +TIndexStructure1_t::LoopIndexedExpression()
        +TIndexStructure2_t::LoopIndexedExpression();
    }
   
    //TTDm00[a][b][i][idx]
    std::string LoopIndexedExpression(
        const std::string& prefix="",
        const std::map<std::string,int>& FixedIndices=std::map<std::string,int>()) 
      const {
      return prefix+VarName()
        +TIndexStructure1_t::LoopIndexedExpression(FixedIndices)
        +TIndexStructure2_t::LoopIndexedExpression(FixedIndices)+"[idx]";
    }

    //TTDm[TInd(a,b,i)][idx]
    std::string CUDALoopIndexedExpression
      (const std::map<std::string,int>& FixedIndices=std::map<std::string,int>()) 
      const {
        const int NFree1=TIndexStructure1_t::NFree;
        return VarName() + "[" + 
          TIndexStructure1_t::CUDALoopIndexedExpression(0,FixedIndices,true)
          +TIndexStructure2_t::CUDALoopIndexedExpression(NFree1,
              FixedIndices,false)+"][idx]";
    }
    
    //for(int b=0; b<4; ++b) 
    std::string ForLoopsOverTensorIndices() const {
      ForLoopHelper<TIndexStructure1_t> Helper1;
      ForLoopHelper<TIndexStructure2_t> Helper2;
      return Helper1.ForLoops() + Helper2.ForLoops();
    }
    
    //Creates for loops, but the first 4 asymmetrical for loops are replaced
    //with if statements (we parallelize over them instead of looping). 
    std::string CUDAForLoopsOverTensorIndices() const {
      CUDAForLoopHelper<TIndexStructure1_t> Helper1;
      CUDAForLoopHelper<TIndexStructure2_t> Helper2;
     
      mParallelInds = std::vector<std::string>(4,"");
      mParallelDims = std::vector<int>(4,1);
      mCnt=0;
      mCUDAForLoopsWasCalled = true;
      return Helper1.ForLoops(mParallelInds, mParallelDims, mCnt) + 
        Helper2.ForLoops(mParallelInds, mParallelDims,mCnt);
    }

    std::string CUDAParallelIndexDeclaration() const{
      REQUIRE(mCUDAForLoopsWasCalled, "Need to initialize for loops before"
          << " calling parallel indices.");
      TensorOutputHelpers O;
      return O.MakeIndVarNames(mParallelInds,mCnt);
    }
    
    std::string CUDABlocksizeNBlocks() const{
      REQUIRE(mCUDAForLoopsWasCalled, "Need to initialize for loops before"
          << " calling blocksize.");
      TensorOutputHelpers O;
      return O.MakeBlocksizeNBlocks(mParallelDims);
    }
    
    std::string ForLoopClosingBraces() const{
      return TExpressionLeaf1_t::ForLoopClosingBraces() 
            +TExpressionLeaf2_t::ForLoopClosingBraces();
    }
    //less for loops for CUDA because we parallelize over indices 
    std::string CUDAForLoopsOverTensorIndices(
        std::vector<std::string>& ParallelInds, 
        std::vector<int>& ParallelDims,
        int& cnt
        ) const {
      CUDAForLoopHelper<TIndexStructure1_t> Helper1;
      CUDAForLoopHelper<TIndexStructure2_t> Helper2;
      return Helper1.ForLoops(ParallelInds, ParallelDims, cnt) + 
        Helper2.ForLoops(ParallelInds, ParallelDims,cnt);
    }
    
  
  };


  //----------------------------------------------------------------
  // Tensor
  //----------------------------------------------------------------

  template<class ...Symm, class ...Indices>
  class TExpressionLeaf<iBinaryOp<TIndexStructure<TSymmetry<Symm...>,
						  Indices...>,
				    EmptyType,
				    DataMesh> 
			>:
    public TExpressionLeafBase {
  public:
    typedef TIndexStructure<TSymmetry<Symm...>,Indices...> TIndexStructure_t;
   
    //TDm00
    std::string VarName() const {
      return +"TDm"+mCtr;
    }

    std::string PointersName() const{
      return VarName()+".GetPointers()";
    }
    
    //const double* TDm00[4][3]
    std::string VarDeclaration(const std::string& prefix="") const {
      return "const double* "+prefix+VarName()+
	TIndexStructure_t::VarDeclaration();
    }

    //Tensor_GPUPointers<Tensor<DataMesh> >& TTDm00
    std::string CUDAVarDeclaration() const{
      return "const double** "+VarName();
    }
      
    //const double** TDm00 (for inside the CUDA kernel)
    std::string KernelVarDeclaration() const{
      return "const double* __restrict__ * __restrict__ "+VarName();
    }

    //lhs.rhs.GetComponent({1,2}).Data(),...
    std::string DoVarInitialization(std::string prefix,
              std::string datastring) const {
      if (prefix=="") prefix=mPrefix;
      std::ostringstream out; 
      const int Nfree=TIndexStructure_t::NFree;
      const std::vector<int> range=TIndexStructure_t::Range();
      REQUIRE(range.size()==Nfree, "internal error");
      std::vector<int> indices(Nfree,0);
      int Nincr=Nfree;
      bool done=false;

      while(!done) {
	// pre-tensor indexing: Open parentheses that were closed in
	// previous iteration (i.e. if Nincr!=0
	if(Nincr>0) 
          //"{" only if Nincr>0
          out << std::string(2*(Nfree-Nincr),' ')
	      << std::string(Nincr,'{') << "\n";
        {
          TensorOutputHelpers O;
          out << O.OutputVarInitGetComponentString(indices,prefix,Nfree);
          out << datastring;
          O.IncrementIndices(indices, done, Nincr, Nfree, range);
          out << O.CloseBracesAndCheckIfDone(Nincr, Nfree, done);
        }
      }
      return out.str(); 
    }

    //lhs.rhs.GetComponent({1,2}).Data(),...
    std::string VarInitialization(std::string prefix="") const {
      return DoVarInitialization(prefix,".Data()");
    }

    std::string CUDAVarInitialization(std::string prefix="") const {
      (void) prefix;
      return  mPrefix + ".GPUPointers().GetPointers()"; 
    }
    
    //lhs.GetComponent({0,0})
    std::string Begin() const {
        std::ostringstream out;
        const int Nfree=TIndexStructure_t::NFree;
        TensorOutputHelpers O;
        out << mPrefix << ".GetComponent(";
        out << O.GetComponentStringForBegin(Nfree);
        out << ")";
        return out.str();
    }

    //TDm[a][i]
    std::string LoopIndexedComment() const {
      return "TDm" +
        TIndexStructure_t::LoopIndexedExpression();
    }
    
    //TDm[a][i][idx]
    std::string LoopIndexedExpression
      (const std::string& prefix="",
       const std::map<std::string,int>& FixedIndices=std::map<std::string,int>()) 
      const {
      return prefix+VarName()+
        TIndexStructure_t::LoopIndexedExpression(FixedIndices)+"[idx]";
    }

    //TDm[TInd(a,i)][idx]
    std::string CUDALoopIndexedExpression
      (const std::map<std::string,int>& FixedIndices=std::map<std::string,int>()) 
      const {
        std::string laststring = 
          TIndexStructure_t::CUDALoopIndexedExpression(0,FixedIndices,false);
        return VarName()+"["+laststring+"][idx]";
    }
    
    //for (int a=0; a<4; ++a)...
    std::string ForLoopsOverTensorIndices() const {
      ForLoopHelper<TIndexStructure_t> Helper;
      return Helper.ForLoops();
    }
    
    //
    std::string ForLoopClosingBraces() const{
      const int Nfree = TIndexStructure_t::NFree;
      std::string braces = "";
      for(int i=0; i<Nfree; ++i) braces+="}\n";
      return braces;
    }

    //Creates for loops, but the first 4 asymmetrical for loops are replaced
    //with if statements (we parallelize over them instead of looping). 
    std::string CUDAForLoopsOverTensorIndices() const {
      CUDAForLoopHelper<TIndexStructure_t> Helper;
     
      mParallelInds = std::vector<std::string>(4,"");
      mParallelDims = std::vector<int>(4,1);
      mCnt=0;
      mCUDAForLoopsWasCalled = true;
      return Helper.ForLoops(mParallelInds, mParallelDims, mCnt); 
    }

    std::string CUDAParallelIndexDeclaration() const{
      REQUIRE(mCUDAForLoopsWasCalled, "Need to initialize for loops before"
          << " calling parallel indices.");
      TensorOutputHelpers O;
      return O.MakeIndVarNames(mParallelInds, mCnt);
    }
    
    std::string CUDABlocksizeNBlocks() const{
      REQUIRE(mCUDAForLoopsWasCalled, "Need to initialize for loops before"
          << " calling blocksize.");
      TensorOutputHelpers O;
      return O.MakeBlocksizeNBlocks(mParallelDims);
    }
    
  };


  //Non const access to TDm and TTDm
  template<class ...Symm, class ...Indices>
  class TExpressionLeaf<NonConstiBinaryOp<TIndexStructure<TSymmetry<Symm...>,
							  Indices...>,
					  EmptyType,
					  DataMesh> 
			>:
    public TExpressionLeaf<iBinaryOp<TIndexStructure<TSymmetry<Symm...>,
						     Indices...>,
				     EmptyType,
				     DataMesh> 
			   > {
  public:
    typedef TExpressionLeaf<iBinaryOp<TIndexStructure<TSymmetry<Symm...>,
						      Indices...>,
				      EmptyType,
				      DataMesh> > BASE;

    TExpressionLeaf(int ctr, const std::string& prefix)
       :BASE() 
    {
      TExpressionLeafBase::SetLocation(ctr, prefix); 
    }
    
   typedef typename BASE::TIndexStructure_t TIndexStructure_t;
    
    std::string PointersName() const{
      return BASE::VarName()+".GetPointersNonConst()";
    }
   
    std::string CUDAVarInitialization(std::string prefix="") const {
      (void)prefix;
      return BASE::mPrefix + ".GPUPointers().GetPointersNonConst()"; 
    }
   
   std::string VarDeclaration(const std::string& prefix="") const {
      return "double* "+prefix+BASE::VarName()+
        TIndexStructure_t::VarDeclaration();
    }
   
    std::string CUDAVarDeclaration() const{
      return "double** "+BASE::VarName();
    }
    //const double** TDm00 (for inside the CUDA kernel)
    std::string KernelVarDeclaration() const{
      return "double* __restrict__ * __restrict__ "+BASE::VarName();
    }
    
    /* 
    std::string CUDAVarDeclaration() const {
      std::string rankstr = IntToString(TIndexStructure_t::NFree);
      
      return "CUDA_ContainerRank"+rankstr+"<"
        +TIndexStructure_t::CUDAVarDeclaration()
        +"> " + BASE::VarName(); 
    }
  
    //lhs.rhs.GetComponent({1,2}).DeviceData(),...
    std::string CUDAVarInitialization(std::string prefix="") const {
      std::ostringstream out;
      out << "{";
      out << BASE::DoVarInitialization(prefix,".DeviceDataNonConst()");
      out << "}";
      return out.str();
    }      
    */
  };
}

#endif
    
