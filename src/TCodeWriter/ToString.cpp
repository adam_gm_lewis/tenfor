#include "ToString.hpp"
#include "Require.hpp"
#include "Assert.hpp"
#include <string>
#include <iomanip>
#include <sstream>


bool GetNextChar(std::string& rString, char& rChar)
{
  if(0==rString.length()) return false;
  rChar = '\n';
  while(rString.length()>0 && '\n'==rChar) {
    rChar = rString[0];
    rString.erase(0,1);
    if('#'==rChar) { // then we are parsing a comment line so ignore it
      size_t i = rString.find("\n");
      rString.erase(0,i);
      if(0==rString.length()) return false;
      rChar = '\n';
    }
  }
  if('\n'!=rChar) return true;
  ASSERT(0==rString.length(),"String = " << rString);
  return false;
}

std::string RemoveLeadingAndTrailingWhitespace(const std::string& rString)
{
  const std::string::size_type len = rString.size();
  std::string::size_type apos      = 0;
  while(apos<len   && isspace(rString[apos])) ++apos;

  if(apos==len) {
    return std::string();
  }
  std::string::size_type bpos      = len-1;
  while(bpos>apos  && isspace(rString[bpos])) --bpos;

  return rString.substr(apos,bpos-apos+1);
}

void SplitParentheses(const std::string& rString,
		      std::string& Head,
		      std::string& Inside,
		      const std::string Left,
		      const std::string Right)
{
  const std::string::size_type pLeft = rString.find(Left);
  const std::string::size_type pRight = rString.rfind(Right);
  if(std::string::npos!=pLeft) {
    REQUIRE(std::string::npos!=pRight,"SplitParentheses found Left = " <<
	    Left << " but not Right = " << Right << " in the std::string:\n" <<
	    rString);
    REQUIRE(pLeft<pRight,"SplitParentheses found Right = " << Right <<
	    " before Left = " << Left << " in the std::string:\n" << rString);
    Head = RemoveLeadingAndTrailingWhitespace(std::string(rString,0,pLeft));
    Inside = RemoveLeadingAndTrailingWhitespace(std::string(rString,pLeft+1,
							    pRight-pLeft-1));
    std::string Tail(rString,pRight+1);
    char ShouldBeWhitespace;
    while(GetNextChar(Tail,ShouldBeWhitespace)) {
      REQUIRE(isspace(ShouldBeWhitespace),"Tail = " <<
	      ShouldBeWhitespace + Tail << "\nFound in std::string:\n" << rString
	      << "\nHead = " << Head << "\nInside = " << Inside);
    }
  } else {
    REQUIRE(std::string::npos==pRight,"SplitParentheses found Right = " <<
	    Right << " but not Left = " << Left << " in the std::string:\n" <<
	    rString);
    Head = RemoveLeadingAndTrailingWhitespace(rString);
    Inside = "";
  }
}

std::string IntToStringPaddedZero(const long int val, const int p) {
  std::ostringstream s;
  s.fill('0');
  s.width(p);
  s.setf(std::ios_base::internal,std::ios_base::adjustfield);
  s << val;
  return s.str();
}
