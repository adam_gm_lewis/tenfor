#ifndef TExpressionOperator_hpp
#define TExpressionOperator_hpp

#include "TExpressionOperatorBase.hpp"
#include "Tensor.hpp"

namespace TCodeWriter {

  using namespace ImplicitTensorLoops;

  
  /// This class represents an operator within an expression, i.e. a
  /// certain variable used in an expression.

  // Generic template:  Handles **unary** operators (because there are so many of these)
  template<class Op>
  class TExpressionOperator: public TExpressionOperatorBase {
  public:
    std::string GenerateExpressionString(
          std::unique_ptr<TExpressionNode> const & /* Lnode */,
	  std::unique_ptr<TExpressionNode> const & Rnode,
          const std::map<std::string, int>& FixedIndices=std::map<std::string,int>()
          ) const {

      
      return TNameHelper<Op>::ShortName()+"("+
        Rnode->PrintExpression(FixedIndices) + ")"; 
    }

    std::string GenerateCUDAExpressionString(
          std::unique_ptr<TExpressionNode> const & /* Lnode */,
	  std::unique_ptr<TExpressionNode> const & Rnode,
          const std::map<std::string, int>& FixedIndices=std::map<std::string,int>()
          ) const {

      
      return TNameHelper<Op>::ShortName()+"("+
        Rnode->PrintCUDAExpression(FixedIndices) + ")"; 
    }
    
    std::string GenerateCommentString(
            std::unique_ptr<TExpressionNode> const & /* Lnode */,
	    std::unique_ptr<TExpressionNode> const & Rnode
            ) const {
      
      return TNameHelper<Op>::ShortName()+"(" + Rnode->PrintComment() + ")"; 
    }
  };

  
  //Specialization (0):SumOp
  //This is where FixedIndex gets added to.
  //First template argument is a TIndex, *NOT* TIndexSlot, because the summed
  //index will never have an offset.
  template<int dim, int label, class iBinaryOp_t>
  class TExpressionOperator<iBinaryOp<TIndex<dim,label>, SumOp, iBinaryOp_t> >: 
    public TExpressionOperatorBase {
  public:
    std::string GenerateExpressionString(
          std::unique_ptr<TExpressionNode> const & /* Lnode */,
	  std::unique_ptr<TExpressionNode> const & Rnode,
          const std::map<std::string, int>& FixedIndices=std::map<std::string,int>()
          ) const {
      //FixedIndices is a map of index labels to integers - i.e. "i" to whatever
      //fixed value (1,2...) it should be replaced with. This allows us to 
      //generate sums correctly without an extra for loop (CUDA tends not to
      //like for loops).

    
      std::string out;
      //Strip the const by just copying the map - efficiency isn't a big deal 
      //here
      std::map<std::string,int> NonConstFixedInd(FixedIndices); 
      
      //Loop over dim - the dimension of the index being summed over. Thus
      //we generate one term for each value of the index.
      for(int this_idx=0; this_idx<dim; ++this_idx) {
        
        //Get the name of the index. Note it's a TIndex, not a TIndexSlot -
        //we don't want any +1's here.
        std::string ind = TNameHelper<TIndex<dim,label>>::ShortName();
       
        //If that index already appears in the map (which would occur if we 
        //are inside a recursive call, so that a key mapping the index to 
        //this_ind-1 exists already), delete it. 
        //When std::map.erase fails (because the key wasn't found)
        //it throws an exception. The try-catch
        //prevents this from crashing the code.
        try{
          NonConstFixedInd.erase(ind);
        }catch(const std::out_of_range& oor) 
        {/* Do Nothing*/ }

        //Now add a key mapping ind to this_idx.
        NonConstFixedInd.insert(std::pair<std::string,int>(ind,this_idx) );
        //The key now gets passed to TExpressionNode.hpp, then back to 
        //TExpressionOp, then back here until we hit a Leaf. Then it is
        //passed to Leaf::LoopIndexedExpression, and finally to 
        //TIndexStructure::LoopIndexedExpression, which actually performs
        //the substitution.
        out+=Rnode->PrintExpression(NonConstFixedInd);
        //Put a "+" sign between terms. If this_idx != dim-1, we are at the 
        //end of the sum, so no "+" sign.
	if(this_idx != dim-1) out += " + "; // +
      }
      
      return out; 
    }
    
    std::string GenerateCUDAExpressionString(
          std::unique_ptr<TExpressionNode> const & /* Lnode */,
	  std::unique_ptr<TExpressionNode> const & Rnode,
          const std::map<std::string, int>& FixedIndices=std::map<std::string,int>()
          ) const {


      std::string out;
      //this is the only function that should modify the map
      //achieve this by just copying it - efficiency isn't a big deal 
      //here
      std::map<std::string,int> NonConstFixedInd(FixedIndices); 
      for(int this_idx=0; this_idx<dim; ++this_idx) {
        //get the index
        std::string ind = TNameHelper<TIndex<dim,label>>::ShortName();
        
        try{
          NonConstFixedInd.erase(ind);
        }catch(const std::out_of_range& oor) 
        {/* Do Nothing*/ }

        NonConstFixedInd.insert(std::pair<std::string,int>(ind,this_idx) );
        out+=Rnode->PrintCUDAExpression(NonConstFixedInd);
	if(this_idx != dim-1) out += " + "; // +
      }
      
      return out; 
    }
    //Sum{i}(...)
    std::string GenerateCommentString(
            std::unique_ptr<TExpressionNode> const & /* Lnode */,
	    std::unique_ptr<TExpressionNode> const & Rnode
            ) const {
      //No need to recurse here

      std::string ind = TNameHelper<TIndex<dim,label>>::ShortName();
      return TNameHelper<SumOp>::ShortName()+"{"+ind+"}"
              +Rnode->PrintComment(); 
    }
  };
  
  // Specialization (1) AddOp
  template<>
  class TExpressionOperator<AddOp>: public TExpressionOperatorBase {
  public:
    typedef TNameHelper<AddOp> me_t; 
    std::string GenerateExpressionString(
            std::unique_ptr<TExpressionNode> const & Lnode,
	    std::unique_ptr<TExpressionNode> const & Rnode,
            const std::map<std::string, int>& FixedIndices=std::map<std::string,int>()
            ) const {
      
      return Lnode->PrintExpression(FixedIndices)+
                me_t::ShortName()+
                Rnode->PrintExpression(FixedIndices); 
    }
  
    std::string GenerateCUDAExpressionString(
            std::unique_ptr<TExpressionNode> const & Lnode,
	    std::unique_ptr<TExpressionNode> const & Rnode,
            const std::map<std::string, int>& FixedIndices=std::map<std::string,int>()
            ) const {
      
      return Lnode->PrintCUDAExpression(FixedIndices)+
                me_t::ShortName()+
                Rnode->PrintCUDAExpression(FixedIndices); 
    }
    
    std::string GenerateCommentString(
            std::unique_ptr<TExpressionNode> const & Lnode,
	    std::unique_ptr<TExpressionNode> const & Rnode
            ) const {
      
      return Lnode->PrintComment()+
                me_t::ShortName()+
                Rnode->PrintComment(); 
    }
  
  
  };




  // Specialization (2) SubOp
  template<>
  class TExpressionOperator<SubOp>: public TExpressionOperatorBase {
  public:
    typedef TNameHelper<SubOp> me_t; 
    std::string GenerateExpressionString(
          std::unique_ptr<TExpressionNode> const & Lnode,
	   std::unique_ptr<TExpressionNode> const & Rnode,
           const std::map<std::string, int>& FixedIndices=std::map<std::string,int>()
           ) const {
      
      return Lnode->PrintExpression(FixedIndices)+me_t::ShortName()+
            Rnode->PrintExpression(FixedIndices); 
    }
  
    std::string GenerateCUDAExpressionString(
          std::unique_ptr<TExpressionNode> const & Lnode,
	   std::unique_ptr<TExpressionNode> const & Rnode,
           const std::map<std::string, int>& FixedIndices=std::map<std::string,int>()
           ) const {
      
      return Lnode->PrintCUDAExpression(FixedIndices)+me_t::ShortName()+
            Rnode->PrintCUDAExpression(FixedIndices); 
    }
    std::string GenerateCommentString(
            std::unique_ptr<TExpressionNode> const &Lnode,
	    std::unique_ptr<TExpressionNode> const &Rnode
            ) const {
      
      return Lnode->PrintComment()+
                me_t::ShortName()+
                Rnode->PrintComment(); 
    }
  
  
  };
  // Specialization (3) MultOp
  template<>
  class TExpressionOperator<MultOp>: public TExpressionOperatorBase {
  public:
    typedef TNameHelper<MultOp> me_t; 
    std::string GenerateExpressionString(
          std::unique_ptr<TExpressionNode> const & Lnode,
	  std::unique_ptr<TExpressionNode> const & Rnode,
          const std::map<std::string, int>& FixedIndices=std::map<std::string,int>()
          ) const {
      
      return Lnode->PrintExpression(FixedIndices)+me_t::ShortName()
            +Rnode->PrintExpression(FixedIndices); 
    }

    std::string GenerateCUDAExpressionString(
          std::unique_ptr<TExpressionNode> const & Lnode,
	  std::unique_ptr<TExpressionNode> const & Rnode,
          const std::map<std::string, int>& FixedIndices=std::map<std::string,int>()
          ) const {
      
      return Lnode->PrintCUDAExpression(FixedIndices)+me_t::ShortName()
            +Rnode->PrintCUDAExpression(FixedIndices); 
    }
    
    std::string GenerateCommentString(
            std::unique_ptr<TExpressionNode> const &Lnode,
	    std::unique_ptr<TExpressionNode> const &Rnode
            ) const {
      
      return Lnode->PrintComment()+
                me_t::ShortName()+
                Rnode->PrintComment(); 
    }
  };
  // Specialization (4) DivOp
  template<>
  class TExpressionOperator<DivOp>: public TExpressionOperatorBase {
  public:
    typedef TNameHelper<DivOp> me_t; 
    std::string GenerateExpressionString(
          std::unique_ptr<TExpressionNode> const & Lnode,
	  std::unique_ptr<TExpressionNode> const & Rnode,
          const std::map<std::string, int>& FixedIndices=std::map<std::string,int>()
          ) const {
      
      return Lnode->PrintExpression(FixedIndices)+me_t::ShortName()+
              Rnode->PrintExpression(FixedIndices); 
    }

    std::string GenerateCUDAExpressionString(
          std::unique_ptr<TExpressionNode> const & Lnode,
	  std::unique_ptr<TExpressionNode> const & Rnode,
          const std::map<std::string, int>& FixedIndices=std::map<std::string,int>()
          ) const {
      
      return Lnode->PrintCUDAExpression(FixedIndices)+me_t::ShortName()+
              Rnode->PrintCUDAExpression(FixedIndices); 
    }
    
    std::string GenerateCommentString(
            std::unique_ptr<TExpressionNode> const & Lnode,
	    std::unique_ptr<TExpressionNode> const & Rnode
            ) const {
      
      return Lnode->PrintComment()+
                me_t::ShortName()+
                Rnode->PrintComment(); 
    }
  };

  // Specialization (5) Atan2Op
  template<>
  class TExpressionOperator<Atan2Op>: public TExpressionOperatorBase {
  public:
    typedef TNameHelper<Atan2Op> me_t; 
    std::string GenerateExpressionString(
          std::unique_ptr<TExpressionNode> const & Lnode,
	  std::unique_ptr<TExpressionNode> const & Rnode,
          const std::map<std::string, int>& FixedIndices=std::map<std::string,int>()
          ) const {
      
      return me_t::ShortName()+"("+
              Lnode->PrintExpression(FixedIndices)+","+
                  Rnode->PrintExpression(FixedIndices)+")"; 
    }

    std::string GenerateCUDAExpressionString(
          std::unique_ptr<TExpressionNode> const & Lnode,
	  std::unique_ptr<TExpressionNode> const & Rnode,
          const std::map<std::string, int>& FixedIndices=std::map<std::string,int>()
          ) const {
      
      return me_t::ShortName()+"("+
              Lnode->PrintCUDAExpression(FixedIndices)+","+
                  Rnode->PrintCUDAExpression(FixedIndices)+")"; 
    }
    
    std::string GenerateCommentString(
            std::unique_ptr<TExpressionNode> const & Lnode,
	    std::unique_ptr<TExpressionNode> const & Rnode
            ) const {
      
      return me_t::ShortName()+"("+Lnode->PrintComment()+","
                +Rnode->PrintComment()+")";
    }
  };

  // Specialization (6) PowOp
  template<>
  class TExpressionOperator<PowOp>: public TExpressionOperatorBase {
  public:
    typedef TNameHelper<PowOp> me_t; 
    std::string GenerateExpressionString(
          std::unique_ptr<TExpressionNode> const & Lnode,
	  std::unique_ptr<TExpressionNode> const & Rnode,
          const std::map<std::string, int>& FixedIndices=std::map<std::string,int>()
          ) const {
      
      return me_t::ShortName()+"("+
              Lnode->PrintExpression(FixedIndices)+","+
                  Rnode->PrintExpression(FixedIndices)+")"; 
    }

    std::string GenerateCUDAExpressionString(
          std::unique_ptr<TExpressionNode> const & Lnode,
	  std::unique_ptr<TExpressionNode> const & Rnode,
          const std::map<std::string, int>& FixedIndices=std::map<std::string,int>()
          ) const {
      
      return me_t::ShortName()+"("+
              Lnode->PrintCUDAExpression(FixedIndices)+","+
                  Rnode->PrintCUDAExpression(FixedIndices)+")"; 
    }
    
    std::string GenerateCommentString(
            std::unique_ptr<TExpressionNode> const & Lnode,
	    std::unique_ptr<TExpressionNode> const & Rnode
            ) const {
      
      return me_t::ShortName()+"("+Lnode->PrintComment()+","
                +Rnode->PrintComment()+")";
    }
  };







}

#endif
    
