#ifndef TCodeWriterNode_hpp
#define TCodeWriterNode_hpp

#include "TExpressionLeafBase.hpp"

namespace TCodeWriter {
  class TCodeWriterNode {

    TCodeWriterNode(const std::string op,
		    const std::vector<unique_ptr<TCodeWriterNode>> nodes,
		    const std::vector<unique_ptr<TExpressionLeafBase>> leafes):
      mOp{op}, mNodes{nodes}, mLeafes{leafes} {
	// check invariant: 
	REQUIRE( (mNodes.empty() && mOp.Empty()) || mLeafes.empty(),
		 "TCodeWriterNode: 
 }

  private:
    std::string mOp; 
    //    unique_ptr<TCodeWriterOperatorBase> mOp; 
    std::vector<unique_ptr<TCodeWriterNode>> mNodes; 
    std::vector<std::unique_ptr<TExpressionLeafBase>> mLeafes;
  }


#endif
