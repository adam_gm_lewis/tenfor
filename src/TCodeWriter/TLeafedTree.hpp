#ifndef TLeafedTree_hpp
#define TLeafedTree_hpp


namespace TCodeWriter {

  /// A C++ class **instance** represenentation of a 
  /// iBinaryOp template.  
  ///
  /// This representation is **independent** of the precise
  /// ImplicitTensorLoop-templates, so if the code here changes, only
  /// modest recompilations within SpEC are needed.
  
  class TAbstractExpressionNode {
  public:
    //--------------------------------
    // CONSTRUCTORS
    //--------------------------------

    // Constructor 1:  for binary operator
    TAbstractExpressionNode(TAbstractExpressionNode& lhs,
			    const std::string op,
			    TAbstractExpressionNode& rhs):
      mOp(op),
      mSubNodes{lhs, rhs},
      mLeaf{}
    { }
    // Constructor 2:  for unary operator
    TAbstractExpressionNode(const std::string op,
			    TAbstractExpressionNode& rhs):
      mOp(op),
      mSubNodes{rhs},
      mLeaf{}
    { }
    // Constructor 3:  for leaf
    TAbstractExpressionNode(std::unique_ptr<TExpressionLeafBase>& leaf):
      mOp{},
      mSubNodes{},
      mLeaf{leaf}
    { }

    //--------------------------------
    // SETUP
    //--------------------------------

    // iterate through all sub-nodes, keeping track of the location in
    // the hierachty (i.e. a sequence like "lhs.rhs.lhs.lhs"). For each
    // leaf: call leaf.SetLocation(), increment ctr, and push_back the leaf
    // into leafes.
    void AssignLocation(int& ctr, const std::string& prefix, 
			std::vector<TExpressionLeafBase*>& leafes) {
      switch mSubNodes.size():
    case 0:
      mLeaf->SetLocation(ctr, prefix);
      ++ctr; 
      break; 
    case 1:
      mSubNodes[0].AssignLocation(ctr, prefix+".rhs", leafes);
      break;
    case 2:
      mSubNodes[0].AssignLocation(ctr, prefix+".lhs", leafes);
      mSubNodes[1].AssignLocation(ctr, prefix+".rhs", leafes);
      break;
    }

    //--------------------------------
    // ACCESS
    //--------------------------------
    
    const std::vector<TAbstractExpressionNode>& SubNodes() const {
      return mSubNodes;
    }
    const TExpressionLeafBase* LeafPtr() const {
      REQUIRE(mLeaf!=nullptr, "This node does not have a leaf");
      return mLeaf.get();
    }
    
  private:
    std::string Op;

    // invariant: either children, OR a leaf.  I.e. mLeaf is only
    // used if mChildren.empty()
    std::vector<TAbstractExpressionNode> mSubNodes;
    std::unique_ptr<TExpressionLeafBase>> mLeaf;
  }


  class TAbstractExpressionTree {
  public:
    using iterator=mLeafes::iterator;
    using const_iterator=mLeafes::const_iterator;
    const_iterator begin() const { return mLeafes.begin(); }
    const_iterator end() const { return mLeafes.end(); }

    // construct from a iBinaryOp
    // The implementation of this template is generally not #include'd,
    // to avoid compile-dependencies.
    template<class iBinaryOp> 
    TAbstractExpressionTree(const iBinaryOp& op); 

  private:
    
    // vector of all leafes: By storing it as a vector, iteration over
    // all leafes is easy.
    std::vector<TExpressionLeafBase*> mLeafes;

    // root node of the expression.
    TAbstractExpressionNode mRootNode;
  }



#endif
