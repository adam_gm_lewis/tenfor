#ifndef TExpressionNode_hpp
#define TExpressionNode_hpp
#include "TExpressionOperatorBase.hpp"
#include "TExpressionLeafBase.hpp"
#include <memory>
namespace TCodeWriter {

  //This code that represents iBinaryOp C++ expressions
  //as hierarchies of 'nodes'. Each node represents either a 'leaf'
  //(an object that can be operated on) or an operator. Operators have
  //either 1 (for unary operators) or 2 (binary) 'SubNodes' that represent
  //operands. 
  //
  //The fragment "B + C", for example, would generate three nodes: 
  //the binary operator '+' and its two SubNodes 'B' and 'C'. 
  //
  //For more complicated expressions the SubNodes may themselves be operators 
  //with SubNodes of their own. "A + sqrt(B * C)" would generate six nodes: 
  //the '+' operator, its SubNodes 'A' and 'sqrt', the 'sqrt' operator's SubNode
  //'*', and '*'s two SubNodes 'B' and 'C'.
  //
  //Once this hierarchy is generated, the expression can be recursed through in 
  //the correct order of operations. By performing this recursion and 
  //outputting a C++-executable string at each step, we can translate the 
  //iBinaryOp into executable code. 
  //
  //The recursion and translation is done by the function PrintExpression()
  //defined below. With the exception of TExpressionOperator, 
  //external code should call this function from the *top*
  //node only (i.e. the node stored as mpRootNode in a TExpressionTree).
  //Otherwise only a partial expression will be generated.
  //
  //PrintExpression checks whether a node is a leaf or a TExpressionOperator
  //and calls TExpressionOperator->GenerateExpressionString(subnodes) in the 
  //latter case. This function generates the C++ string appropriate to the
  //operator, recursively calling PrintExpression again upon the operands. This
  //recursion generates the entire expression.


  /// A C++ class **instance** represenentation of a 
  /// iBinaryOp template.  
  ///
  /// This representation is **independent** of the precise
  /// ImplicitTensorLoop-templates, so if the code here changes, only
  /// modest recompilations within SpEC are needed.
  
  class TExpressionNode {
  public:
    //--------------------------------
    // CONSTRUCTORS
    //--------------------------------

    // Constructor 1:  for binary operator
    TExpressionNode(std::unique_ptr<TExpressionNode> lhs,
		    //std::string op,
                    TExpressionOperatorBase* op,
		    std::unique_ptr<TExpressionNode> rhs):
      mIsLeaf(false),
      mOp{op},
      mSubNodes(),
      mLeaf{}
    { 
      mSubNodes.emplace_back(std::move(lhs));
      mSubNodes.emplace_back(std::move(rhs));
    }
    // Constructor 2:  for unary operator
    TExpressionNode(//std::string op,
		TExpressionOperatorBase* op,    
                std::unique_ptr<TExpressionNode> rhs):
      mIsLeaf(false),
      mOp{op},
      mSubNodes(),
      mLeaf{}//,
    {
      mSubNodes.emplace_back(std::move(rhs));
    }
    // Constructor 3:  for leaf
    TExpressionNode(TExpressionLeafBase* leaf):
      mIsLeaf(true),
      mOp{},
      mSubNodes{},
      mLeaf{leaf}//,
    { }

    //--------------------------------
    // SETUP
    //--------------------------------

    // iterate through all sub-nodes, keeping track of the location in
    // the hierarchy (i.e. a sequence like "lhs.rhs.lhs.lhs"). For each
    // leaf: call leaf.SetLocation(), increment ctr, and push_back the leaf
    // into leafes.
    void AssignLocation(int& ctr, const std::string& prefix, 
			std::vector<TExpressionLeafBase*>& leafes) {
      
      //std::cout << "AssignLocation: ctr=" << ctr << ", prefix=" << prefix
      //	<< " mSubNodes.size()=" << mSubNodes.size() << std::endl;
      switch(mSubNodes.size()) {
	case 0:
	  mLeaf->SetLocation(ctr, prefix);
	  leafes.push_back(mLeaf.get());
	  ++ctr; 
	  break; 
	case 1:
	  mSubNodes[0]->AssignLocation(ctr, prefix+".rhs", leafes);
	  break;
	case 2:
	  mSubNodes[0]->AssignLocation(ctr, prefix+".lhs", leafes);
	  mSubNodes[1]->AssignLocation(ctr, prefix+".rhs", leafes);
	  break;
	}
    }
    //--------------------------------
    // ACCESS
    //--------------------------------
    
    const std::vector<std::unique_ptr<TExpressionNode>>* SubNodes() const {
      return &mSubNodes;
    }
    const TExpressionLeafBase* LeafPtr() const {
      REQUIRE(mLeaf!=nullptr, "This node does not have a leaf");
      return mLeaf.get();
    }

    //This prints the expression that goes in the comment
    std::string PrintComment() const {
      std::string TheComment="";
        switch(mSubNodes.size()) {
          
          //leaf
          case 0:
            TheComment += mLeaf->LoopIndexedComment(); 
            break; 
          
          //unary op
          case 1:
            //First option is null
            TheComment += mOp->GenerateCommentString
                                  (mSubNodes[0],mSubNodes[0]);
            break;
          
          //binary op
          case 2:
            TheComment += "("+mOp->GenerateCommentString
                                    (mSubNodes[0],mSubNodes[1])+")";
            break;

          default:
            REQUIRE(false, "Error: mSubNodes had unexpected size.");
        }
        return TheComment;
    }
      

    //This prints the expression that actually gets executed
    //See TExpressionOperator.hpp (the SumOp specialization) for explanation
    //of FixedIndices, which maps indices to integer values they should be 
    //replaced with.
    std::string PrintExpression
      (const std::map<std::string,int>& FixedIndices
        =std::map<std::string,int>()) const {
        std::string TheExpression="";
        
        switch(mSubNodes.size()) {
          
          //leaf
          case 0:
            TheExpression += mLeaf->LoopIndexedExpression("",FixedIndices); 
            break; 
          
          //unary op
          case 1:
            //First option is null
            TheExpression += "(" + mOp->GenerateExpressionString
                                    (mSubNodes[0],mSubNodes[0],
                                     FixedIndices) + ")";
            break;
          
          //binary op
          case 2:
            TheExpression += "(" + mOp->GenerateExpressionString
                                    (mSubNodes[0],mSubNodes[1],
                                     FixedIndices) + ")";
            break;

          default:
            REQUIRE(false, "Error: mSubNodes had unexpected size.");
        }
        return TheExpression;
    }
    
    //This prints the expression that actually gets executed
    std::string PrintCUDAExpression
      (const std::map<std::string,int>& FixedIndices
        =std::map<std::string,int>()) const {
        std::string TheExpression="";
        
        switch(mSubNodes.size()) {
          
          //leaf
          case 0:
            TheExpression += mLeaf->CUDALoopIndexedExpression(FixedIndices); 
            break; 
          
          //unary op
          case 1:
            //First option is null
            TheExpression += "(" + mOp->GenerateCUDAExpressionString
                                    (mSubNodes[0],mSubNodes[0],
                                     FixedIndices) + ")";
            break;
          
          //binary op
          case 2:
            TheExpression += "(" + mOp->GenerateCUDAExpressionString
                                    (mSubNodes[0],mSubNodes[1],
                                     FixedIndices) + ")";
            break;

          default:
            REQUIRE(false, "Error: mSubNodes had unexpected size.");
        }
        return TheExpression;
    }
    bool IsLeaf(){ return mIsLeaf; } 

  private:
    const bool mIsLeaf;
    std::unique_ptr<TExpressionOperatorBase> mOp;
    std::vector<std::unique_ptr<TExpressionNode>> mSubNodes;
    std::unique_ptr<TExpressionLeafBase> mLeaf;
  };

}



#endif
