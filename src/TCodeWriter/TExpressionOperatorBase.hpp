#ifndef TExpressionOperatorBase_hpp
#define TExpressionOperatorBase_hpp
#include <memory>
//#include "TExpressionNode.hpp"


namespace TCodeWriter{

  class TExpressionNode;
  class TExpressionOperatorBase {
      // return '+'  or 'atan2' 
      // virtual std::string ShortName() const;
      
      // for '+' ->  "("+Lname+"+"+Rname+")"
      // for 'sqrt' -> "sqrt("+Rname+")" 
      // for 'atan2' -> "atan2("+Lname+","+Rname+")"
      public:
      virtual std::string GenerateExpressionString(
          std::unique_ptr<TExpressionNode> const &Lnode,
          std::unique_ptr<TExpressionNode> const &Rnode,
          const std::map<std::string,int>& FixedIndices
            =std::map<std::string,int>()
          ) const = 0;
    
      virtual std::string GenerateCUDAExpressionString(
          std::unique_ptr<TExpressionNode> const &Lnode,
          std::unique_ptr<TExpressionNode> const &Rnode,
          const std::map<std::string,int>& FixedIndices
            =std::map<std::string,int>()
          ) const = 0;
      
      virtual std::string GenerateCommentString(
          std::unique_ptr<TExpressionNode> const &Lnode,
          std::unique_ptr<TExpressionNode> const &Rnode
          ) const = 0;
        
  };
}

#endif
