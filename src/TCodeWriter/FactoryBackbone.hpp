//=============================================================================
#ifndef FactoryBackbone_hpp
#define FactoryBackbone_hpp
//=============================================================================
#include "NameOf.hpp"
#include "NonCopyable.hpp"
#include "FactoryBackboneImpl.hpp"
//#include "Utils/ErrorHandling/Deprecated.hpp"
//=============================================================================
#include <string>
#include <map>
#include <type_traits>
//=============================================================================

namespace Factory {

  class FactoryBackboneImpl;
  template<class T> class FactoryHolder;


  // Non-templated base-class wrapping class FactoryBackboneImpl.
  class FactoryBackbone {
    // interface for derived Factory<BASE> class
  protected: 
    FactoryBackbone();
    virtual ~FactoryBackbone();
    typedef void(*voidFn)();
    bool Register(const std::string& ID, voidFn ptr, const std::string& help);
    // implementation of deprecation registration
    //template<class DERIVED> bool RegisterDeprecated(const std::string& ID) {
      //if(std::is_base_of<Deprecated,DERIVED>::value) {
        //mpImpl->RegisterDeprecated(ID);
        //return true;
      //}
      //return false;
    //}
  public:
    static std::map<std::string,FactoryBackboneImpl*>& BaseClasses();

    voidFn Retrieve(const std::string& ID) const;
    /// publicly available member-functions.

    /// Returns whether a function name has been registered.
    bool KeyExists(const std::string& ID) const;
    /// Returns whether a registered function name is deprecated
    //bool KeyIsDeprecated(const std::string& ID) const;

    std::string Help() const;

    const FactoryBackboneImpl& Impl() const { return *mpImpl; }

    // Derived classes define this and include the template-type.  
    // For useful error messages. 
    virtual std::string Title() const = 0;
  private:
    class FactoryBackboneImpl* mpImpl;
  };

  //=================================================================
  // template<class BASE> Factory<BASE>
  //
  // This classe provides a factory for a concrete base class BASE.
  // 'BASE' must have a typedef 'CallingSequence' which represents
  // a pointer to a function returning a BASE*, with the function
  // taking whichever arguments are to be passed into the constructor
  // of the classes derived from BASE.
  //=================================================================

  template<class BASE> 
  class Factory: public FactoryBackbone, private NonCopyable {
    // start uniqueness enforcement 
    friend class FactoryHolder<Factory<BASE> >; 
  private:  
  public:
    Factory() {}
    ~Factory() {}
    // end uniqueness enforcement 
  public:
    // inherit KeyExists from FactoryBackbone
    // inherit KeyIsDeprecated from FactoryBackbone

    typedef typename BASE::CallingSequence FnPtrType;

    /// Register a function returning a BASE* with this Factory.
    /// The calling sequence of the function must be BASE::CallingSequence
    bool Register(const std::string& ID, FnPtrType FnPtr, 
                  const std::string& help) {
      return FactoryBackbone::Register(ID,
                                       reinterpret_cast<voidFn>(FnPtr),help);
    };

    /// Register DERIVED as deprecated if it inherits from Deprecated
    //template<class DERIVED> bool RegisterDeprecated(const std::string& ID) {
      //return FactoryBackbone::RegisterDeprecated<DERIVED>(ID);
    //}

    std::string Title() const { 
      return std::string("Factory<"+NameOf<BASE>()+">");
    };
  };


  //=================================================================
  // Exceedingly simple singleton interface.  The basic purpose of
  // this construct is to hold one variable which holds the
  // instantiation of Factory<BASE>.  More elaborate uniqueness
  // enforcement, or more elaborate singleton patters (a la
  // Alexandrescu) seem overkill.
  //=================================================================
  template<class BASE> 
  class FactoryHolder {
  public:
    static Factory<BASE>& Instance() {
      static Factory<BASE> value;
      return value; 
    }
  };

  // access function to avoid typing "FactoryHolder<...>.Instance()"
  template<class BASE>
  Factory<BASE>& TheFactory() { 
    return FactoryHolder<BASE>::Instance(); 
  }


  //=================================================================
  // MyClassID_Base allows base classes (inheriting from CallCreateN
  // or EnableCreationN) to know about the static function ClassID()
  // that all derived classes (inheriting from Register) must
  // implement. This is necessary because "virtual static" doesn't
  // exist!  See Factory.tex for details.
  //=================================================================
  class MyClassID_Base {
  public:
    virtual ~MyClassID_Base() { };
    virtual std::string MyClassID() const = 0;
  };
}


#endif
