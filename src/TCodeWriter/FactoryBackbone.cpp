//=============================================================================
#include "FactoryBackbone.hpp" // should be first include
//=============================================================================

namespace Factory {

  std::map<std::string,FactoryBackboneImpl*>& FactoryBackbone::BaseClasses() {
    static std::map<std::string,FactoryBackboneImpl*> baseClasses;
    return baseClasses;
  }

  FactoryBackbone::FactoryBackbone(): 
    mpImpl(new FactoryBackboneImpl) 
  { }
  FactoryBackbone::~FactoryBackbone() { 
    delete mpImpl; 
  }

  //============================================================
  // FactoryBackbone::Register
  //============================================================
  bool FactoryBackbone::Register(const std::string& ID, voidFn ptr,
                                 const std::string& help) {
    const std::string& title = Title();
    mpImpl->Register(ID, ptr, help, title);
    BaseClasses()[title] = mpImpl;
    return true;
  }

  FactoryBackbone::voidFn 
  FactoryBackbone::Retrieve(const std::string& ID) const {
    return mpImpl->Retrieve(ID, Title());
  }

  //================================================================

  std::string FactoryBackbone::Help() const {
    return mpImpl->Help();
  }

  //================================================================

  bool FactoryBackbone::KeyExists(const std::string& ID) const {
    return mpImpl->KeyExists(ID);
  }

  //bool FactoryBackbone::KeyIsDeprecated(const std::string& ID) const {
    //return mpImpl->KeyIsDeprecated(ID);
  //}

} // namespace Factory
