#ifndef CreateTExpressionTree_hpp
#define CreateTExpressionTree_hpp

#include "TExpressionTree.hpp"
#include "TExpressionTree-impl.hpp"
namespace TCodeWriter {
  template<class RHS>
  struct CreateTExpressionTree { 
    static TExpressionTree Create(std::string& prefix) {
      RHS* ptr = 0;
      TExpressionTree TempTree(*ptr, prefix);
      return TempTree;
    }
  };
}
#endif


