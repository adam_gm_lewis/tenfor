//=============================================================================
#ifndef INCLUDED_FactoryBackboneImpl_hpp
#define INCLUDED_FactoryBackboneImpl_hpp
//=============================================================================
#include <vector>
#include <string>
#include <list>
//#include "Utils/MyContainers/MyList.hpp"

//template <class> class MyVector;
//=============================================================================

namespace Factory {

  // This class stores a set of ID's, void* (*)()'s and Help-texts.
  // Whenever an ID is added the second time, or when a non-existing
  // ID is retrieved, it throws an exception.
  class FactoryBackboneImpl {
  public:
    typedef void(*voidFn)();
    bool Register(const std::string& ID, voidFn ptr, const std::string& help,
                  const std::string& title);
    //void RegisterDeprecated(const std::string& ID);
    voidFn Retrieve(const std::string& ID, const std::string& title) const;
    bool KeyExists(const std::string& ID) const;
    //bool KeyIsDeprecated(const std::string& ID) const;

    /// Returns the names of all previously registered
    /// functions.  (needs to be called through a derived class
    /// Factory<BASE>::RegisteredClassIDs()
    std::list<std::string> RegisteredClassIDs() const;
    //std::list<std::string> DeprecatedClassIDs() const;
    std::string Help() const;
  private:
    struct Thingy {
      Thingy(const std::string& id, voidFn& ptr, const std::string& help):
        mID(id), mFunPtr(ptr), mHelp(help) { };
      std::string mID;
      voidFn mFunPtr;
      std::string mHelp;
      bool operator<(const Thingy& r) const { return mID<r.mID;}
      bool operator==(const Thingy& r)const { return mID==r.mID;}
      bool operator==(const std::string& r) const { return mID==r; }
    };
    std::vector<Thingy> mData;
    std::list<std::string> mDeprecatedIDs;
  };

  /// free function to format the output of RegisteredClassIDs() nicely.
  std::string FormatRegisteredClassIDs(const std::list<std::string>& ids);
  /// free function to format the output of RegisteredClassIDs() nicely when
  /// more than one base class is possible.
  std::string
  FormatRegisteredClassIDs(const std::vector<std::list<std::string> >& ids);

}


#endif
