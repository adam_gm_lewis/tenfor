//=============================================================================
/// \file
/// Implements several Factory helper functions, which are in
/// a separate file to avoid pollution with header-files.
//=============================================================================
#ifndef INCLUDED_FactoryHelperFunctions_hpp
#define INCLUDED_FactoryHelperFunctions_hpp
//=============================================================================
#include "Factory.hpp"
#include "FactoryBackboneImpl.hpp"
#include <list>
//=============================================================================


namespace Factory {

  /// Does the key 'ID' exist in the Factory creating 'BASE'?
  template<class BASE>
  bool KeyExists(const std::string& ID) {
    return TheFactory<BASE>().KeyExists(ID);
  }

  /// List of all keys in the Factory creating 'BASE'.
  template<class BASE>
  std::list<std::string> RegisteredClassIDs() {
    const FactoryBackboneImpl& impl = TheFactory<BASE>().Impl();
    return impl.RegisteredClassIDs();
  }

  /// Is the key 'ID' in Factory<BASE> deprecated?
  //template<class BASE>
  //bool KeyIsDeprecated(const std::string& ID) {
    //return TheFactory<BASE>().KeyIsDeprecated(ID);
  //}

  /// List of all keys in the Factory creating 'BASE' that are deprecated.
  //template<class BASE>
  //std::list<std::string> DeprecatedClassIDs() {
    //const FactoryBackboneImpl& impl = TheFactory<BASE>().Impl();
    //return impl.DeprecatedClassIDs();
  //}
}

//=============================================================================

#endif  // FactoryHelperFunctions_hpp
