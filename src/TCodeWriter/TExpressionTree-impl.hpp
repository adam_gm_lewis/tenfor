#ifndef TExpressionTree_impl_hpp
#define TExpressionTree_impl_hpp

#include "TExpressionTree.hpp"
#include "TExpressionLeaf.hpp"
#include "Tensor.hpp"
#include "OperatorSymbols.hpp"
#include "TExpressionOperator.hpp"
#include <iostream>
namespace TCodeWriter {
  //  using namespace ImplicitTensorLoops;


  template<class iBinaryOp_t>
  struct ConstructTExpressionNode;
  

  // SPECIALIZATIONS
  
  // Recursion 1: binary operators
  template<class L, class Op, class R>
  struct ConstructTExpressionNode<iBinaryOp<L,Op,R> > {
    
    typedef ConstructTExpressionNode<L> CL;
    typedef ConstructTExpressionNode<R> CR;
    
    static std::unique_ptr<TExpressionNode> construct() {
      return std::unique_ptr<TExpressionNode>
	    (new TExpressionNode(
			     CL::construct(),
			     new TExpressionOperator<Op>(),
			     CR::construct()
			     )
	    );
    }
  };

  // Recursion 2: unary operators
  template<class Op, class R>
  struct ConstructTExpressionNode<iBinaryOp<EmptyType,Op,R> > {
    
    typedef ConstructTExpressionNode<R> CR;
    
    static std::unique_ptr<TExpressionNode> construct() {
      return std::unique_ptr<TExpressionNode>
	    (new TExpressionNode(
			     new TExpressionOperator<Op>(),
			     CR::construct()
			     )
	    );
    }
  };
  
  /* 
  template<class TIndex_t, class R>
  struct ConstructTExpressionNode<iBinaryOp<TIndex_t, SumOp,R> > {  
    
    typedef iBinaryOp<TIndex_t,SumOp,R> sumBinOp_t;
    typedef typename sumBinOp_t::ExpandIndices_t EIT;
    typedef ConstructTExpressionNode<EIT> CR;
      static std::unique_ptr<TExpressionNode> construct() {
        return std::unique_ptr<TExpressionNode>
            (new TExpressionNode(
                                new TExpressionOperator<SumOp>(),
                                CR::construct()
                                )
            );
      }
  };
  */
  // Recursion 3: SumOp
  
  template<class TIndex_t, class R>
  struct ConstructTExpressionNode<iBinaryOp<TIndex_t, SumOp,R> > {  
      
    typedef ConstructTExpressionNode<R> CR;
      static std::unique_ptr<TExpressionNode> construct() {
        //std::cout << TNameHelper<iBinaryOp<TIndex_t,SumOp,R> >::CPlusPlusName() << std::endl;
        return std::unique_ptr<TExpressionNode>
            (new TExpressionNode(
                new TExpressionOperator<iBinaryOp<TIndex_t,SumOp,R> >(),
                                CR::construct()
                                )
            );
      }
  };
  
  // BinaryOp recursions (instead of iBinaryOp - i.e. for DataMesh
  // instead of Tensor)
  // Recursion 4: DM binary operator 
  template<class L, class Op, class R>
  struct ConstructTExpressionNode<BinaryOp<L,Op,R> > {

    typedef ConstructTExpressionNode<L> CL;
    typedef ConstructTExpressionNode<R> CR;
    
    static std::unique_ptr<TExpressionNode> construct() {
      return std::unique_ptr<TExpressionNode>
	    (new TExpressionNode(
			     CL::construct(),
			     new TExpressionOperator<Op>(),
			     CR::construct()
			     )
	    );
    }
  };

  // Recursion 5: DM unary operator 
  template<class Op, class R>
  struct ConstructTExpressionNode<BinaryOp<EmptyType,Op,R> > {
    
    typedef ConstructTExpressionNode<R> CR;
    
    static std::unique_ptr<TExpressionNode> construct() {
      return std::unique_ptr<TExpressionNode>
	    (new TExpressionNode(
			     new TExpressionOperator<Op>(),
			     CR::construct()
			     )
	    );
    }
  };

  // Leaf 1: double, DataMesh
  template<class T>
  struct ConstructTExpressionNode {
    
    static std::unique_ptr<TExpressionNode>
    construct() {
      return std::unique_ptr<TExpressionNode>
	    (new TExpressionNode(new TExpressionLeaf<T>()));
    }
  };



  template<class Structure>
  struct ConstructTExpressionNode<NonConstiBinaryOp<Structure,    
						    EmptyType,
						    DataMesh> 
				  > {
    typedef iBinaryOp<Structure, EmptyType, DataMesh> T;
    static std::unique_ptr<TExpressionNode> construct() {
      return std::unique_ptr<TExpressionNode>
	    (new TExpressionNode(new TExpressionLeaf<T>()));
    }
  };


  // Leaf 2: Tensor, Tensor<Tensor<DataMesh>>
  //
  // For Tensor, L = TIndexStructure, 
  // For T<T > , L = pair<TIndexStructure1, TIndexStructure2>
  template<class Structure>
  struct ConstructTExpressionNode<iBinaryOp<Structure,    
					    EmptyType,
					    DataMesh> 
				  > {
    typedef iBinaryOp<Structure, EmptyType, DataMesh> T;
    static std::unique_ptr<TExpressionNode> construct() {
      return std::unique_ptr<TExpressionNode>
	    (new TExpressionNode(new TExpressionLeaf<T>()));
    }
  };
  

  
  //================================================================
  // TExpressionTree
  //================================================================
  
  template<class iBinaryOp_t> 
  TExpressionTree::TExpressionTree(const iBinaryOp_t&, 
				   const std::string& prefix ):
    mpRootNode{ConstructTExpressionNode<iBinaryOp_t>::construct()},
    mLeafes{}
  {
    // iterate through all nodes, collect the leafes, set calling sequence
    int ctr=1;
    mpRootNode->AssignLocation(ctr, prefix, mLeafes);
 
  }
  
}

#endif
