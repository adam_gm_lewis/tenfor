#ifndef CudaCodeWriter_impl_hpp
#define CudaCodeWriter_impl_hpp
#ifdef CODE_WRITER
#include "ImplicitTensorLoops.hpp"
#include "TExpressionTree.hpp"
#include "TExpressionTree-impl.hpp"
#include "TExpressionNode.hpp"
#include "ConcreteTExpressionTreeHouse.hpp"
#include <string>
namespace TCodeWriter{
  class CudaCodeWriter {
    public:
      void Write(); //this is the only one to be called from outside
    private:
      void Prep();
      void PrepAutoExpressions_cpp();
      void PrepAutoExpressions_hpp();
      void PrepAutoExpressionCUDAPointerBundlers_cpp();
      void PrepAutoExpressionCUDAPointerBundlers_hpp();
      void PrepAutoExpressionCUDAFunctions_cu();
      void PrepAutoExpressionCUDAFunctions_hpp();

      void Finalize();
      void FinalizeAFile(const int number_of_endifs, const std::string& fname,
          bool closing_brace);
      
      std::string Comment(TExpressionTreeHouseBase* TreeHouse_ptr,
          const int& gnumber);
      std::string WriteAccelDebugCall(TExpressionTreeHouseBase* TreeHouse_ptr);
      std::string WriteAccelDebugComparison(
            TExpressionTreeHouseBase* TreeHouse_ptr,
            const int gnumber);
      std::string TLoopApplyString(TExpressionTreeHouseBase* TreeHouse_ptr);
      void AddEntryToAutoExpressions_cpp(TExpressionTreeHouseBase* TreeHouse_ptr, 
            const int gnumber);
      void AddEntryToAutoExpressions_hpp(TExpressionTreeHouseBase* TreeHouse_ptr, 
            const int gnumber);
     
      std::string CUDAFunctionArguments
            (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber); 
      std::string CUDAKernelArguments
            (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber); 
      std::string PointerBundlerString
          (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber);
      void AddEntryToAutoExpressionCUDAPointerBundlers_cpp
        (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber);
      void AddEntryToAutoExpressionCUDAPointerBundlers_hpp
        (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber);
      
      std::string CUDAKernel
            (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber); 
      std::string CUDAWrapper_cu(TExpressionTreeHouseBase* TreeHouse_ptr, 
          const int gnumber); 
      void AddEntryToAutoExpressionCUDAFunctions_cu
        (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber);
      void AddEntryToAutoExpressionCUDAFunctions_hpp
        (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber);
      
      void AddEntry(TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber);
  };
}
#endif
#endif
