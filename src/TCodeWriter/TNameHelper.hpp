#ifndef TNameHelper_hpp
#define TNameHelper_hpp

#include <typeinfo>
#include <string>
#include "ToString.hpp"

class DataMesh;
template<class X> class Tensor;

namespace ImplicitTensorLoops {

  template<class T>
  struct TNameHelper; 
  //{
  //  static std::string ShortName() { return typeid(T).name(); }
  //  static std::string CPlusPlusName() { return typeid(T).name(); }
  //};

  //================================================================
  // MultOp, PlusOp, MinusOp, DivOp
  //================================================================
  template<>
  struct TNameHelper<AddOp> {
    static std::string ShortName() { return "+"; }
    static std::string CPlusPlusName() { return "AddOp"; }
  };
  template<>
  struct TNameHelper<SubOp> {
    static std::string ShortName() { return "-"; }
    static std::string CPlusPlusName() { return "SubOp"; }
  };
  template<>
  struct TNameHelper<MultOp> {
    static std::string ShortName() { return "*"; }
    static std::string CPlusPlusName() { return "MultOp"; }
  };
  template<>
  struct TNameHelper<DivOp> {
    static std::string ShortName() { return "/"; }
    static std::string CPlusPlusName() { return "DivOp"; }
  };
    
  //SumOp
  template<>
  struct TNameHelper<SumOp> {
    static std::string ShortName() { return "Sum"; }
    static std::string CPlusPlusName() { return "SumOp"; }
  };
  
  //================================================================
  // TNameHelper's for operators of DataMeshes
  // (see DataMesh/ExpressionTemplates.hpp)
  //================================================================

  template<>
  struct TNameHelper<EmptyType> {
    static std::string ShortName() { return ""; }
    static std::string CPlusPlusName() { return "EmptyType"; }
  };
 
  //BinaryOp Binary operators
  //In Utils/DataMesh/ExpressionTemplates.hpp
  //Add, Sub, Mult, Div OK
  template<>
  struct TNameHelper<PowOp> {
    static std::string ShortName() { return "pow"; }
    static std::string CPlusPlusName() { return "PowOp"; }
  };
  template<>
  struct TNameHelper<Atan2Op> {
    static std::string ShortName() { return "atan2"; }
    static std::string CPlusPlusName() { return "Atan2Op"; }
  };
  template<>
  struct TNameHelper<MinOp> {
    static std::string ShortName() { return "min"; }
    static std::string CPlusPlusName() { return "MinOp"; }
  };
  template<>
  struct TNameHelper<MaxOp> {
    static std::string ShortName() { return "max"; }
    static std::string CPlusPlusName() { return "MaxOp"; }
  };

  //Unaries
  //Allow for calls to the unary BinaryOp operators
  //These are defined in Utils/DataMesh/ExpressionTemplates.hpp
  template<>
  struct TNameHelper<doNothingOp> {
    static std::string ShortName() { return ""; }
    static std::string CPlusPlusName() { return "doNothingOp"; }
  };
 
  template<>
  struct TNameHelper<negateOp> {
    static std::string ShortName() { return "-"; }
    static std::string CPlusPlusName() { return "negateOp"; }
  };
  
  template<>
  struct TNameHelper<sqrtOp> {
    static std::string ShortName() { return "sqrt"; }
    static std::string CPlusPlusName() { return "sqrtOp"; }
  };
  template<>
  struct TNameHelper<sqrOp> {
    static std::string ShortName() { return "sqr"; }
    static std::string CPlusPlusName() { return "sqrOp"; }
  };
  template<>
  struct TNameHelper<cubeOp> {
    static std::string ShortName() { return "cube"; }
    static std::string CPlusPlusName() { return "cubeOp"; }
  };
  template<>
  struct TNameHelper<fabsOp> {
    static std::string ShortName() { return "fabs"; }
    static std::string CPlusPlusName() { return "fabsOp"; }
  };
  template<>
  struct TNameHelper<expOp> {
    static std::string ShortName() { return "exp"; }
    static std::string CPlusPlusName() { return "expOp"; }
  };
  template<>
  struct TNameHelper<logOp> {
    static std::string ShortName() { return "log"; }
    static std::string CPlusPlusName() { return "logOp"; }
  };
  template<>
  struct TNameHelper<log10Op> {
    static std::string ShortName() { return "log10"; }
    static std::string CPlusPlusName() { return "log10Op"; }
  };
  template<>
  struct TNameHelper<cosOp> {
    static std::string ShortName() { return "cos"; }
    static std::string CPlusPlusName() { return "cosOp"; }
  };
  template<>
  struct TNameHelper<acosOp> {
    static std::string ShortName() { return "acos"; }
    static std::string CPlusPlusName() { return "acosOp"; }
  };
  template<>
  struct TNameHelper<sinOp> {
    static std::string ShortName() { return "sin"; }
    static std::string CPlusPlusName() { return "sinOp"; }
  };
  template<>
  struct TNameHelper<asinOp> {
    static std::string ShortName() { return "asin"; }
    static std::string CPlusPlusName() { return "asinOp"; }
  };
  template<>
  struct TNameHelper<coshOp> {
    static std::string ShortName() { return "cosh"; }
    static std::string CPlusPlusName() { return "coshOp"; }
  };
  template<>
  struct TNameHelper<sinhOp> {
    static std::string ShortName() { return "sinh"; }
    static std::string CPlusPlusName() { return "sinhOp"; }
  };
  template<>
  struct TNameHelper<tanhOp> {
    static std::string ShortName() { return "tanh"; }
    static std::string CPlusPlusName() { return "tanhOp"; }
  };
  template<>
  struct TNameHelper<tanOp> {
    static std::string ShortName() { return "tan"; }
    static std::string CPlusPlusName() { return "tanOp"; }
  };
  template<>
  struct TNameHelper<atanOp> {
    static std::string ShortName() { return "atan"; }
    static std::string CPlusPlusName() { return "atanOp"; }
  };
  template<>
  struct TNameHelper<RoundOp> {
    static std::string ShortName() { return "round"; }
    static std::string CPlusPlusName() { return "RoundOp"; }
  };
  template<>
  struct TNameHelper<StepFunctionOp> {
    static std::string ShortName() { return "StepFunction"; }
    static std::string CPlusPlusName() { return "StepFunctionOp"; }
  };
  template<>
  struct TNameHelper<floorOp> {
    static std::string ShortName() { return "floor"; }
    static std::string CPlusPlusName() { return "floorOp"; }
  };
  template<>
  struct TNameHelper<ceilOp> {
    static std::string ShortName() { return "ceil"; }
    static std::string CPlusPlusName() { return "ceilOp"; }
  };
  //================================================================
  // TIndex<dim,label>,   int
  //================================================================
  
  template<int dim, int label>
  struct TNameHelper<TIndex<dim,label>> {
    static std::string ShortName();
    static std::string CPlusPlusName();
  };

  template<>
  struct TNameHelper<int> {
    static std::string ShortName() { return "int"; }
    static std::string CPlusPlusName() { return "int"; }
  };


  //================================================================
  // TIndexSlot<dim,label, offset>
  //================================================================
  
  template<int d, int l, int o>
  struct TNameHelper<TIndexSlot<d,l,o>> {
    static std::string ShortName(){return TIndexSlot<d, l, o>::VarName();}
    static std::string CPlusPlusName()
        {return TIndexSlot<d,l,o>::CPlusPlusName();}
    
  };

  //================================================================
  // TAssignmentHelpers
  //================================================================

  template<>
  struct TNameHelper<TSetEqualOp> {
    static std::string ShortName() { return "="; }
    static std::string CPlusPlusName() { return "TSetEqualOp"; }
  };
  template<>
  struct TNameHelper<TPlusEqualOp> {
    static std::string ShortName() { return "+="; }
    static std::string CPlusPlusName() { return "TPlusEqualOp"; }
  };
  template<>
  struct TNameHelper<TMinusEqualOp> {
    static std::string ShortName() { return "-="; }
    static std::string CPlusPlusName() { return "TMinusEqualOp"; }
  };
  template<>
  struct TNameHelper<TMultEqualOp> {
    static std::string ShortName() { return "*="; }
    static std::string CPlusPlusName() { return "TMultEqualOp"; }
  };
  template<>
  struct TNameHelper<TDivEqualOp> {
    static std::string ShortName() { return "/="; }
    static std::string CPlusPlusName() { return "TDivEqualOp"; }
  };

  //================================================================
  // TInequality<pos1,pos2>
  //================================================================

  template<int pos1, int pos2>
  struct TNameHelper<TInequality<pos1,pos2>> {
    static std::string ShortName() {
    return "#"+std::to_string(pos1)+">=#"+std::to_string(pos2);
  }
    static std::string CPlusPlusName() {
      return "TInequality<"+std::to_string(pos1)+","+std::to_string(pos2)+">";
    }
  };

  //================================================================
  // TNameHelperList -- output comma-separated list of items
  //================================================================
  
  template<class ...Tail>
  struct TNameHelperList;

  // empty
  template<>
  struct TNameHelperList<> {
    static std::string ShortName() { return ""; }
    static std::string CPlusPlusName() { return ""; }
  };
  
  // one element
  template<class T>
  struct TNameHelperList<T> {
    typedef TNameHelper<T> H;
    static std::string ShortName() { return H::ShortName(); }
    static std::string CPlusPlusName() { return H::CPlusPlusName(); }
  };
  
  // two+ elements
  template<class T, class ...Tail>
  struct TNameHelperList<T, Tail...> {
    typedef TNameHelper<T> H;
    typedef TNameHelperList<Tail...> Tail_t;
    static std::string ShortName() { 
      return H::ShortName()+", "+Tail_t::ShortName(); }
    static std::string CPlusPlusName() { 
      return H::CPlusPlusName()+", "+Tail_t::CPlusPlusName(); }
  };

  //================================================================
  // TSymmetry<TInequality<pos1,pos2>, ...>
  //================================================================

  template <class ...Symm>
  struct TNameHelper<TSymmetry<Symm...> > {
    typedef TNameHelperList<Symm...> R;
    static std::string ShortName() { 
      return "{"+R::ShortName()+"}"; 
    }
    static std::string CPlusPlusName() { 
      return "TSymmetry<"+R::CPlusPlusName()+">"; 
    }
  };


  //================================================================
  // TIndexStructure<TSymmetry<...>, Indices...>
  //================================================================

  template <class ...Symm, class ...Indices>
  struct TNameHelper<TIndexStructure<TSymmetry<Symm...>, Indices...> > {
    typedef TNameHelper<TSymmetry<Symm...>> TS;
    typedef TNameHelperList<Indices...> R;
    static std::string ShortName() { 
      return TS::ShortName()+", "+R::ShortName(); 
    }
    static std::string CPlusPlusName() { 
      return "TIndexStructure<"
	+TS::CPlusPlusName()+", "+R::CPlusPlusName()+">"; 
    }
  };


  //================================================================
  // NonConstiBinaryOp
  //================================================================

  template <class TIndexStructure_t>
  struct TNameHelper<NonConstiBinaryOp<TIndexStructure_t, 
				       EmptyType, DataMesh> > {
    typedef TNameHelper<TIndexStructure_t> T;
    static std::string ShortName() { 
      return "NonConstTDm("+T::ShortName()+")"; 
    }
    static std::string CPlusPlusName() { 
      return "NonConstiBinaryOp<"+T::CPlusPlusName()+", EmptyType, DataMesh>"; 
    }
  };


  //================================================================
  // iBinaryOp
  //================================================================


  // Leaf 1: Tensor
  template <class TIndexStructure_t>
  struct TNameHelper<iBinaryOp<TIndexStructure_t, 
			       EmptyType, DataMesh> > {
    typedef TNameHelper<TIndexStructure_t> T;
    static std::string ShortName() { 
      return "TDm("+T::ShortName()+")"; 
    }
    static std::string CPlusPlusName() { 
      return "iBinaryOp<"+T::CPlusPlusName()+", EmptyType, DataMesh>"; 
    }
    static std::string Expression(int& ctr) {
      return "TDm"+IntToStringPaddedZero(ctr++,2)+"["
	+T::ShortName()+"]";
    }
  };


  // Leaf 2:  Tensor<Tensor>
  
  template <class TIndexStructure1, class TIndexStructure2>
  struct TNameHelper<iBinaryOp<std::pair<TIndexStructure1, 
					 TIndexStructure2>,
			       EmptyType, DataMesh > > {
    typedef TNameHelper<TIndexStructure1> T1;
    typedef TNameHelper<TIndexStructure2> T2;
    static std::string ShortName() { 
      return "TTDm("+T1::ShortName()+")("+T2::ShortName()+")"; 
    }
    static std::string CPlusPlusName() { 
      return "iBinaryOp<std::pair<"+T1::CPlusPlusName()
	+", "+T2::CPlusPlusName()+"> , EmptyType, DataMesh>"; 
    }
    static std::string Expression(int& ctr) {
      return "TDm"+IntToStringPaddedZero(ctr++,2);
    }
  };
 
  //this one involves Tensor<DataMesh> - but it doesn't
  //seem to actually be used
  template <class TIndexStructure1, class TIndexStructure2>
  struct TNameHelper<iBinaryOp<std::pair<TIndexStructure1, 
					 TIndexStructure2>,
			       EmptyType, Tensor<DataMesh> > > {
    typedef TNameHelper<TIndexStructure1> T1;
    typedef TNameHelper<TIndexStructure2> T2;
    static std::string ShortName() { 
      return "TTDm("+T1::ShortName()+")("+T2::ShortName()+")"; 
    }
    static std::string CPlusPlusName() { 
      return "iBinaryOp<std::pair<"+T1::CPlusPlusName()
	+", "+T2::CPlusPlusName()+"> , EmptyType, Tensor<DataMesh>>"; 
    }
    static std::string Expression(int& ctr) {
      return "TDm"+IntToStringPaddedZero(ctr++,2);
    }
  };
  
  // Leaf 3:  double
  // let's try to specialize "double" -- may fail
  template <>
  struct TNameHelper<double> {
    static std::string ShortName() { 
      return "d";
    }
    static std::string CPlusPlusName() { 
      return "double";
    }
    static std::string Expression(int& ctr) {
      return "d"+IntToStringPaddedZero(ctr++,2);
    }

  };
  // Leaf 4: DataMesh
  // let's try to specialize "DataMesh" -- may fail
  template <>
  struct TNameHelper<DataMesh> {
    static std::string ShortName() { 
      return "Dm";
    }
    static std::string CPlusPlusName() { 
      return "DataMesh";
    }

    static std::string Expression(int& ctr) {
      return "DM"+IntToStringPaddedZero(ctr++,2);
    }
  };
  
  // Recursion 1: binary operators
  template<class L, class Op, class R>
  struct TNameHelper<iBinaryOp<L,Op,R> > {
    typedef TNameHelper<L> HL;
    typedef TNameHelper<Op> HOp;
    typedef TNameHelper<R> HR;
    static std::string ShortName() {
      return "iBOp<"+HL::ShortName()+HOp::ShortName()+HR::ShortName()
	+">";
    }
    static std::string CPlusPlusName() {
      return "iBinaryOp<"+HL::CPlusPlusName()+", "
	+HOp::CPlusPlusName()+", "+HR::CPlusPlusName()
	+">";
    }

    static std::string Expression(int& ctr) {
      std::string lhs=HL::Expression(ctr);
      std::string rhs=HR::Expression(ctr);
      return "("+lhs+HOp::ShortName()+rhs+")";
    }
  };


  // Recursion 2: Unary operators
  template<class Op, class R>
  struct TNameHelper<iBinaryOp<EmptyType,Op,R> > {
    typedef TNameHelper<Op> HOp;
    typedef TNameHelper<R> HR;
    static std::string ShortName() {
      return "iBOp<"+HOp::ShortName()+HR::ShortName()
	+">";
    }
    static std::string CPlusPlusName() {
      return "iBinaryOp<EmptyType, "
	+HOp::CPlusPlusName()+", "+HR::CPlusPlusName()
	+">";
    }

    static std::string Expression(int& ctr) {
      std::string rhs=HR::Expression(ctr);
      return "("+HOp::ShortName()+rhs+")";
    }
  };


  // Recursion 3: SumOp
  template<class TIndex_t, class R>
  struct TNameHelper<iBinaryOp<TIndex_t, SumOp,R> > {   
    typedef TNameHelper<TIndex_t> HT;
    typedef TNameHelper<R> HR;
    static std::string ShortName() {
      return "Sum("+HT::ShortName()+", "+
	HR::ShortName()+")";
    }
    static std::string CPlusPlusName() {
      return "iBinaryOp<"+HT::CPlusPlusName()+", SumOp, "+
	HR::CPlusPlusName()+">";
    }

    static std::string Expression(int& ctr) {
      std::string rhs=HR::Expression(ctr);
      return "Sum("+HT::ShortName()+", "+rhs+")";
    }
  };



  //================================================================
  // BinaryOp
  //================================================================

  template<class L, class Op, class R>
  struct TNameHelper<BinaryOp<L,Op,R>> {
    typedef TNameHelper<L> LH;
    typedef TNameHelper<Op> LOp;
    typedef TNameHelper<R> LR;

    static std::string ShortName() { 
      return "bOp<"+LH::ShortName()+", "
                +LOp::ShortName()+", "+LR::ShortName()+">";
    }
    static std::string CPlusPlusName() { 
      return "BinaryOp<"
	+LH::CPlusPlusName()+", "
        +LOp::CPlusPlusName()+", "+LR::CPlusPlusName()+">";
    } 

    static std::string Expression(int& ) {
      return "binOpExpression";
    }
  };

}


#endif
