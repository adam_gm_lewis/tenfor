#ifndef CUDAForLoopHelper_hpp
#define CUDAForLoopHelper_hpp

#include "TIndexStructure.hpp"
#include <iostream>
using namespace ImplicitTensorLoops;

template <typename... Args> 
    struct CUDAForLoopHelper;

//Recurse over all the indices and print a for loop for each.

//Case 1: First index is free and not part of a symmetry
//If none of the specializations below match, we are on a free index
//with no symmetry restriction. After recursing all the way to the bottom,
//print the appropriate for loop. This will eventually return *all* the 
//for loops appropriate to the tensor.
template<class ...Symm, 
        int d, int l, int o, class ...Tail>
struct CUDAForLoopHelper<TIndexStructure<TSymmetry<Symm...>,
        TIndexSlot<d,l,o>, Tail...> >
{
    std::string ForLoops(std::vector<std::string>& ParallelInds, 
        std::vector<int>& ParallelDims,
        int& cnt){
        //Don't print loops for up to 4 asymmetric indices. We'll parallelize 
        //these ourselves.
        if(cnt < 4){
          const int thiscnt=cnt;
          ++cnt;
          //these should be if statements instead of loops
          std::string the_loops = "";
          //do recursion *first* - helps deal with symmetries
          typedef TIndexStructure<TSymmetry<Symm...>, 
                  TIndexSlot<d,l,o>, Tail...> this_TStruc;
          typedef typename GetNthBaseClass<1, this_TStruc>::type next_TStruc;
          CUDAForLoopHelper<next_TStruc> NextHelper;
          the_loops += NextHelper.ForLoops(ParallelInds,ParallelDims, cnt);
          
          //retrieve the name of this index
          //Get the TIndex_t (NOT TIndexSlot_t) associated with this
          //TIndexStructure - we don't want any +1's in the for loop
          typedef typename this_TStruc::FirstSlotTIndexBare_t this_TIndex_t;
          TNameHelper<this_TIndex_t> this_TNameHelper;
          std::string this_ind = this_TNameHelper.ShortName(); //"i", "j",etc.
          ParallelDims[thiscnt]=d;
          ParallelInds[thiscnt] = this_ind;  
          //the dimension
          std::string dstr = std::to_string(d);
          the_loops += "if (" + this_ind + "<" + dstr + ") { \n";
          return the_loops;
          
        }else{
          std::string the_loops = "";
          //do recursion *first* - helps deal with symmetries

          typedef TIndexStructure<TSymmetry<Symm...>, 
                  TIndexSlot<d,l,o>, Tail...> this_TStruc;
          typedef typename GetNthBaseClass<1, this_TStruc>::type next_TStruc;
          CUDAForLoopHelper<next_TStruc> NextHelper;
          the_loops += NextHelper.ForLoops(ParallelInds,ParallelDims, cnt);
         
          //retrieve the name of this index

          //Get the TIndex_t (NOT TIndexSlot_t) associated with this
          //TIndexStructure - we don't want any +1's in the for loop
          typedef typename this_TStruc::FirstSlotTIndexBare_t this_TIndex_t;
          TNameHelper<this_TIndex_t> this_TNameHelper;
          std::string this_ind = this_TNameHelper.ShortName(); //"i", "j",etc.
          
          //the dimension
          std::string dstr = std::to_string(d);
          the_loops += "#pragma unroll\n";
          the_loops += "for(int " + this_ind + "=0; " +
              this_ind + "<" + dstr + "; ++" + this_ind + ") {\n";
          return the_loops;
      }

  }
};

//Case 2: First index is fixed.
//Continue recursion, but don't add anything. 
template<class ...Symm, class...Indices>
struct CUDAForLoopHelper<TIndexStructure<TSymmetry<Symm...>,int,Indices...>>
{
    std::string ForLoops(std::vector<std::string>& ParallelInds, 
        std::vector<int>& ParallelDims, int& cnt){
        std::string the_loops = "";
        typedef TIndexStructure<TSymmetry<Symm...>,int,Indices...> this_TStruc;
        typedef typename GetNthBaseClass<1, this_TStruc>::type next_TStruc;
        CUDAForLoopHelper<next_TStruc> NextHelper;
        the_loops += NextHelper.ForLoops(ParallelInds,ParallelDims, cnt);
        
        return the_loops;
    }
};

//Case 3: The first index is free and symmetric with an index further
//down the chain. After recursing, find that index and print out the 
//appropriate for loop.
template<int pos2, class ...TailSymm, 
        int d, int l, int o, class ...Tail>
    struct CUDAForLoopHelper<TIndexStructure<TSymmetry<TInequality<0, pos2>, TailSymm...>,
                        TIndexSlot<d,l,o>, Tail...>>
{
    std::string ForLoops(std::vector<std::string>& ParallelInds, 
        std::vector<int>& ParallelDims, int& cnt) {
        
        std::string the_loops = "";
        //convenience
        typedef TIndexStructure<TSymmetry<TInequality<0, pos2>, TailSymm...>,
                TIndexSlot<d,l,o>, Tail...> this_TStruc;
        //recursion
        typedef typename GetNthBaseClass<1, this_TStruc>::type next_TStruc;
        CUDAForLoopHelper<next_TStruc> NextHelper;
        the_loops += NextHelper.ForLoops(ParallelInds,ParallelDims, cnt);

        //Print the for loop. We need the name of the symmetric partner,
        //so this is a bit more complicated than in the other cases.
        //get the other index in this TInequality from this_type
        typedef typename GetNthBaseClass<pos2, this_TStruc>::type partner_TStruc;
        
        //get the type of the leading TIndex<..> from partner_TStruc
        typedef typename partner_TStruc::FirstSlotTIndexBare_t partner_TIndex;

        //a TNameHelper from partner_Index lets us get the index name
        TNameHelper<partner_TIndex> partner_TNameHelper;
        std::string part_ind = partner_TNameHelper.ShortName();

        //now get this index's name
        //We actually do want the +1 here - for example
        //for(int b=0; b<4; ++b){
        //   for(int a=b+1; a<4; ++a){...
        //              ^we want this if b is offset
        //     TDm00[b+1][a]+=....
        //                ^otherwise this will point to the wrong place
        typedef typename this_TStruc::FirstSlotTIndex_t this_TIndex_t;
        TNameHelper<this_TIndex_t> this_TNameHelper;
        std::string this_ind = this_TNameHelper.ShortName(); //"i", "j",etc.
        
        //the dimension
        std::string dstr = std::to_string(d);

        the_loops += "for(int " + this_ind + "=" + part_ind + "; " +
            this_ind + "<" + dstr + "; ++" + this_ind + ") {\n";
        return the_loops;
    }
};

//Case 4: There is no first index; i.e. we've reached the bottom. Stop.
template<>
struct CUDAForLoopHelper<TIndexStructure<TSymmetry<> > > {
    std::string ForLoops(std::vector<std::string>&, 
        std::vector<int>&, int&){
      return "";
    }
};

#endif

