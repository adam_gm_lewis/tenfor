#include "FactoryReporter.hpp"
//#include "Utils/ErrorHandling/MpiEnvironment.hpp"
//#include "Utils/LowLevelUtils/ConvertNumberToString.hpp"

#include <fstream>
#include <sstream>

namespace FactoryReporter {

  bool& ReportingEnabled() {
    static bool reportingEnabled = false;
    return reportingEnabled;
  }

  bool EnableReporting() {
    ReportingEnabled() = true;
    std::ofstream("petsc.input", std::ios::app);
    return true;
  }

  void RawReport(const std::string& str) {
    std::string filename = "FactoryReport.txt";
    //if(MpiEnvironment::IsInitialized() && MpiEnvironment::GlobalSize() > 1) {
      //filename
        //= "FactoryReport" + IntToString(MpiEnvironment::GlobalRank()) + ".txt";
    //}
    std::ofstream out(filename.c_str(), std::ios::app);
    out << str << "\n";
  }

  /// Report the creation of a class
  void ReportClass(void (*helper)()) {
    if(ReportingEnabled()) {
      std::ostringstream s;
      s << reinterpret_cast<void*>(helper);
      RawReport(s.str());
    }
  }

  void ReportLibrary(const std::string& name) {
    if(ReportingEnabled()) {
      RawReport("library:" + name);
    }
  }

}
