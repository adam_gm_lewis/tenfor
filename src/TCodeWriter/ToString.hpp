#ifndef TO_STRING_HPP
#define TO_STRING_HPP

#include <string>

//============================================================================
// Get the next char in rString, remove it from rString and put it in rChar.
// Ignore (i.e. remove from rString, but don't return) comments and newlines.
// Return true if a valid char is found, otherwise return false.
// NOTE:: rString IS MODIFIED.
// NOTE: This is slow because of its modification of the string.
bool GetNextChar(std::string& rString, char& rChar);

/// Removes leading and trailing whitespace from rString
std::string RemoveLeadingAndTrailingWhitespace(const std::string& rString);

/// Split rString into two pieces Head and Inside based on Left and Right.  
/// Return the piece between the first occurance of Left and the final
/// occurance of Right in Inside.
/// Return the piece before the first occurance of Left in Head.
/// Leading & trailing whitespace are removed from Head and Inside.
/// If neither Left nor Right is found, Inside="".
/// If only Left or only Right is found, an exception is thrown.
/// Checks that the substring after the final occurance of Right contains only
/// whitespace, newlines, or comments.  If not an exception is thrown.
/// Does not remove newlines or comments ( # ... \n ) from Head or Inside.
/// Examples:
/// \code
///   s = "bla(blurbs)"        // returns Head = "bla"  Inside = "blurbs"
///   s = "bla2"               // returns Head = "bla2" Inside = ""
///   s = "bla3(bl(bleh),yip)" // returns Head = "bla3" Inside = "bl(bleh),yip"
///   s = "(bleh)"             // returns Head = ""     Inside = "bleh"
///   s = "bleh(yip"           // terminates
///   s = "bla(bla)bla"        // terminates
/// \endcode
void SplitParentheses(const std::string& rString,
		      std::string& Head, 
		      std::string& Inside, 
		      const std::string Left="(", 
		      const std::string Right=")");

//=============================================================================
/// Outputs integers with exactly 'p' characters width, filling in with zeros
/// instead of white-space.  The field-width overflows if the number does not 
/// fit into this width.
/// Example:
/// \code
///   IntToStringPaddedZero( 12,    5);  //  >00012< 
///   IntToStringPaddedZero(-431,   5);  //  >-0431<
///   IntToStringPaddedZero(-41000, 5);  //  >-41000<
/// \endcode
std::string IntToStringPaddedZero(const long int val, const int p);
#endif
