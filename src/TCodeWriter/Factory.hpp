//=============================================================================
/// \file
/// Defines Factory.  The Factory provides a way to register
/// derived classes, and creating them later based on a string.
///
/// See Factory.tex for more information.
//=============================================================================
#ifndef Factory_hpp
#define Factory_hpp
//=============================================================================
#include "ToString.hpp"
#include "FactoryBackbone.hpp"

#include <utility>
//=============================================================================
/// Contains the Factory, a mechanism to create derived
/// classes based on the class-names.
/// See MiscUtils/Factory.tex for more information.
namespace Factory {


  /// Add Factory methods to base classes to construct derived classes.
  /// The template parameter is the type of the base class.
  /// The constructor of the derived class must take no arguments.
  template<class BASE>
  struct EnableCreation0 : public virtual MyClassID_Base {
    typedef BASE Base;
    typedef BASE* (*CallingSequence)();

    // calling-arguments are known *here*, so place Create() here.
    static BASE* CreateDerivedClass(const std::string& id) {
      CallingSequence FnPtr
        = reinterpret_cast<CallingSequence>(TheFactory<BASE>().Retrieve(id));
      return (*FnPtr)();
    }

    template<class Derived>
    static BASE* CreateHelper() {
      return new Derived();
    }
  };

  //--------------------------------------------------------------------------
  /// Like EnableCreation0, but takes one or more additional template params.
  /// The first parameter is A1, and the next are variadic templates Args.
  /// These extra template parameters are the types of the one or more
  /// additional arguments being passed to the constructor of the derived class.
  template<class BASE, class A1, class... Args>
  struct EnableCreation : public virtual MyClassID_Base {
    typedef BASE Base;
    typedef BASE* (*CallingSequence)(A1, Args...);

    // calling-arguments are known *here*, so place Create() here.
    static BASE* CreateDerivedClass(const std::string& id,
                                    A1 a1, Args... args) {
      CallingSequence FnPtr
        = reinterpret_cast<CallingSequence>(TheFactory<BASE>().Retrieve(id));
      return (*FnPtr)(std::forward<A1>(a1), std::forward<Args>(args)...);
    }
    static BASE* Create(const std::string& opts, Args... args) {
      std::string head, inside;
      SplitParentheses(opts, head, inside);
      return CreateDerivedClass(head, inside, std::forward<Args>(args)...);
    }
    template<class Derived>
    static BASE* CreateHelper(A1 a1, Args... args) {
      return new Derived(std::forward<A1>(a1), std::forward<Args>(args)...);
    }
  };

  //==========================================================================
  /// Add Factory methods to base classes to construct derived classes.
  /// The template parameter is the type of the base class.
  /// Calls a static Create() function of DERIVED instead of its constructor.
  /// The static Create() function of the derived class must take no arguments.
  template<class BASE>
  struct CallCreate0 : public virtual MyClassID_Base {
    typedef BASE Base;
    typedef BASE* (*CallingSequence)();

    // calling-arguments are known *here*, so place Create() here.
    static BASE* CreateDerivedClass(const std::string& id) {
      CallingSequence FnPtr
        = reinterpret_cast<CallingSequence>(TheFactory<BASE>().Retrieve(id));
      return (*FnPtr)();
    }
    static BASE* Create(const std::string& opts) {
      return CreateDerivedClass(opts);
    }
    template<class DERIVED>
    static BASE* CreateHelper() {
      return DERIVED::Create();
    }
  };

  //--------------------------------------------------------------------------
  /// Like CallCreate0, but takes one or more additional template params.
  /// The first parameter is A1, and the next are variadic templates Args.
  /// These extra template parameters are the types of the one or more
  /// additional arguments being passed to the static Create() function of
  /// the derived class.
  template<class BASE, class A1, class... Args>
  struct CallCreate : public virtual MyClassID_Base {
    typedef BASE Base;
    typedef BASE* (*CallingSequence)(A1, Args...);

    // calling-arguments are known *here*, so place Create() here.
    static BASE* CreateDerivedClass(const std::string& id,
                                    A1 a1, Args... args) {
      CallingSequence FnPtr
        = reinterpret_cast<CallingSequence>(TheFactory<BASE>().Retrieve(id));
      return (*FnPtr)(std::forward<A1>(a1), std::forward<Args>(args)...);
    }
    static BASE* Create(const std::string& opts, Args... args) {
      std::string head, inside;
      SplitParentheses(opts, head, inside);
      return CreateDerivedClass(head, inside, std::forward<Args>(args)...);
    }
    template<class DERIVED>
    static BASE* CreateHelper(A1 a1, Args... args) {
      return DERIVED::Create(std::forward<A1>(a1),
                             std::forward<Args>(args)...);
    }
  };

  //==========================================================================
  /// Template to register a derived class 'DERIVED' with the appropriate
  /// Factory - use by publicly inheriting 'DERIVED' from Register<DERIVED>.
  ///
  /// 'DERIVED' should be derived from an abstract base-class 'BASE',
  /// which in turn must be derived from EnableFactoryCreationN<BASE,
  /// A1, A2, ...>, where the types A1, ...  are the parameters passed
  /// into the constructor of 'DERIVED'.
  template<class DERIVED>
  class Register : public virtual MyClassID_Base {
  public:
    // this constructor enforces instantiation of the static bools
    Register(): mReg(mregistered_) {} //, mDep(mDeprecated_) { }
    std::string MyClassID() const { return DERIVED::ClassID(); }
  private:
    //static bool mDeprecated_;
    static bool mregistered_;
    const bool mReg; // necessary to enforce instantiation of mregistered_
    //const bool mDep; // necessary to enforce instantiation of mDeprecated_
  };

  // the initializer of mregistered_ calls Factory<BASE>::Register()
  // of the Factory, and thus registers DERIVED with the Factory.
  template<class DERIVED>
  bool Register<DERIVED>::mregistered_=
  ::Factory::TheFactory<typename DERIVED::Base>()
    .Register(DERIVED::ClassID(),
              DERIVED::Base::template CreateHelper<DERIVED>,
              DERIVED::Help()
              );

  // the initializer of mDeprecated_ calls Factory<BASE>::RegisterDeprecated()
  // and so will be registered as deprecated if it inherits from Deprecated.
  //template<class DERIVED>
  //bool Register<DERIVED>::mDeprecated_=
  //::Factory::TheFactory<typename DERIVED::Base>()
    //.template RegisterDeprecated<DERIVED>(DERIVED::ClassID());

} // namespace Factory

#endif

