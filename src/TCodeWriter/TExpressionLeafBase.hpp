#ifndef TExpressionLeafBase_hpp
#define TExpressionLeafBase_hpp
#include <string>
#include <map>
#include <vector>
#include "ToString.hpp"
/// Contains classes that are needed for automatic creation of
/// CUDA code for Tensor Expressions
namespace TCodeWriter {
  
  /// Abstracts writing of elements of source-code for TCodeWriter.
  /// This class represents a leaf of an expression, i.e. a certain
  /// variable used in an expression.
  ///
  /// There will be derived classes for specific types of leafes like
  /// a DataMesh or iBinaryOp<TIndexStructure_t, EmptyType, DataMesh> 
  class TExpressionLeafBase {
  public:
    void SetLocation(const int ctr, const std::string& prefix) {
      mCtr=IntToStringPaddedZero(ctr, 2);
      mPrefix=prefix;
    }
    
    /// code-segment for the variable name
    /// e.g.: "TDm2"
    virtual std::string VarName() const = 0;

    /// The argument to the kernel call
    //  e.g.: "TDm00.GetPointers()"
    virtual std::string PointersName() const = 0;

    /// code-segment for the variable declaration
    /// e.g.: "const double* TDm2[3]" 
    virtual std::string VarDeclaration(const std::string& prefix="") const = 0;
    
    // different string needed for CUDA code
    // "const Tensor_GPUPointers<Tensor<DataMesh> >& TDm02";
    virtual std::string CUDAVarDeclaration() const = 0;

    //for inside the kernel
    virtual std::string KernelVarDeclaration() const = 0;

    /// code-segment used to initialize this variable
    /// e.g. "{ prefix.TheTensor()(0).Data(), prefx.TheTensor()(1).Data(),...}"
    virtual std::string VarInitialization(std::string prefix="") const = 0;

    // Should be ".DeviceData()" in the CUDA code
    virtual std::string CUDAVarInitialization(std::string prefix="") const = 0;
    
    /// The variable as it appears in the expression, formatted for 
    /// the comment TODO add example
    virtual std::string LoopIndexedComment() const = 0;
    
    /// the variable as it appears in the expression, with the loop-indices
    /// replaced by symbols for the TIndices.
    //TODO explain FixedIndices
    virtual std::string LoopIndexedExpression(
      const std::string& prefix="",
      const std::map<std::string,int>& FixedIndices=std::map<std::string,int>()
    ) const = 0;

    /// need to add a ".p" in the CUDA code
    virtual std::string CUDALoopIndexedExpression(
      const std::map<std::string,int>& FixedIndices=std::map<std::string,int>()
    ) const = 0;
    
    /// outputs for loops over the tensor indices; e.g. T[i][j] would output
    //  for (int i=0; i<DIM; ++i) {
    //      for (int j=0; j<DIM; ++j) {
    virtual std::string ForLoopsOverTensorIndices() const = 0;
    
  protected:
    // count within the expression for this leaf
    std::string mCtr; 
    // prefix within the expression to reach this leaf
    std::string mPrefix; 
    mutable std::vector<std::string> mParallelInds;
    mutable std::vector<int> mParallelDims;
    mutable int mCnt;
    mutable bool mCUDAForLoopsWasCalled;
  };

}

#endif
    
