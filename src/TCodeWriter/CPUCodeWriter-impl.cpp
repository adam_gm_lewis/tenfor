#ifdef CODE_WRITER
#include "CPUCodeWriter-impl.hpp"
#include "Factory.hpp"
#include "FactoryHelperFunctions.hpp"
#include "LHSStrings.hpp"
#include "TabCounter.hpp"
#include <fstream>
#include <algorithm> //for std::remove
#include <list>
//global variables - file names
const std::string AutoExpressions_cpp = "AutoExpressions.cpp";
const std::string AutoExpressions_hpp = "AutoExpressions.hpp";
const std::string CPUPointers_cpp = "AutoExpressionCPUPointerBundlers.cpp";
const std::string CPUPointers_hpp = "AutoExpressionCPUPointerBundlers.hpp";
const std::string CUDAPointers_cpp = "AutoExpressionCUDAPointerBundlers.cpp";
const std::string CUDAPointers_hpp = "AutoExpressionCUDAPointerBundlers.hpp";
const std::string CPUFunctions_cpp = "AutoExpressionCPUFunctions.cpp";
const std::string CPUFunctions_hpp = "AutoExpressionCPUFunctions.hpp";

namespace TCodeWriter{
  using namespace ImplicitTensorLoops;

  //***************************************************************************
  //PREP FUNCTIONS
  //***************************************************************************
  void CPUCodeWriter::PrepAutoExpressions_cpp(){
    std::ofstream cpp;
    cpp.open(AutoExpressions_cpp);
    cpp << "#ifndef CODE_WRITER\n";
    cpp << "#include \"Tensor.hpp\"\n";
    cpp << "#include \"DataMesh.hpp\"\n";
    cpp << "#include \"TLoopApply.hpp\"\n";
    cpp << "#include \"" << AutoExpressions_hpp << "\"\n\n";
    cpp << "#ifdef ACCEL_CPU \n";
    cpp << "#include \"" << CPUPointers_hpp << "\"\n";
    cpp << "#elif defined ACCEL_CUDA \n";
    cpp << "#include \"" << CUDAPointers_hpp << "\"\n";
    cpp << "#endif \n\n";
    cpp << "namespace ImplicitTensorLoops { \n";
    cpp.close();
  }

  void CPUCodeWriter::PrepAutoExpressions_hpp(){
    std::ofstream hpp;
    hpp.open(AutoExpressions_hpp);
    hpp << "#ifndef AUTO_EXPRESSIONS_HPP\n";
    hpp << "#define AUTO_EXPRESSIONS_HPP\n\n";
    hpp << "#ifndef CODE_WRITER\n";
    hpp << "namespace ImplicitTensorLoops { \n";
    
    //Needs advance definition of 
    //iBinaryOp, NonConstiBinaryOp, SumOp
    hpp << "template<class LHS, class OP, class RHS>\n";
    hpp << "struct iBinaryOp;\n\n";
    hpp << "template<class LHS, class OP, class RHS>\n";
    hpp << "struct NonConstiBinaryOp;\n\n";
    hpp << "struct SumOp;\n";
    hpp.close();
  }
  
  void CPUCodeWriter::PrepAutoExpressionCPUPointerBundlers_cpp(){
    std::ofstream cpp;
    cpp.open(CPUPointers_cpp);
    cpp << "#ifndef CODE_WRITER\n";
    cpp << "#include \"UtilsForTesting.hpp\"\n";
    cpp << "#include \"Tensor.hpp\"\n";
    cpp << "#include \"" << CPUPointers_hpp << "\"\n";
    cpp << "#include \"" << CPUFunctions_hpp << "\"\n";
    cpp << "namespace ImplicitTensorLoops { \n";
    cpp.close();
  }

  void CPUCodeWriter::PrepAutoExpressionCPUPointerBundlers_hpp(){
    std::ofstream hpp;
    hpp.open(CPUPointers_hpp);
    hpp << "#ifndef AUTO_EXPRESSION_CPU_POINTER_BUNDLERS_HPP\n";
    hpp << "#define AUTO_EXPRESSION_CPU_POINTER_BUNDLERS_HPP\n\n";
    hpp << "#ifndef CODE_WRITER\n";
    
    hpp << "#include \"DataMesh.hpp\"\n";
    
    hpp << "namespace ImplicitTensorLoops { \n";
    //Needs advance definition of 
    //iBinaryOp, NonConstiBinaryOp, SumOp
    hpp << "template<class LHS, class OP, class RHS>\n";
    hpp << "struct iBinaryOp;\n\n";
    hpp << "template<class LHS, class OP, class RHS>\n";
    hpp << "struct NonConstiBinaryOp;\n\n";
    hpp << "struct SumOp;\n\n\n";
    
    hpp.close();
  }
  
  void CPUCodeWriter::PrepAutoExpressionCPUFunctions_cpp(){
    std::ofstream cpp;
    cpp.open(CPUFunctions_cpp);
    cpp << "#ifndef CODE_WRITER\n";
    cpp << "#include \"" << CPUFunctions_hpp << "\"\n";
    cpp << "#include <math.h>\n";
    cpp << "#include \"CPUOperators.hpp\"\n";
    cpp << "using namespace CPUOps;\n";
    cpp.close();
  }
  
  void CPUCodeWriter::PrepAutoExpressionCPUFunctions_hpp(){
    std::ofstream hpp;
    hpp.open(CPUFunctions_hpp);
    hpp << "#ifndef AUTO_EXPRESSION_CPU_FUNCTIONS_HPP\n";
    hpp << "#define AUTO_EXPRESSION_CPU_FUNCTIONS_HPP\n\n";
    hpp << "#ifndef CODE_WRITER\n";
    hpp.close();
  }
    
  //Open and initialize all the files.
  void CPUCodeWriter::Prep(){
    PrepAutoExpressions_cpp();
    PrepAutoExpressions_hpp();
    
    PrepAutoExpressionCPUPointerBundlers_cpp();
    PrepAutoExpressionCPUPointerBundlers_hpp();

    PrepAutoExpressionCPUFunctions_cpp();
    PrepAutoExpressionCPUFunctions_hpp();
  } 
 
  //***************************************************************************
  //FINALIZE FUNCTIONS
  //***************************************************************************
  void CPUCodeWriter::FinalizeAFile(const int number_of_endifs, 
      const std::string& fname, bool closing_brace=true){
    std::ofstream file;
    file.open(fname, std::ios::app);
    
    if (closing_brace) file << "}"; //namespace
    file << "\n";
    for (int i=0; i<number_of_endifs; ++i){
      file << "#endif\n";
    }
    file.close();
  }

  //Open and initialize all the files.
  void CPUCodeWriter::Finalize(){
    //need 1 #endif for #ifndef CODE_WRITER in all cases
    //1 more for #ifdef ACCEL_CPU for the ACCEL-only stuff
    //1 more for #include guards for hpp files
    FinalizeAFile(1, AutoExpressions_cpp);
    FinalizeAFile(2, AutoExpressions_hpp);
    
    FinalizeAFile(1, CPUPointers_cpp);
    FinalizeAFile(2, CPUPointers_hpp);
    
    FinalizeAFile(1, CPUFunctions_cpp, false);
    FinalizeAFile(2, CPUFunctions_hpp, false);
  }
  
  //***************************************************************************
  //BODY FUNCTIONS
  //***************************************************************************
  
  //Write out the comment above each expression
  //i.e.
    //************************************************************
    // EXPRESSION xxxx 
    //------------------------------------------------------------
    // DM = DM - DM
    //************************************************************
  std::string CPUCodeWriter::Comment(TExpressionTreeHouseBase* TreeHouse_ptr,
      const int& gnumber)
  {
    const LHS_Strings& lhs_strings=TreeHouse_ptr->lhs_strings();
    
    std::string stars(60,'*');
    std::string bars(60,'-');
    
    std::ostringstream out;
    out << "\n\n";
    
    //The expression number
    out << "//" << stars << "\n";
    out << "//" << " EXPRESSION " << IntToStringPaddedZero(gnumber,5) << ": \n";
    out << "//" << bars << "\n";
    
    //Comment sketching out what expression we are dealing with.
    //These are generated by Leaf (on the LHS) and Tree (on the RHS).
    out << "//" << lhs_strings.LoopIndexedComment() <<
        TreeHouse_ptr->AssignOpShortName() <<
        TreeHouse_ptr->Tree().PrintComment() << std::endl;
    out << "//" << stars << "\n";
    return out.str();
  }

  //***************************************************************************
  //AutoExpressions
  //***************************************************************************
  //The function call/definition - 
  // 'void TLoopApply_impl(NonConstiBinaryOp<CPlusPlusName> & lhs,
  //    AssignOpCPlusPlusName& op, iBinaryOp<RHSCPlusPlusName>& rhs)'
  std::string CPUCodeWriter::TLoopApplyString
      (TExpressionTreeHouseBase* TreeHouse_ptr)
  {
    const LHS_Strings& lhs_strings = TreeHouse_ptr->lhs_strings();
    std::ostringstream out;
    out << "void TLoopApply_impl(" 
        << lhs_strings.CPlusPlusName() << "& lhs," 
        << "const " << TreeHouse_ptr->AssignOpCPlusPlusName() << "& op, "
        << "const " << TreeHouse_ptr->RHSCPlusPlusName() << "& rhs)";
    return out.str();
  }

  void CPUCodeWriter::AddEntryToAutoExpressions_cpp
    (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber)
  {
    std::ofstream cpp;
    cpp.open(AutoExpressions_cpp, std::ios::app);
    cpp << Comment(TreeHouse_ptr,gnumber);
    cpp << TLoopApplyString(TreeHouse_ptr) << "{\n\n";
    cpp << "\n\n//Call the function\n";
    cpp << "#ifdef ACCEL_CUDA\n";
    cpp << "CUDAPointerBundler_g_"<<IntToStringPaddedZero(gnumber,5)
        <<"(lhs,op,rhs);\n";
    cpp << "#elif defined ACCEL_CPU\n";
    cpp << "CPUPointerBundler_g_"<<IntToStringPaddedZero(gnumber,5)
        <<"(lhs,op,rhs);\n";
    cpp << "#else\n";
    cpp << "REQUIRE(false, \"Must specify an ACCEL flag when using ACCEL code.\");\n";
    cpp << "#endif\n";
    cpp << "}";
    cpp.close();
  }

  void CPUCodeWriter::AddEntryToAutoExpressions_hpp
    (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber)
  {
    std::ofstream hpp;
    hpp.open(AutoExpressions_hpp, std::ios::app);
    hpp << Comment(TreeHouse_ptr,gnumber);
    hpp << TLoopApplyString(TreeHouse_ptr) << ";";
    hpp.close();
  }

  //***************************************************************************
  //AutoExpressionCPUPointerBundlers
  //***************************************************************************
  
  //The argument list to the wrapper (with Tensor_GPUPointers) 
  std::string CPUCodeWriter::CPUFunctionArguments
        (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber) 
  {
    const LHS_Strings& lhs_strings = TreeHouse_ptr->lhs_strings();
    std::ostringstream out;
    out << "g_" << IntToStringPaddedZero(gnumber,5) << "(const int sz, "
        << lhs_strings.VarDeclaration();
    for(auto leaf: TreeHouse_ptr->Tree()) {
      out << ", " << leaf->VarDeclaration();
    }
    out << ")";
    return out.str();
  }
  
  //The function call/definition 
  std::string CPUCodeWriter::PointerBundlerString
      (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber)
  {
    const LHS_Strings& lhs_strings = TreeHouse_ptr->lhs_strings();
    std::ostringstream out;
    out << "void CPUPointerBundler_g_" 
        << IntToStringPaddedZero(gnumber,5) << "(" 
        << lhs_strings.CPlusPlusName() << "& lhs," 
        << "const " << TreeHouse_ptr->AssignOpCPlusPlusName() << "&, "
        << "const " << TreeHouse_ptr->RHSCPlusPlusName() << "& rhs)";
    return out.str();
  }
  
  void CPUCodeWriter::AddEntryToAutoExpressionCPUPointerBundlers_cpp
    (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber)
  {
    const LHS_Strings& lhs_strings = TreeHouse_ptr->lhs_strings();
    std::ofstream cpp;
    cpp.open(CPUPointers_cpp, std::ios::app);
    cpp << Comment(TreeHouse_ptr,gnumber);
    
    
    cpp << PointerBundlerString(TreeHouse_ptr,gnumber) << "{\n";
    
    cpp     <<  "//--------------------------------\n"
	    <<  "// variable declarations\n"
	    <<  "//--------------------------------\n";

    //prints e.g. double* TDm00[3][3] =
    //{{
    //  lhs.GetComponent({0,0}).Data(),
    //  lhs.GetComponent({1,0}).Data(),
    //  lhs.GetComponent({2,0}).Data(),
    //},
    //{
    //  lhs.GetComponent({0,1}).Data(),...
    //...
    //}};
    cpp     << "\n"
	    << lhs_strings.VarDeclaration() << " =\n"
	    << lhs_strings.VarInitialization() << ";\n\n";

    //prints e.g. 
    //const double* TDm01[3] =
    //{
    //  rhs.lhs.GetComponent({0}).Data(),
    //  rhs.lhs.GetComponent({1}).Data(),
    //  rhs.lhs.GetComponent({2)}.Data(),
    //};
    //
    //const double* TDm02[3] = ...
    for(auto leaf: TreeHouse_ptr->Tree()) {
      cpp   << "\n"
	    << leaf->VarDeclaration() << " =\n"
	    << leaf->VarInitialization() << ";\n\n";
    }
    
    cpp << "const int sz=" << lhs_strings.Begin() << ".Size();\n\n"; 
    
    cpp << "CPUFunction_g_"<<IntToStringPaddedZero(gnumber,5)
        << "(sz, " << lhs_strings.VarName();
    for(auto  leaf:TreeHouse_ptr->Tree())
      cpp << ", " << leaf->VarName(); //", TDm1, TDm2, ...);"
    cpp << ");\n\n";
    
    cpp << "}";
    cpp.close();
  }
  
  void CPUCodeWriter::AddEntryToAutoExpressionCPUPointerBundlers_hpp
    (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber)
  {
    std::ofstream hpp;
    hpp.open(CPUPointers_hpp, std::ios::app);
    hpp << Comment(TreeHouse_ptr,gnumber);
    hpp << PointerBundlerString(TreeHouse_ptr,gnumber) << ";";
    hpp.close();
  }
  //***************************************************************************
  //AutoExpressionCPUFunctions
  //***************************************************************************
  //The actual function 
  std::string CPUCodeWriter::CPUFunction
        (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber) 
  {
    const LHS_Strings& lhs_strings = TreeHouse_ptr->lhs_strings();

    std::ostringstream out;
    out << "void CPUFunction_" 
        << CPUFunctionArguments(TreeHouse_ptr, gnumber) << "{ \n";
    out << lhs_strings.ForLoopsOverTensorIndices(); 
    out << "for (int idx=0; idx<sz; ++idx){\n"; 
    //TDm00[i][j][idx]=
    std::string lhs_op = TreeHouse_ptr->AssignOpShortName();
    out << lhs_strings.LoopIndexedExpression() << lhs_op;
    
    //The RHS
    out << TreeHouse_ptr->Tree().PrintExpression() << ";\n";

    //The closing braces
    out << "}\n"; //close idx loop
    out << lhs_strings.ForLoopClosingBraces();
    out << "}"; //close kernel
    return out.str();
  }

    
  void CPUCodeWriter::AddEntryToAutoExpressionCPUFunctions_cpp
    (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber)
  {
    std::ofstream cpp;
    cpp.open(CPUFunctions_cpp, std::ios::app);
    cpp << Comment(TreeHouse_ptr,gnumber);
    cpp << CPUFunction(TreeHouse_ptr, gnumber) << "\n\n";
    cpp.close();
  }
  
  void CPUCodeWriter::AddEntryToAutoExpressionCPUFunctions_hpp
    (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber)
  {
    std::ofstream hpp;
    hpp.open(CPUFunctions_hpp, std::ios::app);
    hpp << Comment(TreeHouse_ptr,gnumber);
    hpp << "void CPUFunction_" 
        << CPUFunctionArguments(TreeHouse_ptr, gnumber) << ";";
    hpp.close();
  }
  
  //Wrapper 
  void CPUCodeWriter::AddEntry(TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber){
    AddEntryToAutoExpressions_cpp(TreeHouse_ptr, gnumber);
    AddEntryToAutoExpressions_hpp(TreeHouse_ptr, gnumber);
    
    AddEntryToAutoExpressionCPUPointerBundlers_cpp(TreeHouse_ptr, gnumber);
    AddEntryToAutoExpressionCPUPointerBundlers_hpp(TreeHouse_ptr, gnumber);

    AddEntryToAutoExpressionCPUFunctions_cpp(TreeHouse_ptr, gnumber);
    AddEntryToAutoExpressionCPUFunctions_hpp(TreeHouse_ptr, gnumber);
  } 
  
  
  //***************************************************************************
  //MAIN LOOP
  //***************************************************************************
  void CPUCodeWriter::Write(){
    Prep();
  
    int gnumber=0;
    const std::list<std::string> Expressions
                =Factory::RegisteredClassIDs<TExpressionTreeHouseBase>();
    
    std::cout << "CPUCodeWriter_impl..." << std::endl;
    
    
    //loop over all the TExpressionTreeHouseBases in the Factory.
    //Each one represents one expression template to be generated.
    //The Factory allows us to create the derived class 
    //"ConcreteTExpressionTreeHouse" from the ExpressionTreeHouseBases,
    //which in turn tell us the equals operator and RHS ExpressionTree
    //we need. This is enough information to generate the expression.
    for(auto ExpTag:Expressions)
    {
      std::cout << "Expression number " << gnumber << std::endl;
      std::cout << ExpTag << std::endl;
      ++gnumber;
      TExpressionTreeHouseBase* TreeHouse_ptr = 
        TExpressionTreeHouseBase::CreateDerivedClass(ExpTag);
      
      //copy the (static) lhs_strings into a non-static instance
      AddEntry(TreeHouse_ptr, gnumber); 
      delete TreeHouse_ptr; 
    }
    
    //Loop();
    Finalize();
  }
}
#endif
