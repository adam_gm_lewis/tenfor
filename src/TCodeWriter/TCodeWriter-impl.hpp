#ifndef TCodeWriter_impl_hpp
#define TCodeWriter_impl_hpp
#ifdef CODE_WRITER
#include <string>
namespace TCodeWriter{
  void CreateAndPrepCodeWriterFiles(std::string& fname);
  void FinalizeCodeWriterFiles(std::string& fname);
  //void WriteEntry(TExpressionTreeHouseBase* TreeHouse_ptr,
  //    const int& gnumber, std::string& fname);
  void LoopOverExpressionsAndWriteFiles(std::string& fname);
}
#endif
#endif
