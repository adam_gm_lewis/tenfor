#ifndef tab_counter_hpp
#define tab_counter_hpp
#include <string>
//Keep track of the indentation level in CodeWriter
//to make formatting the output easier.
class TabCounter {
  public:
    TabCounter(int SpacesPerTab=4); //construct by setting a number of spaces
                                  //to add with each ++
    std::string indent(); //retrieve string of appropriate number of spaces
    size_t NTabs();          //return mNTabs
    TabCounter& operator++(); //add a new tab
    TabCounter& operator--(); //remove a tab
  private:
    const size_t mSpacesPerTab;
    size_t mNTabs, mNSpaces; //how many tabs to output 
};

#endif
