#ifdef CODE_WRITER
#include "CudaCodeWriter-impl.hpp"
#include "Factory.hpp"
#include "FactoryHelperFunctions.hpp"
#include "LHSStrings.hpp"
#include "TabCounter.hpp"
#include <algorithm> //for std::remove
#include <fstream>
#include <list>
//global variables - file names
const std::string AutoExpressions_cpp = "AutoExpressions.cpp";
const std::string AutoExpressions_hpp = "AutoExpressions.hpp";
const std::string CUDAPointers_cpp = "AutoExpressionCUDAPointerBundlers.cpp";
const std::string CUDAPointers_hpp = "AutoExpressionCUDAPointerBundlers.hpp";
const std::string CPUPointers_cpp = "AutoExpressionCPUPointerBundlers.cpp";
const std::string CPUPointers_hpp = "AutoExpressionCPUPointerBundlers.hpp";
const std::string CUDAFunctions_cu = "AutoExpressionCUDAFunctions.cu";
const std::string CUDAFunctions_hpp = "AutoExpressionCUDAFunctions.hpp";

namespace TCodeWriter{
  using namespace ImplicitTensorLoops;

  //***************************************************************************
  //PREP FUNCTIONS
  //***************************************************************************
  void CudaCodeWriter::PrepAutoExpressions_cpp(){
    std::ofstream cpp;
    cpp.open(AutoExpressions_cpp);
    cpp << "#ifndef CODE_WRITER\n";
    cpp << "#include \"Tensor.hpp\"\n";
    cpp << "#include \"DataMesh.hpp\"\n";
    cpp << "#include \"TLoopApply.hpp\"\n";
    cpp << "#include \"" << AutoExpressions_hpp << "\"\n\n";
    cpp << "#ifdef ACCEL_CPU \n";
    cpp << "#include \"" << CPUPointers_hpp << "\"\n";
    cpp << "#elif defined ACCEL_CUDA \n";
    cpp << "#include \"" << CUDAPointers_hpp << "\"\n";
    cpp << "#endif \n\n";
    cpp << "namespace ImplicitTensorLoops { \n";
    cpp.close();
  }

  void CudaCodeWriter::PrepAutoExpressions_hpp(){
    std::ofstream hpp;
    hpp.open(AutoExpressions_hpp);
    hpp << "#ifndef AUTO_EXPRESSIONS_HPP\n";
    hpp << "#define AUTO_EXPRESSIONS_HPP\n\n";
    hpp << "#ifndef CODE_WRITER\n";
    
    //very important that Tensor and DataMesh not go after the namespace
    hpp << "template<class X> class Tensor;\n";
    hpp << "class DataMesh;\n"; 
    hpp << "namespace ImplicitTensorLoops { \n";
    
    //Needs advance definition of 
    //iBinaryOp, NonConstiBinaryOp, SumOp
    hpp << "template<class LHS, class OP, class RHS>\n";
    hpp << "struct iBinaryOp;\n\n";
    hpp << "template<class LHS, class OP, class RHS>\n";
    hpp << "struct NonConstiBinaryOp;\n\n";
    hpp << "struct SumOp;\n";
    hpp.close();
  }
  
  void CudaCodeWriter::PrepAutoExpressionCUDAPointerBundlers_cpp(){
    std::ofstream cpp;
    cpp.open(CUDAPointers_cpp);
    cpp << "#ifndef CODE_WRITER\n";
    cpp << "#include \"UtilsForTesting.hpp\"\n";
    cpp << "#include \"DataMesh.hpp\"\n";
    cpp << "#include \"Tensor.hpp\"\n";
    cpp << "#include \"GPU/cuda_wrapper.hpp\"\n";
    cpp << "#include \"" << CUDAPointers_hpp << "\"\n";
    cpp << "#include \"" << CUDAFunctions_hpp << "\"\n";
    cpp << "namespace ImplicitTensorLoops { \n";
    cpp.close();
  }

  void CudaCodeWriter::PrepAutoExpressionCUDAPointerBundlers_hpp(){
    std::ofstream hpp;
    hpp.open(CUDAPointers_hpp);
    hpp << "#ifndef AUTO_EXPRESSION_CUDA_POINTER_BUNDLERS_HPP\n";
    hpp << "#define AUTO_EXPRESSION_CUDA_POINTER_BUNDLERS_HPP\n\n";
    hpp << "#ifndef CODE_WRITER\n";
    hpp << "template<class X> class Tensor;\n";
    hpp << "class DataMesh;\n"; 
    hpp << "namespace ImplicitTensorLoops { \n";
    //Needs advance definition of 
    //iBinaryOp, NonConstiBinaryOp, SumOp
    hpp << "template<class LHS, class OP, class RHS>\n";
    hpp << "struct iBinaryOp;\n\n";
    hpp << "template<class LHS, class OP, class RHS>\n";
    hpp << "struct NonConstiBinaryOp;\n\n";
    hpp << "struct SumOp;\n\n\n";
    
    hpp.close();
  }
  
  void CudaCodeWriter::PrepAutoExpressionCUDAFunctions_cu(){
    std::ofstream cpp;
    cpp.open(CUDAFunctions_cu);
    cpp << "#ifndef CODE_WRITER\n";
    cpp << "#include \"CUDA_binary_ops.cu\"\n";
    cpp << "#ifdef ACCEL_DEBUG\n";
    cpp << "#include \"Require.hpp\"\n";
    cpp << "#include \"GPU/cuda_wrapper.hpp\"\n";
    cpp << "#endif\n";
    cpp << "#include \"" << CUDAFunctions_hpp << "\"\n";
    cpp.close();
  }
  
  void CudaCodeWriter::PrepAutoExpressionCUDAFunctions_hpp(){
    std::ofstream hpp;
    hpp.open(CUDAFunctions_hpp);
    hpp << "#ifndef AUTO_EXPRESSION_CUDA_FUNCTIONS_HPP\n";
    hpp << "#define AUTO_EXPRESSION_CUDA_FUNCTIONS_HPP\n\n";
    hpp << "#ifndef CODE_WRITER\n";
    hpp.close();
  }
    
  //Open and initialize all the files.
  void CudaCodeWriter::Prep(){
    PrepAutoExpressions_cpp();
    PrepAutoExpressions_hpp();
    
    PrepAutoExpressionCUDAPointerBundlers_cpp();
    PrepAutoExpressionCUDAPointerBundlers_hpp();

    PrepAutoExpressionCUDAFunctions_cu();
    PrepAutoExpressionCUDAFunctions_hpp();
  } 
 
  //***************************************************************************
  //FINALIZE FUNCTIONS
  //***************************************************************************
  void CudaCodeWriter::FinalizeAFile(const int number_of_endifs, 
      const std::string& fname, bool closing_brace=true){
    std::ofstream file;
    file.open(fname, std::ios::app);
    
    if (closing_brace) file << "}"; //namespace
    file << "\n";
    for (int i=0; i<number_of_endifs; ++i){
      file << "#endif\n";
    }
    file.close();
  }

  //Open and initialize all the files.
  void CudaCodeWriter::Finalize(){
    //need 1 #endif for #ifndef CODE_WRITER in all cases
    //1 more for #ifdef ACCEL_CUDA for the CUDA-only stuff
    //1 more for #include guards for hpp files
    FinalizeAFile(1, AutoExpressions_cpp);
    FinalizeAFile(2, AutoExpressions_hpp);
    
    FinalizeAFile(1, CUDAPointers_cpp);
    FinalizeAFile(2, CUDAPointers_hpp);
    
    FinalizeAFile(1, CUDAFunctions_cu, false);
    FinalizeAFile(2, CUDAFunctions_hpp, false);
  }
  
  //***************************************************************************
  //BODY FUNCTIONS
  //***************************************************************************
  
  //Write out the comment above each expression
  //i.e.
    //************************************************************
    // EXPRESSION xxxx 
    //------------------------------------------------------------
    // DM = DM - DM
    //************************************************************
  std::string CudaCodeWriter::Comment(TExpressionTreeHouseBase* TreeHouse_ptr,
      const int& gnumber)
  {
    const LHS_Strings& lhs_strings=TreeHouse_ptr->lhs_strings();
    
    std::string stars(60,'*');
    std::string bars(60,'-');
    
    std::ostringstream out;
    out << "\n\n";
    
    //The expression number
    out << "//" << stars << "\n";
    out << "//" << " EXPRESSION " << IntToStringPaddedZero(gnumber,5) << ": \n";
    out << "//" << bars << "\n";
    
    //Comment sketching out what expression we are dealing with.
    //These are generated by Leaf (on the LHS) and Tree (on the RHS).
    out << "//" << lhs_strings.LoopIndexedComment() <<
        TreeHouse_ptr->AssignOpShortName() <<
        TreeHouse_ptr->Tree().PrintComment() << std::endl;
    out << "//" << stars << "\n";
    return out.str();
  }

  //***************************************************************************
  //AutoExpressions
  //***************************************************************************
  //The function call/definition - 
  // 'void TLoopApply_impl(NonConstiBinaryOp<CPlusPlusName> & lhs,
  //    AssignOpCPlusPlusName& op, iBinaryOp<RHSCPlusPlusName>& rhs)'
  std::string CudaCodeWriter::TLoopApplyString
      (TExpressionTreeHouseBase* TreeHouse_ptr)
  {
    const LHS_Strings& lhs_strings = TreeHouse_ptr->lhs_strings();
    std::ostringstream out;
    out << "void TLoopApply_impl(" 
        << lhs_strings.CPlusPlusName() << "& lhs," 
        << "const " << TreeHouse_ptr->AssignOpCPlusPlusName() << "& op, "
        << "const " << TreeHouse_ptr->RHSCPlusPlusName() << "& rhs)";
    return out.str();
  }

  void CudaCodeWriter::AddEntryToAutoExpressions_cpp
    (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber)
  {
    std::ofstream cpp;
    cpp.open(AutoExpressions_cpp, std::ios::app);
    cpp << Comment(TreeHouse_ptr,gnumber);
    cpp << TLoopApplyString(TreeHouse_ptr) << "{\n\n";
    cpp << "\n\n//Call the function\n";
    cpp << "#ifdef ACCEL_CUDA\n";
    cpp << "CUDAPointerBundler_g_"<<IntToStringPaddedZero(gnumber,5)
        <<"(lhs,op,rhs);\n";
    cpp << "#else\n";
    cpp << "AccelCPUExpression_g_"<<IntToStringPaddedZero(gnumber,5)
        <<"(lhs,op,rhs);\n";
    cpp << "#endif\n";
    cpp << "}";
    cpp.close();
  }

  void CudaCodeWriter::AddEntryToAutoExpressions_hpp
    (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber)
  {
    std::ofstream hpp;
    hpp.open(AutoExpressions_hpp, std::ios::app);
    hpp << Comment(TreeHouse_ptr,gnumber);
    hpp << TLoopApplyString(TreeHouse_ptr) << ";";
    hpp.close();
  }

  //***************************************************************************
  //AutoExpressionCUDAPointerBundlers
  //***************************************************************************
  //The argument list to the kernel 
  //i.e. g_0000(const int sz, double* DM00, const double* DM01...);
  std::string CudaCodeWriter::CUDAKernelArguments
        (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber) 
  {
    const LHS_Strings& lhs_strings = TreeHouse_ptr->lhs_strings();
    std::ostringstream out;
    out << "g_" << IntToStringPaddedZero(gnumber,5) << "(const int sz, "
        << lhs_strings.KernelVarDeclaration();
    for(auto leaf: TreeHouse_ptr->Tree()) {
      out << ", " << leaf->KernelVarDeclaration();
    }
    out << ")";
    return out.str();
  }
  
  //The argument list to the wrapper (with Tensor_GPUPointers) 
  std::string CudaCodeWriter::CUDAFunctionArguments
        (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber) 
  {
    const LHS_Strings& lhs_strings = TreeHouse_ptr->lhs_strings();
    std::ostringstream out;
    out << "g_" << IntToStringPaddedZero(gnumber,5) << "(const int sz, "
        << lhs_strings.CUDAVarDeclaration();
    for(auto leaf: TreeHouse_ptr->Tree()) {
      out << ", " << leaf->CUDAVarDeclaration();
    }
    out << ")";
    return out.str();
  }
  
  //The function call/definition 
  std::string CudaCodeWriter::PointerBundlerString
      (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber)
  {
    const LHS_Strings& lhs_strings = TreeHouse_ptr->lhs_strings();
    std::ostringstream out;
    out << "void CUDAPointerBundler_g_" 
        << IntToStringPaddedZero(gnumber,5) << "(" 
        << lhs_strings.CPlusPlusName() << "& lhs," 
        << "const " << TreeHouse_ptr->AssignOpCPlusPlusName() << "&, "
        << "const " << TreeHouse_ptr->RHSCPlusPlusName() << "& rhs)";
    return out.str();
  }
  
  void CudaCodeWriter::AddEntryToAutoExpressionCUDAPointerBundlers_cpp
    (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber)
  {
    const LHS_Strings& lhs_strings = TreeHouse_ptr->lhs_strings();
    std::ofstream cpp;
    cpp.open(CUDAPointers_cpp, std::ios::app);
    cpp << Comment(TreeHouse_ptr,gnumber);
    
    
    cpp << PointerBundlerString(TreeHouse_ptr,gnumber) << "{\n";
    
    cpp     <<  "//--------------------------------\n"
	    <<  "// variable declarations\n"
	    <<  "//--------------------------------\n";

    //prints e.g. double* TDm00[3][3] =
    //{{
    //  lhs.GetComponent({0,0}).Data(),
    //  lhs.GetComponent({1,0}).Data(),
    //  lhs.GetComponent({2,0}).Data(),
    //},
    //{
    //  lhs.GetComponent({0,1}).Data(),...
    //...
    //}};
    cpp     << "\n"
	    << lhs_strings.CUDAVarDeclaration() << " =\n"
	    << lhs_strings.CUDAVarInitialization() << ";\n\n";

    //prints e.g. 
    //const double* TDm01[3] =
    //{
    //  rhs.lhs.GetComponent({0}).Data(),
    //  rhs.lhs.GetComponent({1}).Data(),
    //  rhs.lhs.GetComponent({2)}.Data(),
    //};
    //
    //const double* TDm02[3] = ...
    for(auto leaf: TreeHouse_ptr->Tree()) {
      cpp   << "\n"
	    << leaf->CUDAVarDeclaration() << " =\n"
	    << leaf->CUDAVarInitialization() << ";\n\n";
    }
    
    cpp << "const int sz=" << lhs_strings.Begin() << ".Size();\n\n"; 
    
    cpp << "CUDAWrapper_g_"<<IntToStringPaddedZero(gnumber,5)
        << "(sz, " << lhs_strings.VarName();
    for(auto  leaf:TreeHouse_ptr->Tree())
      cpp << ", " << leaf->VarName(); //", TDm1, TDm2, ...);"
    cpp << ");\n\n";
    
    cpp << "}";
    cpp.close();
  }
  
  void CudaCodeWriter::AddEntryToAutoExpressionCUDAPointerBundlers_hpp
    (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber)
  {
    std::ofstream hpp;
    hpp.open(CUDAPointers_hpp, std::ios::app);
    hpp << Comment(TreeHouse_ptr,gnumber);
    hpp << PointerBundlerString(TreeHouse_ptr,gnumber) << ";";
    hpp.close();
  }
  //***************************************************************************
  //AutoExpressionCUDAFunctions
  //***************************************************************************
  //The actual kernel
  std::string CudaCodeWriter::CUDAKernel
        (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber) 
  {
    const LHS_Strings& lhs_strings = TreeHouse_ptr->lhs_strings();

    std::ostringstream out;
    out << "__global__\n void " 
        << CUDAKernelArguments(TreeHouse_ptr, gnumber) << "{ \n";
    out << lhs_strings.CUDAParallelIndexDeclaration(); 
    out << "int idx = blockIdx.x*blockDim.x + threadIdx.x;\n"; 
    out << "if (idx < sz){\n";
    //for(int j=0; j<3; ++j){, etc
    out << lhs_strings.CUDAForLoopsOverTensorIndices(); 
    


    //out << "for (int idx = blockIdx.x*blockDim.x + threadIdx.x;"
    //    << " idx<sz; idx+=blockDim.x*gridDim.x){\n";
    
    //TDm00[i][j][idx]=
    std::string lhs_op = TreeHouse_ptr->AssignOpShortName();
    out << lhs_strings.CUDALoopIndexedExpression() << lhs_op;
    
    //The RHS
    out << TreeHouse_ptr->Tree().PrintCUDAExpression() << ";\n";

    //The closing braces
    out << "}\n"; //close idx loop
    out << lhs_strings.ForLoopClosingBraces();
    out << "}"; //close kernel
    return out.str();
  }

  //The wrapper allowing c++ code to call the kernel without seeing it
  //The implementation (for the .cu file)
  std::string CudaCodeWriter::CUDAWrapper_cu(TExpressionTreeHouseBase* TreeHouse_ptr, 
      const int gnumber) 
  {
    std::ostringstream out;
    const LHS_Strings& lhs_strings = TreeHouse_ptr->lhs_strings();
    out << "void CUDAWrapper_" 
        << CUDAFunctionArguments(TreeHouse_ptr, gnumber) << "{ \n\n";
   
    //const int blocksize_x = ...
    out << lhs_strings.CUDABlocksizeNBlocks();
    
    out << "g_"<<IntToStringPaddedZero(gnumber,5)<<"<<<nblocks,blocksize>>>"
        << "(sz, " << lhs_strings.VarName();
    for(auto  leaf:TreeHouse_ptr->Tree())
      out << ", " << leaf->VarName(); //", TDm1, TDm2, ...);"
    out << ");\n\n";

    out << "#ifdef ACCEL_DEBUG\n"
        << "GPU::dSynchronize();\n"
        << "std::string errstring;\n"
        << "bool err = GPU::dLastError(errstring);\n"
        << "REQUIRE(err,errstring);\n"
        << "#endif\n";
    out << "}"; //close wrapper
    return out.str();
  }
    
  void CudaCodeWriter::AddEntryToAutoExpressionCUDAFunctions_cu
    (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber)
  {
    std::ofstream cu;
    cu.open(CUDAFunctions_cu, std::ios::app);
    cu << Comment(TreeHouse_ptr,gnumber);
    cu << CUDAKernel(TreeHouse_ptr, gnumber) << "\n\n";
    cu << CUDAWrapper_cu(TreeHouse_ptr, gnumber) << "\n\n";
    cu.close();
  }
  
  void CudaCodeWriter::AddEntryToAutoExpressionCUDAFunctions_hpp
    (TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber)
  {
    std::ofstream hpp;
    hpp.open(CUDAFunctions_hpp, std::ios::app);
    hpp << Comment(TreeHouse_ptr,gnumber);
    hpp << "void CUDAWrapper_" 
        << CUDAFunctionArguments(TreeHouse_ptr, gnumber) << ";";
    hpp.close();
  }
  
  //Wrapper 
  void CudaCodeWriter::AddEntry(TExpressionTreeHouseBase* TreeHouse_ptr, const int gnumber){
    AddEntryToAutoExpressions_cpp(TreeHouse_ptr, gnumber);
    AddEntryToAutoExpressions_hpp(TreeHouse_ptr, gnumber);
    
    AddEntryToAutoExpressionCUDAPointerBundlers_cpp(TreeHouse_ptr, gnumber);
    AddEntryToAutoExpressionCUDAPointerBundlers_hpp(TreeHouse_ptr, gnumber);

    AddEntryToAutoExpressionCUDAFunctions_cu(TreeHouse_ptr, gnumber);
    AddEntryToAutoExpressionCUDAFunctions_hpp(TreeHouse_ptr, gnumber);
  } 
  
  
  //***************************************************************************
  //MAIN LOOP
  //***************************************************************************
  void CudaCodeWriter::Write(){
    Prep();
  
    int gnumber=0;
    const std::list<std::string> Expressions
                =Factory::RegisteredClassIDs<TExpressionTreeHouseBase>();
    
    std::cout << "CudaCodeWriter_impl..." << std::endl;
    
    
    //loop over all the TExpressionTreeHouseBases in the Factory.
    //Each one represents one expression template to be generated.
    //The Factory allows us to create the derived class 
    //"ConcreteTExpressionTreeHouse" from the ExpressionTreeHouseBases,
    //which in turn tell us the equals operator and RHS ExpressionTree
    //we need. This is enough information to generate the expression.
    for(auto ExpTag:Expressions)
    {
      std::cout << "Expression number " << gnumber << std::endl;
      std::cout << ExpTag << std::endl;
      ++gnumber;
      TExpressionTreeHouseBase* TreeHouse_ptr = 
        TExpressionTreeHouseBase::CreateDerivedClass(ExpTag);
      
      //copy the (static) lhs_strings into a non-static instance
      AddEntry(TreeHouse_ptr, gnumber); 
      delete TreeHouse_ptr; 
    }
    
    //Loop();
    Finalize();
  }
}
#endif
