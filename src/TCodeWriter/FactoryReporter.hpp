/// \file
/// Routines for reporting the use of factory

#ifndef FactoryReporter_hpp
#define FactoryReporter_hpp

#include <string>

namespace FactoryReporter {

  bool EnableReporting();

  /// Report the creation of a class
  void ReportClass(void (*helper)());

  /// Report the use of a library
  void ReportLibrary(const std::string& name);

}

#endif
