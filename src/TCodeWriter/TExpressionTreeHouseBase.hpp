#ifndef TExpressionTreeHouse_Base_hpp
#define TExpressionTreeHouse_Base_hpp

#include "TExpressionTree.hpp"
#include "Factory.hpp"
#include "NameOf.hpp"
#include "LHSStrings.hpp"
namespace TCodeWriter {
  class TExpressionTreeHouseBase:
    public Factory::EnableCreation0<TExpressionTreeHouseBase> {
  public:
    //virtual ~TExpressionTreeHouseBase() {};
    //virtual const std::string& Info() const=0;
    virtual const std::string AssignOpShortName() const =0;
    virtual const std::string AssignOpCPlusPlusName() const =0;
    virtual const std::string RHSCPlusPlusName() const =0;
    virtual const TExpressionTree& Tree() const =0;
    virtual const LHS_Strings& lhs_strings() const =0;
  };
}

REGISTER_NAME(TCodeWriter::TExpressionTreeHouseBase);


#endif
