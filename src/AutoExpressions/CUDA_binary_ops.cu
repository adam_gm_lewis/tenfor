//Binary ops for the CUDA functions
__device__
inline double sqr(double A){
  return A*A;
}

__device__
inline double cube(double A){
  return A*A*A;
}

__device__
inline double StepFunction(double Q) {
  if(Q<=0.) return 0.; 
  return 1.; 
};

/*
__device__
inline double max(double x, double y){
  return fmax(x, y);
}

__device__
inline double min(double x, double y){
  return fmin(x, y);
}
*/
__device__ inline int
TInd3(const int n1){
  return n1;
};

__device__ inline int
TInd3(const int n1, const int n2){
  return n1+3*n2;
};

__device__ inline int
TInd3(const int n1, const int n2, const int n3){
  return n1+3*(n2+3*n3);
};

__device__ inline int
TInd3(const int n1, const int n2, 
    const int n3, const int n4){
  return n1+3*(n2+3*(n3+3*n4));
};

__device__ inline int
TInd3(const int n1, const int n2, const int n3, const int n4, const int n5){
  return n1+3*(n2+3*(n3+3*(n4+3*n5)));
};

__device__ inline int
TInd3(const int n1, const int n2, const int n3, const int n4, const int n5,
    const int n6){
  return n1+3*(n2+3*(n3+3*(n4+3*(n5+3*n6))));
};

__device__ inline int
TInd3(const int n1, const int n2, const int n3, const int n4, const int n5,
    const int n6, const int n7){
  return n1+3*(n2+3*(n3+3*(n4+3*(n5+3*(n6+3*n7)))));
};

__device__ inline int
TInd(const int n1){
  return n1;
};

__device__ inline int
TInd(const int n1, const int n2){
  return n1+4*n2;
};

__device__ inline int
TInd(const int n1, const int n2, const int n3){
  return n1+4*(n2+4*n3);
};

__device__ inline int
TInd(const int n1, const int n2, 
    const int n3, const int n4){
  return n1+4*(n2+4*(n3+4*n4));
};

__device__ inline int
TInd(const int n1, const int n2, const int n3, const int n4, const int n5){
  return n1+4*(n2+4*(n3+4*(n4+4*n5)));
};

__device__ inline int
TInd(const int n1, const int n2, const int n3, const int n4, const int n5,
    const int n6){
  return n1+4*(n2+4*(n3+4*(n4+4*(n5+4*n6))));
};

__device__ inline int
TInd(const int n1, const int n2, const int n3, const int n4, const int n5,
    const int n6, const int n7){
  return n1+4*(n2+4*(n3+4*(n4+4*(n5+4*(n6+4*n7)))));
};

