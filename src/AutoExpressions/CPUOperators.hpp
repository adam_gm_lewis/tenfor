#ifndef CPUOperators_hpp
#define CPUOperators_hpp
#include <algorithm>

namespace CPUOps{
  inline double sqr(double A){
    return A*A;
  }
  inline double cube(double A){
    return A*A*A;
  }
  inline double StepFunction(double A){
    if(A<=0.) return 0.;
    return 1;
  };
  inline double max(double A, double B){
    return std::max(A,B);
  }
  inline double min(double A, double B){
    return std::min(A,B);
  }
}

#endif
