#ifndef CUDA_Container_hpp
#define CUDA_Container_hpp

template<int Tsz>
struct CUDA_Container_Flat{
  const double* p[Tsz];
};

template<int Tsz>
struct CUDA_Container_FlatNonConst{
  double* p[Tsz];
};



//These structs wrap arrays of pointers-to-DataMeshes (i.e. Tensors) 
//so they can be passed by value into CUDA kernels. This avoids an 
//extra copy of the pointers.
//*************************************************************
//RANK 6
//*************************************************************
template<int I, int J, int K, int L, int M, int N>
struct CUDA_ContainerRank6 {
  double* p[I][J][K][L][M][N];
  double* operator()(int i, int j, int k, int l, int m, int n){
    return p[i][j][k][l][m][n];
  }
};

template<int I, int J, int K, int L, int M, int N>
struct cnstCUDA_ContainerRank6 {
  const double* p[I][J][K][L][M][N];
  const double* operator()(int i, int j, int k, int l, int m, int n){
    return p[i][j][k][l][m][n];
  }
};

//*************************************************************
//RANK 5
//*************************************************************
template<int I, int J, int K, int L, int M>
struct CUDA_ContainerRank5 {
  double* p[I][J][K][L][M];
  double* operator()(int i, int j, int k, int l, int m){
    return p[i][j][k][l][m];
  }
};

template<int I, int J, int K, int L, int M>
struct cnstCUDA_ContainerRank5 {
  const double* p[I][J][K][L][M];
  const double* operator()(int i, int j, int k, int l, int m){
    return p[i][j][k][l][m];
  }
};
//*************************************************************
//RANK 4
//*************************************************************
template<int I, int J, int K, int L>
struct CUDA_ContainerRank4 {
  double* p[I][J][K][L];
  double* operator()(int i, int j, int k, int l){
    return p[i][j][k][l];
  }
};

template<int I, int J, int K, int L>
struct cnstCUDA_ContainerRank4 {
  const double* p[I][J][K][L];
  const double* operator()(int i, int j, int k, int l){
    return p[i][j][k][l];
  }
};
//*************************************************************
//RANK 3
//*************************************************************

template<int I, int J, int K>
struct CUDA_ContainerRank3 {
  double* p[I][J][K];
  double* operator()(int i, int j, int k){
    return p[i][j][k];
  }
};

template<int I, int J, int K>
struct cnstCUDA_ContainerRank3 {
  const double* p[I][J][K];
  const double* operator()(int i, int j, int k){
    return p[i][j][k];
  }
};
//*************************************************************
//RANK 2
//*************************************************************

template<int I, int J>
struct CUDA_ContainerRank2 {
  double* p[I][J];
  double* operator()(int i, int j){
    return p[i][j];
  }
};

template<int I, int J>
struct cnstCUDA_ContainerRank2 {
  const double* p[I][J];
  const double* operator()(int i, int j){
    return p[i][j];
  }
};

//*************************************************************
//RANK 1
//*************************************************************
template<const int I>
struct CUDA_ContainerRank1 {
  double* p[I];
  double* operator()(int i){
    return p[i];
  }
};

template<const int I>
struct cnstCUDA_ContainerRank1 {
  const double* p[I];
  
  const double* operator()(int i) const{
    return p[i];
  }
  
};

#endif
