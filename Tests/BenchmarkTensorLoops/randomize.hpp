#ifndef RANDOMIZE_HPP
#define RANDOMIZE_HPP
class IPoint;
class TensorStructure;
template<class X> class Tensor;
class DataMesh;

Tensor<DataMesh> MakeRandomTensor(const TensorStructure& structure, 
                                  const IPoint& extents);
#endif
