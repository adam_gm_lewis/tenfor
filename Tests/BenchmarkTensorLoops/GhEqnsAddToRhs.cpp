#include "DataMesh.hpp"
#include "Tensor.hpp"

void GhEqnsAddToRhsTest
   (
    // output variables
    Tensor<DataMesh>& dtpsi,
    Tensor<DataMesh>& dtkappa,

    // evolved variables & their spatial derivatives
    const Tensor<DataMesh>& psi,
    const Tensor<Tensor<DataMesh> >& dpsi,
    const Tensor<DataMesh>& kappa,
    const Tensor<Tensor<DataMesh> >& dkappa,

    // gauge source functions, and their spacetime derivatives
    const Tensor<DataMesh>& H,        // gauge source function 
    const Tensor<Tensor<DataMesh> >& d4H, // SpacetimeDerivOfH

    // Constraint-damping parameters. Some of these will always have
    // their default values, so could be ommitted.
    const Tensor<DataMesh>& gamma0,
    const Tensor<DataMesh>& gamma1,
    const Tensor<DataMesh>& gamma2,

    const bool AddG1ShiftCorr,
    const Tensor<DataMesh>& G1ShiftCorr,

    // derived quantities.  Given psi and dpsi above, these could be
    // computed from scratch.
    const Tensor<DataMesh>& Lapse,
    const Tensor<DataMesh>& Shift,
    const Tensor<DataMesh>& Invg,
    const Tensor<DataMesh>& Invpsi,
    const Tensor<DataMesh>& Gamma1,   // 4-D Christoffel 1st kind
    const Tensor<DataMesh>& Gamma2,   // 4-D Christoffel2ndKind
    const Tensor<DataMesh>& TrGamma1, // TrChristoffel1stKind
    const Tensor<DataMesh>& T_i,      // UnitTimelikeNormalOneForm
    const Tensor<DataMesh>& T_I       // UnitTimelikeNormalVector
    ) {

  const Mesh& mesh      = Lapse();
  const int SpatialDim  = Shift.Dim();

  // SimpleProfiler prof("GeneralizedHarmonicEquations:AfterDataBoxGet");

  // -----------------------------------------------------------------
  // First thing we do is define some temp variables for optimization

  // Temporary variable for optimization
  // ***NOTE: kappa1up(0,mu,nu) is undefined.***
  TDm kappa1up(SpatialDim+1,"122",mesh);
  for(int mu=0;mu<=SpatialDim;++mu) {
    for(int nu=mu;nu<=SpatialDim;++nu) {
      for(int m=0;m<SpatialDim;++m) {
	kappa1up(m+1,mu,nu) = 0.0;
	for(int n=0;n<SpatialDim;++n) {
	  kappa1up(m+1,mu,nu) += Invg(m,n)*kappa(n+1,mu,nu);
	}
      }
    }
  }

  // Temporary variable for optimization
  TDm kappa3up(SpatialDim+1,"123",mesh);
  for(int mu=0;mu<=SpatialDim;++mu) {
    for(int nu=0;nu<=SpatialDim;++nu) {
      for(int alpha=0;alpha<=SpatialDim;++alpha) {
	kappa3up(mu,nu,alpha) = 0.0;
	for(int beta=0;beta<=SpatialDim;++beta) {
	  kappa3up(mu,nu,alpha) += Invpsi(alpha,beta)*kappa(mu,nu,beta);
	}
      }
    }
  }

  // Temporary variable for optimization
  TDm Gamma13up(SpatialDim+1,"123",mesh);
  for(int mu=0;mu<=SpatialDim;++mu) {
    for(int nu=0;nu<=SpatialDim;++nu) {
      for(int alpha=0;alpha<=SpatialDim;++alpha) {
	Gamma13up(mu,nu,alpha) = 0.0;
	for(int beta=0;beta<=SpatialDim;++beta) {
	  Gamma13up(mu,nu,alpha) += Invpsi(alpha,beta)*Gamma1(mu,nu,beta);
	}
      }
    }
  }

  // Temporary variable for optimization
  TDm PiT(SpatialDim+1,"1",mesh);
  for(int mu=0;mu<=SpatialDim;++mu) {
    PiT(mu)=0.0;
    for(int nu=0;nu<=SpatialDim;++nu) {
      PiT(mu) += T_I(nu)*kappa(0,nu,mu);
    }
  }

  // Temporary variable for optimization
  TDm PiTT(SpatialDim+1,"",mesh);
  PiTT() = 0.0;
  for(int mu=0;mu<=SpatialDim;++mu) {
    PiTT() += T_I(mu)*PiT(mu);
  }

  // Temporary variable for optimization
  // ***NOTE: PhiT(0,...) is undefined.***
  TDm PhiT(SpatialDim+1,"12",mesh,0);
  for(int n=0;n<SpatialDim;++n) {
    for(int nu=0;nu<=SpatialDim;++nu) {
      PhiT(n+1,nu)=0.0;
      for(int mu=0;mu<=SpatialDim;++mu) {
	PhiT(n+1,nu) += T_I(mu)*kappa(n+1,nu,mu);
      }
    }
  }
  // Temporary variable for optimization
  TDm PhiTT(SpatialDim,"1",mesh);
  for(int n=0;n<SpatialDim;++n) {
    PhiTT(n) = 0.0;
    for(int mu=0;mu<=SpatialDim;++mu) {
      PhiTT(n) += T_I(mu)*PhiT(n+1,mu);
    }
  }

  // Temporary variable for optimization
  TDm C3(SpatialDim+1,"122",mesh);
  for(int nu=0;nu<=SpatialDim;++nu) {
    for(int mu=nu;mu<=SpatialDim;++mu) {
      for(int n=0;n<SpatialDim;++n) {
	C3(n+1,mu,nu) = dpsi(mu,nu)(n) - kappa(n+1,mu,nu);
      }
    }
  }


  // Temporary variable for optimization
  TDm C = H;
  for(int nu=0;nu<=SpatialDim;++nu) C(nu) += TrGamma1(nu);

  // Temporary variable for optimization
  TDm TC(SpatialDim+1,"",mesh,0);
  for(int mu=0;mu<=SpatialDim;++mu) TC() += T_I(mu)*C(mu);

  bool DefaultGamma1 = true;
  for(int s=0;s<mesh.Size();++s) DefaultGamma1 &= (gamma1()[s] == -1);
  DataMesh gamma1p1(DataMesh::Empty);
  if(not DefaultGamma1) gamma1p1 = 1.0 + gamma1();
  DataMesh gamma1gamma2 = gamma1()*gamma2();

  TDm Shift_C3(SpatialDim+1,"aa",mesh,0);
  for(int mu=0;mu<=SpatialDim;++mu) {
    for(int nu=mu;nu<=SpatialDim;++nu) {
      for(int m=0;m<SpatialDim;++m) {
        Shift_C3(mu,nu) += Shift(m)*C3(m+1,mu,nu);
      }
    }
  }


  // ------------------------------------------------------------
  // Here are the actual equations


  // Equation for dtpsi
  for(int mu=0;mu<=SpatialDim;++mu) {
    for(int nu=mu;nu<=SpatialDim;++nu) {
      dtpsi(mu,nu) += -Lapse()*kappa(0,mu,nu);
      if(!DefaultGamma1) dtpsi(mu,nu) += gamma1p1*Shift_C3(mu,nu);
      for(int m=0;m<SpatialDim;++m) dtpsi(mu,nu) += Shift(m)*kappa(m+1,mu,nu);
    }
  }

  // Equation for dt Pi
  for(int mu=0;mu<=SpatialDim;++mu) {
    for(int nu=mu;nu<=SpatialDim;++nu) {

      //----------------------------------------------------
      // Almost all the terms here are proportional to the
      // lapse. As an optimization, let's first put in
      // all the terms proportional to lapse, without the factor
      // of lapse in front, then multiply by lapse, and then
      // add other terms.
      //----------------------------------------------------

      // Gradient of H terms, 1 of 2
      dtkappa(0,mu,nu)  += -d4H(mu)(nu) - d4H(nu)(mu)
	// PiTT term
	-                 0.5*PiTT()*kappa(0,mu,nu)
	+       gamma0()*(T_i(mu)*C(nu) + T_i(nu)*C(mu));
      dtkappa(0,mu,nu) -= gamma0()*psi(mu,nu)*TC();
      for(int delta=0;delta<=SpatialDim;++delta) {
	// Gradient of H terms, 2 of 2
	dtkappa(0,mu,nu) += 2.0*Gamma2(delta,mu,nu)*H(delta);

       
	// 2N psi^{ab} \psi^{cd} partial_c psi_ua partial_d psi_vb, 2 of 2
	dtkappa(0,mu,nu) -= 2.0*kappa(0,mu,delta)*kappa3up(0,nu,delta);
	for(int n=0;n<SpatialDim;++n) {
	  // 2N psi^{ab} \psi^{cd} partial_c psi_ua partial_d psi_vb, 1 of 2
	  dtkappa(0,mu,nu)+=2.0*kappa1up(n+1,mu,delta)*kappa3up(n+1,nu,delta);
	}
	for(int alpha=0;alpha<=SpatialDim;++alpha) {
	  // Gamma*Gamma term 1 of 1
	  dtkappa(0,mu,nu) -= 2.0*Gamma13up(mu,alpha,delta)
	    *                     Gamma13up(nu,delta,alpha);
	}
      }
      for(int m=0;m<SpatialDim;++m) {
	// PiT term
	dtkappa(0,mu,nu)   -= PiT(m+1)*kappa1up(m+1,mu,nu);
	for(int n=0;n<SpatialDim;++n) {


	  // Principal derivative term
	  dtkappa(0,mu,nu) -= Invg(m,n)*dkappa(n+1,mu,nu)(m);
	}
      }

      //----------------------------------------------
      // Multiply by lapse
      //----------------------------------------------
      dtkappa(0,mu,nu) *= Lapse();

      //----------------------------------------------
      // Now add terms not proportional to the lapse
      //----------------------------------------------
      dtkappa(0,mu,nu) += gamma1gamma2*Shift_C3(mu,nu);
      for(int m=0;m<SpatialDim;++m) {
	// DualFrame term
	dtkappa(0,mu,nu) += Shift(m)*dkappa(0,mu,nu)(m);
	
      }
    }
  }


  // dt Phi equation
  for(int i=0;i<SpatialDim;++i) {
    for(int mu=0;mu<=SpatialDim;++mu) {
      for(int nu=mu;nu<=SpatialDim;++nu) {
	
	// Multiply by lapse later for optimization, as is done above...
	
	// PhiTT term
	dtkappa(i+1,mu,nu) += 0.5*kappa(0,mu,nu)*PhiTT(i)
	  // Deriv term
	  -                    dkappa(0,mu,nu)(i)
	  // Constraint damping term
	  +                    gamma2()*C3(i+1,mu,nu);
	for(int n=0;n<SpatialDim;++n) {
	  // PhiT term
	  dtkappa(i+1,mu,nu) += PhiT(i+1,n+1)*kappa1up(n+1,mu,nu);
	}
	
	// Multiply by lapse
	dtkappa(i+1,mu,nu)   *= Lapse();
	
	// Shift term
	for(int m=0;m<SpatialDim;++m) {
	  dtkappa(i+1,mu,nu) += Shift(m)*dkappa(i+1,mu,nu)(m);
	}
      }
    }
  }

  if(AddG1ShiftCorr) {
    // Correction for Gamma1Shift, if necessary
    for(int mu=0;mu<=SpatialDim;++mu) {
      for(int nu=mu;nu<=SpatialDim;++nu) {
	for(int m=0;m<SpatialDim;++m) {

          const DataMesh tmp=gamma1()*G1ShiftCorr(m)*C3(m+1,mu,nu);
	  dtpsi(mu,nu) += tmp;
	  dtkappa(0,mu,nu) += gamma2()*tmp;
	}
      }
    }
  }

}
