void GhEqnsAddToRhsTest
(
 // output variables
 Tensor<DataMesh>& dtpsi,
 Tensor<DataMesh>& dtkappa,
 
 // evolved variables & their spatial derivatives
 const Tensor<DataMesh>& psi,
 const Tensor<Tensor<DataMesh> >& dpsi,
 const Tensor<DataMesh>& kappa,
 const Tensor<Tensor<DataMesh> >& dkappa,
 
 // gauge source functions, and their spacetime derivatives
 const Tensor<DataMesh>& H,        // gauge source function 
 const Tensor<Tensor<DataMesh> >& d4H, // SpacetimeDerivOfH
 
 // Constraint-damping parameters. Some of these will always have
 // their default values, so could be ommitted.
 const Tensor<DataMesh>& gamma0,
 const Tensor<DataMesh>& gamma1,
 const Tensor<DataMesh>& gamma2,
 const bool AddG1ShiftCorr,
 const Tensor<DataMesh>& G1ShiftCorr,
 
 // derived quantities.  Given psi and dpsi above, these could be
 // computed from scratch.
 const Tensor<DataMesh>& Lapse,
 const Tensor<DataMesh>& Shift,
 const Tensor<DataMesh>& Invg,
 const Tensor<DataMesh>& Invpsi,
 const Tensor<DataMesh>& Gamma1,   // 4-D Christoffel 1st kind
 const Tensor<DataMesh>& Gamma2,   // 4-D Christoffel2ndKind
 const Tensor<DataMesh>& TrGamma1, // TrChristoffel1stKind
 const Tensor<DataMesh>& T_i,      // UnitTimelikeNormalOneForm
 const Tensor<DataMesh>& T_I       // UnitTimelikeNormalVector
 );

void GhEqnsAddToRhsSimplified
(
 // output variables
 Tensor<DataMesh>& dtpsi,
 Tensor<DataMesh>& dtkappa,
 
 // evolved variables & their spatial derivatives
 const Tensor<DataMesh>& psi,
 const Tensor<Tensor<DataMesh> >& dpsi,
 const Tensor<DataMesh>& kappa,
 const Tensor<Tensor<DataMesh> >& dkappa,
 
 // gauge source functions, and their spacetime derivatives
 const Tensor<DataMesh>& H,        // gauge source function 
 const Tensor<Tensor<DataMesh> >& d4H, // SpacetimeDerivOfH
 
 // Constraint-damping parameters. Some of these will always have
 // their default values, so could be ommitted.
 const Tensor<DataMesh>& gamma0,
 const Tensor<DataMesh>& gamma1,
 const Tensor<DataMesh>& gamma2,
 const bool AddG1ShiftCorr,
 const Tensor<DataMesh>& G1ShiftCorr,
 
 // derived quantities.  Given psi and dpsi above, these could be
 // computed from scratch.
 const Tensor<DataMesh>& Lapse,
 const Tensor<DataMesh>& Shift,
 const Tensor<DataMesh>& Invg,
 const Tensor<DataMesh>& Invpsi,
 const Tensor<DataMesh>& Gamma1,   // 4-D Christoffel 1st kind
 const Tensor<DataMesh>& Gamma2,   // 4-D Christoffel2ndKind
 const Tensor<DataMesh>& TrGamma1, // TrChristoffel1stKind
 const Tensor<DataMesh>& T_i,      // UnitTimelikeNormalOneForm
 const Tensor<DataMesh>& T_I       // UnitTimelikeNormalVector
 );

void GhEqnsAddToRhsImplicitLoopsTest
(
 // output variables
 Tensor<DataMesh>& dtpsi,
 Tensor<DataMesh>& dtkappa,
 
 // evolved variables & their spatial derivatives
 const Tensor<DataMesh>& psi,
 const Tensor<Tensor<DataMesh> >& dpsi,
 const Tensor<DataMesh>& kappa,
 const Tensor<Tensor<DataMesh> >& dkappa,
 
 // gauge source functions, and their spacetime derivatives
 const Tensor<DataMesh>& H,        // gauge source function 
 const Tensor<Tensor<DataMesh> >& d4H, // SpacetimeDerivOfH
 
 // Constraint-damping parameters. Some of these will always have
 // their default values, so could be ommitted.
 const Tensor<DataMesh>& gamma0,
 const Tensor<DataMesh>& gamma1,
 const Tensor<DataMesh>& gamma2,
 const bool AddG1ShiftCorr,
 const Tensor<DataMesh>& G1ShiftCorr,
 
 // derived quantities.  Given psi and dpsi above, these could be
 // computed from scratch.
 const Tensor<DataMesh>& Lapse,
 const Tensor<DataMesh>& Shift,
 const Tensor<DataMesh>& Invg,
 const Tensor<DataMesh>& Invpsi,
 const Tensor<DataMesh>& Gamma1,   // 4-D Christoffel 1st kind
 const Tensor<DataMesh>& Gamma2,   // 4-D Christoffel2ndKind
 const Tensor<DataMesh>& TrGamma1, // TrChristoffel1stKind
 const Tensor<DataMesh>& T_i,      // UnitTimelikeNormalOneForm
 const Tensor<DataMesh>& T_I       // UnitTimelikeNormalVector
 );
