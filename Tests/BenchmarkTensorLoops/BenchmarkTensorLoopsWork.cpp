//#include "UtilsForTiming.hpp"
#include "Ran2Class.hpp"
#include "UtilsForTesting.hpp"
#include "Tensor.hpp"
#include "Mesh.hpp"
#include "DataMesh.hpp"

//#include "Utils/SizeStatistics/GpuCounterDatabase.hpp"

//#include "Utils/SizeStatistics/GpuCounter.hpp"
//#include "Utils/Tensor/TCodeWriter.hpp"
//#include "Utils/MiscUtils/GenericProfiler.hpp"
#include <iostream>
#include <chrono>
#include <iomanip>

//#include "DataMeshNorms.hpp"
#include <list>
#include <iostream>
#include <fstream>

//#include "GhEqnsAddToRhs.hpp"
#ifdef ACCEL_CUDA
#include "cuda_wrapper.hpp"
#include "BenchmarkKernels.hpp"
#include "CudaMemoryHandler.hpp"
#include "TComparisonHelpers.hpp"
#endif

//================================================================
// some helper functions
//================================================================

template<class func>
void operator<<(Tensor<DataMesh>& lhs, func& functor) {
  for(auto& a:lhs) {
    for(int i=0; i<a.Size(); ++i)
      a[i]=functor();
  }
};


template<class func>
void operator<<(Tensor<Tensor<DataMesh> >& lhs, func& functor) {
  for(auto& a:lhs) {
    a << functor;
  }
};


namespace {
  // some variables that will be used throughout

  Mesh aMesh(std::vector<int>({2,3,4}));
  Ran2 ran(212312);
  
  Tensor<DataMesh> A1(3,"1", aMesh);
  Tensor<DataMesh> A12(3, "12", aMesh);
  Tensor<DataMesh> A11(3, "11", aMesh);
  Tensor<DataMesh> A123(3, "123", aMesh);
  Tensor<DataMesh> A122(3, "122", aMesh);
  Tensor<DataMesh> A112(3, "112", aMesh);
  Tensor<DataMesh> A121(3, "121", aMesh);
  Tensor<DataMesh> A111(3, "111", aMesh);
    
  DataMesh DM(aMesh,0.);
  Tensor<DataMesh> A12312(3, "12312", aMesh);
}

//================================================================
// using directives
// ================================================================
// spelled out here once, rather than duplicated into each function.

using namespace ImplicitTensorLoops;

using TIndex3::i_;
using TIndex3::j_;
using TIndex3::k_;
using TIndex3::l_;

using TIndex4::a_;
using TIndex4::b_;
using TIndex4::c_;

void WriteFileHeaders(std::string fname, std::list<std::string>& cols){
  std::ofstream file;
  file.open(fname);
  for(auto c:cols) file<<c;
  file << "\n";
  file.close();
}

void WriteToFile(std::string fname, 
    int gsize, std::list<double>& data){

  std::ofstream file;
  file.open(fname,std::ios::app);
  file << gsize << "\t";
  for(auto d:data) file << d << "\t";
  file << "\n";
  file.close();
}

//void BenchmarkGhEqns() {
  //std::cout << "# TIMING OF GH-EQUATIONS-RHS" << std::endl;
  //std::cout << "# Size    trad'l-abs    trad'l-avg   implicit-abs  implicit-avg   ratio" << std::endl;

  //for(int Nz=1; Nz<=20000; Nz*=2) {
    //Mesh mesh(std::vector<int>(MV::fill, 1,1,Nz));

    //// evolved variables & their spatial derivatives
    //Tensor<DataMesh> psi(4,"11", mesh); psi << ran;
    //Tensor<Tensor<DataMesh> > dpsi(4,"11", 3, "1", mesh); dpsi << ran;
    //Tensor<DataMesh> kappa(4,"122", mesh); kappa << ran;
    //Tensor<Tensor<DataMesh> > dkappa(4,"122", 3,"1", mesh); dkappa << ran;

    //// gauge source functions, and their spacetime derivatives
    //Tensor<DataMesh> H(4,"1", mesh); H<<ran;
    //Tensor<Tensor<DataMesh> > d4H(4,"1", 4, "1", mesh); d4H << ran;

    //// Constraint-damping parameters. Some of these will always have
    //// their default values, so could be ommitted.
    //Tensor<DataMesh> gamma0(3,"", mesh); gamma0 << ran;
    //Tensor<DataMesh> gamma1(3,"", mesh); gamma1 << ran;
    //Tensor<DataMesh> gamma2(3,"", mesh); gamma2 << ran;
    //const bool AddG1ShiftCorr=true;

    //Tensor<DataMesh> G1ShiftCorr(3, "1", mesh); G1ShiftCorr << ran;

    //// derived quantities.  Given psi and dpsi above, these could be
    //// computed from scratch.
    //Tensor<DataMesh> Lapse(3,"",mesh); Lapse << ran;
    //Tensor<DataMesh> Shift(3,"1", mesh); Shift << ran;
    //Tensor<DataMesh> Invg(3,"11", mesh); Invg << ran;
    //Tensor<DataMesh> Invpsi(4,"11", mesh); Invpsi << ran;
    //Tensor<DataMesh> Gamma1(4,"122", mesh); Gamma1 << ran;
    //Tensor<DataMesh> Gamma2(4,"122", mesh); Gamma2 << ran;
    //Tensor<DataMesh> TrGamma1(4,"1", mesh); TrGamma1 << ran;
    //Tensor<DataMesh> T_i(4, "1", mesh); T_i << ran;
    //Tensor<DataMesh> T_I(4, "1", mesh); T_I << ran;

    //// output variables to be filled
    //Tensor<DataMesh> dtpsi(4,"11", mesh, 0.);
    //Tensor<DataMesh> dtkappa(4,"122", mesh, 0.);

    
    //std::list<std::string> columns;
    //columns.push_back("# [1] = DataMesh\n");
    //columns.push_back("# [2] = ImplicitTensorLoops\n");
    
    //WriteFileHeaders("GhEqnsTimings.dat",columns);
    //const int N=100;
    //using namespace std::chrono;
    //auto t0=high_resolution_clock::now();

    //for(int k=0; k<N+1; ++k) {
      //if(k==1)
	//t0=high_resolution_clock::now();
      //for(int a=0; a<4; ++a) {
	//for(int b=a; b<4; ++b) {
	  //dtpsi(a,b)=0;
	  //for(int c=0; c<4; ++c)
	    //dtkappa(c,a,b)=0;
	//}
      //}

      //GhEqnsAddToRhsTest(
		     //dtpsi, dtkappa,
		     //psi, dpsi, kappa, dkappa,
		     //H, d4H,
		     //gamma0, gamma1, gamma2, AddG1ShiftCorr, G1ShiftCorr,
		     //Lapse, Shift, Invg, Invpsi,
		     //Gamma1, Gamma2, TrGamma1, T_i, T_I
		     //);
    //}

    //auto t1=high_resolution_clock::now();
    //auto DeltaT0 = duration_cast<nanoseconds>(t1-t0).count();

    //// output variables to be filled
    //Tensor<DataMesh> dtpsi_Loops(4,"11", mesh, 0.);
    //Tensor<DataMesh> dtkappa_Loops(4,"122", mesh, 0.);

    //for(int k=0; k<N+1; ++k) {
      //if(k==1)
	//t0=high_resolution_clock::now();
      //dtpsi_Loops(sym<0,1>(),a_,b_)=0;
      //dtkappa_Loops(sym<1,2>(),a_,b_,c_)=0;
      //GhEqnsAddToRhsImplicitLoopsTest
	//(
	 //// output variables
	 //dtpsi_Loops,
	 //dtkappa_Loops,
	 //psi, dpsi, kappa, dkappa,
	 //H, d4H,
	 //gamma0, gamma1, gamma2, AddG1ShiftCorr, G1ShiftCorr,
	 //Lapse, Shift, Invg, Invpsi,
	 //Gamma1, Gamma2, TrGamma1, T_i, T_I
	 //);
    //}
    //t1=high_resolution_clock::now();
    //auto DeltaT1 = duration_cast<nanoseconds>(t1-t0).count();
    //std::cout << std::setw(7) << mesh.Size()
	      //<< std::setw(10) << DeltaT0
	      //<< "   " << double(DeltaT0)/(mesh.Size()*N)
	      //<< std::setw(9) << DeltaT1
	      //<< "   " << double(DeltaT1)/(mesh.Size()*N)
	      //<< "      " << double(DeltaT1)/(double(DeltaT0)+1e-9)
	      //<< std::endl;


    //// check result


    //// report sometimes errors 100-1000x above roundoff, because
    //// dtkappa and dtpsi can be O(100-1000).
    //// IS_EQUAL_EPS(dtpsi, dtpsi_Loops, "dtpsi - IS_EQUAL_EPS");
    //// IS_EQUAL_EPS(dtkappa, dtkappa_Loops, "dtkappa -- IS_EQUAL_EPS");
    
    //[>
    //for(int a=0; a<4; ++a) {
      //for(int b=a; b<4; ++b) {
	//dtpsi(a,b) = (dtpsi(a,b)-dtpsi_Loops(a,b))
	  ///(fabs(dtpsi(a,b))+fabs(dtpsi_Loops(a,b))+1.);
	//for(int c=0; c<4; ++c){
	  ////std::cout << "dtkappa(" << c << ", " << a << ", " << b << ")"
	  ////<< ", min=" << Min(fabs(dtkappa(c,a,b)))
	  ////<< ", max=" << Max(fabs(dtkappa(c,a,b)))
	  ////<< ", max|diff|="
	  ////<< Max( fabs(dtkappa(c,a,b)-dtkappa_Loops(c,a,b)))
	  ////<< ", max|rel diff|="
	  ////<< Max(
	  ////fabs(dtkappa(c,a,b)-dtkappa_Loops(c,a,b))/
	  ////   (fabs(dtkappa(c,a,b))+fabs(dtkappa_Loops(c,a,b))))
	  ////	     << ", max|diff/(sum+1)|="
	  ////	     << Max(
	  ////		    fabs(dtkappa(c,a,b)-dtkappa_Loops(c,a,b))/
	  ////	  (1.+fabs(dtkappa(c,a,b))+fabs(dtkappa_Loops(c,a,b))))
	  ////		     << std::endl;

	  //// without normalization "+1", would report large errors, because
	  //// sometimes dtkappa and dtkappa_Loops can be small (1e-3..1e-4),
	  //// but since this is the consequence of subtracting O(1) numbers,
	  //// truncation error of the difference is larger.
	  //dtkappa(c,a,b)= (dtkappa(c,a,b)-dtkappa_Loops(c,a,b))/
	    //(fabs(dtkappa(c,a,b))+fabs(dtkappa_Loops(c,a,b))+1.);

	  //// std::cout << "dtkappa(" << c << ", " << a << ", " << b << ")"
	  //// 	    << " Linf=" << LinfNorm(dtkappa(c,a,b))
	  //// 	    << " L2=" << L2Norm(dtkappa(c,a,b)) << std::endl;

	//}
      //}
    //}

    //IS_ZERO_EPS(dtpsi, "dtpsi");
    //IS_ZERO_EPS(dtkappa, "dtkappa");
    //*/
  //}
   
//}

void RaiseIndexBenchmark() {

  std::cout << "K^i_jk = g^il K_ljk- g(3,\"11\"); K(3,\"122\")" << std::endl; 
  std::cout << "    Size      host   OldCW      TCW      LinearArrays     GPUPointers  " << std::endl;
  std::ofstream file;
  
  std::list<std::string> columns;
  columns.push_back("# [1] = Gridsize\n");
  columns.push_back("# [2] = CPU\n");
  columns.push_back("# [3] = OldCW\n");
  columns.push_back("# [4] = TCodeWriter\n");
  //columns.push_back("# [5] = LinearArrays\n");
  //columns.push_back("# [6] = GPU_Tensor1\n");
  //columns.push_back("# [7] = Unrolled\n");
  
  
  WriteFileHeaders("RaiseIndex1of3Timings.dat",columns);
  WriteFileHeaders("RaiseIndex1of3Bandwidths.dat",columns);
  
  
  std::list<std::string> columns_speed;
  columns_speed.push_back("# [1] = Gridsize\n");
  columns_speed.push_back("# [2] = OldCW\n");
  columns_speed.push_back("# [3] = TCodeWriter\n");
  //columns_speed.push_back("# [4] = LinearArrays\n");
  //columns_speed.push_back("# [5] = GPU_Tensor1\n");
  //columns_speed.push_back("# [6] = Unrolled\n");
  WriteFileHeaders("RaiseIndex1of3Speedups.dat",columns_speed);
  for(int Nz=1; Nz<=100000; Nz+=(Nz/10 + 10)) {
  //{int Nz = 2000;
  const int N=100;
    
  const int Nx=1;
    const int Ny=1;
    Mesh mesh(std::vector<int>({Nx,Ny,Nz}));
    
    std::list<double> times;
    std::list<double> bandwidths;
    std::list<double> speedups;
    
    const double DSz = 8.*mesh.Size() * (
              TensorStructure(3,"11").Size()+TensorStructure(3,"122").Size() 
              + TensorStructure(3,"122").Size() );
    
    std::cout << std::setw(7) << mesh.Size() << std::flush;
    // evolved variables & their spatial derivatives
    using namespace std::chrono;
    auto t0=high_resolution_clock::now();
    auto t1=high_resolution_clock::now();
    Tensor<DataMesh> KupHost(3,"122",mesh,0); 
    Tensor<DataMesh> gtmpl(3,"11",mesh,1);
    gtmpl << ran;
    Tensor<DataMesh> Kdntmpl(3,"122",mesh,2);
    Kdntmpl<<ran;
    Tensor<DataMesh> Kuptmpl(3,"122",mesh,0);
    
    
    //POINTERS
    {
      Tensor<DataMesh> g(gtmpl);
      //V << ran;
      Tensor<DataMesh> Kdn(Kdntmpl); //W << ran;
      
      std::vector<Tensor<DataMesh> > KupVecH(N,Kuptmpl);
      //Tensor<DataMesh> Kup(Kuptmpl); //W << ran;
      
      //sync to host
      
      for(int n=0; n<N; ++n) {
        for(int i=0; i<3; ++i){
          for(int j=0; j<3; ++j){
            for(int k=j; k<3; ++k){
              //Kup(i,j,k)=0;
              double* Kup_d = KupVecH[n](i,j,k).Data();
              for(int l=0;l<3;++l){
                const double* g_d = g(i,l).Data();
                const double* Kdn_d = Kdn(l,j,k).Data();
                for(int x=0; x<mesh.Size(); ++x){
                  Kup_d[x] += g_d[x] * Kdn_d[x];
                  Kup_d[x] = 0;
                }
              }
            }
          }
        }
      } 
      
      //time DataMesh ops
      t0=high_resolution_clock::now();
      for(int n=0; n<N; ++n) {
        for(int i=0; i<3; ++i){
          for(int j=0; j<3; ++j){
            for(int k=j; k<3; ++k){
              //Kup(i,j,k)=0;
              double* Kup_d = KupVecH[n](i,j,k).Data();
              for(int l=0;l<3;++l){
                const double* g_d = g(i,l).Data();
                const double* Kdn_d = Kdn(l,j,k).Data();
                for(int x=0; x<mesh.Size(); ++x){
                  Kup_d[x] += g_d[x] * Kdn_d[x];
                }
              }
            }
          }
        }
      }
      
      t1=high_resolution_clock::now();
      KupHost=KupVecH[0];
    }
    auto DeltaT_hst = duration_cast<nanoseconds>(t1-t0).count();
    const double host_time = (double)(DeltaT_hst/N);
    std::cout << std::setw(10) << host_time << "||" << std::setw(10)  
        << std::flush;
    times.push_back(host_time);
    bandwidths.push_back(DSz/host_time);
    //speedups.push_back(1) 
    
    #ifdef ACCEL_CUDA
    cudaError_t err; 
    std::string errstring;
    
    cudaEvent_t GPUt1;
    cudaEventCreate(&GPUt1);
    cudaEvent_t GPUt2;
    cudaEventCreate(&GPUt2);
    float GPUtime;
    
    //TENSOR<DATAMESH>
    {

      Tensor<DataMesh> g(gtmpl);
      //V << ran;
      Tensor<DataMesh> Kdn(Kdntmpl); //W << ran;
      
      Tensor<DataMesh> Kup(Kuptmpl); //W << ran;
      std::vector<Tensor<DataMesh> > KupVec(N,Kuptmpl);
      
      //time DataMesh ops
      GPU::dSynchronize();
      GPU::cuda_tick(GPUt1);
      for(int n=0; n<N; ++n) {
        for(int i=0; i<3; ++i){
          for(int j=0; j<3; ++j){
            for(int k=j; k<3; ++k){
              //Kup(i,j,k)=0;
              for(int l=0;l<3;++l){
                KupVec[n](i,j,k) += g(i,l) * Kdn(l,j,k); 
              }
            }
          }
        }
      }
      GPU::cuda_tock(GPUt1,GPUt2,GPUtime);
      GPUtime*=1E6;
      GPU::dSynchronize();
      compareTDM(KupVec[0],KupHost,"\nDMCodeWriter\n");
    
    }
    const double TDM_time =(double)(GPUtime/N);
    std::cout << std::setw(10) << TDM_time << "||" << std::setw(10)  
        << std::flush;
    times.push_back(TDM_time);
    bandwidths.push_back(DSz/TDM_time);
    speedups.push_back(host_time/TDM_time); 
    
    
    //IMPLICIT LOOPS 
    {
      Tensor<DataMesh> g(gtmpl);
      //V << ran;
      Tensor<DataMesh> Kdn(Kdntmpl); //W << ran;
      
      Tensor<DataMesh> Kup(Kuptmpl); //W << ran;
      
      //build indices
      IPoint gidx({-1,-1});
      IPoint Kidx({-1,-1,-1});
      { 
        const double** gpt(g.GPUPointers(2,gidx).GetPointers());
        (void) gpt;
        const double** KDpt(Kdn.GPUPointers(3,Kidx).GetPointers());
        (void) KDpt;
        double** KUpt(Kup.GPUPointers(3,Kidx).GetPointersNonConst());
        (void) KUpt;
      }
      
      GPU::dSynchronize();
      GPU::cuda_tick(GPUt1);
      for(int k=0; k<N; ++k) {
          Kup(sym<1,2>(),i_,j_,k_) = Sum(l_,g(i_,l_)*Kdn(l_,j_,k_));
      }
      GPU::cuda_tock(GPUt1,GPUt2,GPUtime);
      GPUtime*=1E6;
      GPU::dSynchronize();  
      
      compareTDM(Kup,KupHost,"\nTCodeWriter\n");
      err=cudaGetLastError();
      REQUIRE(GPU::dError(err,errstring),errstring);
      const double impl_time = (double)(GPUtime/N);
      std::cout << std::setw(10) << impl_time << "||" << std::setw(10)  
          << std::flush;
      times.push_back(impl_time);
      bandwidths.push_back(DSz/impl_time);
      speedups.push_back(host_time/impl_time); 
    } 
    /* 
    //Linear device arrays 
    {
      //make the arrays
      double* g_host = new double[mesh.Size()*3*3];
      double* Kup_host = new double[mesh.Size()*3*3*3];
      double* Kdn_host = new double[mesh.Size()*3*3*3];
      for(int i=0; i<mesh.Size()*3*3;++i) g_host[i] = 2;
      for(int j=0; j<mesh.Size()*3*3*3;++j){
        Kup_host[j] = 0.;
        Kdn_host[j] = 3.;
      }

      double* gd = (double*)CudaMemAllocate(mesh.Size()*3*3*sizeof(double));
      double* Kupd = (double*)CudaMemAllocate(mesh.Size()*3*3*3*sizeof(double));
      double* Kdnd = (double*)CudaMemAllocate(mesh.Size()*3*3*3*sizeof(double));
      

      GPU::dSynchronize();
      GPU::cuda_tick(GPUt1);
      for(int k=0; k<N; ++k) {
        GPU_tensor_arrays(mesh.Size(),gd,Kdnd,Kupd);  
      }
      GPU::cuda_tock(GPUt1,GPUt2,GPUtime);
      GPUtime*=1E6;
      GPU::dSynchronize();  
      delete[] g_host;
      delete[] Kup_host;
      delete[] Kdn_host;
      //compareTDM(Kup,KupHost,"\nTCodeWriter\n");
      CudaMemRelease(gd,mesh.Size()*3*3*sizeof(double));
      CudaMemRelease(Kupd,mesh.Size()*3*3*3*sizeof(double));
      CudaMemRelease(Kdnd,mesh.Size()*3*3*3*sizeof(double));
      
      err=cudaGetLastError();
      REQUIRE(GPU::dError(err,errstring),errstring);
      const double array_time = (double)(GPUtime/N);
      std::cout << std::setw(10) << array_time << "||" << std::setw(10)  
          << std::flush;
      times.push_back(array_time);
      bandwidths.push_back(DSz/array_time);
      speedups.push_back(host_time/array_time); 
    } 
    //GPU_Tensor
    {
      Tensor<DataMesh> g(gtmpl);
      //V << ran;
      Tensor<DataMesh> Kdn(Kdntmpl); //W << ran;
      
      Tensor<DataMesh> Kup(Kuptmpl); //W << ran;
      
      //First build the indices
      typedef Tensor_GPUPointers<Tensor<DataMesh> >  DPT;
      IPoint gidx(MV::fill,-1,-1);
      IPoint Kidx(MV::fill,-1,-1,-1);
      { 
        const double** gpt(g.GPUPointers(2,gidx).GetPointers());
        (void) gpt;
        const double** KDpt(Kdn.GPUPointers(3,Kidx).GetPointers());
        (void) KDpt;
        double** KUpt(Kup.GPUPointers(3,Kidx).GetPointersNonConst());
        (void) KUpt;
      }
      
      GPU::dSynchronize();

      GPU::cuda_tick(GPUt1);
      for(int k=0;k<N;++k){
        DPT& gpt(g.GPUPointers(2,gidx));
        DPT& KDpt(Kdn.GPUPointers(3,Kidx));
        DPT& KUpt(Kup.GPUPointers(3,Kidx));
        GPU_Tensor_Kernel_Wrapper(mesh.Size(),
            gpt.GetPointers(),
             
            KDpt.GetPointers(),
            
            KUpt.GetPointersNonConst()
            );
      
        #ifdef ACCEL_DEBUG
        GPU::dSynchronize();
        err=cudaGetLastError();
        REQUIRE(GPU::dError(err,errstring),errstring);
        #endif  
      }
      GPU::cuda_tock(GPUt1,GPUt2,GPUtime);
      GPUtime*=1E6;
      GPU::dSynchronize();
      compareTDM(Kup,KupHost,"\nGPUTensor\n");
      err=cudaGetLastError();
      REQUIRE(GPU::dError(err,errstring),errstring);
    }
    
    const double GPUTensor_time = (double)(GPUtime/N);
    std::cout << std::setw(10) << GPUTensor_time << "||" << std::setw(10)  
       << std::flush;
    times.push_back(GPUTensor_time);
    bandwidths.push_back(DSz/GPUTensor_time);
    speedups.push_back(host_time/GPUTensor_time); 

    
    //GPU_TensorOnly_i
    //Parallel on i; sum done serial
    {
      Tensor<DataMesh> g(gtmpl);
      //V << ran;
      Tensor<DataMesh> Kdn(Kdntmpl); //W << ran;
      
      Tensor<DataMesh> Kup(Kuptmpl); //W << ran;
      
      //First build the indices
      IPoint gidx(MV::fill,-1,-1);
      IPoint Kidx(MV::fill,-1,-1,-1);
      { 
        typedef Tensor_GPUPointers<Tensor<DataMesh> >  DPT;
        DPT& gpt(g.GPUPointers(2,gidx));
        (void) gpt;
        DPT& KDpt(Kdn.GPUPointers(3,Kidx));
        (void) KDpt;
        DPT& KUpt(Kup.GPUPointers(3,Kidx));
        (void) KUpt;
      }
      
      GPU::dSynchronize();
      GPU::cuda_tick(GPUt1);
      for(int k=0;k<N;++k){
        typedef Tensor_GPUPointers<Tensor<DataMesh> >  DPT;
        DPT& gpt(g.GPUPointers(2,gidx));
        DPT& KDpt(Kdn.GPUPointers(3,Kidx));
        DPT& KUpt(Kup.GPUPointers(3,Kidx));
        GPU_Tensor_Kernel_WrapperV2(mesh.Size(),
            gpt.GetPointers(),
             
            KDpt.GetPointers(),
            
            KUpt.GetPointersNonConst()
            );
        #ifdef ACCEL_DEBUG
        GPU::dSynchronize();
        err=cudaGetLastError();
        REQUIRE(GPU::dError(err,errstring),errstring);
        #endif  
      }
      GPU::cuda_tock(GPUt1,GPUt2,GPUtime);
      GPUtime*=1E6;
      GPU::dSynchronize();
      compareTDM(Kup,KupHost,"\nUnrolled\n");
      err=cudaGetLastError();
      REQUIRE(GPU::dError(err,errstring),errstring);


    }
    
    const double GPUTensorOnlyI_time = (double)(GPUtime/N);
    std::cout << std::setw(10) << GPUTensorOnlyI_time << std::setw(10)  
        << std::flush;
    times.push_back(GPUTensorOnlyI_time);
    bandwidths.push_back(DSz/GPUTensorOnlyI_time);
    speedups.push_back(host_time/GPUTensorOnlyI_time); 
 */  
    #endif
  
    std::cout << std::endl; 
  WriteToFile("RaiseIndex1of3Timings.dat",mesh.Size(),times);
  WriteToFile("RaiseIndex1of3Bandwidths.dat",mesh.Size(),bandwidths);
  WriteToFile("RaiseIndex1of3Speedups.dat",mesh.Size(),speedups);
  #ifdef ACCEL_CUDA 
  CudaMemReleaseReserved();
  #endif
  } 
}


int BenchmarkTensorLoops() {
  UtilsForTesting::SetEps(1e-14);
  //RaiseIndexProfile();
  RaiseIndexBenchmark();
  //BenchmarkGhEqns();
  return UtilsForTesting::NumberOfTestsFailed();
}

