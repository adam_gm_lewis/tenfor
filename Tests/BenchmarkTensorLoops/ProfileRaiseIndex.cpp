#include "Utils/LowLevelUtils/UtilsForTiming.hpp"
#include "Utils/LowLevelUtils/Ran2Class.hpp"
#include "Utils/ErrorHandling/UtilsForTesting.hpp"
#include "Utils/Tensor/Tensor.hpp"
#include "Utils/MiscUtils/GenericProfiler.hpp"
#include <iostream>
#include <chrono>
#include <iomanip>
#include <fstream>
#include "Utils/DataMesh/DataMeshNorms.hpp"
#include "Utils/MyContainers/MyList.hpp"

#include "Utils/Tests/TestTensorLoops/GhEqnsAddToRhs.hpp"
#include "Utils/Tests/BenchmarkTensorLoops/BenchmarkWriter.hpp"
#include "Utils/Tensor/ImplicitTensorLoops/TComparisonHelpers.hpp"
#include "GPU/cuda_wrapper.hpp"
#include "BenchmarkKernels.hpp"
#include "Utils/DataMesh/CudaMemoryHandler.hpp"
//================================================================
// some helper functions
//================================================================
template<class func>
void operator<<(Tensor<DataMesh>& lhs, func& functor) {
  for(auto& a:lhs) {
    for(int i=0; i<a.Size(); ++i)
      a[i]=functor();
  }
};

template<class func>
void operator<<(Tensor<Tensor<DataMesh> >& lhs, func& functor) {
  for(auto& a:lhs) {
    a << functor;
  }
};

namespace {
  Ran2 ran(212312);
}

//================================================================
// using directives
// ================================================================
// spelled out here once, rather than duplicated into each function.
using namespace ImplicitTensorLoops;

using TIndex3::i_;
using TIndex3::j_;
using TIndex3::k_;
using TIndex3::l_;

using TIndex4::a_;
using TIndex4::b_;
using TIndex4::c_;
using namespace std::chrono;

void ProfileRaiseIndex(int maxnz) {
  std::cout << "T^i_jk = g^il T_ljk- g(3,\"11\"); T(3,\"122\")" << std::endl; 
  const TensorStructure Tst(3,"122");
  const TensorStructure gst(3,"11");
  
  for(int Nz=1; Nz<=maxnz; Nz+=(Nz/10+10)){
    Mesh mesh(MyVector<int>(MV::fill, 1,1,Nz));

    Tensor<DataMesh> T_compare(Tst, mesh, 0.);
    // create right-hand-side data, and fill with random numbers
    Tensor<DataMesh> t(Tst, mesh, 0.); t << ran; //passing ran in here gives
                                                   //const correctness errors
    Tensor<DataMesh> g(gst, mesh, 0.); g <<ran;     //host pointer timings
                                   
    //impl timings
    {
      Tensor<DataMesh> T(Tst, mesh, 0);
      //run once to build indices
      T(sym<1,2>(),i_,j_,k_) = Sum(l_,g(i_,l_)*t(l_,j_,k_));
      #ifdef ACCEL_CUDA 
      GPU::dSynchronize();
      //profile
      GPU::profilerStart();
      #endif
      T(sym<1,2>(),i_,j_,k_) = Sum(l_,g(i_,l_)*t(l_,j_,k_));
      #ifdef ACCEL_CUDA
      GPU::profilerStop();
      #endif
    }

    #ifdef ACCEL_CUDA
    //Clear the device memory we allocated.
    CudaMemReleaseReserved();
    #endif
  }
}
int ProfileExpressions() {
  ProfileRaiseIndex(20000);
  return 0;
}
