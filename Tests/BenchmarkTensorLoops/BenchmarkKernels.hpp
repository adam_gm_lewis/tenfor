#ifndef BENCHMARK_KERNELS_HPP
#define BENCHMARK_KERNELS_HPP

  //Naive version; all indices serial
void GPU_Tensor_Kernel_Wrapper(const int sz, const double** g,  
      const double** Kdn,  double** Kup );
void GPU_tensor_arrays(const int sz, const double* g, const double* Kdn,
    double* Kup);
//Unrolls some loops
void GPU_Tensor_Kernel_WrapperV2(const int sz, const double** g,  
      const double** Kdn,  double** Kup );

//Reverses the index order
void GPU_Tensor_Kernel_WrapperBackwards(const int sz, const double** g,  
      const double** Kdn,  double** Kup );

//Parallel on i and j; sum done with a loop.
void GPU_Tensor_ParallelTensorsWrapper(const int sz, const double** g,  
      const double** Kdn,  double** Kup );


//Serial on all indexes; sum done with shared memory
void GPU_Tensor_KernelSharedSumWrapper(const int sz, const double** g,  
      const double** Kdn,  double** Kup );

//Do the sum with shared memory, parallel on i and j.
void GPU_Tensor_ParallelSharedWrapper(const int sz, const double** g,  
      const double** Kdn,  double** Kup );

//Parallel on i,j,and k; sum done with a loop
//CURRENT BEST IMPL
void GPU_Tensor_ParallelTensorsAllWrapper(const int sz, const double** g,  
      const double** Kdn,  double** Kup );

//Parallel on i,j, and k; sum done with shared memory
void GPU_Tensor_ParallelTensorsAllSharedWrapper(const int sz, const double** g,  
      const double** Kdn,  double** Kup );

//Parallel on i; sum done with a loop
void GPU_Tensor_ParallelTensors_Only_iWrapper(const int sz, const double** g,  
      const double** Kdn,  double** Kup );

  





#endif
