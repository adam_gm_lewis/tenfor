#ifndef BENCHMARK_WRITER_HPP
#define BENCHMARK_WRITER_HPP

#include <fstream>
//Represents a file containing raw timings and bandwidths for one tensor
//expression.
class BenchmarkWriter {
  public:
    BenchmarkWriter(const std::string& fname, const int expSz,
        const int dSz=0,
        const bool host_pointers=false);

    //Writes to the files. Feed it a gridsize, then timing for the DM,
    //implicit loops, and double* pointers expressions (if the latter are used).
    //The timings should be in nanoseconds.
    void Write(const int gridsz, const double DMt, const double impl, 
        const double dbl=-1); 
  
  private:
    //The effective bandwidth for a given timing.
    double bandwidth(const int gridsz, const double t)
      { const int intsize = static_cast<const int>(sizeof(double));
        return ((mSz*gridsz+mDsz)*intsize)/t;}

    //String specifying which SpEC 'COMPILE_VARIANT' flag was used for this 
    //benchmark.
    std::string accelflag();

    //The C++ compiler version (clang, intel, or GNU)
    std::string compiler();
   
    //The CUDA version. If not using ACCEL_CUDA, an empty string.
    std::string cudaversion();
   
    //the name of the output file
    std::string filename(const std::string& fname);
    
    //members
    const std::string mFname; //the name of the file to be written to
    const int mSz;         //number of DataMeshes involved in the expression 
    const int mDsz;        //number of doubles involved in the expression
                              //(outside of DataMesh)
    const bool mHstPntrs; 
    std::ofstream mFile;      //ofstream of the file
};

#endif
