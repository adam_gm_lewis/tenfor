//#include "Utils/LowLevelUtils/UtilsForTiming.hpp"
#include "Ran2Class.hpp"
#include "UtilsForTesting.hpp"
#include "Tensor.hpp"
#include "DataMesh.hpp"
#include "Mesh.hpp"
//#include "Utils/MiscUtils/GenericProfiler.hpp"
#include <iostream>
#include <chrono>
#include <iomanip>
#include <fstream>
//#include "Utils/DataMesh/DataMeshNorms.hpp"
//#include "Utils/MyContainers/std::list.hpp"
#include <list>
#include "Assert.hpp"

#include "GhEqnsAddToRhs.hpp"
#include "BenchmarkWriter.hpp"
#include "TComparisonHelpers.hpp"
#ifdef ACCEL_CUDA
#include "GPU/cuda_wrapper.hpp"
#include "BenchmarkKernels.hpp"
#include "Utils/DataMesh/CudaMemoryHandler.hpp"
#endif
//================================================================
// some helper functions
//================================================================
template<class func>
void operator<<(Tensor<DataMesh>& lhs, func& functor) {
  for(auto& a:lhs) {
    for(int i=0; i<a.Size(); ++i)
      a[i]=functor();
  }
};

template<class func>
void operator<<(Tensor<Tensor<DataMesh> >& lhs, func& functor) {
  for(auto& a:lhs) {
    a << functor;
  }
};

namespace {
  Ran2 ran(212312);
}
//================================================================
// using directives
// ================================================================
// spelled out here once, rather than duplicated into each function.
using namespace ImplicitTensorLoops;

using TIndex3::i_;
using TIndex3::j_;
using TIndex3::k_;
using TIndex3::l_;

using TIndex4::a_;
using TIndex4::b_;
using TIndex4::c_;
using TIndex4::d_;
using namespace std::chrono;


void BenchmarkChristoffelSymbols2ndKind(std::string& fname,int maxnz, int N) {
  std::cout << "Gam^i_jk = 0.5*(g^il)(g_lj,k + g_lk,j - g_jk,l);" << std::endl;
  const TensorStructure Gam_st(3,"122");
  const TensorStructure gst(3,"11");
  const TensorStructure gcomst(3,"1");
  const std::string expname = fname + "Christoffel2ndKind";
  
  //the number of DataMeshes we move
  const int DMSz=Gam_st.Size() + gst.Size()*gcomst.Size()*3 + gst.Size();
  //the number of doubles
  const int DSz = 1;
  BenchmarkWriter Writer(expname, DMSz, DSz,true);
  double t_DM=0; double t_impl=0; double t_dbl=0; 
  //initialize timers
  #ifdef ACCEL_CUDA
  cudaError_t err;
  std::string errstring;
  cudaEvent_t GPUt1;
  cudaEventCreate(&GPUt1);
  cudaEvent_t GPUt2;
  cudaEventCreate(&GPUt2);
  float GPUtime;
  #endif
  for(int Nz=1; Nz<=maxnz; Nz+=(Nz/10+10)){
    Mesh mesh(std::vector<int>({1,1,Nz}));
    const int sz = mesh.Size();
    
    Tensor<DataMesh> Gam_compare(Gam_st, mesh, 0.);
    // create right-hand-side data, and fill with random numbers
    Tensor<DataMesh> g0(gst, mesh, 0.); g0 << ran; //passing ran in here gives
                                                   //const correctness errors
    Tensor<DataMesh> gcomma(gcomst,mesh,0.); gcomma << ran;
  
    Tensor<Tensor<DataMesh> > gdir(gst,gcomma); gdir << ran;
    //host pointer timings
    {
      Tensor<DataMesh> Gam(Gam_st, mesh, 0);

      //Get test data
      for(int i=0; i<3; ++i){
        for(int j=0; j<3; ++j){
          for(int k=j; k<3; ++k){
            double* Gam_d = Gam(i,j,k).Data();
            for(int l=0; l<3; ++l){
              const double* g_d = g0(i,l).Data();
              const double* gd1_d = gdir(j,l)(k).Data();
              const double* gd2_d = gdir(k,l)(j).Data();
              const double* gd3_d = gdir(j,k)(l).Data();
              for(int x=0; x<sz; ++x){
                Gam_d[x] += 0.5*g_d[x]*(gd1_d[x] + gd2_d[x] - gd3_d[x]);
              }
            }
          }
        }
      }
      Gam_compare = Gam;
      Gam = Tensor<DataMesh>(Gam_st,mesh,0);
      //Sync to host
      for(int i=0; i<3; ++i){
        for(int j=0; j<3; ++j){
          for(int k=j; k<3; ++k){
            double* Gam_d = Gam(i,j,k).Data();
            for(int l=0; l<3; ++l){
              const double* g_d = g0(i,l).Data();
              const double* gd1_d = gdir(j,l)(k).Data();
              const double* gd2_d = gdir(k,l)(j).Data();
              const double* gd3_d = gdir(j,k)(l).Data();
              for(int x=0; x<sz; ++x){
                Gam_d[x] += 0.5*g_d[x]*(gd1_d[x] + gd2_d[x] - gd3_d[x]);
                Gam_d[x] = 0.;
              }
            }
          }
        }
      }
      //Now actually time it
      auto t0=high_resolution_clock::now();
      
      //MODIFY - replace with the correct SpEC code
      for(int n=0; n<N; ++n){
        for(int i=0; i<3; ++i){
          for(int j=0; j<3; ++j){
            for(int k=j; k<3; ++k){
              double* Gam_d = Gam(i,j,k).Data();
              for(int l=0; l<3; ++l){
                const double* g_d = g0(i,l).Data();
                const double* gd1_d = gdir(j,l)(k).Data();
                const double* gd2_d = gdir(k,l)(j).Data();
                const double* gd3_d = gdir(j,k)(l).Data();
                for(int x=0; x<sz; ++x){
                  Gam_d[x] += 0.5*g_d[x]*(gd1_d[x] + gd2_d[x] - gd3_d[x]);
                }
              }
            }
          }
        }
      }
      //tock
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_dbl=static_cast<double>(hsttime/N);
      //save for future error check
    }
    
    //DM timings
    {
      Tensor<DataMesh> GamTest(Gam_st, mesh, 0);
      //Get test data
      for(int i=0; i<3; ++i){
        for(int j=0; j<3; ++j){
          for(int k=j; k<3; ++k){
            for(int l=0; l<3; ++l){
              GamTest(i,j,k) += 0.5*g0(i,l)*(gdir(j,l)(k) + gdir(k,l)(j) - 
                                                gdir(j,k)(l));
            }
          }
        }
      }
      
      Tensor<DataMesh> Gam(Gam_st, mesh, 0);
      //tick
      #ifdef ACCEL_CUDA
        GPU::dSynchronize();
        GPU::cuda_tick(GPUt1);
      #else
        auto t0=high_resolution_clock::now();
      #endif
      //MODIFY - replace with the correct SpEC code
      for(int n=0; n<N; ++n){
        for(int i=0; i<3; ++i){
          for(int j=0; j<3; ++j){
            for(int k=j; k<3; ++k){
              for(int l=0; l<3; ++l){
                Gam(i,j,k) += 0.5*g0(i,l)*(gdir(j,l)(k) + gdir(k,l)(j) - 
                                                  gdir(j,k)(l));
              }
            }
          }
        }
      }
    

      //tock
      #ifdef ACCEL_CUDA
      GPU::cuda_tock(GPUt1,GPUt2,GPUtime);
      GPUtime*=(1E6/N);
      //const double d= static_cast<double>(GPUtime);
      t_DM= static_cast<double>(GPUtime);
      GPU::dSynchronize();
      err = cudaGetLastError();
      REQUIRE(GPU::dError(err,errstring),errstring);

      #else
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_DM=static_cast<double>(hsttime/N);
      #endif
      
      //error check
      compareTDM(GamTest,Gam_compare,expname);
    }

    //impl timings
    {
      Tensor<DataMesh> Gam(Gam_st, mesh, 0);
      //tick
      #ifdef ACCEL_CUDA
        GPU::dSynchronize();
        GPU::cuda_tick(GPUt1);
      #else
        auto t0=high_resolution_clock::now();
      #endif

      for(int n=0; n<N; ++n){
        Gam(sym<1,2>(),i_,j_,k_) = 0.5*Sum(l_, g0(i_,l_)*(gdir(j_,l_)(k_)
                          + gdir(k_,l_)(j_) - gdir(j_,k_)(l_)));
      }
      
      //tock
      #ifdef ACCEL_CUDA
      GPU::cuda_tock(GPUt1,GPUt2,GPUtime);
      GPUtime*=(1E6/N);
      //const double d= static_cast<double>(GPUtime);
      t_impl= static_cast<double>(GPUtime);
      GPU::dSynchronize();
      err = cudaGetLastError();
      REQUIRE(GPU::dError(err,errstring),errstring);

      #else
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_impl=static_cast<double>(hsttime/N);
      #endif
      
      //error check
      compareTDM(Gam,Gam_compare,expname);
    }

    //Now write to the timing file. Also prints to screen.
    Writer.Write(sz, t_DM, t_impl, t_dbl);
    
    #ifdef ACCEL_CUDA
    //Clear the device memory we allocated.
    CudaMemReleaseReserved();
    #endif
  }
}



void Benchmark2NgPlusShiftShift(std::string& fname,int maxnz, int N) {
  std::cout << "K(i,j) = 2*N*g(i,j) + beta(i)*beta(j);" << std::endl;
  const TensorStructure Kst(3,"11");
  const TensorStructure gst(3,"11");
  const TensorStructure bst(3,"1");
  const std::string expname = fname + "Kij_2_N_gij_bi_bj_";
  
  //the number of DataMeshes we move
  const int DMSz=Kst.Size() + gst.Size() + 2*bst.Size() + 1;
  //the number of doubles
  const int DSz = 1;
  BenchmarkWriter Writer(expname, DMSz, DSz,true);
  double t_DM=0; double t_impl=0; double t_dbl=0; 
  //initialize timers
  #ifdef ACCEL_CUDA
  cudaError_t err;
  std::string errstring;
  cudaEvent_t GPUt1;
  cudaEventCreate(&GPUt1);
  cudaEvent_t GPUt2;
  cudaEventCreate(&GPUt2);
  float GPUtime;
  #endif
  for(int Nz=1; Nz<=maxnz; Nz+=(Nz/10+10)){
    Mesh mesh(std::vector<int>({1,1,Nz}));
    const int sz = mesh.Size();

    Tensor<DataMesh> K_compare(Kst, mesh, 0.);
    // create right-hand-side data, and fill with random numbers
    Tensor<DataMesh> g0(gst, mesh, 0.); g0 << ran; //passing ran in here gives
                                                   //const correctness errors
    Tensor<DataMesh> beta0(bst, mesh, 0.); beta0 << ran;
    const DataMesh N0(mesh, ran);

    //host pointer timings
    {
      Tensor<DataMesh> K(Kst, mesh, 0);

      //Test run to sync to host
      for(int i=0; i<3; ++i){
        for(int j=i; j<3; ++j){
          double* K_d = K(i,j).Data();
          const double* N_d = N0.Data();
          const double* g_d = g0(i,j).Data();
          const double* b1_d = beta0(i).Data();
          const double* b2_d = beta0(j).Data();
          for(int x=0; x<sz; ++x){
            K_d[x] = 2.*N_d[x]*g_d[x]+b1_d[x]*b2_d[x];
            K_d[x] = 0.;
          }
        }
      }

      //Now actually time it
      auto t0=high_resolution_clock::now();
      
      //MODIFY - replace with the correct SpEC code
      for(int n=0; n<N; ++n){
        //Test run to sync to host
        for(int i=0; i<3; ++i){
          for(int j=i; j<3; ++j){
            double* K_d = K(i,j).Data();
            const double* N_d = N0.Data();
            const double* g_d = g0(i,j).Data();
            const double* b1_d = beta0(i).Data();
            const double* b2_d = beta0(j).Data();
            for(int x=0; x<sz; ++x){
              K_d[x] = 2.*N_d[x]*g_d[x]+b1_d[x]*b2_d[x];
            }
          }
        }
      }
      //tock
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_dbl=static_cast<double>(hsttime/N);
      //save for future error check
      K_compare = K;
    }
    
    //DM timings
    {
      Tensor<DataMesh> K(Kst, mesh, 0);
      
      //tick
      #ifdef ACCEL_CUDA
        GPU::dSynchronize();
        GPU::cuda_tick(GPUt1);
      #else
        auto t0=high_resolution_clock::now();
      #endif

      for(int n=0; n<N; ++n){
        for(int i=0; i<3; ++i){
          for(int j=i; j<3; ++j){
            K(i,j) = 2.*N0*g0(i,j)+beta0(i)*beta0(j);
          }
        }
      }
      //tock
      #ifdef ACCEL_CUDA
      GPU::cuda_tock(GPUt1,GPUt2,GPUtime);
      GPUtime*=(1E6/N);
      //const double d= static_cast<double>(GPUtime);
      t_DM= static_cast<double>(GPUtime);
      GPU::dSynchronize();
      err = cudaGetLastError();
      REQUIRE(GPU::dError(err,errstring),errstring);

      #else
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_DM=static_cast<double>(hsttime/N);
      #endif
      
      //error check
      compareTDM(K,K_compare,expname);
    }

    //impl timings
    {
      Tensor<DataMesh> K(Kst, mesh, 0);
      //tick
      #ifdef ACCEL_CUDA
        GPU::dSynchronize();
        GPU::cuda_tick(GPUt1);
      #else
        auto t0=high_resolution_clock::now();
      #endif

      for(int n=0; n<N; ++n){
        K(sym<0,1>(),i_,j_) = 2.*N0*g0(i_,j_)+beta0(i_)*beta0(j_);
      }
      
      //tock
      #ifdef ACCEL_CUDA
      GPU::cuda_tock(GPUt1,GPUt2,GPUtime);
      GPUtime*=(1E6/N);
      //const double d= static_cast<double>(GPUtime);
      t_impl= static_cast<double>(GPUtime);
      GPU::dSynchronize();
      err = cudaGetLastError();
      REQUIRE(GPU::dError(err,errstring),errstring);

      #else
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_impl=static_cast<double>(hsttime/N);
      #endif
      
      //error check
      compareTDM(K,K_compare,expname);
    }

    //Now write to the timing file. Also prints to screen.
    Writer.Write(sz, t_DM, t_impl, t_dbl);
    
    #ifdef ACCEL_CUDA
    //Clear the device memory we allocated.
    CudaMemReleaseReserved();
    #endif
  }
}

void BenchmarkGHEqns(std::string& fname,int maxnz, int N) {
  std::cout << "GHEqnsRHS" << std::endl;
  const std::string expname = fname + "GHEqnsRHS";
  //count all the DataMeshes going in, not including derived quantities
  int DMSz=0;
  {
    int S411 = TensorStructure(4,"11").Size();
    int S4122 = TensorStructure(4,"122").Size();
    int S31 = TensorStructure(3,"1").Size();
    int S41 = TensorStructure(4,"1").Size();
    int S30 = TensorStructure(3,"").Size();
    int S311 = TensorStructure(3,"11").Size();

    int minDMSz = S411+S411*S31+S4122+S4122*S31+S41+S41*S41+3*S30+S31;
    std::cout << "minDMSz: " << minDMSz << std::endl;
    int derivedDMSz = S30+S31+S311+S411+2*S4122+3*S41; 
    std::cout << "derDMSz: " << derivedDMSz << std::endl;
    DMSz = minDMSz + derivedDMSz;
  }
  //the number of doubles
  const int DSz = 0;
  BenchmarkWriter Writer(expname, DMSz, DSz, false);
  double t_DM=0; double t_impl=0;  
  //initialize timers
  #ifdef ACCEL_CUDA
  cudaError_t err;
  std::string errstring;
  cudaEvent_t GPUt1;
  cudaEventCreate(&GPUt1);
  cudaEvent_t GPUt2;
  cudaEventCreate(&GPUt2);
  float GPUtime;
  #endif
  for(int Nz=1; Nz<=maxnz; Nz+=(Nz/10+10)){
    Mesh mesh(std::vector<int>({1,1,Nz}));
    const int sz = mesh.Size();

    // evolved variables & their spatial derivatives
    Tensor<DataMesh> psi(4,"11", mesh); psi << ran;
    Tensor<Tensor<DataMesh> > dpsi(4,"11", 3, "1", mesh); dpsi << ran;
    Tensor<DataMesh> kappa(4,"122", mesh); kappa << ran;
    Tensor<Tensor<DataMesh> > dkappa(4,"122", 3,"1", mesh); dkappa << ran;

    // gauge source functions, and their spacetime derivatives
    Tensor<DataMesh> H(4,"1", mesh); H<<ran;
    Tensor<Tensor<DataMesh> > d4H(4,"1", 4, "1", mesh); d4H << ran;

    // Constraint-damping parameters. Some of these will always have
    // their default values, so could be ommitted.
    Tensor<DataMesh> gamma0(3,"", mesh); gamma0 << ran;
    Tensor<DataMesh> gamma1(3,"", mesh); gamma1 << ran;
    Tensor<DataMesh> gamma2(3,"", mesh); gamma2 << ran;
    const bool AddG1ShiftCorr=true;

    Tensor<DataMesh> G1ShiftCorr(3, "1", mesh); G1ShiftCorr << ran;

    // derived quantities.  Given psi and dpsi above, these could be
    // computed from scratch.
    Tensor<DataMesh> Lapse(3,"",mesh); Lapse << ran;
    Tensor<DataMesh> Shift(3,"1", mesh); Shift << ran;
    Tensor<DataMesh> Invg(3,"11", mesh); Invg << ran;
    Tensor<DataMesh> Invpsi(4,"11", mesh); Invpsi << ran;
    Tensor<DataMesh> Gamma1(4,"122", mesh); Gamma1 << ran;
    Tensor<DataMesh> Gamma2(4,"122", mesh); Gamma2 << ran;
    Tensor<DataMesh> TrGamma1(4,"1", mesh); TrGamma1 << ran;
    Tensor<DataMesh> T_i(4, "1", mesh); T_i << ran;
    Tensor<DataMesh> T_I(4, "1", mesh); T_I << ran;

    // output variables to be filled
    Tensor<DataMesh> dtpsi_compare(4,"11", mesh, 0.);
    Tensor<DataMesh> dtkappa_compare(4,"122", mesh, 0.);
    
    //DM timings
    {
      Tensor<DataMesh> dtpsi(4,"11", mesh, 0.);
      Tensor<DataMesh> dtkappa(4,"122", mesh, 0.);
      //tick
      #ifdef ACCEL_CUDA
        GPU::dSynchronize();
        GPU::cuda_tick(GPUt1);
      #else
        auto t0=high_resolution_clock::now();
      #endif
      for(int n=0; n<N;++n){
        for(int a=0; a<4; ++a) {
          for(int b=a; b<4; ++b) {
            dtpsi(a,b)=0;
            for(int c=0; c<4; ++c)
              dtkappa(c,a,b)=0;
          }
        }
        GhEqnsAddToRhsTest(
                       dtpsi, dtkappa,
                       psi, dpsi, kappa, dkappa,
                       H, d4H,
                       gamma0, gamma1, gamma2, AddG1ShiftCorr, G1ShiftCorr,
                       Lapse, Shift, Invg, Invpsi,
                       Gamma1, Gamma2, TrGamma1, T_i, T_I
                       );
      }
      
        
      //tock
      #ifdef ACCEL_CUDA
      GPU::cuda_tock(GPUt1,GPUt2,GPUtime);
      GPUtime*=(1E6/N);
      //const double d= static_cast<double>(GPUtime);
      t_DM= static_cast<double>(GPUtime);
      GPU::dSynchronize();
      err = cudaGetLastError();
      REQUIRE(GPU::dError(err,errstring),errstring);

      #else
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_DM=static_cast<double>(hsttime/N);
      #endif
      
      //error check
      dtpsi_compare = dtpsi;
      dtkappa_compare = dtkappa;
      //std::string tag("DM dtpsi");
      //compareTDM(dtpsi,dtpsi_compare,tag);
      //tag="DM dtkappa";
      //compareTDM(dtkappa,dtkappa_compare,tag);
    }

    //impl timings
    {
      Tensor<DataMesh> dtpsi(4,"11", mesh, 0.);
      Tensor<DataMesh> dtkappa(4,"122", mesh, 0.);
      //tick
      #ifdef ACCEL_CUDA
        GPU::dSynchronize();
        GPU::cuda_tick(GPUt1);
      #else
        auto t0=high_resolution_clock::now();
      #endif
      for(int n=0; n<N;++n){
        dtpsi(sym<0,1>(),a_,b_) = 0;
        dtkappa(sym<1,2>(),a_,b_,c_)=0;
        //std::cout << dtpsi << std::endl;
        //std::cout << dtkappa << std::endl;
        //std::cout << psi << std::endl;
        //std::cout << kappa << std::endl;
        //std::cout << dkappa << std::endl;
        //std::cout << H << std::endl;
        //std::cout << d4H << std::endl;
        //std::cout << gamma0 << std::endl;
        //std::cout << gamma1 << std::endl;
        //std::cout << gamma2 << std::endl;
        //std::cout << Lapse << std::endl;
        //std::cout << AddG1ShiftCorr << std::endl;
        //std::cout << G1ShiftCorr << std::endl;
        //std::cout << Shift << std::endl;
        //std::cout << Invg << std::endl;
        //std::cout << Invpsi << std::endl;
        //std::cout << Gamma1 << std::endl;
        //std::cout << Gamma2 << std::endl;
        //std::cout << TrGamma1 << std::endl;
        //std::cout << T_i << std::endl;
        //std::cout << T_I << std::endl;
        //std::cout << "DONE" << std::endl;
        GhEqnsAddToRhsImplicitLoopsTest(
                       dtpsi, dtkappa,
                       psi, dpsi, kappa, dkappa,
                       H, d4H,
                       gamma0, gamma1, gamma2, AddG1ShiftCorr, G1ShiftCorr,
                       Lapse, Shift, Invg, Invpsi,
                       Gamma1, Gamma2, TrGamma1, T_i, T_I
                       );
      }
      
        
      //tock
      #ifdef ACCEL_CUDA
      GPU::cuda_tock(GPUt1,GPUt2,GPUtime);
      GPUtime*=(1E6/N);
      //const double d= static_cast<double>(GPUtime);
      t_impl= static_cast<double>(GPUtime);
      GPU::dSynchronize();
      err = cudaGetLastError();
      REQUIRE(GPU::dError(err,errstring),errstring);

      #else
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_impl=static_cast<double>(hsttime/N);
      #endif
      
      //error check
      std::string tag("impl dtpsi");
      compareTDM(dtpsi,dtpsi_compare,tag);
      tag="impl dtkappa";
      compareTDM(dtkappa,dtkappa_compare,tag);
    
    }

    //Now write to the timing file. Also prints to screen.
    Writer.Write(sz, t_DM, t_impl);
    
    #ifdef ACCEL_CUDA
    //Clear the device memory we allocated.
    CudaMemReleaseReserved();
    #endif
  }
}

void BenchmarkRaiseIndex(std::string& fname,int maxnz, int N) {
  std::cout << "T^i_jk = g^il T_ljk- g(3,\"11\"); T(3,\"122\")" << std::endl; 
  const TensorStructure Tst(3,"122");
  const TensorStructure gst(3,"11");
  const std::string expname = fname + "Tijk_gil_Tljk";
  
  //the number of DataMeshes we move
  const int DMSz=2*Tst.Size()+gst.Size();
  const int DSz=0;
  BenchmarkWriter Writer(expname, DMSz, DSz, true);
  double t_DM=0; double t_impl=0; double t_dbl=0; 
  //initialize timers
  #ifdef ACCEL_CUDA
  cudaError_t err;
  std::string errstring;
  cudaEvent_t GPUt1;
  cudaEventCreate(&GPUt1);
  cudaEvent_t GPUt2;
  cudaEventCreate(&GPUt2);
  float GPUtime;
  #endif
  for(int Nz=1; Nz<=maxnz; Nz+=(Nz/10+10)){
    Mesh mesh(std::vector<int>({1,1,Nz}));
    const int sz = mesh.Size();

    Tensor<DataMesh> T_compare(Tst, mesh, 0.);
    // create right-hand-side data, and fill with random numbers
    Tensor<DataMesh> t(Tst, mesh, 0.); t << ran; //passing ran in here gives
                                                   //const correctness errors
    Tensor<DataMesh> g(gst, mesh, 0.); g <<ran;     //host pointer timings
                                   
    {
      Tensor<DataMesh> T(Tst, mesh, 0);

      //Run once for the check and again for sync
      for(int i=0; i<3; ++i){
        for(int j=0; j<3; ++j){
          for(int k=j; k<3; ++k){
            for(int x=0; x<mesh.Size(); ++x){
              double* T_d = T(i,j,k).Data();
              double sum = 0;
              for(int l=0;l<3;++l){
                const double* g_d = g(i,l).Data();
                const double* t_d = t(l,j,k).Data();
                sum += g_d[x] * t_d[x];
              }
              T_d[x] = sum;
            }
          }
        }
      }
      
      T_compare=T;
      
      for(int i=0; i<3; ++i){
        for(int j=0; j<3; ++j){
          for(int k=j; k<3; ++k){
            for(int x=0; x<mesh.Size(); ++x){
              double* T_d = T(i,j,k).Data();
              double sum = 0;
              for(int l=0;l<3;++l){
                const double* g_d = g(i,l).Data();
                const double* t_d = t(l,j,k).Data();
                sum += g_d[x] * t_d[x];
              }
              T_d[x] = sum;
              T_d[x] = 0;
            }
          }
        }
      }
      

      //Now actually time it
      auto t0=high_resolution_clock::now();
      
    for(int n=0; n<N; ++n){
      for(int i=0; i<3; ++i){
        for(int j=0; j<3; ++j){
          for(int k=j; k<3; ++k){
              double* T_d = T(i,j,k).Data();
              for(int l=0;l<3;++l){
                const double* g_d = g(i,l).Data();
                const double* t_d = t(l,j,k).Data();
                for(int x=0; x<mesh.Size(); ++x)
                  T_d[x] += g_d[x] * t_d[x];
              }
            }
          }
        }
      }
      
      
      //tock
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_dbl=static_cast<double>(hsttime/N);
      //save for future error check
    }
    
    //DM timings
    {
      Tensor<DataMesh> T(Tst,mesh,0);
      Tensor<DataMesh> test(Tst,mesh,0); 
     
      //run once for the error check
      for(int i=0; i<3; ++i){
        for(int j=0; j<3; ++j){
          for(int k=j; k<3; ++k){
            for(int l=0;l<3;++l){
              test(i,j,k)+=g(i,l)*t(l,j,k);
            }
          }
        }
      }
      //tick
      #ifdef ACCEL_CUDA
        GPU::dSynchronize();
        GPU::cuda_tick(GPUt1);
      #else
        auto t0=high_resolution_clock::now();
      #endif

      //timing
      for(int n=0; n<N; ++n){
        for(int i=0; i<3; ++i){
          for(int j=0; j<3; ++j){
            for(int k=j; k<3; ++k){
              for(int l=0;l<3;++l){
                T(i,j,k)+=g(i,l)*t(l,j,k);
              }
            }
          }
        }
      }
      //tock
      #ifdef ACCEL_CUDA
      GPU::cuda_tock(GPUt1,GPUt2,GPUtime);
      GPUtime*=(1E6/N);
      //const double d= static_cast<double>(GPUtime);
      t_DM= static_cast<double>(GPUtime);
      GPU::dSynchronize();
      err = cudaGetLastError();
      REQUIRE(GPU::dError(err,errstring),errstring);

      #else
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_DM=static_cast<double>(hsttime/N);
      #endif
     

      //error check
      
      std::string tag("RaiseIndex DM");
      compareTDM(test,T_compare,tag);
    
    }

    //impl timings
    {
      Tensor<DataMesh> T(Tst, mesh, 0);
      //tick
      #ifdef ACCEL_CUDA
        GPU::dSynchronize();
        GPU::cuda_tick(GPUt1);
      #else
        auto t0=high_resolution_clock::now();
      #endif
      //run once to build indices
      T(sym<1,2>(),i_,j_,k_) = Sum(l_,g(i_,l_)*t(l_,j_,k_));

      //timing
      for(int n=0; n<N; ++n){
        T(sym<1,2>(),i_,j_,k_) = Sum(l_,g(i_,l_)*t(l_,j_,k_));
      }
      //tock
      #ifdef ACCEL_CUDA
      GPU::cuda_tock(GPUt1,GPUt2,GPUtime);
      GPUtime*=(1E6/N);
      //const double d= static_cast<double>(GPUtime);
      t_impl= static_cast<double>(GPUtime);
      GPU::dSynchronize();
      err = cudaGetLastError();
      REQUIRE(GPU::dError(err,errstring),errstring);

      #else
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_impl=static_cast<double>(hsttime/N);
      #endif
      
      //error check
      std::string tag("RaiseIndex Impl");
      compareTDM(T,T_compare,tag);
    
    }

    //Now write to the timing file. Also prints to screen.
    Writer.Write(sz, t_DM, t_impl, t_dbl);
    
    #ifdef ACCEL_CUDA
    //Clear the device memory we allocated.
    CudaMemReleaseReserved();
    #endif
  }
}
void BenchmarkAiBi(std::string& fname,int maxnz, int N) {
  std::cout << "A(i) = 2*B(i);" << std::endl;
  const TensorStructure Ast(3,"1");
  const TensorStructure Bst(3,"1");
  const std::string expname = fname + "Ai_2_Bi_";
  
  //the number of DataMeshes we move
  const int DMSz=Ast.Size() + Bst.Size();
  //the number of doubles
  const int DSz = 1;
  BenchmarkWriter Writer(expname, DMSz, DSz,true);
  double t_DM=0; double t_impl=0; double t_dbl=0; 
  //initialize timers
  #ifdef ACCEL_CUDA
  cudaError_t err;
  std::string errstring;
  cudaEvent_t GPUt1;
  cudaEventCreate(&GPUt1);
  cudaEvent_t GPUt2;
  cudaEventCreate(&GPUt2);
  float GPUtime;
  #endif
  for(int Nz=1; Nz<=maxnz; Nz+=(Nz/10+10)){
    Mesh mesh(std::vector<int>({1,1,Nz}));
    const int sz = mesh.Size();

    Tensor<DataMesh> A_compare(Ast, mesh, 0.);
    // create right-hand-side data, and fill with random numbers
    Tensor<DataMesh> B(Bst, mesh, 0.); B << ran; //passing ran in here gives
                                                   //const correctness errors
    //host pointer timings
    {
      Tensor<DataMesh> A(Ast, mesh, 0);

      //Test run to sync to host
      for(int i=0; i<3; ++i){
        double* Apt = A(i).Data();
        const double* Bpt = B(i).Data();
        for(int x=0; x<sz; ++x){
          Apt[x] = 2.*Bpt[x];
          Apt[x] = 0.;
        }
      }

      //Now actually time it
      auto t0=high_resolution_clock::now();
      
      //MODIFY - replace with the correct SpEC code
      for(int n=0; n<N; ++n){
        for(int i=0; i<3; ++i){
          double* Apt = A(i).Data();
          const double* Bpt = B(i).Data();
          for(int x=0; x<sz; ++x){
            Apt[x] = 2.*Bpt[x];
          }
        }
      }
      //tock
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_dbl=static_cast<double>(hsttime/N);
      //save for future error check
      A_compare = A;
    }
    
    //DM timings
    {
      Tensor<DataMesh> A(Ast, mesh, 0);
      //tick
      #ifdef ACCEL_CUDA
        GPU::dSynchronize();
        GPU::cuda_tick(GPUt1);
      #else
        auto t0=high_resolution_clock::now();
      #endif

      //MODIFY - replace with the correct SpEC code
      for(int n=0; n<N; ++n){
        for(int i=0; i<3; ++i){
          A(i) = 2.*B(i);
        }
      }
      //tock
      #ifdef ACCEL_CUDA
      GPU::cuda_tock(GPUt1,GPUt2,GPUtime);
      GPUtime*=(1E6/N);
      //const double d= static_cast<double>(GPUtime);
      t_DM= static_cast<double>(GPUtime);
      GPU::dSynchronize();
      err = cudaGetLastError();
      REQUIRE(GPU::dError(err,errstring),errstring);

      #else
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_DM=static_cast<double>(hsttime/N);
      #endif
      
      //error check
      compareTDM(A,A_compare,expname);
    }

    //impl timings
    {
      Tensor<DataMesh> A(Ast, mesh, 0);
      //tick
      #ifdef ACCEL_CUDA
        GPU::dSynchronize();
        GPU::cuda_tick(GPUt1);
      #else
        auto t0=high_resolution_clock::now();
      #endif

      for(int n=0; n<N; ++n){
        A(i_) = 2.*B(i_);
      }
      
      //tock
      #ifdef ACCEL_CUDA
      GPU::cuda_tock(GPUt1,GPUt2,GPUtime);
      GPUtime*=(1E6/N);
      //const double d= static_cast<double>(GPUtime);
      t_impl= static_cast<double>(GPUtime);
      GPU::dSynchronize();
      err = cudaGetLastError();
      REQUIRE(GPU::dError(err,errstring),errstring);

      #else
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_impl=static_cast<double>(hsttime/N);
      #endif
      
      //error check
      compareTDM(A,A_compare,expname);
    }

    //Now write to the timing file. Also prints to screen.
    Writer.Write(sz, t_DM, t_impl, t_dbl);
    
    #ifdef ACCEL_CUDA
    //Clear the device memory we allocated.
    CudaMemReleaseReserved();
    #endif
  }
}

void BenchmarkSymmetrization(std::string& fname,int maxnz, int N) {
  std::cout << "A(i,j) = 0.5*(B(i,j)+B(j,i));" << std::endl;
  const TensorStructure Ast(3,"11");
  const TensorStructure Bst(3,"12");
  const std::string expname = fname + "Symmetrization";
  
  //the number of DataMeshes we move
  const int DMSz=Ast.Size() + 2*Bst.Size();
  //the number of doubles
  const int DSz = 1;
  BenchmarkWriter Writer(expname, DMSz, DSz,true);
  double t_DM=0; double t_impl=0; double t_dbl=0; 
  //initialize timers
  #ifdef ACCEL_CUDA
  cudaError_t err;
  std::string errstring;
  cudaEvent_t GPUt1;
  cudaEventCreate(&GPUt1);
  cudaEvent_t GPUt2;
  cudaEventCreate(&GPUt2);
  float GPUtime;
  #endif
  for(int Nz=1; Nz<=maxnz; Nz+=(Nz/10+10)){
    Mesh mesh(std::vector<int>({1,1,Nz}));
    const int sz = mesh.Size();

    Tensor<DataMesh> A_compare(Ast, mesh, 0.);
    // create right-hand-side data, and fill with random numbers
    Tensor<DataMesh> B(Bst, mesh, 0.); B << ran; //passing ran in here gives
                                                   //const correctness errors
    //host pointer timings
    {
      Tensor<DataMesh> A(Ast, mesh, 0);

      //Test run to sync to host
      for(int i=0; i<3; ++i){
        for(int j=i; j<3; ++j){
          double* Apt = A(i,j).Data();
          const double* Bpt1 = B(i,j).Data();
          const double* Bpt2 = B(j,i).Data();
          for(int x=0; x<sz; ++x){
            Apt[x] = 0.5*(Bpt1[x]+Bpt2[x]);
            Apt[x] = 0.;
          }
        }
      }

      //Now actually time it
      auto t0=high_resolution_clock::now();
      
      //MODIFY - replace with the correct SpEC code
      for(int n=0; n<N; ++n){
        for(int i=0; i<3; ++i){
          for(int j=i; j<3; ++j){
            double* Apt = A(i,j).Data();
            const double* Bpt1 = B(i,j).Data();
            const double* Bpt2 = B(j,i).Data();
            for(int x=0; x<sz; ++x){
              Apt[x] = 0.5*(Bpt1[x]+Bpt2[x]);
            }
          }
        }
      }
      //tock
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_dbl=static_cast<double>(hsttime/N);
      //save for future error check
      A_compare = A;
    }
    
    //DM timings
    {
      Tensor<DataMesh> A(Ast, mesh, 0);
      //tick
      #ifdef ACCEL_CUDA
        GPU::dSynchronize();
        GPU::cuda_tick(GPUt1);
      #else
        auto t0=high_resolution_clock::now();
      #endif

      //MODIFY - replace with the correct SpEC code
      for(int n=0; n<N; ++n){
        for(int i=0; i<3; ++i){
          for(int j=i; j<3; ++j){
            A(i,j) = 0.5*(B(i,j)+B(j,i));
          }
        }
      }
      //tock
      #ifdef ACCEL_CUDA
      GPU::cuda_tock(GPUt1,GPUt2,GPUtime);
      GPUtime*=(1E6/N);
      //const double d= static_cast<double>(GPUtime);
      t_DM= static_cast<double>(GPUtime);
      GPU::dSynchronize();
      err = cudaGetLastError();
      REQUIRE(GPU::dError(err,errstring),errstring);

      #else
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_DM=static_cast<double>(hsttime/N);
      #endif
      
      //error check
      compareTDM(A,A_compare,expname);
    }

    //impl timings
    {
      Tensor<DataMesh> A(Ast, mesh, 0);
      //tick
      #ifdef ACCEL_CUDA
        GPU::dSynchronize();
        GPU::cuda_tick(GPUt1);
      #else
        auto t0=high_resolution_clock::now();
      #endif

      for(int n=0; n<N; ++n){
        A(sym<0,1>(),i_,j_) = 0.5*(B(i_,j_)+B(j_,i_));
      }
      
      //tock
      #ifdef ACCEL_CUDA
      GPU::cuda_tock(GPUt1,GPUt2,GPUtime);
      GPUtime*=(1E6/N);
      //const double d= static_cast<double>(GPUtime);
      t_impl= static_cast<double>(GPUtime);
      GPU::dSynchronize();
      err = cudaGetLastError();
      REQUIRE(GPU::dError(err,errstring),errstring);

      #else
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_impl=static_cast<double>(hsttime/N);
      #endif
      
      //error check
      compareTDM(A,A_compare,expname);
    }

    //Now write to the timing file. Also prints to screen.
    Writer.Write(sz, t_DM, t_impl, t_dbl);
    
    #ifdef ACCEL_CUDA
    //Clear the device memory we allocated.
    CudaMemReleaseReserved();
    #endif
  }
}

void BenchmarkAijBij(std::string& fname,int maxnz, int N) {
  std::cout << "A(i,j) = 2*B(i,j);" << std::endl;
  const TensorStructure Ast(3,"12");
  const TensorStructure Bst(3,"12");
  const std::string expname = fname + "Aij_2_Bij_";
  
  //the number of DataMeshes we move
  const int DMSz=Ast.Size() + Bst.Size();
  //the number of doubles
  const int DSz = 1;
  BenchmarkWriter Writer(expname, DMSz, DSz,true);
  double t_DM=0; double t_impl=0; double t_dbl=0; 
  //initialize timers
  #ifdef ACCEL_CUDA
  cudaError_t err;
  std::string errstring;
  cudaEvent_t GPUt1;
  cudaEventCreate(&GPUt1);
  cudaEvent_t GPUt2;
  cudaEventCreate(&GPUt2);
  float GPUtime;
  #endif
  for(int Nz=1; Nz<=maxnz; Nz+=(Nz/10+10)){
    Mesh mesh(std::vector<int>({1,1,Nz}));
    const int sz = mesh.Size();

    Tensor<DataMesh> A_compare(Ast, mesh, 0.);
    // create right-hand-side data, and fill with random numbers
    Tensor<DataMesh> B(Bst, mesh, 0.); B << ran; //passing ran in here gives
                                                   //const correctness errors
    //host pointer timings
    {
      Tensor<DataMesh> A(Ast, mesh, 0);

      //Test run to sync to host
      for(int i=0; i<3; ++i){
        for(int j=0; j<3; ++j){
          double* Apt = A(i,j).Data();
          const double* Bpt = B(i,j).Data();
          for(int x=0; x<sz; ++x){
            Apt[x] = 2.*Bpt[x];
            Apt[x] = 0.;
          }
        }
      }

      //Now actually time it
      auto t0=high_resolution_clock::now();
      
      //MODIFY - replace with the correct SpEC code
      for(int n=0; n<N; ++n){
        for(int i=0; i<3; ++i){
          for(int j=0; j<3; ++j){
            double* Apt = A(i,j).Data();
            const double* Bpt = B(i,j).Data();
            for(int x=0; x<sz; ++x){
              Apt[x] = 2.*Bpt[x];
            }
          }
        }
      }
      //tock
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_dbl=static_cast<double>(hsttime/N);
      //save for future error check
      A_compare = A;
    }
    
    //DM timings
    {
      Tensor<DataMesh> A(Ast, mesh, 0);
      //tick
      #ifdef ACCEL_CUDA
        GPU::dSynchronize();
        GPU::cuda_tick(GPUt1);
      #else
        auto t0=high_resolution_clock::now();
      #endif

      //MODIFY - replace with the correct SpEC code
      for(int n=0; n<N; ++n){
        for(int i=0; i<3; ++i){
          for(int j=0; j<3; ++j){
            A(i,j) = 2.*B(i,j);
          }
        }
      }
      //tock
      #ifdef ACCEL_CUDA
      GPU::cuda_tock(GPUt1,GPUt2,GPUtime);
      GPUtime*=(1E6/N);
      //const double d= static_cast<double>(GPUtime);
      t_DM= static_cast<double>(GPUtime);
      GPU::dSynchronize();
      err = cudaGetLastError();
      REQUIRE(GPU::dError(err,errstring),errstring);

      #else
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_DM=static_cast<double>(hsttime/N);
      #endif
      
      //error check
      compareTDM(A,A_compare,expname);
    }

    //impl timings
    {
      Tensor<DataMesh> A(Ast, mesh, 0);
      //tick
      #ifdef ACCEL_CUDA
        GPU::dSynchronize();
        GPU::cuda_tick(GPUt1);
      #else
        auto t0=high_resolution_clock::now();
      #endif

      for(int n=0; n<N; ++n){
        A(i_,j_) = 2.*B(i_,j_);
      }
      
      //tock
      #ifdef ACCEL_CUDA
      GPU::cuda_tock(GPUt1,GPUt2,GPUtime);
      GPUtime*=(1E6/N);
      //const double d= static_cast<double>(GPUtime);
      t_impl= static_cast<double>(GPUtime);
      GPU::dSynchronize();
      err = cudaGetLastError();
      REQUIRE(GPU::dError(err,errstring),errstring);

      #else
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_impl=static_cast<double>(hsttime/N);
      #endif
      
      //error check
      compareTDM(A,A_compare,expname);
    }

    //Now write to the timing file. Also prints to screen.
    Writer.Write(sz, t_DM, t_impl, t_dbl);
    
    #ifdef ACCEL_CUDA
    //Clear the device memory we allocated.
    CudaMemReleaseReserved();
    #endif
  }
}

void BenchmarkAijkBijk(std::string& fname,int maxnz, int N) {
  std::cout << "A(i,j,k) = 2*B(i,j,k);" << std::endl;
  const TensorStructure Ast(3,"123");
  const TensorStructure Bst(3,"123");
  const std::string expname = fname + "Aijk_2_Bijk_";
  
  //the number of DataMeshes we move
  const int DMSz=Ast.Size() + Bst.Size();
  //the number of doubles
  const int DSz = 1;
  BenchmarkWriter Writer(expname, DMSz, DSz,true);
  double t_DM=0; double t_impl=0; double t_dbl=0; 
  //initialize timers
  #ifdef ACCEL_CUDA
  cudaError_t err;
  std::string errstring;
  cudaEvent_t GPUt1;
  cudaEventCreate(&GPUt1);
  cudaEvent_t GPUt2;
  cudaEventCreate(&GPUt2);
  float GPUtime;
  #endif
  for(int Nz=1; Nz<=maxnz; Nz+=(Nz/10+10)){
    Mesh mesh(std::vector<int>({1,1,Nz}));
    const int sz = mesh.Size();

    Tensor<DataMesh> A_compare(Ast, mesh, 0.);
    // create right-hand-side data, and fill with random numbers
    Tensor<DataMesh> B(Bst, mesh, 0.); B << ran; //passing ran in here gives
                                                   //const correctness errors
    //host pointer timings
    {
      Tensor<DataMesh> A(Ast, mesh, 0);

      //Test run to sync to host
      for(int i=0; i<3; ++i){
        for(int j=0; j<3; ++j){
          for(int k=0; k<3; ++k){
            double* Apt = A(i,j,k).Data();
            const double* Bpt = B(i,j,k).Data();
            for(int x=0; x<sz; ++x){
              Apt[x] = 2.*Bpt[x];
              Apt[x] = 0.;
            }
          }
        }
      }

      //Now actually time it
      auto t0=high_resolution_clock::now();
      
      //MODIFY - replace with the correct SpEC code
      for(int n=0; n<N; ++n){
        for(int i=0; i<3; ++i){
          for(int j=0; j<3; ++j){
            for(int k=0; k<3; ++k){
              double* Apt = A(i,j,k).Data();
              const double* Bpt = B(i,j,k).Data();
              for(int x=0; x<sz; ++x){
                Apt[x] = 2.*Bpt[x];
              }
            }
          }
        }
      }
      //tock
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_dbl=static_cast<double>(hsttime/N);
      //save for future error check
      A_compare = A;
    }
    
    //DM timings
    {
      Tensor<DataMesh> A(Ast, mesh, 0);
      //tick
      #ifdef ACCEL_CUDA
        GPU::dSynchronize();
        GPU::cuda_tick(GPUt1);
      #else
        auto t0=high_resolution_clock::now();
      #endif

      //MODIFY - replace with the correct SpEC code
      for(int n=0; n<N; ++n){
        for(int i=0; i<3; ++i){
          for(int j=0; j<3; ++j){
            for(int k=0; k<3; ++k){
              A(i,j,k) = 2.*B(i,j,k);
            }
          }
        }
      }
      //tock
      #ifdef ACCEL_CUDA
      GPU::cuda_tock(GPUt1,GPUt2,GPUtime);
      GPUtime*=(1E6/N);
      //const double d= static_cast<double>(GPUtime);
      t_DM= static_cast<double>(GPUtime);
      GPU::dSynchronize();
      err = cudaGetLastError();
      REQUIRE(GPU::dError(err,errstring),errstring);

      #else
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_DM=static_cast<double>(hsttime/N);
      #endif
      
      //error check
      compareTDM(A,A_compare,expname);
    }

    //impl timings
    {
      Tensor<DataMesh> A(Ast, mesh, 0);
      //tick
      #ifdef ACCEL_CUDA
        GPU::dSynchronize();
        GPU::cuda_tick(GPUt1);
      #else
        auto t0=high_resolution_clock::now();
      #endif

      for(int n=0; n<N; ++n){
        A(i_,j_,k_) = 2.*B(i_,j_,k_);
      }
      
      //tock
      #ifdef ACCEL_CUDA
      GPU::cuda_tock(GPUt1,GPUt2,GPUtime);
      GPUtime*=(1E6/N);
      //const double d= static_cast<double>(GPUtime);
      t_impl= static_cast<double>(GPUtime);
      GPU::dSynchronize();
      err = cudaGetLastError();
      REQUIRE(GPU::dError(err,errstring),errstring);

      #else
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_impl=static_cast<double>(hsttime/N);
      #endif
      
      //error check
      compareTDM(A,A_compare,expname);
    }

    //Now write to the timing file. Also prints to screen.
    Writer.Write(sz, t_DM, t_impl, t_dbl);
    
    #ifdef ACCEL_CUDA
    //Clear the device memory we allocated.
    CudaMemReleaseReserved();
    #endif
  }
}
//Tensor<Tensor<DataMesh> >
void BenchmarkAijkBi_jk(std::string& fname,int maxnz, int N) {
  std::cout << "A(i,j,k) = 2*B(i)(j,k);" << std::endl;
  const TensorStructure Ast(3,"123");
  const TensorStructure Bst(3,"1");
  const TensorStructure Bost(3,"12");
  const std::string expname = fname + "Aijk_2_Bi_(jk)_";
  
  //the number of DataMeshes we move
  const int DMSz=Ast.Size() + Bst.Size()*Bost.Size();
  //the number of doubles
  const int DSz = 1;
  BenchmarkWriter Writer(expname, DMSz, DSz,true);
  double t_DM=0; double t_impl=0; double t_dbl=0; 
  //initialize timers
  #ifdef ACCEL_CUDA
  cudaError_t err;
  std::string errstring;
  cudaEvent_t GPUt1;
  cudaEventCreate(&GPUt1);
  cudaEvent_t GPUt2;
  cudaEventCreate(&GPUt2);
  float GPUtime;
  #endif
  for(int Nz=1; Nz<=maxnz; Nz+=(Nz/10+10)){
    Mesh mesh(std::vector<int>({1,1,Nz}));
    const int sz = mesh.Size();
    
    Tensor<DataMesh> A_compare(Ast,mesh,0.);
    // create right-hand-side data, and fill with random numbers
    Tensor<DataMesh> Bin(Bost, mesh, 0.); Bin << ran; //passing ran in here gives
                                                   //const correctness errors
                                                   
    Tensor<Tensor<DataMesh> > B(Bst, Bin);
    //host pointer timings
    {
      
      Tensor<DataMesh> A(Ast,mesh,0.);
      //Test run to sync to host
      for(int i=0; i<3; ++i){
        for(int j=0; j<3; ++j){
          for(int k=0; k<3; ++k){
            double* Apt = A(i,j,k).Data();
            const double* Bpt = B(i)(j,k).Data();
            for(int x=0; x<sz; ++x){
              Apt[x] = 2.*Bpt[x];
              Apt[x] = 0.;
            }
          }
        }
      }

      //Now actually time it
      auto t0=high_resolution_clock::now();
      
      //MODIFY - replace with the correct SpEC code
      for(int n=0; n<N; ++n){
        for(int i=0; i<3; ++i){
          for(int j=0; j<3; ++j){
            for(int k=0; k<3; ++k){
              double* Apt = A(i,j,k).Data();
              const double* Bpt = B(i)(j,k).Data();
              for(int x=0; x<sz; ++x){
                Apt[x] = 2.*Bpt[x];
              }
            }
          }
        }
      }
      //tock
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_dbl=static_cast<double>(hsttime/N);
      //save for future error check
      A_compare = A;
    }
    
    //DM timings
    {
      Tensor<DataMesh> A(Ast,mesh,0.);
      //tick
      #ifdef ACCEL_CUDA
        GPU::dSynchronize();
        GPU::cuda_tick(GPUt1);
      #else
        auto t0=high_resolution_clock::now();
      #endif

      //MODIFY - replace with the correct SpEC code
      for(int n=0; n<N; ++n){
        for(int i=0; i<3; ++i){
          for(int j=0; j<3; ++j){
            for(int k=0; k<3; ++k){
              A(i,j,k) = 2.*B(i)(j,k);
            }
          }
        }
      }
      //tock
      #ifdef ACCEL_CUDA
      GPU::cuda_tock(GPUt1,GPUt2,GPUtime);
      GPUtime*=(1E6/N);
      //const double d= static_cast<double>(GPUtime);
      t_DM= static_cast<double>(GPUtime);
      GPU::dSynchronize();
      err = cudaGetLastError();
      REQUIRE(GPU::dError(err,errstring),errstring);

      #else
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_DM=static_cast<double>(hsttime/N);
      #endif
      
      //error check
      compareTDM(A,A_compare,expname);
    }

    //impl timings
    {
      Tensor<DataMesh> A(Ast,mesh,0.);
      //tick
      #ifdef ACCEL_CUDA
        GPU::dSynchronize();
        GPU::cuda_tick(GPUt1);
      #else
        auto t0=high_resolution_clock::now();
      #endif

      for(int n=0; n<N; ++n){
        A(i_,j_,k_) = 2.*B(i_)(j_,k_);
      }
      
      //tock
      #ifdef ACCEL_CUDA
      GPU::cuda_tock(GPUt1,GPUt2,GPUtime);
      GPUtime*=(1E6/N);
      //const double d= static_cast<double>(GPUtime);
      t_impl= static_cast<double>(GPUtime);
      GPU::dSynchronize();
      err = cudaGetLastError();
      REQUIRE(GPU::dError(err,errstring),errstring);

      #else
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_impl=static_cast<double>(hsttime/N);
      #endif
      
      //error check
      compareTDM(A,A_compare,expname);
    }

    //Now write to the timing file. Also prints to screen.
    Writer.Write(sz, t_DM, t_impl, t_dbl);
    
    #ifdef ACCEL_CUDA
    //Clear the device memory we allocated.
    CudaMemReleaseReserved();
    #endif
  }
}

//this one has an offset index
void BenchmarkOffsetRaiseIndex(std::string& fname,int maxnz, int N) {
  std::cout << "T(i+1,a,b) = g(i,j)*t(j+1,a,b);" << std::endl;
  const TensorStructure Tst(4,"122");
  const TensorStructure gst(3,"11");
  const std::string expname = fname + "OffsetRaiseIndex";
  
  //the number of DataMeshes we move
  const int DMSz=2*Tst.Size()+gst.Size();
  const int DSz=0;
  BenchmarkWriter Writer(expname, DMSz, DSz, true);
  double t_DM=0; double t_impl=0; double t_dbl=0; 
  //initialize timers
  #ifdef ACCEL_CUDA
  cudaError_t err;
  std::string errstring;
  cudaEvent_t GPUt1;
  cudaEventCreate(&GPUt1);
  cudaEvent_t GPUt2;
  cudaEventCreate(&GPUt2);
  float GPUtime;
  #endif
  for(int Nz=1; Nz<=maxnz; Nz+=(Nz/10+10)){
    Mesh mesh(std::vector<int>({1,1,Nz}));
    const int sz = mesh.Size();

    Tensor<DataMesh> T_compare(Tst, mesh, 0.);
    // create right-hand-side data, and fill with random numbers
    Tensor<DataMesh> t(Tst, mesh, 0.); t << ran; //passing ran in here gives
                                                   //const correctness errors
    Tensor<DataMesh> g(gst, mesh, 0.); g <<ran;     //host pointer timings
                                   
    {
      Tensor<DataMesh> T(Tst, mesh, 0);

      //Run once for the check and again for sync
      for(int i=0; i<3; ++i){
        for(int a=0; a<4; ++a){
          for(int b=a; b<4; ++b){
            for(int x=0; x<mesh.Size(); ++x){
              double* T_d = T(i+1,a,b).Data();
              double sum = 0;
              for(int l=0;l<3;++l){
                const double* g_d = g(i,l).Data();
                const double* t_d = t(l+1,a,b).Data();
                sum += g_d[x] * t_d[x];
              }
              T_d[x] = sum;
            }
          }
        }
      }
      
      T_compare=T;
      
      for(int i=0; i<3; ++i){
        for(int a=0; a<4; ++a){
          for(int b=a; b<4; ++b){
            for(int x=0; x<mesh.Size(); ++x){
              double* T_d = T(i+1,a,b).Data();
              double sum = 0;
              for(int l=0;l<3;++l){
                const double* g_d = g(i,l).Data();
                const double* t_d = t(l+1,a,b).Data();
                sum += g_d[x] * t_d[x];
              }
              T_d[x] = sum;
              T_d[x] = 0;
            }
          }
        }
      }

      //Now actually time it
      auto t0=high_resolution_clock::now();
      
    for(int n=0; n<N; ++n){
      for(int i=0; i<3; ++i){
        for(int a=0; a<4; ++a){
          for(int b=a; b<4; ++b){
              double* T_d = T(i+1,a,b).Data();
              for(int l=0;l<3;++l){
                const double* g_d = g(i,l).Data();
                const double* t_d = t(l+1,a,b).Data();
                for(int x=0; x<mesh.Size(); ++x)
                  T_d[x] += g_d[x] * t_d[x];
              }
            }
          }
        }
      }
      
      
      //tock
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_dbl=static_cast<double>(hsttime/N);
      //save for future error check
    }
    
    //DM timings
    {
      Tensor<DataMesh> T(Tst,mesh,0);
      Tensor<DataMesh> test(Tst,mesh,0); 
     
      //run once for the error check
      for(int i=0; i<3; ++i){
        for(int a=0; a<4; ++a){
          for(int b=a; b<4; ++b){
            for(int l=0;l<3;++l){
              test(i+1,a,b)+=g(i,l)*t(l+1,a,b);
            }
          }
        }
      }
      //tick
      #ifdef ACCEL_CUDA
        GPU::dSynchronize();
        GPU::cuda_tick(GPUt1);
      #else
        auto t0=high_resolution_clock::now();
      #endif

      //timing
      for(int n=0; n<N; ++n){
        for(int i=0; i<3; ++i){
          for(int a=0; a<4; ++a){
            for(int b=a; b<4; ++b){
              for(int l=0;l<3;++l){
                T(i+1,a,b)+=g(i,l)*t(l+1,a,b);
              }
            }
          }
        }
      }
      //tock
      #ifdef ACCEL_CUDA
      GPU::cuda_tock(GPUt1,GPUt2,GPUtime);
      GPUtime*=(1E6/N);
      //const double d= static_cast<double>(GPUtime);
      t_DM= static_cast<double>(GPUtime);
      GPU::dSynchronize();
      err = cudaGetLastError();
      REQUIRE(GPU::dError(err,errstring),errstring);

      #else
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_DM=static_cast<double>(hsttime/N);
      #endif
     

      //error check
      
      std::string tag("RaiseIndex DM");
      compareTDM(test,T_compare,tag);
    
    }

    //impl timings
    {
      Tensor<DataMesh> T(Tst, mesh, 0);
      //tick
      #ifdef ACCEL_CUDA
        GPU::dSynchronize();
        GPU::cuda_tick(GPUt1);
      #else
        auto t0=high_resolution_clock::now();
      #endif
      //run once to build indices
      T(sym<1,2>(),i_+1,a_,b_) = Sum(l_,g(i_,l_)*t(l_+1,a_,b_));

      //timing
      for(int n=0; n<N; ++n){
        T(sym<1,2>(),i_+1,a_,b_) = Sum(l_,g(i_,l_)*t(l_+1,a_,b_));
      }
      //tock
      #ifdef ACCEL_CUDA
      GPU::cuda_tock(GPUt1,GPUt2,GPUtime);
      GPUtime*=(1E6/N);
      //const double d= static_cast<double>(GPUtime);
      t_impl= static_cast<double>(GPUtime);
      GPU::dSynchronize();
      err = cudaGetLastError();
      REQUIRE(GPU::dError(err,errstring),errstring);

      #else
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_impl=static_cast<double>(hsttime/N);
      #endif
      
      //error check
      std::string tag("RaiseIndex Impl");
      compareTDM(T,T_compare,tag);
    
    }

    //Now write to the timing file. Also prints to screen.
    Writer.Write(sz, t_DM, t_impl, t_dbl);
    
    #ifdef ACCEL_CUDA
    //Clear the device memory we allocated.
    CudaMemReleaseReserved();
    #endif
  }
}

void BenchmarkFixedIndexDoubleSum(std::string& fname,int maxnz, int N) {
  std::cout << "T(0,a,b) = -2.0*g(a,c,d)*g(b,d,c);" << std::endl;
  const TensorStructure Tst(4,"211");
  const TensorStructure gst(4,"123");
  const std::string expname = fname + "Tab_Gacd_Gbdc_DoubleSum_";
  
  //the number of DataMeshes we move
  const int DMSz=10+2*gst.Size();
  const int DSz=1;
  BenchmarkWriter Writer(expname, DMSz, DSz, true);
  double t_DM=0; double t_impl=0; double t_dbl=0; 
  //initialize timers
  #ifdef ACCEL_CUDA
  cudaError_t err;
  std::string errstring;
  cudaEvent_t GPUt1;
  cudaEventCreate(&GPUt1);
  cudaEvent_t GPUt2;
  cudaEventCreate(&GPUt2);
  float GPUtime;
  #endif
  for(int Nz=1; Nz<=maxnz; Nz+=(Nz/10+10)){
    Mesh mesh(std::vector<int>({1,1,Nz}));
    const int sz = mesh.Size();

    Tensor<DataMesh> T_compare(Tst, mesh, 0.);
    // create right-hand-side data, and fill with random numbers
    Tensor<DataMesh> t(Tst, mesh, 0.); t << ran; //passing ran in here gives
                                                   //const correctness errors
    Tensor<DataMesh> g(gst, mesh, 0.); g <<ran;     //host pointer timings
                                   
    {
      //get the check
      {
        Tensor<DataMesh> T(Tst, mesh, 0);
        for(int a=0; a<4; ++a){
          for(int b=a; b<4; ++b){
            double* T_d = T(0,a,b).Data();
            for(int c=0; c<4; ++c){
              for(int d=0; d<4; ++d){
                const double* g1_d = g(a,c,d).Data();
                const double* g2_d = g(b,d,c).Data();
                for(int x=0; x<mesh.Size(); ++x)
                  T_d[x] += -2*g1_d[x]*g2_d[x];
              }
            }
          }
        }
        T_compare=T;
      }
      //sync with host
      Tensor<DataMesh> T(Tst, mesh, 0);
      for(int a=0; a<4; ++a){
        for(int b=a; b<4; ++b){
          double* T_d = T(0,a,b).Data();
          for(int c=0; c<4; ++c){
            for(int d=0; d<4; ++d){
              const double* g1_d = g(a,c,d).Data();
              const double* g2_d = g(b,d,c).Data();
              for(int x=0; x<mesh.Size(); ++x){
                T_d[x] += -2*g1_d[x]*g2_d[x];
                T_d[x] = 0;
              }
            }
          }
        }
      }
      //Now actually time it
      auto t0=high_resolution_clock::now();
      
      for(int n=0; n<N; ++n){
        for(int a=0; a<4; ++a){
          for(int b=a; b<4; ++b){
            double* T_d = T(0,a,b).Data();
            for(int c=0; c<4; ++c){
              for(int d=0; d<4; ++d){
                const double* g1_d = g(a,c,d).Data();
                const double* g2_d = g(b,d,c).Data();
                for(int x=0; x<mesh.Size(); ++x)
                  T_d[x] += -2*g1_d[x]*g2_d[x];
              }
            }
          }
        }
      }
      
      
      //tock
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_dbl=static_cast<double>(hsttime/N);
      //save for future error check
    }
    
    //DM timings
    {
      Tensor<DataMesh> T(Tst,mesh,0);
      Tensor<DataMesh> test(Tst,mesh,0); 
     
      //run once for the error check
      for(int a=0; a<4; ++a){
        for(int b=a; b<4; ++b){
          for(int c=0; c<4; ++c){
            for(int d=0; d<4; ++d){
              test(0,a,b) += -2.0*g(a,c,d)*g(b,d,c);
            }
          }
        }
      }
      
      
      //tick
      #ifdef ACCEL_CUDA
        GPU::dSynchronize();
        GPU::cuda_tick(GPUt1);
      #else
        auto t0=high_resolution_clock::now();
      #endif

      //timing
      for(int n=0; n<N; ++n){
        for(int a=0; a<4; ++a){
          for(int b=a; b<4; ++b){
            for(int c=0; c<4; ++c){
              for(int d=0; d<4; ++d){
                T(0,a,b) += -2.0*g(a,c,d)*g(b,d,c);
              }
            }
          }
        }
      }
      //tock
      #ifdef ACCEL_CUDA
      GPU::cuda_tock(GPUt1,GPUt2,GPUtime);
      GPUtime*=(1E6/N);
      //const double d= static_cast<double>(GPUtime);
      t_DM= static_cast<double>(GPUtime);
      GPU::dSynchronize();
      err = cudaGetLastError();
      REQUIRE(GPU::dError(err,errstring),errstring);

      #else
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_DM=static_cast<double>(hsttime/N);
      #endif
     

      //error check
      
      std::string tag("RaiseIndex DM");
      compareTDM(test,T_compare,tag);
    
    }

    //impl timings
    {
      Tensor<DataMesh> T(Tst, mesh, 0);
      //tick
      #ifdef ACCEL_CUDA
        GPU::dSynchronize();
        GPU::cuda_tick(GPUt1);
      #else
        auto t0=high_resolution_clock::now();
      #endif
      //run once to build indices
      T(sym<1,2>(),0,a_,b_) =
        -2.0*Sum(c_,Sum(d_,g(a_,c_,d_)*g(b_,d_,c_)));
      //timing
      for(int n=0; n<N; ++n){
        T(sym<1,2>(),0,a_,b_) =
          -2.0*Sum(c_,Sum(d_,g(a_,c_,d_)*g(b_,d_,c_)));
      }
      //tock
      #ifdef ACCEL_CUDA
      GPU::cuda_tock(GPUt1,GPUt2,GPUtime);
      GPUtime*=(1E6/N);
      //const double d= static_cast<double>(GPUtime);
      t_impl= static_cast<double>(GPUtime);
      GPU::dSynchronize();
      err = cudaGetLastError();
      REQUIRE(GPU::dError(err,errstring),errstring);

      #else
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_impl=static_cast<double>(hsttime/N);
      #endif
      
      //error check
      std::string tag("RaiseIndex Impl");
      compareTDM(T,T_compare,tag);
    
    }

    //Now write to the timing file. Also prints to screen.
    Writer.Write(sz, t_DM, t_impl, t_dbl);
    
    #ifdef ACCEL_CUDA
    //Clear the device memory we allocated.
    CudaMemReleaseReserved();
    #endif
  }
}

void BenchmarkDoubleSum(std::string& fname,int maxnz, int N) {
  std::cout << "T(a,b) = -2.0*g(a,c,d)*g(b,d,c);" << std::endl;
  const TensorStructure Tst(4,"11");
  const TensorStructure gst(4,"123");
  const std::string expname = fname + "Tab_Gacd_Gbdc_DoubleSum_";
  
  //the number of DataMeshes we move
  const int DMSz=Tst.Size()+2*gst.Size();
  const int DSz=1;
  BenchmarkWriter Writer(expname, DMSz, DSz, true);
  double t_DM=0; double t_impl=0; double t_dbl=0; 
  //initialize timers
  #ifdef ACCEL_CUDA
  cudaError_t err;
  std::string errstring;
  cudaEvent_t GPUt1;
  cudaEventCreate(&GPUt1);
  cudaEvent_t GPUt2;
  cudaEventCreate(&GPUt2);
  float GPUtime;
  #endif
  for(int Nz=1; Nz<=maxnz; Nz+=(Nz/10+10)){
    Mesh mesh(std::vector<int>({1,1,Nz}));
    const int sz = mesh.Size();

    Tensor<DataMesh> T_compare(Tst, mesh, 0.);
    // create right-hand-side data, and fill with random numbers
    Tensor<DataMesh> t(Tst, mesh, 0.); t << ran; //passing ran in here gives
                                                   //const correctness errors
    Tensor<DataMesh> g(gst, mesh, 0.); g <<ran;     //host pointer timings
                                   
    {
      //get the check
      {
        Tensor<DataMesh> T(Tst, mesh, 0);
        for(int a=0; a<4; ++a){
          for(int b=a; b<4; ++b){
            double* T_d = T(a,b).Data();
            for(int c=0; c<4; ++c){
              for(int d=0; d<4; ++d){
                const double* g1_d = g(a,c,d).Data();
                const double* g2_d = g(b,d,c).Data();
                for(int x=0; x<mesh.Size(); ++x)
                  T_d[x] += -2*g1_d[x]*g2_d[x];
              }
            }
          }
        }
        T_compare=T;
      }
      //sync with host
      Tensor<DataMesh> T(Tst, mesh, 0);
      for(int a=0; a<4; ++a){
        for(int b=a; b<4; ++b){
          double* T_d = T(a,b).Data();
          for(int c=0; c<4; ++c){
            for(int d=0; d<4; ++d){
              const double* g1_d = g(a,c,d).Data();
              const double* g2_d = g(b,d,c).Data();
              for(int x=0; x<mesh.Size(); ++x){
                T_d[x] += -2*g1_d[x]*g2_d[x];
                T_d[x] = 0;
              }
            }
          }
        }
      }
      //Now actually time it
      auto t0=high_resolution_clock::now();
      
      for(int n=0; n<N; ++n){
        for(int a=0; a<4; ++a){
          for(int b=a; b<4; ++b){
            double* T_d = T(a,b).Data();
            for(int c=0; c<4; ++c){
              for(int d=0; d<4; ++d){
                const double* g1_d = g(a,c,d).Data();
                const double* g2_d = g(b,d,c).Data();
                for(int x=0; x<mesh.Size(); ++x)
                  T_d[x] += -2*g1_d[x]*g2_d[x];
              }
            }
          }
        }
      }
      
      
      //tock
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_dbl=static_cast<double>(hsttime/N);
      //save for future error check
    }
    
    //DM timings
    {
      Tensor<DataMesh> T(Tst,mesh,0);
      Tensor<DataMesh> test(Tst,mesh,0); 
     
      //run once for the error check
      for(int a=0; a<4; ++a){
        for(int b=a; b<4; ++b){
          for(int c=0; c<4; ++c){
            for(int d=0; d<4; ++d){
              test(a,b) += -2.0*g(a,c,d)*g(b,d,c);
            }
          }
        }
      }
      
      
      //tick
      #ifdef ACCEL_CUDA
        GPU::dSynchronize();
        GPU::cuda_tick(GPUt1);
      #else
        auto t0=high_resolution_clock::now();
      #endif

      //timing
      for(int n=0; n<N; ++n){
        for(int a=0; a<4; ++a){
          for(int b=a; b<4; ++b){
            for(int c=0; c<4; ++c){
              for(int d=0; d<4; ++d){
                T(a,b) += -2.0*g(a,c,d)*g(b,d,c);
              }
            }
          }
        }
      }
      //tock
      #ifdef ACCEL_CUDA
      GPU::cuda_tock(GPUt1,GPUt2,GPUtime);
      GPUtime*=(1E6/N);
      //const double d= static_cast<double>(GPUtime);
      t_DM= static_cast<double>(GPUtime);
      GPU::dSynchronize();
      err = cudaGetLastError();
      REQUIRE(GPU::dError(err,errstring),errstring);

      #else
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_DM=static_cast<double>(hsttime/N);
      #endif
     

      //error check
      
      std::string tag("RaiseIndex DM");
      compareTDM(test,T_compare,tag);
    
    }

    //impl timings
    {
      Tensor<DataMesh> T(Tst, mesh, 0);
      //tick
      #ifdef ACCEL_CUDA
        GPU::dSynchronize();
        GPU::cuda_tick(GPUt1);
      #else
        auto t0=high_resolution_clock::now();
      #endif
      //run once to build indices
      T(sym<0,1>(),a_,b_) =
        -2.0*Sum(c_,Sum(d_,g(a_,c_,d_)*g(b_,d_,c_)));
      //timing
      for(int n=0; n<N; ++n){
        T(sym<0,1>(),a_,b_) =
          -2.0*Sum(c_,Sum(d_,g(a_,c_,d_)*g(b_,d_,c_)));
      }
      //tock
      #ifdef ACCEL_CUDA
      GPU::cuda_tock(GPUt1,GPUt2,GPUtime);
      GPUtime*=(1E6/N);
      //const double d= static_cast<double>(GPUtime);
      t_impl= static_cast<double>(GPUtime);
      GPU::dSynchronize();
      err = cudaGetLastError();
      REQUIRE(GPU::dError(err,errstring),errstring);

      #else
      auto t1=high_resolution_clock::now();
      auto hsttime = duration_cast<nanoseconds>(t1-t0).count();
      //const double dtime = static_cast<double>(hsttime/N);
      t_impl=static_cast<double>(hsttime/N);
      #endif
      
      //error check
      std::string tag("RaiseIndex Impl");
      compareTDM(T,T_compare,tag);
    
    }

    //Now write to the timing file. Also prints to screen.
    Writer.Write(sz, t_DM, t_impl, t_dbl);
    
    #ifdef ACCEL_CUDA
    //Clear the device memory we allocated.
    CudaMemReleaseReserved();
    #endif
  }
}


int ProductionTensorLoops() {
  UtilsForTesting::SetEps(1e-12);
  //std::string path("/scratch/adlewis/LoopTimings/");
  std::string path("./timings/");
  const double sz = 100;
  const double inc = 5;
  BenchmarkGHEqns(path,sz,inc);
  //BenchmarkChristoffelSymbols2ndKind(path, sz,inc);
  //BenchmarkSymmetrization(path,sz,inc);
  //Benchmark2NgPlusShiftShift(path,sz,inc); 
  //BenchmarkAiBi(path,sz,inc);
  //BenchmarkAijBij(path,sz,inc);
  //BenchmarkAijkBijk(path,sz,inc);
  //BenchmarkAijkBi_jk(path,sz,inc);
  //BenchmarkRaiseIndex(path, sz, inc);
  //BenchmarkOffsetRaiseIndex(path, sz, inc);
  //BenchmarkDoubleSum(path, sz, inc);
  //BenchmarkFixedIndexDoubleSum(path, sz, inc);
  return UtilsForTesting::NumberOfTestsFailed();
}
