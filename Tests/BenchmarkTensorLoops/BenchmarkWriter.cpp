#include "BenchmarkWriter.hpp"
#include <iostream>
#include "Require.hpp"
//#include "GPU/cuda_wrapper.hpp"
#include <unistd.h> //for gethostname using intel
//Represents a file containing raw timings and bandwidths for one tensor
//expression.
BenchmarkWriter::BenchmarkWriter(const std::string& fname, const int expSz,
        const int dSz/*default 0*/,
        const bool host_pointers/*default false*/)
      :mFname(filename(fname)), mSz(expSz), mDsz(dSz), 
       mHstPntrs(host_pointers), mFile()
    {
      std::ostringstream header;
      
      //Make header string for Xmgrblock.
      header << "# [1] = Gridsize\n"
             << "# [2] = timing(DM,ns)\n"
             << "# [3] = BW_eff (DM, Gbps)\n"
             << "# [4] = timing(TLoops, ns)\n"
             << "# [5] = BW_eff (TLoops, Gbps)\n";
      
      //You only want these ones if double* equivalents of the expressions exist.
      if(mHstPntrs)
        header << "#[6] = timing(double* ptr)\n"
               << "#[7] = BW_eff(double* ptr, Gbps)\n\n";

      mFile.open(mFname);
      mFile << header.str();  
      mFile.close();
    }

    //Writes to the files. Feed it a gridsize, then timing for the DM,
    //implicit loops, and double* pointers expressions (if the latter are used).
    //The timings should be in nanoseconds.
    void BenchmarkWriter::Write(const int gridsz, const double DMt, 
      const double impl,const double dbl/*default -1*/)
    {
      std::ostringstream strm;
      if(!mHstPntrs) REQUIRE(-1==dbl, "Attempted to write host pointer times"<<
          "without specifying they would be used in constructor.");
      if(mHstPntrs) REQUIRE(dbl>=0, "Host pointer time should be nonnegative"
          <<"(did you construct for host pointers when you didn't want them?)");
      const double DMbw = bandwidth(gridsz, DMt);
      const double implbw = bandwidth(gridsz, impl);
      const double dblbw = bandwidth(gridsz, dbl);

      strm << gridsz << "\t" << DMt << "\t" << DMbw << "\t"
                              << impl << "\t" << implbw << "\t";
      if(mHstPntrs) strm << dbl << "\t" << dblbw;
      strm << "\n";
      std::cout << strm.str();
      
      mFile.open(mFname, std::ios::app);
      mFile << strm.str();
      mFile.close();
    }
  
    //private: 
    //String specifying which SpEC 'COMPILE_VARIANT' flag was used for this 
    //benchmark.
    std::string BenchmarkWriter::accelflag(){
      std::ostringstream out;
      #ifdef ACCEL_CUDA
      out << "ACCEL_CUDA";
      #elif defined ACCEL_CPU
      out << "ACCEL_CPU";
      #else
        #ifndef NONACCEL
          std::cerr << "Warning:: running implict loop code without defining " 
              << "'COMPILE_VARIANT = ACCEL_CUDA/ACCEL_CPU/NONACCEL' " 
              << "may cause unexpected behaviour." << std::endl;
          out << "NONACCEL";
        #endif
      #endif
      return out.str();
    }
   
    //The C++ compiler version (clang, intel, or GNU)
    std::string BenchmarkWriter::compiler(){
      std::ostringstream out;
      #if defined(__clang__)
        out << "CLANG"<<__clang_major__ << __clang_minor__ 
            << __clang_patchlevel__;
      #elif defined(__ICC) || defined(__INTEL_COMPILER)
        out << "INTEL"<<__INTEL_COMPILER;
      #elif defined(__GNUC__) || defined(__GNUG__)
        out << "GCC" << __GNUC__<< __GNUC_MINOR__ <<__GNUC_PATCHLEVEL__;
      #endif
        return out.str(); 
      
    }
   
    //The CUDA version. If not using ACCEL_CUDA, an empty string.
    std::string BenchmarkWriter::cudaversion(){
      #ifndef ACCEL_CUDA
      return "";
      #else
      std::ostringstream out;
      int version; 
      std::string errstring;
      bool error = GPU::RuntimeGetVersion(&version,errstring);
      REQUIRE(error, errstring); 
      out << version;
      return out.str();
      #endif
    }
   
    //the name of the output file
    std::string BenchmarkWriter::filename(const std::string& fname){
      char hostname[1024];
      int error = gethostname(hostname,1024);
      REQUIRE(error==0, "gethostname failed with code " << error);
      std::ostringstream fnamestream;
      fnamestream << fname << hostname << "_" << accelflag() << 
         cudaversion() << "_" << compiler() << ".dat";
      return fnamestream.str();
    }
