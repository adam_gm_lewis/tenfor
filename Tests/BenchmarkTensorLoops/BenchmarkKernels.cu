#include "Utils/Tensor/AutoExpressions/CUDA_binary_ops.cu"
#include "Utils/ErrorHandling/Require.hpp"
#include <string>
#include "GPU/cuda_wrapper.hpp"
#include <stdio.h>
#include <iostream>

template<int n1>
struct TIndTemp1{
  int idx(){ return n1; }
};

template<>
struct TIndTemp1<0>{
  int idx(){ return 0; }
};

template<>
struct TIndTemp1<1>{
  int idx(){ return 1; }
};
template<>
struct TIndTemp1<2>{
  int idx(){ return 2; }
};
template<>
struct TIndTemp1<3>{
  int idx(){ return 3; }
};

__device__ inline int TIndex_T(int n1, int n2){
  switch (n2){
    case 0:
      switch (n1){
        case 0:
          return 0; 
        case 1:
          return 1;
        case 2:
          return 2;
        case 3:
          return 3; 
        default:
          return -1;
      }
    case 1:
      switch (n1){
        case 0:
          return 4; 
        case 1:
          return 5; 
        case 2:
          return 6; 
        case 3:
          return 7; 
        default:
          return -1;
      }
      
    case 2:
      switch (n1){
        case 0:
          return 8; 
        case 1:
          return 9; 
        case 2:
          return 10; 
        case 3:
          return 11; 
        default:
          return -1;
      }
      
    case 3:
      switch (n1){
        case 0:
          return 12; 
        case 1:
          return 13; 
        case 2:
          return 14; 
        case 3:
          return 15; 
        default:
          return -1;
      }
    default:
      return -1;
  }
}


__global__
void GPU_Tensor_ParallelShared(
    const int sz, const double** g, 
    const double** Kdn,  double** Kup ){
  const int idx=blockIdx.x*blockDim.x+threadIdx.x;
  
  __shared__ double Sum;
  if(idx<sz){
    const int i = threadIdx.y;
    const int j = blockIdx.y;
    const int l = threadIdx.z;
    if(l==0) Sum=0;
    __syncthreads();
    double tmp = g[TInd(i,l)][idx];
    for(int k=j;k<3;++k){
      tmp *= Kdn[TInd(l,j,k)][idx];
      Sum += tmp;
      __syncthreads();
      if(l==0) Kup[TInd(i,j,k)][idx]=Sum;
    }
    
  }
}

__global__
void GPU_Tensor_ParallelTensors(
    const int sz, const double** __restrict__ g, 
    const double** __restrict__ Kdn,  double** __restrict__ Kup ){
  const int idx=blockIdx.x*blockDim.x+threadIdx.x;
  if(idx<sz){
    const int i = threadIdx.y;
    const int j = blockIdx.y;
    double greg[3];
    for(int k=j;k<3;++k){
        
        greg[0] = g[TInd(i,0)][idx];
        greg[1] = g[TInd(i,1)][idx];
        greg[2] = g[TInd(i,2)][idx];
        Kup[TInd(i,j,k)][idx] = 
           greg[0] * Kdn[TInd(0,j,k)][idx]
          +greg[1] * Kdn[TInd(1,j,k)][idx]
          +greg[2] * Kdn[TInd(2,j,k)][idx];
      }
    
  }

}
/*
__global__
void GPU_Tensor_ParallelTensorsShareLoad(
    const int sz, const double** g, 
    const double** Kdn,  double** Kup ){
  const int idx=blockIdx.x*blockDim.x+threadIdx.x;
  const int i = threadIdx.z;
  const int j = blockIdx.y;
  const int k = threadIdx.y;
  //__shared__ int KdnI[3];
  __shared__ double Kdnsh[3];
  
  
  if(idx< sz && k >= j){
      Kdnsh[i] = Kdn[TInd(i,j,k)][idx];
      __syncthreads();
       
      Kup[TInd(i,j,k)][idx] = 
        g[TInd(i,0)][idx]*Kdnsh[0]
        +g[TInd(i,1)][idx]*Kdnsh[1]
        +g[TInd(i,2)][idx]*Kdnsh[2];
  }
}
*/

__global__
void GPU_Tensor_ParallelTensorsAll(
    const int sz, const double** g, 
    const double** Kdn,  double** Kup ){
  const int idx=blockIdx.x*blockDim.x+threadIdx.x;
  const int i = blockIdx.z;
  const int j = blockIdx.y;
  const int k = threadIdx.y;
  
  if(idx< sz && k >= j){
    Kup[TInd(i,j,k)][idx] = 
      g[TInd(i,0)][idx]*Kdn[TInd(0,j,k)][idx]
      +g[TInd(i,1)][idx]*Kdn[TInd(1,j,k)][idx]
      +g[TInd(i,2)][idx]*Kdn[TInd(2,j,k)][idx];
  }
}

__global__
void GPU_Tensor_ParallelTensorsAllShared(
    const int sz, const double** g, 
    const double** Kdn,  double** Kup ){
  const int idx=blockIdx.x*blockDim.x+threadIdx.x;
  const int i = blockIdx.z;
  const int l = threadIdx.z;
  const int j = blockIdx.y;
  const int k = threadIdx.y;
  
  __shared__ double Sum;
  if (l==0) Sum=0;
  __syncthreads();
  if(idx< sz && k >= j){
    Sum+= g[TInd(i,l)][idx]*Kdn[TInd(l,j,k)][idx];
    __syncthreads();
    if(l==0) Kup[TInd(i,j,k)][idx]=Sum;
  }
    
}

__global__
void GPU_Tensor_ParallelTensors_Only_i(
    const int sz, const double** g, 
    const double** Kdn,  double** Kup ){
  const int idx=blockIdx.x*blockDim.x+threadIdx.x;
  if(idx<sz){
    const int i = threadIdx.y;
    //const double g0 = g[TInd(i,0)][idx];
    //const double g1 = g[TInd(i,1)][idx];
    //const double g2 = g[TInd(i,2)][idx];
    for(int j=0; j<3; ++j){
      for(int k=j;k<3;++k){
        
        Kup[TInd(i,j,k)][idx] = 
           g[TInd(i,0)][idx] * Kdn[TInd(0,j,k)][idx]
          +g[TInd(i,1)][idx] * Kdn[TInd(1,j,k)][idx]
          +g[TInd(i,2)][idx] * Kdn[TInd(2,j,k)][idx];
        
        /* 
        Kup[TInd(i,j,k)][idx] = 
           g0 * Kdn[TInd(0,j,k)][idx]
          +g1 * Kdn[TInd(1,j,k)][idx]
          +g2 * Kdn[TInd(2,j,k)][idx];
        */
      }
    }
  }

}

__global__ 
void GPU_Tensor_KernelSharedSum(const int sz, const double** g,  
      const double** Kdn,  double** Kup ){

  
  __shared__ double Sum; 
  int idx = blockIdx.x*blockDim.x+threadIdx.x;
  if(idx<sz){
    int l = threadIdx.y;
    if (l==0) Sum=0;
    __syncthreads();
    double tmp;
    for(int i=0; i<3; ++i){
      tmp = g[TInd(i,l)][idx];
      for(int j=0; j<3; ++j){
        for(int k=j; k<3; ++k){
          tmp *= Kdn[TInd(l,j,k)][idx];    
          Sum+=tmp;
          __syncthreads();
          if(l==0) Kup[TInd(i,j,k)][idx]=Sum;
        }

      }
    }
  }


}

__global__
void GPU_Tensor_BackwardsIndexing(
    const int sz, const double** g,  
      const double** Kdn,  double** Kup ){
  const int idx = blockIdx.x*blockDim.x+threadIdx.x;
  if (idx<sz){
    for(int k=0; k<3; ++k){
      
      for(int j=k; j<3; ++j){
        for(int i=0; i<3; ++i){
          Kup[TInd(i,j,k)][idx] = 
             g[TInd(i,0)][idx] * Kdn[TInd(0,j,k)][idx]
            +g[TInd(i,1)][idx] * Kdn[TInd(1,j,k)][idx]
            +g[TInd(i,2)][idx] * Kdn[TInd(2,j,k)][idx];
        
        }
      }
    }
  }
}


__device__
inline double testKup(double** Kup, const int i, const int j, const int k, const int idx){
  Kup[TInd(i,j,k)][idx] = 0;
  //std::cout << "Kup["<<TInd(i,j,k)<<"]= " << Kup[TInd(i,j,k)] << std::endl;
  return Kup[TInd(i,j,k)][idx];
}

__device__
inline double testKdn(const double** Kdn, const int i, const int j, const int k, const int idx){
  //std::cout << "Kdn["<<TInd(i,j,k)<<"]" << "= " << Kdn[TInd(i,j,k)] << std::endl;
  return Kdn[TInd(i,j,k)][idx];
}


__device__
inline double testg(const double** g, const int i, const int j, const int idx){
  //std::cout << "g["<<TInd(i,j)<<"]" << "= " << g[TInd(i,j)] << std::endl;
  return g[TInd(i,j)][idx];
}

__global__
void 
//__launch_bounds__(96,5)
GPU_Tensor_KernelV2(const int sz, 
    const double *__restrict__ *__restrict__ g,  
      const double *__restrict__ *__restrict__ Kdn,  
      double *__restrict__ *__restrict__ Kup ){
  
  const int idx = blockIdx.x*blockDim.x+threadIdx.x;
  if (idx<sz){
    for(int k=0; k<3; ++k){
      for(int j=k; j<3; ++j){
        for(int i=0; i<3; ++i){
          Kup[TInd(i,j,k)][idx] =  
             g[TIndex_T(i,0)][idx] * Kdn[TInd(0,j,k)][idx]
            +g[TIndex_T(i,1)][idx] * Kdn[TInd(1,j,k)][idx]
            +g[TIndex_T(i,2)][idx] * Kdn[TInd(2,j,k)][idx];
        }
      }
    }
  }
}

__global__
void 
//__launch_bounds__(96,5)
GPU_Tensor_Kernel(const int sz, 
    const double *__restrict__ *__restrict__ g,  
      const double *__restrict__ *__restrict__ Kdn,  
      double *__restrict__ *__restrict__ Kup ){
  
  const int idx = blockIdx.x*blockDim.x+threadIdx.x;
  if (idx<sz){
    for(int k=0; k<3; ++k){
      for(int j=k; j<3; ++j){
        for(int i=0; i<3; ++i){
          
          Kup[TInd(i,j,k)][idx] =  
             g[TInd(i,0)][idx] * Kdn[TInd(0,j,k)][idx]
            +g[TInd(i,1)][idx] * Kdn[TInd(1,j,k)][idx]
            +g[TInd(i,2)][idx] * Kdn[TInd(2,j,k)][idx];
        
        }
      }
    }
  }
}



__global__
void GPU_Tensor_KernelArrays( 
    const int sz,
    const double *__restrict__ g,  
      const double *__restrict__ Kdn,  
      double *__restrict__ Kup){
  
  const int idx = blockIdx.x*blockDim.x+threadIdx.x;
  if (idx<sz){
    for(int k=0; k<3; ++k){
      for(int j=k; j<3; ++j){
        for(int i=0; i<3; ++i){
         
         //printf("%u,%u,%u =>%u \n",i,j,k,TInd(i,j,k));
         // double a = testKup(Kup,i,j,k,idx);
         //const double b = testKdn(Kdn,i,j,k,idx);
         //const double c = testg(g,i,j,idx);
          
          Kup[TInd3(i,j,k)*idx] =  
             g[TInd3(i,0)*idx] * Kdn[TInd3(0,j,k)*idx]
            +g[TInd3(i,1)*idx] * Kdn[TInd3(1,j,k)*idx]
            +g[TInd3(i,2)*idx] * Kdn[TInd3(2,j,k)*idx];
        
        }
      }
    }
  }
}
//WRAPPERS
//Do the sum with shared memory, parallel on i and j.
void GPU_Tensor_ParallelSharedWrapper(const int sz, const double** g,  
      const double** Kdn,  double** Kup ){
  const int blocksizeX=32;
  const int nblocksX = sz/blocksizeX + (sz%blocksizeX==0?0:1);

  const int blocksizeY = 3;
  const int nblocksY = 3;

  const int blocksizeZ = 3;
  const int nblocksZ = 1;

  dim3 blocksize(blocksizeX,blocksizeY,blocksizeZ);
  dim3 nblocks(nblocksX,nblocksY,nblocksZ); 
  GPU_Tensor_ParallelShared<<<nblocks,blocksize>>>(sz, g,  Kdn,  Kup
        );
}

//Parallel on i and j; sum done with a loop.
void GPU_Tensor_ParallelTensorsWrapper(const int sz, const double** g,  
      const double** Kdn,  double** Kup ){
  const int blocksizeX=96;
  const int nblocksX = sz/blocksizeX + (sz%blocksizeX==0?0:1);

  const int blocksizeY = 3;
  const int nblocksY = 3;

  dim3 blocksize(blocksizeX,blocksizeY,1);
  dim3 nblocks(nblocksX,nblocksY,1); 
  GPU_Tensor_ParallelTensors<<<nblocks,blocksize>>>(sz, g,  Kdn,  Kup
        );
}

//Parallel on i,j,and k; sum done with a loop
void GPU_Tensor_ParallelTensorsAllWrapper(const int sz, const double** g,  
      const double** Kdn,  double** Kup ){
  const int blocksizeX=32;
  const int nblocksX = sz/blocksizeX + (sz%blocksizeX==0?0:1);

  const int blocksizeY = 3;
  const int nblocksY = 3;

  const int blocksizeZ = 1;
  const int nblocksZ = 3;

  dim3 blocksize(blocksizeX,blocksizeY,blocksizeZ);
  dim3 nblocks(nblocksX,nblocksY,nblocksZ); 
  GPU_Tensor_ParallelTensorsAll<<<nblocks,blocksize>>>(sz, g,  Kdn,  Kup
        );
}

//Parallel on i,j, and k; sum done with shared memory
void GPU_Tensor_ParallelTensorsAllSharedWrapper(const int sz, const double** g,  
      const double** Kdn,  double** Kup ){
  const int blocksizeX=32;
  const int nblocksX = sz/blocksizeX + (sz%blocksizeX==0?0:1);

  const int blocksizeY = 3;
  const int nblocksY = 3;

  const int blocksizeZ = 3;
  const int nblocksZ = 3;

  dim3 blocksize(blocksizeX,blocksizeY,blocksizeZ);
  dim3 nblocks(nblocksX,nblocksY,nblocksZ); 
  GPU_Tensor_ParallelTensorsAllShared<<<nblocks,blocksize>>>(sz, g,  Kdn,  Kup
        );
}

//Parallel on i; sum done with a loop
void GPU_Tensor_ParallelTensors_Only_iWrapper(const int sz, const double** g,  
      const double** Kdn,  double** Kup ){
  const int blocksizeX=96;
  const int nblocksX = sz/blocksizeX + (sz%blocksizeX==0?0:1);

  const int blocksizeY = 3;
  const int nblocksY = 1;

  const int blocksizeZ = 1;
  const int nblocksZ = 1;

  dim3 blocksize(blocksizeX,blocksizeY,blocksizeZ);
  dim3 nblocks(nblocksX,nblocksY,nblocksZ); 
  GPU_Tensor_ParallelTensors_Only_i<<<nblocks,blocksize>>>(sz, g,  Kdn,  Kup
        );
}

//Serial on all indexes; sum done with shared memory
void GPU_Tensor_KernelSharedSumWrapper(const int sz, const double** g,  
      const double** Kdn,  double** Kup ){
  const int blocksizeX=96;
  const int nblocksX = sz/blocksizeX + (sz%blocksizeX==0?0:1);

  const int blocksizeY = 3;
  const int nblocksY = 1;

  dim3 blocksize(blocksizeX,blocksizeY,1);
  dim3 nblocks(nblocksX,nblocksY,1); 
  GPU_Tensor_KernelSharedSum<<<nblocks,blocksize>>>(sz, g,  Kdn,  Kup
        );
}
//Reverses the index order
void GPU_Tensor_Kernel_WrapperBackwards(const int sz, const double** g,  
      const double** Kdn,  double** Kup ){
  const int blocksize=128;
  const int nblocks = sz/blocksize + (sz%blocksize==0?0:1);
  GPU_Tensor_BackwardsIndexing<<<nblocks,blocksize>>>(sz, g,  Kdn,  Kup
        );
}

//Unrolls some loops
void GPU_Tensor_Kernel_WrapperV2(const int sz, const double** g,  
      const double** Kdn,  double** Kup ){
  const int blocksize=96;
  const int nblocks = sz/blocksize + (sz%blocksize==0?0:1);
  GPU_Tensor_KernelV2<<<nblocks,blocksize>>>(sz, g,  Kdn,  Kup
        );
  
}

//Naive version; all indices serial
void GPU_tensor_arrays(const int sz, const double* g,  
      const double* Kdn,  double* Kup ){
  const int blocksize=128;
  const int nblocks = sz/blocksize + (sz%blocksize==0?0:1);
  
  GPU_Tensor_KernelArrays<<<nblocks,blocksize>>>(sz,g,  Kdn,  Kup
        );
}


//Naive version; all indices serial
void GPU_Tensor_Kernel_Wrapper(const int sz, const double** g,  
      const double** Kdn,  double** Kup ){
  const int blocksize=96;
  const int nblocks = sz/blocksize + (sz%blocksize==0?0:1);
  

  GPU_Tensor_Kernel<<<nblocks,blocksize>>>(sz, g,  Kdn,  Kup
        );
}



