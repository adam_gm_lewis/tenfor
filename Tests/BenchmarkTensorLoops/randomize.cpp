#include "Tensor.hpp"
#include "DataMesh.hpp"
#include <random>
Tensor<DataMesh> MakeRandomTensor(const TensorStructure& structure, 
                                  const IPoint& extents){
    Mesh mesh(extents);
    Tensor<DataMesh> T(structure, mesh, 0.);
    std::uniform_real_distribution<double> unif(-10., 10.);
    std::default_random_engine re;
    for(auto&& dm:T){
      for(auto&& x:dm){
        x = unif(re); 
      }
    }
    return T;
}
