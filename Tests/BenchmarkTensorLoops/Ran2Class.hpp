//============================================================================
/// \file
/// Numerical Recipes ran2 encapsulated into a class.
//============================================================================
#ifndef Ran2Class_hpp
#define Ran2Class_hpp
//============================================================================
#include <stdint.h> // for int32_t
#include "NonCopyable.hpp"
//============================================================================
/// Numerical Recipes ran2 encapsulated into a class.
/// Returns pseudorandom numbers between 0 and 1.
/// Now you can have two instances of Ran2 that do not interfere with
/// each other.
///
/// Usage:
/// Ran2 ran(212312); // the '212312' is the seed.
/// for(int i=0;i<10;++i) std::cout << "Next random number=" << ran() << "\n";
class Ran2 : public NonCopyable {
public:
  /// Seed can be positive or negative; both accomplish initialization.
  /// The algorithm requires a 32-bit seed (not merely *at least* 32 bits;
  /// if the seed is bigger than the largest 32-bit integer, it can segfault).
  Ran2(const int32_t seed);
  ~Ran2();
  /// Returns number in random sequence. The random number is between 0 and 1.
  float operator()();
private:
  int32_t idum,idum2,iy;
  int32_t* iv;
};
//============================================================================

#endif // Ran2Class_hpp
