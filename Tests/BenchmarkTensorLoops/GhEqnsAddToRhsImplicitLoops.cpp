#include "DataMesh.hpp"
#include "Tensor.hpp"

using namespace TIndex3; //i_,j_,k_,l_,m_
using namespace TIndex4; //a_,b_,c_,d_,e_
void GhEqnsAddToRhsImplicitLoopsTest
   (
    // output variables
    Tensor<DataMesh>& dtpsi,
    Tensor<DataMesh>& dtkappa,

    // evolved variables & their spatial derivatives
    const Tensor<DataMesh>& psi,
    const Tensor<Tensor<DataMesh> >& dpsi,
    const Tensor<DataMesh>& kappa,
    const Tensor<Tensor<DataMesh> >& dkappa,

    // gauge source functions, and their spacetime derivatives
    const Tensor<DataMesh>& H,        // gauge source function 
    const Tensor<Tensor<DataMesh> >& d4H, // SpacetimeDerivOfH

    // Constraint-damping parameters. Some of these will always have
    // their default values, so could be ommitted.
    const Tensor<DataMesh>& gamma0,
    const Tensor<DataMesh>& gamma1,
    const Tensor<DataMesh>& gamma2,

    const bool AddG1ShiftCorr,
    const Tensor<DataMesh>& G1ShiftCorr,

    // derived quantities.  Given psi and dpsi above, these could be
    // computed from scratch.
    const Tensor<DataMesh>& Lapse,
    const Tensor<DataMesh>& Shift,
    const Tensor<DataMesh>& Invg,
    const Tensor<DataMesh>& Invpsi,
    const Tensor<DataMesh>& Gamma1,   // 4-D Christoffel 1st kind
    const Tensor<DataMesh>& Gamma2,   // 4-D Christoffel2ndKind
    const Tensor<DataMesh>& TrGamma1, // TrChristoffel1stKind
    const Tensor<DataMesh>& T_i,      // UnitTimelikeNormalOneForm
    const Tensor<DataMesh>& T_I       // UnitTimelikeNormalVector
    ) {
  const Mesh& mesh      = Lapse();
  const int SpatialDim  = Shift.Dim();

  // SimpleProfiler prof("GeneralizedHarmonicEquations:AfterDataBoxGet");

  // -----------------------------------------------------------------
  // First thing we do is define some temp variables for optimization

  // ***NOTE: kappa1up(0,mu,nu) is undefined.***
  Tensor<DataMesh> kappa1up(SpatialDim+1,"122",mesh);
  kappa1up(sym<1,2>(), i_+1, a_, b_)
    = Sum(j_, Invg(i_, j_)*kappa(j_+1, a_, b_) );

  Tensor<DataMesh> kappa3up(SpatialDim+1,"123",mesh);
  kappa3up(a_, b_, c_) = Sum(d_, Invpsi(c_, d_)*kappa(a_, b_, d_) );

  Tensor<DataMesh> Gamma13up(SpatialDim+1,"123",mesh);
  Gamma13up(a_, b_, c_) = Sum(d_, Invpsi(c_, d_)*Gamma1(a_, b_, d_) );

  Tensor<DataMesh> PiT(SpatialDim+1,"1",mesh);
  PiT(a_) = Sum(b_, T_I(b_)*kappa(0,b_, a_) );
  
  Tensor<DataMesh> PiTT(SpatialDim+1,"",mesh);
  PiTT() = Sum(a_, T_I(a_)*PiT(a_));

  // ***NOTE: PhiT(0,...) is undefined.***
  Tensor<DataMesh> PhiT(SpatialDim+1,"12",mesh);
  PhiT(i_+1, a_) = Sum(b_, kappa(i_+1, a_, b_)*T_I(b_) );

  Tensor<DataMesh> PhiTT(SpatialDim,"1",mesh);
  PhiTT(i_) = Sum(a_, PhiT(i_+1, a_)*T_I(a_) );

  Tensor<DataMesh> C3(SpatialDim+1,"122",mesh);
  C3(sym<1,2>(), i_+1, a_, b_) = dpsi(a_, b_)(i_) - kappa(i_+1, a_, b_); 

  Tensor<DataMesh> C(SpatialDim+1, "1", mesh);
  C(a_) = H(a_)+TrGamma1(a_);

  Tensor<DataMesh> TC(SpatialDim+1,"",mesh);
  TC() = Sum(a_, T_I(a_)*C(a_));

  bool DefaultGamma1 = true;
  for(int s=0;s<mesh.Size();++s) DefaultGamma1 &= (gamma1()[s] == -1);
  DataMesh gamma1p1(DataMesh::Empty);
  if(not DefaultGamma1) gamma1p1 = 1.0 + gamma1();
  DataMesh gamma1gamma2 = gamma1()*gamma2();

  Tensor<DataMesh> Shift_C3(SpatialDim+1,"aa",mesh);
  Shift_C3(sym<0,1>(), a_, b_) = Sum(i_,  Shift(i_)*C3(i_+1, a_, b_));


  // ------------------------------------------------------------
  // Here are the actual equations
  // ------------------------------------------------------------


  // Equation for dtpsi
  dtpsi(sym<0,1>(), a_,b_) +=  
    Sum(i_, Shift(i_)*kappa(i_+1,a_,b_) ) -Lapse()*kappa(0,a_,b_);
  if(!DefaultGamma1) 
    dtpsi(sym<0,1>(), a_,b_) += gamma1p1*Shift_C3(a_,b_);
  

  // Equation for dt Pi
  // Gradient of H
  dtkappa(sym<1,2>(), 0,a_,b_) += 
    -d4H(a_)(b_)-d4H(b_)(a_) + 2*Sum(c_, Gamma2(c_, a_, b_)*H(c_));

  // PiTT, gamma0 
  dtkappa(sym<1,2>(), 0,a_,b_) += 
    - 0.5*PiTT()*kappa(0,a_,b_)
    + gamma0()*(T_i(a_)*C(b_)+T_i(b_)*C(a_))
    - gamma0()*psi(a_,b_)*TC();

  // 2N psi^{ab} \psi^{cd} partial_c psi_ua partial_d psi_vb
  dtkappa(sym<1,2>(), 0,a_, b_) += 
    2.0*Sum(c_, 
	    -kappa(0,a_, c_)*kappa3up(0,b_, c_)
	    +Sum(i_, 
		 kappa1up(i_+1, a_, c_)*kappa3up(i_+1, b_, c_)
		 )
	    );

  // Gamma*Gamma term
  dtkappa(sym<1,2>(), 0, a_, b_) += 
    -2.0*Sum(c_,
	     Sum(d_,
		 Gamma13up(a_, c_, d_)*Gamma13up(b_, d_, c_)
		 )
	     );

  // PiT term
  dtkappa(sym<1,2>(), 0, a_, b_) += -Sum(i_, PiT(i_+1)*kappa1up(i_+1,a_, b_) );

  // Principal derivative term
  dtkappa(sym<1,2>(), 0, a_, b_) += 
    - Sum(i_,
	  Sum(j_,
	      Invg(i_, j_)*dkappa(i_+1,a_, b_)(j_)
	      )
	  );
  

  // Multiply by lapse
  dtkappa(sym<1,2>(), 0, a_, b_) *= Lapse();


  // Terms that are not proportional to the lapse
  dtkappa(sym<1,2>(), 0, a_, b_) += 
    gamma1gamma2*Shift_C3(a_, b_)
    +Sum(i_, Shift(i_)*dkappa(0,a_, b_)(i_));


  // dt Phi equation
  dtkappa(sym<1,2>(), i_+1, a_, b_) += 
    Lapse()*(0.5*kappa(0, a_, b_)*PhiTT(i_)
	     - dkappa(0, a_, b_)(i_)
	     + gamma2()*C3(i_+1, a_, b_)
	     + Sum(j_,
		   PhiT(i_+1,j_+1)*kappa1up(j_+1,a_,b_)
		   )
	     )
    +Sum(j_, Shift(j_)*dkappa(i_+1, a_, b_)(j_) );

  // G1ShiftCorrection
  if(AddG1ShiftCorr) {
    dtpsi(sym<0,1>(), a_, b_) 
      += gamma1()*Sum(i_, G1ShiftCorr(i_)*C3(i_+1, a_, b_));
    dtkappa(sym<1,2>(), 0, a_, b_) 
      += gamma2()*gamma1()*Sum(i_, G1ShiftCorr(i_)*C3(i_+1, a_, b_));
  }

}
