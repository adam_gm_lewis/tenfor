# This file should contain paths to all libraries TCodeWriter should $(MAKE)
# expressions for.

include MakefileRules

#Here we parse the $(MAKE)fileRules flags into the correct target.
ifdef CODE_WRITER
  TARGET = codewriter
else
  ifeq ($(COMPILE_MODE), -DNONACCEL)
    TARGET = nonaccel
  else ifeq ($(COMPILE_MODE), -DACCEL_CPU)
    TARGET = accelcpu
  else ifeq ($(COMPILE_MODE), -DACCEL_CUDA)
    TARGET = accelcuda
  endif
endif

#Unless we are compiling the CodeWriter execs, 
#the target is controlled by the flags in MakefileRules,.a override the 
#default behaviour.
all : $(TARGET)

cleanobjects :
	cd src/ExpressionTemplates&& \
	  $(MAKE) clean&& \
	  cd $(CODE_HOME)/src/TCodeWriter&& \
	  $(MAKE) clean&& \
	  cd $(CODE_HOME)/src/AutoExpressions&& \
	  $(MAKE) clean

clean : cleanobjects

#When compiling with -DNONACCEL we compile src/ExpressionTemplates
#as libNonaccelExpressionTemplates.a.
nonaccel : 
	cd src/ExpressionTemplates&& \
	  $(MAKE) $(CODE_HOME)/lib/libNonaccelExprTemps.a

#With CODE_WRITER defined we compile src/ExpressionTemplates as 
#libCodeWriterExpressionTemplates.a and src/TCodeWriter as libTCodeWriter.a.
codewriter :
	cd src/ExpressionTemplates&& \
	  $(MAKE) codewriter&& \
	  cd $(CODE_HOME)/src/TCodeWriter&& \
	  $(MAKE) $(CODE_HOME)/lib/libTCodeWriter.a \

CPUCodeWriter : codewriter
	cd $(CODE_HOME)/TCodeWriter&& \
	  $(MAKE) CPUCodeWriter&&


CudaCodeWriter : codewriter
	cd $(CODE_HOME)/TCodeWriter&& \
	  $(MAKE) CudaCodeWriter

accelcpu : 
	cd $(CODE_HOME)/src/ExpressionTemplates&& \
	  $(MAKE) $(CODE_HOME)/lib/libAccelCpuExprTemps.a&& \
	  cd $(CODE_HOME)/src/AutoExpressions&& \
	  $(MAKE) $(CODE_HOME)/lib/libCpuAutoExpressions.a 

accelcuda : 
	cd $(CODE_HOME)/src/ExpressionTemplates && \
	  $(MAKE) $(CODE_HOME)/lib/libAccelCudaExprTemps.a && \
	  cd $(CODE_HOME)/src/AutoExpressions&& \
	  $(MAKE) $(CODE_HOME)/lib/libCudaAutoExpressions.a 
