#include "TestDataMesh.hpp"
#include "LowLevelClasses/Mesh.hpp"
#include "LowLevelClasses/IPoint.hpp"
#include "LowLevelClasses/DataMesh.hpp"
#include "LowLevelClasses/Tensor.hpp"
#include <cstdlib>
#include <random>
#include <time.h>

void MakeContainers(){
  IPoint extents(4, 3);
  Mesh beep(extents);
  DataMesh boop(beep, 0.);
  Tensor<DataMesh> beepboop(TensorStructure(3, "111"), boop);
}


Tensor<DataMesh> MakeRandomTensor(const TensorStructure& structure, 
                                  const IPoint& extents){
    Mesh mesh(extents);
    Tensor<DataMesh> T(structure, mesh, 0.);
    std::uniform_real_distribution<double> unif(-10., 10.);
    std::default_random_engine re;
    for(auto&& dm:T){
      for(auto&& x:dm){
        x = unif(re); 
      }
    }
    return T;
}

Tensor<DataMesh> RaiseIndexByHand(const Tensor<DataMesh>& g_il,
                                  const Tensor<DataMesh>& T_ljk){                 
    Mesh mesh(*g_il.begin());
    Tensor<DataMesh> T(T_ljk.Structure(), mesh, 0.);  
    const size_t sz = mesh.Size();
    for(int i=0; i<3; ++i){
      for(int j=0; j<3; ++j){
        for(int k=j; k<3; ++k){
          for(int x=0; x<sz; ++x){
            double* T_d = T(i,j,k).Data();
            double sum = 0;
            for(int l=0; l<3; ++l){
              const double* g_d = g_il(i,l).Data();
              const double* t_d = T_ljk(l,j,k).Data();
              sum += g_d[x] * t_d[x];
            }
            T_d[x] = sum;
          }
        }
      }
    }
    return T;
}

Tensor<DataMesh> RaiseIndexLoops(const Tensor<DataMesh>& g_il,
                                  const Tensor<DataMesh>& T_ljk){                 
    using namespace ImplicitTensorLoops;
    using TIndex3::i_;
    using TIndex3::j_;
    using TIndex3::k_;
    using TIndex3::l_;
    Mesh mesh(*g_il.begin());
    Tensor<DataMesh> T(T_ljk.Structure(), mesh, 0.);  
    T(sym<1,2>(),i_,j_,k_) = Sum(l_, g_il(i_,l_)*T_ljk(l_,j_,k_));
    return T;
}

Tensor<DataMesh> subtract_tensors(const Tensor<DataMesh>& A, 
                                  const Tensor<DataMesh>& B){

    auto Ait = A.begin();
    auto Bit = B.begin();
    Tensor<DataMesh> C(A.Structure(), Mesh(*A.begin()), 0.); 
    auto Cit = C.begin();
    for(;Ait!=A.end();++Ait,++Bit,++Cit){
      for (size_t x=0; x<Ait->Size(); ++x){
        Cit->Data()[x] = Ait->Data()[x] - Bit->Data()[x];
      }
    }
    return C;
}

int main(){
  MakeContainers();
  const TensorStructure g_ilStruct(3, "11");
  const TensorStructure T_ljkStruct(3, "122");
  IPoint extents({3, 3, 3});
  Tensor<DataMesh> compare(T_ljkStruct, Mesh(extents), 0.);
  Tensor<DataMesh> g_il = MakeRandomTensor(g_ilStruct, extents);
  Tensor<DataMesh> T_ljk = MakeRandomTensor(T_ljkStruct, extents);
  Tensor<DataMesh> Traise_forloops = RaiseIndexByHand(g_il, T_ljk);
  Tensor<DataMesh> Traise_indloops = RaiseIndexLoops(g_il, T_ljk);
  Tensor<DataMesh> diff = subtract_tensors(Traise_forloops, Traise_indloops);

  std::cout << diff << std::endl;

}
